import 'package:catex/catex.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/core/misc/adapters.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/core/providers/loadingProvider.dart';
import 'package:hydrogen/core/providers/popupProvider.dart';
import 'package:hydrogen/core/userServices/userProvider.dart';
import 'package:hydrogen/dashboard/services/activityProvider.dart';
import 'package:hydrogen/friends/friendsProvider.dart';
import 'package:hydrogen/friends/services/userReqProvider.dart';
import 'package:hydrogen/home/home_provider.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/quiz/quiz_provider.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:hydrogen/subscriptions/addCards/commitProvider.dart';
import 'package:hydrogen/subscriptions/addSubscriptions/importFromWeb.dart';
import 'package:hydrogen/subscriptions/log/log_provider.dart';
import 'package:hydrogen/subscriptions/moderation/moderators.dart';
import 'package:hydrogen/subscriptions/subscribers/subscribersProvider.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:hydrogen/tritium/tritiumProvider.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

import 'auth/wrapper.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  ErrorWidget.builder = (FlutterErrorDetails details) {
    final exception = details.exception;
    if (exception is CaTeXException) {
      return null;
    }

    var message = '';
    assert(() {
      message = 'Invalid CaTeX equation';
      return true;
    }());

    return exception is FlutterError
        ? ErrorWidget.withDetails(
            message: message,
            error: exception is FlutterError ? exception : null,
          )
        : Text(
            'invalid CaTeX equation',
            style: HomeStyles().buttonText,
          );
  };
  var path = await getApplicationDocumentsDirectory();

  Hive.init(path.path);
  await Hive.openBox('userData');
  await Hive.openBox('categories');
  Hive.registerAdapter(ColorAdapter());
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
    FlutterStatusbarcolor.setStatusBarWhiteForeground(false);
    FlutterStatusbarcolor.setNavigationBarColor(HomeStyles().backgroundColor);
    FlutterStatusbarcolor.setNavigationBarWhiteForeground(false);
    return Phoenix(
        child: MultiProvider(
            providers: [
          ChangeNotifierProvider<SubscriptionProvider>(
            create: (_) => SubscriptionProvider(),
          ),
          ChangeNotifierProvider<ImportProvider>(
            create: (_) => ImportProvider(),
          ),
          ChangeNotifierProvider<CategoriesProvider>(
            create: (_) => CategoriesProvider(),
          ),
          ChangeNotifierProvider<ModeratorProvider>(
            create: (_) => ModeratorProvider(),
          ),
          ChangeNotifierProvider<SubscribersListProvider>(
            create: (_) => SubscribersListProvider(),
          ),
          ChangeNotifierProvider<CommitProvider>(
            create: (_) => CommitProvider(),
          ),
          ChangeNotifierProvider<QuizProvider>(
            create: (_) => QuizProvider(),
          ),
          ChangeNotifierProvider<FriendsProvider>(
            create: (_) => FriendsProvider(),
          ),
          ChangeNotifierProvider<UserProvider>(
            create: (_) => UserProvider(),
          ),
          ChangeNotifierProvider<LogProvider>(
            create: (_) => LogProvider(),
          ),
          ChangeNotifierProvider<SprintProvider>(
            create: (_) => SprintProvider(),
          ),
          ChangeNotifierProvider<HomeProvider>(
            create: (_) => HomeProvider(),
          ),
          ChangeNotifierProvider<UserReqProvider>(
            create: (_) => UserReqProvider(),
          ),
          ChangeNotifierProvider<LoadingProvider>(
            create: (_) => LoadingProvider(),
          ),
          ChangeNotifierProvider<ActivityProvider>(
            create: (_) => ActivityProvider(),
          ),
          ChangeNotifierProvider<TritiumProvider>(
            create: (_) => TritiumProvider(),
          ),
          ChangeNotifierProvider<PopupProvider>(
            create: (_) => PopupProvider(),
          ),
        ],
            child: ScrollConfiguration(
                behavior: AntiScrollGlow(),
                child: MaterialApp(
                  debugShowCheckedModeBanner: false,
                  title: 'Hydrogen',
                  home: Wrapper(),
                ))));
  }
}
