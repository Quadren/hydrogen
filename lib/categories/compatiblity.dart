import 'package:flutter/material.dart';
import 'package:hydrogen/categories/categorySubscriptionWidget.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:hydrogen/widgets/backBar.dart';
import 'package:provider/provider.dart';

class CompatibilityScroll extends StatefulWidget {
  @override
  _CompatibilityScrollState createState() => _CompatibilityScrollState();
}

class _CompatibilityScrollState extends State<CompatibilityScroll> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    List<Map> subscriptions =
        Provider.of<SubscriptionProvider>(context).getSubscriptions;

    List render = Provider.of<CategoriesProvider>(context).getRender;
    bool _allSelected = Provider.of<CategoriesProvider>(context).getAllSelected;

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: Backbar(),
      ),
      backgroundColor: HomeStyles().backgroundColor,
      extendBodyBehindAppBar: true,
      body: Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.1),
          child: ScrollConfiguration(
            behavior: AntiScrollGlow(),
            child: CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: height * 0.1),
                      Text(_allSelected ? 'All decks' : 'Decks in category',
                          style: HomeStyles().headerText),
                      SizedBox(height: height * 0.05),
                    ],
                  ),
                ),
                SliverList(
                    delegate: SliverChildBuilderDelegate((context, int index) {
                  return Container(
                      child: _allSelected
                          ? CategorySubWidget(subscriptions[index], index)
                          : CategorySubWidget(render[index], index));
                },
                        childCount: _allSelected
                            ? subscriptions.length
                            : render.length)),
              ],
            ),
          )),
    );
  }
}
