import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:provider/provider.dart';

void sortSubscriptions(context) {
  List subscriptions = Provider.of<SubscriptionProvider>(context, listen: false)
      .getSubscriptions;

  String category =
      Provider.of<CategoriesProvider>(context, listen: false).getSelectedString;

  Provider.of<CategoriesProvider>(context, listen: false).clearRender();

  subscriptions.forEach((element) {
    Map data = element;
    if (data.containsValue(category)) {
      Provider.of<CategoriesProvider>(context, listen: false).setToRender(data);
    }
  });
}
