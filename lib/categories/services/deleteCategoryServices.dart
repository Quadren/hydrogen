import 'package:flutter/cupertino.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/categories/deleteCategory/deleteCategory.dart';
import 'package:hydrogen/core/userServices/getUserCategories.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:provider/provider.dart';

class DeleteCategoryServices {
  void deleteCategory(String title, int index, BuildContext context) {
    Map categories =
        Provider.of<CategoriesProvider>(context, listen: false).getCategories;

    Map category = categories[title];
    var box = Hive.box('categories');

    List userCategories = box.get('userCategories');
    List subscriptions =
        Provider.of<SubscriptionProvider>(context, listen: false)
            .getSubscriptions;

    subscriptions.forEach((element) {
      Map data = element;

      if (data.containsValue(title)) {
        print("Found a subscription");
        int itemIndex = subscriptions.indexOf(element);

        Provider.of<SubscriptionProvider>(context, listen: false)
            .updateCategory(itemIndex, 'Uncategorized');

        String id = element['id'];
        box.put(id, 'Uncategorized');
      }
    });

    userCategories.removeAt(index - 5);
    box.put('userCategories', userCategories);
    Provider.of<CategoriesProvider>(context, listen: false)
        .removeCategory(title);

    Provider.of<CategoriesProvider>(context, listen: false).clearRender();
    getUserCategories(context);

    deleteCategoryController.nextPage(
        duration: Duration(milliseconds: 500), curve: Curves.easeInOutCubic);
  }
}
