import 'package:flutter/material.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:provider/provider.dart';

void initSort(BuildContext context) {
  List subscriptions = Provider.of<SubscriptionProvider>(context, listen: false)
      .getSubscriptions;

  subscriptions.forEach((element) {
    Map data = element;

    if (data.containsValue('Uncategorized')) {
      Provider.of<CategoriesProvider>(context, listen: false).setToRender(data);
    }
  });
}
