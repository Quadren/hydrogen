import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:provider/provider.dart';

class CreateCategoryServices {
  void createCategory(
      String title, String emoji, Color color, BuildContext context) {
    var box = Hive.box('categories');
    List userCategories = box.get('userCategories');

    var hex = '${color.value.toRadixString(16)}';
    int integer = int.parse(hex, radix: 16);

    Map categories =
        Provider.of<CategoriesProvider>(context, listen: false).getCategories;

    if (categories.containsKey(title)) {
      Fluttertoast.showToast(
          msg: "This already exists!",
          backgroundColor: Colors.red[300],
          textColor: Colors.white);
    } else {
      if (userCategories != null) {
        userCategories.add({'label': title, 'color': integer, 'emoji': emoji});
        box.put('userCategories', userCategories);
      } else {
        userCategories = [
          {'label': title, 'color': integer, 'emoji': emoji}
        ];
        box.put('userCategories', userCategories);
        print("First user category");
      }

      Provider.of<CategoriesProvider>(context, listen: false)
          .createCategory(title, emoji, color);

      Navigator.pop(context);

      Fluttertoast.showToast(
          msg: "Created category!",
          backgroundColor: HomeStyles().accentColor,
          textColor: Colors.white);
    }
  }
}
