import 'package:animated/animated.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/categories/categoriesWidget.dart';
import 'package:hydrogen/categories/categorySubscriptionWidget.dart';
import 'package:hydrogen/categories/compatiblity.dart';
import 'package:hydrogen/categories/createCategory/createCategory.dart';
import 'package:hydrogen/categories/services/sortService.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/core/userServices/userProvider.dart';
import 'package:hydrogen/dashboard/dashboard.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/popups/services/popupDecider.dart';
import 'package:hydrogen/subscriptions/moderation/moderators.dart';
import 'package:hydrogen/subscriptions/newAddSubscriptions/newAddSubSheet.dart';
import 'package:hydrogen/subscriptions/services/getAllSubscriptions.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';

class CategoriesPage extends StatefulWidget {
  @override
  _CategoriesPageState createState() => _CategoriesPageState();
}

class _CategoriesPageState extends State<CategoriesPage>
    with AutomaticKeepAliveClientMixin {
  bool _animate1 = false;
  ScrollController _controller = ScrollController();

  void _animateFunc() {
    Future.delayed(Duration.zero, () {
      Provider.of<SubscriptionProvider>(context, listen: false)
          .setLoading(true);
      Provider.of<ModeratorProvider>(context, listen: false).setLoading(false);
      Provider.of<CategoriesProvider>(context, listen: false)
          .setSelected(-1, 'Uncategorized');
      getSubscriptions(context);
    });
    Future.delayed(Duration(milliseconds: 100), () {
      setState(() {
        _animate1 = true;
      });
    });
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      Provider.of<CategoriesProvider>(context, listen: false)
          .setSelected(0, 'Uncategorized');
      sortSubscriptions(context);
    });
    _animateFunc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    Map categories = Provider.of<CategoriesProvider>(context).getCategories;
    List titles = categories.keys.toList();

    List render = Provider.of<CategoriesProvider>(context).getRender;

    List<Map> subscriptions =
        Provider.of<SubscriptionProvider>(context).getSubscriptions;
    bool _isEmpty = Provider.of<SubscriptionProvider>(context).isEmpty;
    bool _isLoading = Provider.of<SubscriptionProvider>(context).isLoading;
    bool _pfpLoading = Provider.of<UserProvider>(context).getLoading;

    bool _allSelected = Provider.of<CategoriesProvider>(context).getAllSelected;
    String name = Hive.box('userData').get('name');
    String pfp = Provider.of<UserProvider>(context).getPfp;

    return Stack(
      children: [
        ScrollConfiguration(
          behavior: AntiScrollGlow(),
          child: CustomScrollView(
            primary: true,
            slivers: [
              SliverToBoxAdapter(
                  child: Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.topRight,
                      child: BouncingWidget(
                        child: Container(
                          height: height * 0.05,
                          width: height * 0.05,
                          decoration: BoxDecoration(
                            border:
                                Border.all(width: 2, color: Color(0xffEEEEEE)),
                            borderRadius: BorderRadius.circular(15),
                          ),
                          margin: EdgeInsets.only(top: height * 0.05),
                          child: Center(
                            child: FaIcon(
                              FontAwesomeIcons.plus,
                              color: HomeStyles().primaryColor,
                              size: height * 0.025,
                            ),
                          ),
                        ),
                        onPressed: () {
                          showAddNewSubSheet(context);
                        },
                      ),
                    ),
                    Align(
                        alignment: Alignment.topLeft,
                        child: Padding(
                          padding: EdgeInsets.only(top: height * 0.05),
                          child: BouncingWidget(
                            child: Container(
                              height: height * 0.1,
                              width: width * 0.15,
                              decoration: BoxDecoration(
                                color: Color(0xFFEEEEEE),
                                borderRadius: BorderRadius.circular(21),
                                border: Border.all(
                                    width: 2, color: HomeStyles().accentColor),
                              ),
                              child: _pfpLoading
                                  ? null
                                  : ClipRRect(
                                      borderRadius: BorderRadius.circular(19),
                                      child: CachedNetworkImage(
                                        imageUrl: pfp,
                                        fit: BoxFit.cover,
                                        placeholder: (context, url) {
                                          return Container(
                                            height: height * 0.1,
                                            width: width * 0.15,
                                            color: Color(0xFFEEEEEE),
                                          );
                                        },
                                      )),
                            ),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  PageTransition(
                                      child: Dashboard(),
                                      type: PageTransitionType.rightToLeft,
                                      curve: Curves.easeInOutCubic,
                                      duration: Duration(milliseconds: 500)));
                            },
                          ),
                        )),
                  ],
                ),
              )),
              SliverToBoxAdapter(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: height * 0.1),
                      Text(
                        'Hi, $name!',
                        style: HomeStyles().bigText,
                      ),
                      SizedBox(height: height * 0.05),
                    ],
                  ),
                ),
              ),
              Container(
                child: _isLoading
                    ? SliverToBoxAdapter(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(height: height * 0.05),
                            Transform.scale(
                              scale: 1.7,
                              child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation(
                                    HomeStyles().accentColor),
                                backgroundColor: HomeStyles().secondaryAccent,
                                strokeWidth: 7,
                              ),
                            ),
                          ],
                        ),
                      )
                    : SliverToBoxAdapter(
                        child: Container(
                        height: height * 0.2,
                        child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, int index) {
                            return Container(
                              child: (index == 0)
                                  ? allWidget()
                                  : Container(
                                      child: (index == categories.length + 1)
                                          ? newCatWidget()
                                          : CategoryWidget(
                                              categories[titles[index - 1]]
                                                  ['color'],
                                              categories[titles[index - 1]]
                                                  ['emoji'],
                                              index - 1,
                                              titles[index - 1]),
                                    ),
                            );
                          },
                          itemCount: categories.length + 2,
                        ),
                      )),
              ),
              SliverToBoxAdapter(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: height * 0.03),
                    BouncingWidget(
                      child: Container(
                        height: height * 0.05,
                        width: width * 0.45,
                        decoration: BoxDecoration(
                          color: Color(0xffEEEEEE),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Center(
                          child: Text('use compatibility scroll',
                              style: HomeStyles().buttonText2),
                        ),
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            PageTransition(
                                child: CompatibilityScroll(),
                                type: PageTransitionType.rightToLeft,
                                curve: Curves.easeInOutCubic,
                                duration: Duration(milliseconds: 500)));
                      },
                      duration: Duration(milliseconds: 150),
                    ),
                    SizedBox(height: height * 0.05),
                  ],
                ),
              ),
              Container(
                child: _isLoading
                    ? SliverToBoxAdapter()
                    : Container(
                        child: _allSelected
                            ? SliverList(
                                delegate: SliverChildBuilderDelegate(
                                (context, index) {
                                  return Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: width * 0.1),
                                      child: CategorySubWidget(
                                          subscriptions[index], index));
                                },
                                childCount: subscriptions.length,
                              ))
                            : SliverList(
                                delegate: SliverChildBuilderDelegate(
                                    (context, int index) {
                                return Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: width * 0.1),
                                  child:
                                      CategorySubWidget(render[index], index),
                                );
                              }, childCount: render.length)),
                      ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget allWidget() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    List _subscriptions =
        Provider.of<SubscriptionProvider>(context).getSubscriptions;
    bool _allSelected = Provider.of<CategoriesProvider>(context).getAllSelected;

    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.only(
          right: width * 0.05,
          left: width * 0.1,
          top: height * 0.02,
          bottom: height * 0.02),
      height: height * 0.1,
      width: width * 0.25,
      decoration: BoxDecoration(
        color: _allSelected
            ? HomeStyles().accentColor
            : HomeStyles().backgroundColor,
        borderRadius: BorderRadius.circular(21),
        boxShadow: _allSelected
            ? [
                BoxShadow(
                    color: HomeStyles().accentColor.withOpacity(0.4),
                    offset: Offset(0, 4),
                    blurRadius: 4,
                    spreadRadius: 0),
              ]
            : [
                BoxShadow(
                    offset: Offset.zero,
                    color: Colors.black.withOpacity(0.05),
                    blurRadius: 20,
                    spreadRadius: -1)
              ],
      ),
      child: GestureDetector(
        onTap: () {
          if (!_allSelected) {
            Provider.of<CategoriesProvider>(context, listen: false)
                .setSelected(-1, 'Uncategorized');
            Provider.of<CategoriesProvider>(context, listen: false)
                .setAllSelected(true);
          } else {
            Provider.of<CategoriesProvider>(context, listen: false)
                .setAllSelected(false);
          }
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ClipOval(
              child: Container(
                height: height * 0.065,
                width: height * 0.065,
                color: HomeStyles().backgroundColor,
                child: Center(
                  child: Text(
                    '🤘',
                    style: TextStyle(fontSize: 26),
                  ),
                ),
              ),
            ),
            SizedBox(height: height * 0.03),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.04),
              child: Text(
                'All (${_subscriptions.length})',
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontFamily: 'Raleway',
                  fontSize: 12,
                  color: _allSelected
                      ? HomeStyles().backgroundColor
                      : HomeStyles().primaryColor,
                  fontWeight: _allSelected ? FontWeight.w600 : FontWeight.w400,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget newCatWidget() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.only(
          right: width * 0.1,
          left: 0,
          top: height * 0.02,
          bottom: height * 0.02),
      height: height * 0.1,
      width: width * 0.25,
      decoration: BoxDecoration(
        color: HomeStyles().primaryColor,
        borderRadius: BorderRadius.circular(21),
      ),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              PageTransition(
                  child: CreateCategoryPage(),
                  type: PageTransitionType.rightToLeft,
                  curve: Curves.easeInOutCubic,
                  duration: Duration(milliseconds: 500)));
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ClipOval(
              child: Container(
                height: height * 0.065,
                width: height * 0.065,
                color: HomeStyles().backgroundColor,
                child: Center(
                    child: FaIcon(
                  FontAwesomeIcons.plus,
                  size: height * 0.02,
                  color: HomeStyles().primaryColor,
                )),
              ),
            ),
            SizedBox(height: height * 0.03),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.04),
              child: Text(
                'New',
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontFamily: 'Raleway',
                  fontSize: 12,
                  color: HomeStyles().backgroundColor,
                  fontWeight: FontWeight.w400,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget animate(bool trigger, Widget _child) {
    return Animated(
      duration: Duration(seconds: 2),
      curve: Sprung.overDamped,
      value: trigger ? 1 : 0,
      builder: (context, child, animation) => Transform.translate(
        offset: Offset(0, (1 - animation.value) * 40),
        child: Opacity(
          opacity: animation.value,
          child: child,
        ),
      ),
      child: _child,
    );
  }
}
