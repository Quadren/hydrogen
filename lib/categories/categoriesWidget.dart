import 'package:flutter/material.dart';
import 'package:hydrogen/categories/deleteCategory/deleteCategory.dart';
import 'package:hydrogen/categories/services/sortService.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:provider/provider.dart';
import 'package:vibration/vibration.dart';

class CategoryWidget extends StatefulWidget {
  final Color color;
  final String title;
  final int index;
  final String emoji;

  CategoryWidget(this.color, this.emoji, this.index, this.title);

  @override
  _CategoryWidgetState createState() =>
      _CategoryWidgetState(color, emoji, index, title);
}

class _CategoryWidgetState extends State<CategoryWidget> {
  final Color color;
  final String title;
  final int index;
  final String emoji;

  _CategoryWidgetState(this.color, this.emoji, this.index, this.title);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    int selectedIndex = Provider.of<CategoriesProvider>(context).getSelected;
    bool allSelected = Provider.of<CategoriesProvider>(context).getAllSelected;

    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.only(
          right: width * 0.05, top: height * 0.02, bottom: height * 0.02),
      height: height * 0.1,
      width: width * 0.25,
      decoration: BoxDecoration(
        color: (selectedIndex == index && allSelected == false)
            ? color
            : HomeStyles().backgroundColor,
        borderRadius: BorderRadius.circular(21),
        boxShadow: (selectedIndex == index && allSelected == false)
            ? [
                BoxShadow(
                    color: color.withOpacity(0.4),
                    offset: Offset(0, 4),
                    blurRadius: 4,
                    spreadRadius: 0),
              ]
            : [
                BoxShadow(
                    offset: Offset.zero,
                    color: Colors.black.withOpacity(0.05),
                    blurRadius: 20,
                    spreadRadius: -1)
              ],
      ),
      child: GestureDetector(
        onTap: () {
          if (selectedIndex != index) {
            Provider.of<CategoriesProvider>(context, listen: false)
                .setAllSelected(false);
            Provider.of<CategoriesProvider>(context, listen: false)
                .setSelected(index, title);
            Provider.of<CategoriesProvider>(context, listen: false)
                .clearRender();

            sortSubscriptions(context);
          }
        },
        onLongPress: () {
          Vibration.vibrate(duration: 50, amplitude: 200);
          showDeleteCategory(context, title, index);
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ClipOval(
              child: Container(
                height: height * 0.065,
                width: height * 0.065,
                color: HomeStyles().backgroundColor,
                child: Center(
                  child: Text(
                    emoji,
                    style: TextStyle(fontSize: 26),
                  ),
                ),
              ),
            ),
            SizedBox(height: height * 0.03),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.04),
              child: Text(
                title,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontFamily: 'Raleway',
                  fontSize: 12,
                  color: HomeStyles().primaryColor,
                  fontWeight: (selectedIndex == index && allSelected == false)
                      ? FontWeight.w600
                      : FontWeight.w400,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
