import 'package:flutter/material.dart';
import 'package:hydrogen/categories/createCategory/emojis/activities.dart';
import 'package:hydrogen/categories/createCategory/emojis/animals.dart';
import 'package:hydrogen/categories/createCategory/emojis/flags.dart';
import 'package:hydrogen/categories/createCategory/emojis/food.dart';
import 'package:hydrogen/categories/createCategory/emojis/objects.dart';
import 'package:hydrogen/categories/createCategory/emojis/people.dart';
import 'package:hydrogen/categories/createCategory/emojis/smileys.dart';
import 'package:hydrogen/categories/createCategory/emojis/symbols.dart';
import 'package:hydrogen/categories/createCategory/emojis/travel.dart';
import 'package:hydrogen/core/misc/emojis.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/home/home_styles.dart';

void showEmojiCatalog(BuildContext context) {
  showModalBottomSheet(
      backgroundColor: HomeStyles().backgroundColor,
      context: context,
      builder: (ctx) {
        return EmojiCatalog();
      });
}

class EmojiCatalog extends StatefulWidget {
  @override
  _EmojiCatalogState createState() => _EmojiCatalogState();
}

class _EmojiCatalogState extends State<EmojiCatalog> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Container(
        height: height * 0.7,
        padding: EdgeInsets.all(width * 0.1),
        child: ScrollConfiguration(
            behavior: AntiScrollGlow(),
            child: NestedScrollView(
                headerSliverBuilder: (context, bool isInnerBoxScrolled) {
                  return [
                    SliverToBoxAdapter(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'choose an emoji',
                            style: HomeStyles().headerText,
                          ),
                          SizedBox(
                            height: height * 0.05,
                          )
                        ],
                      ),
                    ),
                  ];
                },
                body: PageView(
                  children: [
                    SmileysWidget(),
                    PeopleEmojiWidget(),
                    AnimalsAndNature(),
                    Food(),
                    TravelEmoji(),
                    ActivitiesEmoji(),
                    ObjectsEmoji(),
                    SymbolsEmoji(),
                    FlagsEmoji(),
                  ],
                ))));
  }

  Widget row(List emojis) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(emojis[0], style: TextStyle(fontSize: 24)),
        Text(emojis[1], style: TextStyle(fontSize: 24)),
        Text(emojis[2], style: TextStyle(fontSize: 24)),
        Text(emojis[3], style: TextStyle(fontSize: 24)),
        Text(emojis[4], style: TextStyle(fontSize: 24)),
      ],
    );
  }
}
