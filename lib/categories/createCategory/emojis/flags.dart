import 'package:flutter/material.dart';
import 'package:hydrogen/core/misc/emojis.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:provider/provider.dart';

class FlagsEmoji extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
          child: Column(
            children: [
              Text(
                'flags',
                style: HomeStyles().subtitleText,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: height * 0.05),
            ],
          ),
        ),
        SliverGrid(
          delegate: SliverChildBuilderDelegate((context, int index) {
            return InkWell(
              onTap: () {
                Provider.of<CategoriesProvider>(context, listen: false)
                    .setEmoji(Emojis().flags[index]);
                Navigator.pop(context);
              },
              child: Container(
                padding: EdgeInsets.all(height * 0.02),
                child: Text(
                  Emojis().flags[index],
                  style: TextStyle(fontSize: 24),
                ),
              ),
            );
          }, childCount: Emojis().flags.length),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              childAspectRatio: 1, crossAxisCount: 5),
        ),
      ],
    );
  }
}
