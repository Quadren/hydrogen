import 'dart:math';

import 'package:animated/animated.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hsl_colorpicker/HSLColorPicker.dart';
import 'package:hydrogen/categories/createCategory/emojiCatalog.dart';
import 'package:hydrogen/categories/services/createCategoryServices.dart';
import 'package:hydrogen/core/misc/emojis.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';

class CreateCategoryPage extends StatefulWidget {
  @override
  _CreateCategoryPageState createState() => _CreateCategoryPageState();
}

class _CreateCategoryPageState extends State<CreateCategoryPage> {
  TextEditingController _textEditingController = TextEditingController();
  final _key = GlobalKey<FormState>();

  void _randomEmoji() {
    Provider.of<CategoriesProvider>(context, listen: false)
        .setColor(Color(0xffEEEEEE));
    Random rnd = Random();

    int index = rnd.nextInt(Emojis().animals.length);

    Future.delayed(Duration.zero, () {
      Provider.of<CategoriesProvider>(context, listen: false)
          .setEmoji(Emojis().animals[index]);
    });
  }

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      _randomEmoji();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;

    Color color = Provider.of<CategoriesProvider>(context).getColor;
    String emoji = Provider.of<CategoriesProvider>(context).getEmoji;

    return Scaffold(
      floatingActionButton: Animated(
        duration: Duration(milliseconds: 1500),
        curve: Sprung.criticallyDamped,
        value: (_textEditingController.text.length > 3) ? 1 : 0,
        builder: (context, child, animation) {
          return Transform.translate(
            offset: Offset(0, (1 - animation.value) * 150),
            child: child,
          );
        },
        child: BouncingWidget(
          onPressed: () {
            CreateCategoryServices().createCategory(
                _textEditingController.text, emoji, color, context);
          },
          duration: Duration(milliseconds: 150),
          child: AnimatedContainer(
            duration: Duration(milliseconds: 300),
            height: height * 0.085,
            width: height * 0.085,
            decoration: BoxDecoration(
              color: (_textEditingController.text.length > 3)
                  ? HomeStyles().primaryColor
                  : Color(0xFFAAAAAA),
              borderRadius: BorderRadius.circular(21),
            ),
            child: Center(
              child: FaIcon(FontAwesomeIcons.check,
                  color: HomeStyles().backgroundColor, size: height * 0.025),
            ),
          ),
        ),
      ),
      backgroundColor: HomeStyles().backgroundColor,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: HomeStyles().primaryColor),
        leading: InkWell(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          onTap: () => Navigator.pop(context),
          child: Container(
            margin: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.05,
                top: MediaQuery.of(context).size.height * 0.03),
            decoration: BoxDecoration(
              color: HomeStyles().backgroundColor,
              borderRadius: BorderRadius.circular(100),
            ),
            child: Center(
                child: FaIcon(
              FontAwesomeIcons.arrowLeft,
              color: HomeStyles().primaryColor,
              size: MediaQuery.of(context).size.height * 0.025,
            )),
          ),
        ),
      ),
      body: CustomScrollView(
        primary: false,
        slivers: [
          SliverToBoxAdapter(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: height * 0.15),
                ClipOval(
                  child: Container(
                    height: height * 0.2,
                    width: height * 0.2,
                    color: color,
                    child: Center(
                      child: GestureDetector(
                        onTap: () {
                          showEmojiCatalog(context);
                        },
                        child: Text(
                          emoji,
                          style: TextStyle(fontSize: 48),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: height * 0.05),
                titleForm(),
                SizedBox(height: height * 0.01),
                Text('minimum length: 4 characters',
                    style: HomeStyles().subtitleText),
                SizedBox(height: height * 0.1),
                HSLColorPicker(
                  initialColor: Color(0xffEEEEEE),
                  strokeWidth: 2,
                  onChanged: (color) {
                    Provider.of<CategoriesProvider>(context, listen: false)
                        .setColor(color.toColor());
                  },
                  size: height * 0.3,
                ),
                SizedBox(height: height * 0.05),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget titleForm() {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.2),
      child: Form(
        child: TextFormField(
          controller: _textEditingController,
          maxLength: 16,
          style: HomeStyles().headerText,
          textAlign: TextAlign.center,
          cursorColor: HomeStyles().primaryColor,
          decoration: InputDecoration(
            hintStyle: HomeStyles().subtitleText,
            hintText: 'Name me!',
            counterStyle: HomeStyles().buttonText2,
            errorStyle: TextStyle(
              fontSize: 12,
              color: Colors.red[300],
              fontFamily: 'Raleway',
              fontWeight: FontWeight.w400,
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide:
                  BorderSide(color: HomeStyles().primaryColor, width: 1),
            ),
            errorBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.red[300], width: 1),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide:
                  BorderSide(color: HomeStyles().primaryColor, width: 2),
            ),
          ),
        ),
        key: _key,
      ),
    );
  }
}
