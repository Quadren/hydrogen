import 'package:animated/animated.dart';
import 'package:auto_animated/auto_animated.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/core/userServices/addToUserHistory.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/quiz/pages/wrongCardWidget.dart';
import 'package:hydrogen/quiz/quiz_provider.dart';
import 'package:hydrogen/subscriptions/addCards/commitCardWidget.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';

class FinishPage extends StatefulWidget {
  final int _index;

  FinishPage(this._index);

  @override
  _FinishPageState createState() => _FinishPageState(_index);
}

class _FinishPageState extends State<FinishPage> {
  final int _index;

  _FinishPageState(this._index);

  final ScrollController scrollController = ScrollController();

  bool _animate = false;
  bool _fade1 = false;
  bool _fade2 = false;

  ScrollController controller;

  @override
  void initState() {
    controller = ScrollController()
      ..addListener(() {
        if (controller.offset > 50) {
          if (_animate == true) {
            setState(() {
              _animate = false;
            });
          }
        } else {
          if (_animate == false) {
            setState(() {
              _animate = true;
            });
          }
        }
      });
    Future.delayed(Duration(milliseconds: 100), () {
      _fade1 = true;
    });
    Future.delayed(Duration(milliseconds: 500), () {
      _fade2 = true;
    });
    Future.delayed(Duration(seconds: 2), () {
      setState(() {
        _animate = true;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    List wrongCards = Provider.of<QuizProvider>(context).getWrongCards;

    bool loading = Provider.of<QuizProvider>(context).getLoading;
    String title = Provider.of<SubscriptionProvider>(context)
        .getSubscriptions[_index]['title'];

    List totalCards = Provider.of<SubscriptionProvider>(context)
        .getSubscriptions[_index]['cards'];
    int score = Provider.of<QuizProvider>(context).getScore;

    String id = Provider.of<SubscriptionProvider>(context)
        .getSubscriptions[_index]['id'];

    return Stack(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.1),
          child: ScrollConfiguration(
            behavior: AntiScrollGlow(),
            child: CustomScrollView(
              controller: controller,
              physics: AlwaysScrollableScrollPhysics(),
              slivers: [
                SliverToBoxAdapter(
                  child: animator(
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: height * 0.2),
                          Text('Well done!', style: HomeStyles().bigText),
                          SizedBox(height: height * 0.01),
                          Text(
                            'every step makes you better',
                            style: HomeStyles().subtitleText,
                          ),
                          SizedBox(height: height * 0.05),
                        ],
                      ),
                      _fade1),
                ),
                SliverToBoxAdapter(
                  child: animator(
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          stats(),
                          Text(
                            'cards you got wrong',
                            style: HomeStyles().subtitleText,
                          ),
                          SizedBox(height: height * 0.05)
                        ],
                      ),
                      _fade2),
                ),
                LiveSliverGrid(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: (width * 0.4) / (height * 0.25),
                  ),
                  itemBuilder: (context, index, animation) {
                    return FadeTransition(
                      opacity:
                          Tween<double>(begin: 0, end: 1).animate(animation),
                      child: SlideTransition(
                        position: Tween<Offset>(
                                begin: Offset(0, 0.1), end: Offset(0, 0))
                            .animate(animation),
                        child: WrongCardWidget(
                            wrongCards.toList()[index]['back'],
                            wrongCards.toList()[index]['front'],
                            index),
                      ),
                    );
                  },
                  itemCount: wrongCards.length,
                  controller: scrollController,
                  showItemDuration: Duration(milliseconds: 250),
                ),
              ],
            ),
          ),
        ),
        Animated(
          duration: Duration(milliseconds: 1500),
          curve: Sprung.criticallyDamped,
          value: _animate ? 1 : 0,
          builder: (context, child, animation) => Transform.translate(
            offset: Offset(0, (1 - animation.value) * 200),
            child: child,
          ),
          child: Align(
              alignment: Alignment.bottomCenter,
              child: BouncingWidget(
                  child: Container(
                    margin: EdgeInsets.only(bottom: width * 0.1),
                    height: height * 0.1,
                    width: width * 0.8,
                    decoration: BoxDecoration(
                      color: HomeStyles().primaryColor,
                      borderRadius: BorderRadius.circular(21),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.05),
                          offset: Offset(1, -2),
                          blurRadius: 13,
                          spreadRadius: 0,
                        ),
                        BoxShadow(
                          color: Colors.black.withOpacity(0.05),
                          offset: Offset(-4, 18),
                          blurRadius: 19,
                          spreadRadius: -1,
                        ),
                      ],
                    ),
                    child: Center(
                      child: loading
                          ? Transform.scale(
                              scale: 0.5,
                              child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation(
                                    HomeStyles().backgroundColor),
                                backgroundColor: Colors.transparent,
                              ),
                            )
                          : Text(
                              'Finish',
                              style: TextStyle(
                                fontFamily: 'Raleway',
                                fontSize: 18,
                                color: HomeStyles().backgroundColor,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                    ),
                  ),
                  onPressed: () {
                    DateTime now = DateTime.now();
                    String date = DateFormat('yyyy-MM-dd - kk:mm').format(now);
                    addToUserHistory(
                        "You studied ${(score / 100) + wrongCards.length} cards in '$title' and got ${wrongCards.length} wrong.",
                        date,
                        id,
                        context);
                  })),
        )
      ],
    );
  }

  Widget stats() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    int score = Provider.of<QuizProvider>(context).getScore;
    List wrongCards = Provider.of<QuizProvider>(context).getWrongCards;
    List totalCards = Provider.of<SubscriptionProvider>(context)
        .getSubscriptions[_index]['cards'];

    return Container(
      margin: EdgeInsets.only(bottom: height * 0.05),
      height: height * 0.35,
      width: width * 0.8,
      decoration: BoxDecoration(
        color: Color(0xFFEEEEEEE),
        borderRadius: BorderRadius.circular(21),
      ),
      padding: EdgeInsets.all(width * 0.1),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          infoRow(
            'your score',
            score.toString(),
          ),
          infoRow(
            'cards in deck',
            (totalCards.length).toString(),
          ),
          infoRow(
            'cards answered',
            ((score / 100).round() + wrongCards.length).toString(),
          ),
          infoRow(
            'correct cards',
            (score / 100).round().toString(),
          ),
          infoRow(
            'wrong cards',
            wrongCards.length.toString(),
          ),
        ],
      ),
    );
  }

  Widget infoRow(String leading, String trailing) {
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.03),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            leading,
            style: TextStyle(
              fontFamily: 'Raleway',
              fontSize: 14,
              color: HomeStyles().primaryColor,
              fontWeight: FontWeight.w600,
            ),
          ),
          Text(
            trailing,
            style: TextStyle(
              fontFamily: 'Raleway',
              fontSize: 14,
              color: HomeStyles().primaryColor,
              fontWeight: FontWeight.w400,
            ),
          ),
        ],
      ),
    );
  }

  Widget animator(Widget _child, bool trigger) {
    return Animated(
      duration: Duration(milliseconds: 1500),
      curve: Sprung.overDamped,
      value: trigger ? 1 : 0,
      builder: (context, child, animation) => Transform.translate(
        offset: Offset(0, (1 - animation.value) * 40),
        child: Opacity(
          opacity: animation.value,
          child: child,
        ),
      ),
      child: _child,
    );
  }
}
