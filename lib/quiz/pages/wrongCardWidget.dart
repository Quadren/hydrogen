import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:hydrogen/home/home_styles.dart';

class WrongCardWidget extends StatelessWidget {
  const WrongCardWidget(this._back, this._front, this._index);

  final String _front;
  final String _back;
  final int _index;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double width = size.width;
    return FlipCard(
        front: Container(
          decoration: BoxDecoration(
            color: Color(0xFFEEEEEE),
            borderRadius: BorderRadius.circular(21),
          ),
          padding: EdgeInsets.all(width * 0.05),
          margin: EdgeInsets.all(width * 0.03),
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  child: Text(
                    _front,
                    overflow: TextOverflow.fade,
                    style: TextStyle(
                      fontFamily: 'Raleway',
                      fontSize: 12,
                      color: HomeStyles().primaryColor,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: Text(
                  '${_index + 1}',
                  overflow: TextOverflow.fade,
                  style: TextStyle(
                    fontFamily: 'Raleway',
                    fontSize: 12,
                    color: Color(0xFFAAAAAA),
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ],
          ),
        ),
        back: Container(
          decoration: BoxDecoration(
            color: Color(0xFFEEEEEE),
            borderRadius: BorderRadius.circular(21),
          ),
          padding: EdgeInsets.all(width * 0.05),
          margin: EdgeInsets.all(width * 0.03),
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  child: Text(
                    _back,
                    overflow: TextOverflow.fade,
                    style: TextStyle(
                      fontFamily: 'Raleway',
                      fontSize: 12,
                      color: HomeStyles().primaryColor,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: Text(
                  '${_index + 1}',
                  overflow: TextOverflow.fade,
                  style: TextStyle(
                    fontFamily: 'Raleway',
                    fontSize: 12,
                    color: Color(0xFFAAAAAA),
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
