import 'dart:async';

import 'package:animated/animated.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/quiz/quiz_provider.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';
import '../quiz.dart' as shared;

class QuizPageOne extends StatefulWidget {
  final int _index;

  QuizPageOne(this._index);

  @override
  _QuizPageOneState createState() => _QuizPageOneState(_index);
}

class _QuizPageOneState extends State<QuizPageOne> {
  bool _expand1 = false;
  bool _fade1 = false;
  bool _expand2 = false;
  bool _fade2 = false;

  bool _moveUp = false;
  bool _initialise = false;

  final int _index;

  _QuizPageOneState(this._index);

  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 200), () {
      Provider.of<QuizProvider>(context, listen: false).resetTimer();
      setState(() {
        _initialise = true;
      });
    });
    Future.delayed(Duration(seconds: 1), () {
      setState(() {
        _moveUp = true;
      });
    });

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    String owner = Provider.of<SubscriptionProvider>(context)
        .getSubscriptions[_index]['owner'];

    String title = Provider.of<SubscriptionProvider>(context)
        .getSubscriptions[_index]['title'];

    String id = Provider.of<SubscriptionProvider>(context)
        .getSubscriptions[_index]['id'];

    List cards = Provider.of<SubscriptionProvider>(context)
        .getSubscriptions[_index]['cards'];

    int cardsLength = cards.length;

    return Container(
      padding: EdgeInsets.symmetric(horizontal: width * 0.1),
      child: Stack(
        children: [
          animateWidget(
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: height * 0.2),
                  Text('Ready to start\nstudying?',
                      style: HomeStyles().bigText),
                  SizedBox(height: height * 0.03),
                  Divider(color: Color(0xFFAAAAAA)),
                  SizedBox(height: height * 0.05),
                  Stack(
                    children: [
                      AnimatedContainer(
                        duration: Duration(milliseconds: 500),
                        curve: Curves.easeInOutCubic,
                        height: _expand1 ? height * 0.35 : height * 0.075,
                        width: width * 0.9,
                        decoration: BoxDecoration(
                          color: HomeStyles().accentColor,
                          borderRadius: BorderRadius.circular(21),
                        ),
                        child: OverflowBox(
                          maxHeight: height * 0.35,
                          child: Padding(
                            padding: EdgeInsets.only(
                              bottom: width * 0.1,
                              left: width * 0.1,
                              right: width * 0.1,
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Animated(
                                  duration: Duration(milliseconds: 300),
                                  value: _fade1 ? 1 : 0,
                                  builder: (context, child, animation) =>
                                      Opacity(
                                          opacity: animation.value,
                                          child: child),
                                  child: Text(
                                    '1. Cards will be continuously shown in random order until you stop the quiz.\n\n2. Tap a card to flip it.\n\n3. When you flip to the back, Hydrogen will ask you if you got the answer right or wrong. Answer honestly, this helps you understand your strengths or weaknesses!\n\n4. When you stop the quiz, it will be added to your activities. Good luck!',
                                    style: TextStyle(
                                      fontFamily: 'Raleway',
                                      fontSize: 12,
                                      color: HomeStyles().backgroundColor,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: height * 0.075,
                        width: width * 0.9,
                        decoration: BoxDecoration(
                          color: HomeStyles().accentColor,
                          borderRadius: BorderRadius.circular(21),
                        ),
                        child: Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: width * 0.075),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                'instructions',
                                style: TextStyle(
                                  fontFamily: 'Raleway',
                                  fontSize: 16,
                                  color: HomeStyles().backgroundColor,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  if (_expand1) {
                                    setState(() {
                                      _fade1 = false;
                                    });
                                    Future.delayed(Duration(milliseconds: 500),
                                        () {
                                      setState(() {
                                        _expand1 = false;
                                      });
                                    });
                                  } else {
                                    setState(() {
                                      _expand1 = true;
                                    });
                                    Future.delayed(Duration(milliseconds: 500),
                                        () {
                                      setState(() {
                                        _fade1 = true;
                                      });
                                    });
                                  }
                                },
                                child: Animated(
                                  value: _expand1 ? 1 : 0,
                                  builder: (context, child, animation) =>
                                      Transform.rotate(
                                    angle: animation.value * 0.8,
                                    child: child,
                                  ),
                                  child: FaIcon(FontAwesomeIcons.plus,
                                      color: HomeStyles().backgroundColor,
                                      size: height * 0.02),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: height * 0.02),
                  Stack(
                    children: [
                      AnimatedContainer(
                        duration: Duration(milliseconds: 500),
                        curve: Curves.easeInOutCubic,
                        height: _expand2 ? height * 0.25 : height * 0.075,
                        width: width * 0.9,
                        decoration: BoxDecoration(
                          color: Color(0xFFFF5558),
                          borderRadius: BorderRadius.circular(21),
                        ),
                        child: OverflowBox(
                          maxHeight: height * 0.25,
                          child: Padding(
                            padding: EdgeInsets.only(
                              bottom: width * 0.1,
                              left: width * 0.1,
                              right: width * 0.1,
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Animated(
                                  duration: Duration(milliseconds: 300),
                                  value: _fade2 ? 1 : 0,
                                  builder: (context, child, animation) =>
                                      Opacity(
                                          opacity: animation.value,
                                          child: child),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      subInfo('title', title),
                                      SizedBox(height: height * 0.01),
                                      subInfo('ID', id),
                                      SizedBox(height: height * 0.01),
                                      subInfo('cards', cardsLength.toString()),
                                      SizedBox(height: height * 0.01),
                                      subInfo('owner', owner),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: height * 0.075,
                        width: width * 0.9,
                        decoration: BoxDecoration(
                          color: Color(0xFFFF5558),
                          borderRadius: BorderRadius.circular(21),
                        ),
                        child: Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: width * 0.075),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                'about this subscription',
                                style: TextStyle(
                                  fontFamily: 'Raleway',
                                  fontSize: 16,
                                  color: HomeStyles().backgroundColor,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  if (_expand2) {
                                    setState(() {
                                      _fade2 = false;
                                    });
                                    Future.delayed(Duration(milliseconds: 500),
                                        () {
                                      setState(() {
                                        _expand2 = false;
                                      });
                                    });
                                  } else {
                                    setState(() {
                                      _expand2 = true;
                                    });
                                    Future.delayed(Duration(milliseconds: 500),
                                        () {
                                      setState(() {
                                        _fade2 = true;
                                      });
                                    });
                                  }
                                },
                                child: Animated(
                                  value: _expand2 ? 1 : 0,
                                  builder: (context, child, animation) =>
                                      Transform.rotate(
                                    angle: animation.value * 0.8,
                                    child: child,
                                  ),
                                  child: FaIcon(FontAwesomeIcons.plus,
                                      color: HomeStyles().backgroundColor,
                                      size: height * 0.02),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              _initialise),
          Animated(
            value: _moveUp ? 1 : 0,
            curve: Sprung.criticallyDamped,
            duration: Duration(milliseconds: 1500),
            builder: (context, child, animation) => Transform.translate(
              offset: Offset(0, (1 - animation.value) * 300),
              child: child,
            ),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                  padding: EdgeInsets.only(bottom: width * 0.1),
                  child: BouncingWidget(
                    child: Container(
                      height: height * 0.1,
                      width: width * 0.9,
                      decoration: BoxDecoration(
                        color: HomeStyles().primaryColor,
                        borderRadius: BorderRadius.circular(21),
                      ),
                      child: Center(
                        child: Text(
                          'Start session',
                          style: TextStyle(
                            fontFamily: 'Raleway',
                            fontSize: 16,
                            color: HomeStyles().backgroundColor,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                    onPressed: () {
                      Provider.of<QuizProvider>(context, listen: false)
                          .setInGame(true);
                      shared.quizPageController.nextPage(
                          duration: Duration(milliseconds: 800),
                          curve: Curves.easeInCubic);
                    },
                    duration: Duration(milliseconds: 150),
                  )),
            ),
          ),
        ],
      ),
    );
  }

  Widget animateWidget(Widget _child, bool trigger) {
    return Animated(
      duration: Duration(seconds: 2),
      curve: Sprung.overDamped,
      value: trigger ? 1 : 0,
      builder: (context, child, animation) => Transform.translate(
        offset: Offset(0, (1 - animation.value) * 40),
        child: Opacity(
          opacity: animation.value,
          child: child,
        ),
      ),
      child: _child,
    );
  }

  Widget subInfo(String label, String info) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          label,
          style: TextStyle(
            fontFamily: 'Raleway',
            fontSize: 12,
            fontWeight: FontWeight.w600,
            color: HomeStyles().backgroundColor,
          ),
        ),
        Text(
          info,
          style: TextStyle(
            fontFamily: 'Raleway',
            fontSize: 12,
            fontWeight: FontWeight.w400,
            color: HomeStyles().backgroundColor,
          ),
        ),
      ],
    );
  }
}
