import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/quiz/quiz_provider.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';
import '../quiz.dart' as shared;

void showConfirmClose(BuildContext context) {
  showDialog(
      context: context,
      builder: (ctx) {
        return ConfirmClose();
      });
}

class ConfirmClose extends StatefulWidget {
  @override
  _ConfirmCloseState createState() => _ConfirmCloseState();
}

class _ConfirmCloseState extends State<ConfirmClose>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  @override
  void initState() {
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Sprung.criticallyDamped);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
            height: height * 0.4,
            width: width * 0.75,
            decoration: ShapeDecoration(
                color: HomeStyles().backgroundColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(21))),
            child: Stack(
              children: [
                Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        '🏁',
                        style: TextStyle(
                          fontSize: 48,
                        ),
                      ),
                      SizedBox(height: height * 0.05),
                      Text(
                        'End session?',
                        style: TextStyle(
                          fontFamily: 'Raleway',
                          fontSize: 22,
                          color: HomeStyles().primaryColor,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(height: height * 0.01),
                      Text(
                        'this study session will be closed',
                        style: HomeStyles().subtitleText,
                      ),
                      SizedBox(height: height * 0.075),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    padding: EdgeInsets.all(width * 0.03),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        InkWell(
                          onTap: () {
                            Provider.of<QuizProvider>(context, listen: false)
                                .setInGame(false);
                            Navigator.pop(context);
                            shared.quizPageController.nextPage(
                                duration: Duration(milliseconds: 800),
                                curve: Curves.easeInOutCubic);
                          },
                          child: Container(
                            width: width * 0.33,
                            height: height * 0.065,
                            decoration: BoxDecoration(
                              color: HomeStyles().accentColor,
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: Center(
                              child: FaIcon(FontAwesomeIcons.check,
                                  color: HomeStyles().backgroundColor,
                                  size: height * 0.015),
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            width: width * 0.33,
                            height: height * 0.065,
                            decoration: BoxDecoration(
                              color: Colors.transparent,
                              border: Border.all(
                                  width: 1, color: Color(0xFFAAAAAA)),
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: Center(
                              child: FaIcon(FontAwesomeIcons.times,
                                  color: Color(0xFFAAAAAA),
                                  size: height * 0.015),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
