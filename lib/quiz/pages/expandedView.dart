import 'package:flutter/material.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:sprung/sprung.dart';

void showExpanded(String text, BuildContext context) {
  showDialog(context: context, builder: (ctx) => ExpandedView(text));
}

class ExpandedView extends StatefulWidget {
  final String _text;
  ExpandedView(this._text);
  @override
  _ExpandedViewState createState() => _ExpandedViewState(_text);
}

class _ExpandedViewState extends State<ExpandedView>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  final String _text;
  _ExpandedViewState(this._text);

  @override
  void initState() {
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Sprung.criticallyDamped);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
            height: height * 0.6,
            width: width * 0.75,
            padding: EdgeInsets.all(width * 0.05),
            decoration: ShapeDecoration(
                color: HomeStyles().backgroundColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(21))),
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.topCenter,
                  child:
                      Text('Expanded view', style: HomeStyles().subtitleText),
                ),
                Center(
                    child: Text(
                  _text,
                  style: HomeStyles().buttonText.copyWith(fontSize: 12),
                )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
