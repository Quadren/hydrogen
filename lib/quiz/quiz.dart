import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/quiz/pages/confirmStop.dart';
import 'package:hydrogen/quiz/pages/finishPage.dart';
import 'package:hydrogen/quiz/pages/gamePage.dart';
import 'package:hydrogen/quiz/pages/introPageQuiz.dart';
import 'package:hydrogen/quiz/quiz_provider.dart';
import 'package:provider/provider.dart';

class QuizPage extends StatefulWidget {
  final List _cards;
  final int _index;

  QuizPage(this._cards, this._index);

  @override
  _QuizPageState createState() => _QuizPageState(_cards, _index);
}

PageController quizPageController = PageController(initialPage: 0);

class _QuizPageState extends State<QuizPage> {
  final List cards;
  final int index;

  _QuizPageState(this.cards, this.index);

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      Provider.of<QuizProvider>(context, listen: false).setInGame(false);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool _inGame = Provider.of<QuizProvider>(context).getInGame;

    return Scaffold(
      backgroundColor: HomeStyles().backgroundColor,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: HomeStyles().primaryColor),
        leading: InkWell(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          onTap: () {
            if (_inGame) {
              showConfirmClose(context);
            } else {
              Navigator.pop(context);
            }
          },
          child: Container(
            margin: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.05,
                top: MediaQuery.of(context).size.height * 0.03),
            decoration: BoxDecoration(
              color: HomeStyles().backgroundColor,
              borderRadius: BorderRadius.circular(100),
            ),
            child: Center(
                child: FaIcon(
              _inGame
                  ? FontAwesomeIcons.stopCircle
                  : FontAwesomeIcons.arrowLeft,
              color: HomeStyles().primaryColor,
              size: MediaQuery.of(context).size.height * 0.025,
            )),
          ),
        ),
      ),
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: quizPageController,
        children: [
          QuizPageOne(index),
          GamePage(index),
          FinishPage(index),
        ],
      ),
    );
  }
}
