import 'package:flutter/cupertino.dart';

class QuizProvider extends ChangeNotifier {
  bool _isInGame = false;
  String _front;
  String _back;
  String _eqn;
  List _wrongCards = [];
  List _multipleWrong = [];
  int _score = 0;
  int _time = 0;
  bool _loading = false;

  bool get getInGame => _isInGame;
  String get getFront => _front;
  String get getBack => _back;
  String get getEqn => _eqn;
  List get getWrongCards => _wrongCards;
  int get getScore => _score;
  int get getElapsed => _time;
  bool get getLoading => _loading;

  void setInGame(bool value) {
    _isInGame = value;
    notifyListeners();
  }

  void setCard(String front, String back, String eqn) {
    _front = front;
    _back = back;
    _eqn = eqn;
    notifyListeners();
  }

  void resetWrong() {
    _loading = false;
    _wrongCards = [];
    _multipleWrong = [];
    notifyListeners();
  }

  void addToWrong(Map card) {
    if (_wrongCards.contains(card)) {
      print("Repeated");
    } else {
      _wrongCards.add(card);
    }

    notifyListeners();
  }

  void addScore() {
    _score += 100;
    notifyListeners();
  }

  void tickTimer() {
    _time++;
    notifyListeners();
  }

  void resetTimer() {
    _time = 0;
    notifyListeners();
  }

  void setLoading(bool value) {
    _loading = value;
    notifyListeners();
  }
}
