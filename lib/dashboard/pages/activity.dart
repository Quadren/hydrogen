import 'package:auto_animated/auto_animated.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/dashboard/services/activityProvider.dart';
import 'package:hydrogen/dashboard/services/loadActivities.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/widgets/backBar.dart';
import 'package:provider/provider.dart';

class UserLogPage extends StatefulWidget {
  @override
  _UserLogPageState createState() => _UserLogPageState();
}

class _UserLogPageState extends State<UserLogPage> {
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      loadActivities(context);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    bool loading = Provider.of<ActivityProvider>(context).getLoading;
    List data =
        Provider.of<ActivityProvider>(context).getActivites.reversed.toList();

    return Scaffold(
      backgroundColor: HomeStyles().backgroundColor,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: Backbar(),
      ),
      body: ScrollConfiguration(
        behavior: AntiScrollGlow(),
        child: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: height * 0.2),
                    Text(
                      'Your\nActivity',
                      style: HomeStyles().bigText,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: height * 0.05),
                    Divider(
                      color: Color(0xffAAAAAA),
                    ),
                    SizedBox(height: height * 0.05),
                  ],
                ),
              ),
            ),
            Container(
              child: loading
                  ? SliverToBoxAdapter(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(height: height * 0.2),
                          Transform.scale(
                            scale: 1.5,
                            child: CircularProgressIndicator(
                              strokeWidth: 7,
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  Color(0xff6953F5)),
                              backgroundColor: Color(0xFFFFDDE1),
                            ),
                          ),
                        ],
                      ),
                    )
                  : LiveSliverList(
                      itemBuilder:
                          (context, int index, Animation<double> animation) {
                        return FadeTransition(
                            opacity: Tween<double>(begin: 0, end: 1)
                                .animate(animation),
                            child: SlideTransition(
                              position: Tween<Offset>(
                                      begin: Offset(0, 0.1), end: Offset(0, 0))
                                  .animate(animation),
                              child: logWidget(data[index]['action'],
                                  data[index]['timeStamp'], height, width),
                            ));
                      },
                      itemCount: data.length,
                      controller: _scrollController),
            ),
          ],
        ),
      ),
    );
  }

  Widget logWidget(
      String action, String timeStamp, double height, double width) {
    return FlipCard(
        direction: FlipDirection.VERTICAL,
        front: Container(
          height: height * 0.1,
          width: width * 0.8,
          margin: EdgeInsets.symmetric(
              vertical: height * 0.01, horizontal: width * 0.1),
          decoration: BoxDecoration(
            color: Color(0xffEEEEEE),
            borderRadius: BorderRadius.circular(21),
          ),
          padding: EdgeInsets.symmetric(
            horizontal: width * 0.05,
          ),
          child: Center(
            child: Container(
              height: height * 0.1,
              width: width * 0.8,
              color: Colors.white.withOpacity(0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: width * 0.5,
                    child: Text(
                      action,
                      style: TextStyle(
                        fontFamily: 'Raleway',
                        fontSize: 12,
                        color: HomeStyles().primaryColor,
                        fontWeight: FontWeight.w400,
                      ),
                      overflow: TextOverflow.fade,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        back: Container(
          height: height * 0.1,
          width: width * 0.8,
          margin: EdgeInsets.symmetric(
              vertical: height * 0.01, horizontal: width * 0.1),
          decoration: BoxDecoration(
            color: HomeStyles().accentColor,
            borderRadius: BorderRadius.circular(21),
          ),
          padding: EdgeInsets.symmetric(horizontal: width * 0.05),
          child: Center(
            child: Container(
              height: height * 0.1,
              width: width * 0.8,
              color: Colors.white.withOpacity(0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: width * 0.5,
                    child: Text(
                      'time stamp:\n\n$timeStamp',
                      style: TextStyle(
                        fontFamily: 'Raleway',
                        fontSize: 12,
                        color: HomeStyles().backgroundColor,
                        fontWeight: FontWeight.w600,
                      ),
                      overflow: TextOverflow.fade,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
