import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/home/home_styles.dart';

class PopupSettings extends StatefulWidget {
  @override
  _PopupSettingsState createState() => _PopupSettingsState();
}

class _PopupSettingsState extends State<PopupSettings> {
  bool _cond = false;

  void getSetting() {
    if (Hive.box('userData').get('showPromo') == null) {
      Hive.box('userData').put('showPromo', true);
      setState(() {
        _cond = true;
      });
    } else {
      setState(() {
        _cond = Hive.box('userData').get('showPromo');
      });
    }
  }

  @override
  void initState() {
    getSetting();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text('show me Quadren promos',
              style: HomeStyles().buttonText.copyWith(fontSize: 12)),
          Switch(
              value: _cond,
              onChanged: (value) {
                setState(() {
                  _cond = value;
                });
                Hive.box('userData').put('showPromo', value);
              },
              activeColor: HomeStyles().primaryColor,
              focusColor: HomeStyles().primaryColor),
        ],
      ),
    );
  }
}
