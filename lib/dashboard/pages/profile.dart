import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/core/premium/revenueCatServices.dart';
import 'package:hydrogen/core/providers/loadingProvider.dart';
import 'package:hydrogen/core/userServices/userProvider.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:provider/provider.dart';

void showProfile(BuildContext context) {
  showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(21), topLeft: Radius.circular(21)),
      ),
      builder: (ctx) {
        return ProfilePage();
      });
}

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      Provider.of<LoadingProvider>(context, listen: false)
          .setPricesLoading(false);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    String pfp = Provider.of<UserProvider>(context).getPfp;
    String name = Provider.of<UserProvider>(context).getName;
    String email = Hive.box('userData').get('email');
    bool premium = Hive.box('userData').get('premium');

    bool loading = Provider.of<LoadingProvider>(context).getPricesLoading;

    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: width * 0.1),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: height * 0.05),
                Text(
                  'your profile',
                  style: HomeStyles().subtitleText,
                ),
                SizedBox(height: height * 0.05),
                Container(
                  height: height * 0.22,
                  width: width,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.05),
                        blurRadius: 15,
                        offset: Offset(-4, 18),
                        spreadRadius: -1,
                      )
                    ],
                    gradient: LinearGradient(colors: [
                      premium
                          ? HomeStyles().tertiaryAccent
                          : HomeStyles().paletteColors[3],
                      HomeStyles().secondaryAccent
                    ], begin: Alignment.bottomLeft, end: Alignment.topRight),
                    borderRadius: BorderRadius.circular(21),
                  ),
                  padding: EdgeInsets.all(width * 0.05),
                  child: Stack(
                    children: [
                      Align(
                        alignment: Alignment.topRight,
                        child: Container(
                          height: height * 0.075,
                          width: height * 0.075,
                          decoration: BoxDecoration(
                            color: HomeStyles().backgroundColor,
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(15),
                            child: CachedNetworkImage(
                              imageUrl: pfp,
                              placeholder: (context, url) {
                                return Transform.scale(
                                    scale: 0.5,
                                    child: CircularProgressIndicator(
                                      backgroundColor: Colors.transparent,
                                      valueColor: AlwaysStoppedAnimation(
                                          HomeStyles().primaryColor),
                                    ));
                              },
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomLeft,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: width * 0.5,
                              child: Text(
                                '$name',
                                style: HomeStyles().headerText.copyWith(
                                    color: HomeStyles().backgroundColor),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            SizedBox(height: height * 0.01),
                            Container(
                              width: width * 0.5,
                              child: Text(
                                '$email',
                                style: HomeStyles().subtitleText.copyWith(
                                    color: HomeStyles().backgroundColor),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: height * 0.05),
                BouncingWidget(
                  child: Container(
                    height: height * 0.1,
                    width: width * 0.8,
                    decoration: BoxDecoration(
                      color: HomeStyles().primaryColor,
                      borderRadius: BorderRadius.circular(21),
                    ),
                    child: Center(
                      child: loading
                          ? CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation(
                                  HomeStyles().backgroundColor),
                              backgroundColor: Colors.transparent,
                            )
                          : Text(
                              premium
                                  ? 'Tritium Active. Thank you! <3'
                                  : 'Upgrade to Tritium',
                              style: HomeStyles().buttonText3),
                    ),
                  ),
                  onPressed: () {
                    if (premium) {
                    } else {
                      RevenueCatServices().revenueCatFetch(context);
                    }
                  },
                  duration: Duration(milliseconds: 150),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
