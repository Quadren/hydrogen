import 'package:auto_animated/auto_animated.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get_version/get_version.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/auth/services/googleSignIn.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/dashboard/pages/activity.dart';
import 'package:hydrogen/dashboard/pages/popupSettings.dart';
import 'package:hydrogen/dashboard/pages/profile.dart';

import 'package:hydrogen/friends/friends.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/newLogin/newLogin.dart';
import 'package:page_transition/page_transition.dart';
import 'package:sprung/sprung.dart';
import 'package:url_launcher/url_launcher.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  PageController _controller = PageController();

  String _vCode;

  final List<IconData> _icons = [
    FontAwesomeIcons.list,
    FontAwesomeIcons.userFriends,
    FontAwesomeIcons.doorOpen,
    FontAwesomeIcons.userAstronaut,
    FontAwesomeIcons.questionCircle,
    FontAwesomeIcons.instagram,
  ];

  final List<String> _labels = [
    'Activity',
    'Friends',
    'Log out',
    'Profile',
    'Tutorial',
    'Follow us'
  ];

  void _launch(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      Fluttertoast.showToast(
          msg: 'Failed to launch URL.',
          backgroundColor: Colors.red[300],
          textColor: Colors.white);
    }
  }

  void getVersion() async {
    String code = await GetVersion.projectVersion;
    setState(() {
      _vCode = code;
    });
  }

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      getVersion();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    List<VoidCallback> _callbacks = [
      () {
        Navigator.push(
            context,
            PageTransition(
                child: UserLogPage(),
                type: PageTransitionType.rightToLeft,
                curve: Curves.easeInOutCubic,
                duration: Duration(milliseconds: 500)));
      },
      () {
        Navigator.push(
            context,
            PageTransition(
                child: FriendsPage(),
                type: PageTransitionType.rightToLeft,
                curve: Curves.easeInOutCubic,
                duration: Duration(milliseconds: 500)));
      },
      () {},
      () {
        showProfile(context);
      },
      () {
        _launch('https://quadrendev.gitbook.io/hydrogen-1');
      },
      () {
        _launch('https://instagram.com/quadrendev');
      },
    ];

    return Scaffold(
        backgroundColor: HomeStyles().backgroundColor,
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          iconTheme: IconThemeData(color: HomeStyles().primaryColor),
          leading: InkWell(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              margin: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.05,
                  top: MediaQuery.of(context).size.height * 0.03),
              decoration: BoxDecoration(
                color: HomeStyles().backgroundColor,
                borderRadius: BorderRadius.circular(100),
              ),
              child: Center(
                  child: FaIcon(
                FontAwesomeIcons.arrowLeft,
                color: HomeStyles().primaryColor,
                size: MediaQuery.of(context).size.height * 0.025,
              )),
            ),
          ),
        ),
        body: Stack(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.1),
              child: ScrollConfiguration(
                behavior: AntiScrollGlow(),
                child: CustomScrollView(
                  controller: _controller,
                  slivers: [
                    SliverToBoxAdapter(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: height * 0.22),
                          Text(
                            'Dashboard',
                            style: HomeStyles().bigText,
                          ),
                          SizedBox(height: height * 0.05),
                        ],
                      ),
                    ),
                    LiveSliverGrid(
                        itemBuilder:
                            (context, int index, Animation<double> animation) {
                          return ScaleTransition(
                            scale: CurveTween(
                              curve: Sprung.criticallyDamped,
                            ).animate(Tween<double>(begin: 0, end: 1)
                                .animate(animation)),
                            child: dashboardWidget(_icons[index],
                                _labels[index], index, _callbacks[index]),
                          );
                        },
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2, childAspectRatio: 1),
                        itemCount: _icons.length,
                        controller: _controller),
                    SliverToBoxAdapter(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(height: height * 0.05),
                          Text(
                            'Hydrogen $_vCode by Quadren',
                            textAlign: TextAlign.center,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: HomeStyles().subtitleText,
                          ),
                          SizedBox(height: height * 0.05),
                          Divider(color: Color(0xffAAAAAA)),
                          SizedBox(height: height * 0.03),
                          PopupSettings(),
                          SizedBox(height: height * 0.03),
                          Divider(color: Color(0xffAAAAAA)),
                          SizedBox(height: height * 0.05),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }

  Widget dashboardWidget(
      IconData icon, String label, int index, VoidCallback callback) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    List colors = [
      Color(0xffEE829C),
      Color(0xff9CA2F8),
      Color(0xffF7C96C),
      HomeStyles().primaryColor,
      HomeStyles().secondaryAccent,
      HomeStyles().accentColor,
    ];

    return BouncingWidget(
      child: Container(
        padding: EdgeInsets.only(
            left: width * 0.05,
            bottom: width * 0.05,
            right: width * 0.03,
            top: width * 0.03),
        margin: EdgeInsets.all(width * 0.02),
        height: height * 0.2,
        width: height * 0.2,
        decoration: BoxDecoration(
          border: Border.all(width: 2, color: colors[index]),
          borderRadius: BorderRadius.circular(21),
        ),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.bottomLeft,
              child: Text(
                label,
                style: HomeStyles().buttonText,
              ),
            ),
            Align(
              alignment: Alignment.topRight,
              child: Container(
                height: height * 0.05,
                width: height * 0.05,
                decoration: BoxDecoration(
                  color: colors[index],
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Center(
                  child: FaIcon(
                    icon,
                    color: HomeStyles().backgroundColor,
                    size: height * 0.015,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      onPressed: (index == 2)
          ? () {
              Future.delayed(Duration.zero, () {
                signOutGoogle().whenComplete(() {
                  Hive.box('userData').put('isSignedIn', false);
                  Hive.box('userData').clear();
                  Hive.box('categories').clear();
                  Navigator.pushAndRemoveUntil(
                      context,
                      PageTransition(
                          child: NewLogin(), type: PageTransitionType.fade),
                      (route) => false);
                });
              });
            }
          : callback,
      duration: Duration(milliseconds: 150),
    );
  }
}
