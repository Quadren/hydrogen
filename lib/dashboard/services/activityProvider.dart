import 'package:flutter/material.dart';

class ActivityProvider extends ChangeNotifier {
  List _activities = [];
  bool _loading = false;

  List get getActivites => _activities;
  bool get getLoading => _loading;

  void setActivities(List data) {
    _activities = data;
    notifyListeners();
  }

  void setLoading(bool value) {
    _loading = value;
    notifyListeners();
  }
}
