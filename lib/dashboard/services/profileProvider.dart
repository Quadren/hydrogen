import 'package:flutter/material.dart';

class ProfileProvider extends ChangeNotifier {
  String _name;
  String _pfp;
  bool _premium = false;
  bool _loading = true;

  String get getName => _name;
  String get getPfp => _pfp;
  bool get getPremium => _premium;
  bool get getLoading => _loading;

  void setUserData(String name, String pfp) {
    _name = name;
    _pfp = pfp;
    notifyListeners();
  }

  void setPremium(bool setPremium) {
    _premium = setPremium;
    notifyListeners();
  }

  void setLoading(bool value) {
    _loading = value;
    notifyListeners();
  }
}
