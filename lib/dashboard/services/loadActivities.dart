import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/dashboard/services/activityProvider.dart';
import 'package:provider/provider.dart';

Future<void> loadActivities(BuildContext context) async {
  Firebase.initializeApp().whenComplete(() async {
    Provider.of<ActivityProvider>(context, listen: false).setLoading(true);
    CollectionReference ref = FirebaseFirestore.instance.collection('users');
    String email = Hive.box('userData').get('email');

    try {
      final result = await ref.doc(email).get().then((value) {
        List activites = value.data()['log'];

        Provider.of<ActivityProvider>(context, listen: false)
            .setActivities(activites);
      });

      Provider.of<ActivityProvider>(context, listen: false).setLoading(false);
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.toString(),
          backgroundColor: Colors.red[300],
          textColor: Colors.white);
    }
  });
}
