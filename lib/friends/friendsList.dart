import 'package:animated/animated.dart';
import 'package:auto_animated/auto_animated.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/friends/addFriend/addFriend.dart';
import 'package:hydrogen/friends/friendWidget.dart';
import 'package:hydrogen/friends/friendsProvider.dart';
import 'package:hydrogen/friends/requests/requests.dart';
import 'package:hydrogen/friends/services/getAllFriends.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';

class FriendsList extends StatefulWidget {
  @override
  _FriendsListState createState() => _FriendsListState();
}

class _FriendsListState extends State<FriendsList>
    with AutomaticKeepAliveClientMixin {
  bool _animate1 = false;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      getAllFriends(Hive.box('userData').get('email'), context);
    });
    _animateFunc();
    super.initState();
  }

  void _animateFunc() {
    Future.delayed(Duration(milliseconds: 100), () {
      setState(() {
        _animate1 = true;
      });
    });
  }

  ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    super.build(context);
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    bool loading = Provider.of<FriendsProvider>(context).getLoading;
    bool empty = Provider.of<FriendsProvider>(context).getEmpty;

    List friends = Provider.of<FriendsProvider>(context).getFriends;
    List requests = Provider.of<FriendsProvider>(context).getRequests;

    bool hasRequests = (requests.length > 0) ? true : false;

    return CustomScrollView(
      controller: _scrollController,
      slivers: [
        SliverToBoxAdapter(
            child: Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.1),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: height * 0.15),
              BouncingWidget(
                child: animate(
                  _animate1,
                  Text(
                    'Friends  +',
                    style: HomeStyles().bigText,
                  ),
                ),
                onPressed: () {
                  showAddFriend(context);
                },
                duration: Duration(milliseconds: 150),
              ),
              SizedBox(height: height * 0.05),
              Container(
                child: (hasRequests)
                    ? Container(
                        child: loading
                            ? null
                            : BouncingWidget(
                                duration: Duration(milliseconds: 150),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      PageTransition(
                                          child: RequestsPage(),
                                          type: PageTransitionType.rightToLeft,
                                          curve: Curves.easeInOutCubic,
                                          duration:
                                              Duration(milliseconds: 500)));
                                },
                                child: Container(
                                  height: height * 0.075,
                                  decoration: BoxDecoration(
                                    color: Color(0xFFEEEEEE),
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  child: Center(
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: width * 0.05),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          ClipOval(
                                            child: Container(
                                              height: height * 0.0125,
                                              width: height * 0.0125,
                                              color: HomeStyles().accentColor,
                                            ),
                                          ),
                                          SizedBox(width: width * 0.05),
                                          Text(
                                            'Requests (${requests.length})',
                                            style: TextStyle(
                                              fontFamily: 'Raleway',
                                              color: HomeStyles().primaryColor,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w400,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                      )
                    : null,
              ),
              SizedBox(height: height * 0.05),
            ],
          ),
        )),
        Container(
          child: loading
              ? SliverToBoxAdapter(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: height * 0.3),
                      Transform.scale(
                        scale: 1.5,
                        child: CircularProgressIndicator(
                          strokeWidth: 7,
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Color(0xff6953F5)),
                          backgroundColor: Color(0xFFFFDDE1),
                        ),
                      ),
                    ],
                  ),
                )
              : Container(
                  child: empty
                      ? SliverToBoxAdapter(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(height: height * 0.125),
                              Text(
                                ':/\n',
                                style: HomeStyles().bigText,
                              ),
                              Text(
                                "it's lonely here",
                                style: HomeStyles().subtitleText,
                              ),
                              SizedBox(height: height * 0.03),
                              BouncingWidget(
                                child: Container(
                                  height: height * 0.075,
                                  width: width * 0.4,
                                  decoration: BoxDecoration(
                                    color: HomeStyles().primaryColor,
                                    borderRadius: BorderRadius.circular(21),
                                  ),
                                  child: Center(
                                    child: Text(
                                      'Add a friend',
                                      style: HomeStyles().buttonText3,
                                    ),
                                  ),
                                ),
                                onPressed: () {
                                  showAddFriend(context);
                                },
                                duration: Duration(milliseconds: 150),
                              )
                            ],
                          ),
                        )
                      : LiveSliverList(
                          itemBuilder: (context, index, animation) {
                            return FadeTransition(
                              opacity: Tween<double>(begin: 0, end: 1)
                                  .animate(animation),
                              child: SlideTransition(
                                position: Tween<Offset>(
                                        begin: Offset(0, 0.1),
                                        end: Offset(0, 0))
                                    .animate(animation),
                                child: FriendWidget(
                                    friends[index]['name'],
                                    friends[index]['email'],
                                    friends[index]['pfp']),
                              ),
                            );
                          },
                          itemCount: friends.length,
                          controller: _scrollController,
                        ),
                ),
        ),
      ],
    );
  }

  Widget animate(bool trigger, Widget _child) {
    return Animated(
      duration: Duration(seconds: 1),
      curve: Sprung.overDamped,
      value: trigger ? 1 : 0,
      builder: (context, child, animation) => Transform.translate(
        offset: Offset(0, (1 - animation.value) * 40),
        child: Opacity(
          opacity: animation.value,
          child: child,
        ),
      ),
      child: _child,
    );
  }
}
