import 'package:auto_animated/auto_animated.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/friends/friendsProvider.dart';
import 'package:hydrogen/friends/requests/confirmRequest.dart';
import 'package:hydrogen/friends/requests/userPage.dart';
import 'package:hydrogen/friends/services/getRequests.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/widgets/backBar.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class RequestsPage extends StatefulWidget {
  @override
  _RequestsPageState createState() => _RequestsPageState();
}

class _RequestsPageState extends State<RequestsPage> {
  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      Provider.of<FriendsProvider>(context, listen: false)
          .setRequestsLoading(true);
      getRequests(context);
    });
    super.initState();
  }

  ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    List requests = Provider.of<FriendsProvider>(context).getRenderRequests;
    bool loading = Provider.of<FriendsProvider>(context).getRequestsLoading;

    return Scaffold(
      backgroundColor: HomeStyles().backgroundColor,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: HomeStyles().primaryColor),
        leading: Backbar(),
      ),
      body: ScrollConfiguration(
          behavior: AntiScrollGlow(),
          child: CustomScrollView(
            controller: _scrollController,
            slivers: [
              SliverToBoxAdapter(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: height * 0.15),
                      Text('Requests', style: HomeStyles().bigText),
                      SizedBox(height: height * 0.05),
                    ],
                  ),
                ),
              ),
              Container(
                child: loading
                    ? SliverToBoxAdapter(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(height: height * 0.3),
                            Transform.scale(
                              scale: 1.5,
                              child: CircularProgressIndicator(
                                strokeWidth: 7,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Color(0xff6953F5)),
                                backgroundColor: Color(0xFFFFDDE1),
                              ),
                            ),
                          ],
                        ),
                      )
                    : LiveSliverList(
                        itemBuilder: (context, index, animation) {
                          return FadeTransition(
                            opacity: Tween<double>(begin: 0, end: 1)
                                .animate(animation),
                            child: SlideTransition(
                              position: Tween<Offset>(
                                      begin: Offset(0, 0.1), end: Offset(0, 0))
                                  .animate(animation),
                              child:
                                  requestWidget(height, width, requests, index),
                            ),
                          );
                        },
                        itemCount: requests.length,
                        controller: _scrollController,
                      ),
              )
            ],
          )),
    );
  }

  Widget requestWidget(double height, double width, List requests, int index) {
    return Container(
      width: width * 0.8,
      height: height * 0.133,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: height * 0.085,
                width: width * 0.3,
                decoration: BoxDecoration(
                    color: Color(0xffEEEEEE),
                    border: Border.all(width: 2, color: Color(0xffEEEEEE)),
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(21),
                        bottomRight: Radius.circular(21))),
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(19),
                      bottomRight: Radius.circular(19)),
                  child: CachedNetworkImage(
                    imageUrl: requests[index]['pfp'],
                    fit: BoxFit.cover,
                    placeholder: (context, url) {
                      return Container(
                        height: height * 0.085,
                        width: width * 0.3,
                        padding: EdgeInsets.only(right: width * 0.05),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Transform.scale(
                              scale: 0.5,
                              child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation(
                                    HomeStyles().accentColor),
                                backgroundColor: HomeStyles().secondaryAccent,
                              )),
                        ),
                      );
                    },
                  ),
                ),
              ),
              SizedBox(width: width * 0.03),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${requests[index]['name']}',
                    style: TextStyle(
                      fontFamily: 'Raleway',
                      fontSize: 18,
                      color: HomeStyles().primaryColor,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: height * 0.01),
                  Container(
                    width: width * 0.4,
                    child: Text(
                      '${requests[index]['email']}',
                      style: TextStyle(
                        fontFamily: 'Raleway',
                        fontSize: 12,
                        color: Color(0xffAAAAAA),
                        fontWeight: FontWeight.w400,
                      ),
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                    ),
                  ),
                ],
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(right: width * 0.1),
            child: BouncingWidget(
              child: Align(
                alignment: Alignment.centerRight,
                child: Container(
                  height: height * 0.075,
                  width: width * 0.05,
                  child: Center(
                    child: FaIcon(FontAwesomeIcons.arrowRight,
                        color: HomeStyles().primaryColor, size: height * 0.02),
                  ),
                ),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    PageTransition(
                        child: UserPage(requests[index]['email'],
                            requests[index]['name'], requests[index]['pfp']),
                        type: PageTransitionType.rightToLeft,
                        curve: Curves.easeInOutCubic,
                        duration: Duration(milliseconds: 500)));
              },
              duration: Duration(milliseconds: 150),
            ),
          )
        ],
      ),
    );
  }
}
