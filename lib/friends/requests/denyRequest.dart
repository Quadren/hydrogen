import 'package:flutter/material.dart';
import 'package:hydrogen/friends/services/denyRequestServices.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:lottie/lottie.dart';
import 'package:sprung/sprung.dart';

void denyRequestPopup(BuildContext context, String email) {
  showDialog(
      context: context,
      builder: (ctxt) => DenyRequestPopup(email),
      barrierDismissible: false);
}

class DenyRequestPopup extends StatefulWidget {
  final String _email;

  DenyRequestPopup(this._email);

  @override
  _DenyRequestPopupState createState() => _DenyRequestPopupState(_email);
}

PageController denyRequestController = PageController(initialPage: 0);

class _DenyRequestPopupState extends State<DenyRequestPopup>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  final String _email;

  _DenyRequestPopupState(this._email);

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      denyRequest(_email, context);
    });
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Sprung.criticallyDamped);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
            height: height * 0.4,
            width: width * 0.75,
            decoration: ShapeDecoration(
                color: HomeStyles().backgroundColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(21))),
            child: PageView(
              controller: denyRequestController,
              physics: NeverScrollableScrollPhysics(),
              children: [
                Center(
                  child: Transform.scale(
                    scale: 1.5,
                    child: CircularProgressIndicator(
                      strokeWidth: 7,
                      valueColor:
                          AlwaysStoppedAnimation(HomeStyles().accentColor),
                      backgroundColor: HomeStyles().secondaryAccent,
                    ),
                  ),
                ),
                Center(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.height * 0.2,
                    child: Lottie.asset('assets/lottie/check.json',
                        repeat: false, fit: BoxFit.contain),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
