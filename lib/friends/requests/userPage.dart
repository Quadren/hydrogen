import 'package:animated/animated.dart';
import 'package:auto_animated/auto_animated.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/friends/requests/confirmRequest.dart';
import 'package:hydrogen/friends/requests/denyRequest.dart';
import 'package:hydrogen/friends/services/getInfoAboutUser.dart';
import 'package:hydrogen/friends/services/userReqProvider.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/widgets/backBar.dart';
import 'package:provider/provider.dart';

class UserPage extends StatefulWidget {
  final String _email;
  final String _name;
  final String _pfp;

  UserPage(this._email, this._name, this._pfp);

  @override
  _UserPageState createState() => _UserPageState(_email, _name, _pfp);
}

class _UserPageState extends State<UserPage> {
  final String _email;
  final String _name;
  final String _pfp;
  _UserPageState(this._email, this._name, this._pfp);

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      getInfoAboutReq(_email, context);
      Provider.of<UserReqProvider>(context, listen: false).setLoading(true);
    });
    super.initState();
  }

  ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    List subs = Provider.of<UserReqProvider>(context).getSubs;
    List friends = Provider.of<UserReqProvider>(context).getFriends;
    bool loading = Provider.of<UserReqProvider>(context).getLoading;
    bool premium = Provider.of<UserReqProvider>(context).getPremium;

    List<String> labels = ['friends', 'premium', 'subscriptions'];

    List<dynamic> data = [
      friends.length.toString(),
      premium,
      subs.length.toString(),
    ];

    List<IconData> icons = [
      FontAwesomeIcons.userFriends,
      FontAwesomeIcons.crown,
      FontAwesomeIcons.list,
    ];

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: HomeStyles().primaryColor),
        leading: Backbar(),
      ),
      extendBodyBehindAppBar: true,
      backgroundColor: HomeStyles().backgroundColor,
      body: ScrollConfiguration(
          behavior: AntiScrollGlow(),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: width * 0.1),
            child: CustomScrollView(
              controller: _scrollController,
              slivers: [
                SliverToBoxAdapter(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: height * 0.15),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            child: loading
                                ? null
                                : BouncingWidget(
                                    child: Container(
                                      height: height * 0.05,
                                      width: height * 0.05,
                                      child: Center(
                                        child: FaIcon(
                                          FontAwesomeIcons.check,
                                          color: HomeStyles().accentColor,
                                          size: height * 0.025,
                                        ),
                                      ),
                                    ),
                                    onPressed: () {
                                      showConfirmRequest(context, _email);
                                    }),
                          ),
                          SizedBox(width: width * 0.03),
                          Container(
                            height: height * 0.2,
                            width: height * 0.2,
                            decoration: BoxDecoration(
                              color: Color(0xffEEEEEE),
                              borderRadius: BorderRadius.circular(100),
                              border: Border.all(
                                  width: 7,
                                  color: HomeStyles().secondaryAccent),
                            ),
                            child: ClipOval(
                              child: CachedNetworkImage(
                                imageUrl: _pfp,
                                fit: BoxFit.cover,
                                placeholder: (context, url) {
                                  return Center(
                                    child: CircularProgressIndicator(
                                      strokeWidth: 4,
                                      valueColor: AlwaysStoppedAnimation(
                                          HomeStyles().accentColor),
                                      backgroundColor:
                                          HomeStyles().secondaryAccent,
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                          SizedBox(width: width * 0.03),
                          Container(
                            child: loading
                                ? null
                                : BouncingWidget(
                                    child: Container(
                                      height: height * 0.05,
                                      width: height * 0.05,
                                      child: Center(
                                        child: FaIcon(
                                          FontAwesomeIcons.times,
                                          color: Color(0xffFF5558),
                                          size: height * 0.025,
                                        ),
                                      ),
                                    ),
                                    onPressed: () {
                                      denyRequestPopup(context, _email);
                                    }),
                          ),
                        ],
                      ),
                      SizedBox(height: height * 0.03),
                      Text('$_name',
                          style: TextStyle(
                              fontFamily: 'Raleway',
                              fontSize: 22,
                              color: HomeStyles().primaryColor,
                              fontWeight: FontWeight.w600)),
                      SizedBox(
                        height: height * 0.05,
                      )
                    ],
                  ),
                ),
                Container(
                  child: loading
                      ? SliverToBoxAdapter(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(height: height * 0.15),
                              Transform.scale(
                                scale: 1.5,
                                child: CircularProgressIndicator(
                                  strokeWidth: 7,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Color(0xff6953F5)),
                                  backgroundColor: Color(0xFFFFDDE1),
                                ),
                              ),
                            ],
                          ),
                        )
                      : LiveSliverGrid(
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  childAspectRatio:
                                      (width * 0.4) / (height * 0.2)),
                          itemBuilder: (context, int index,
                              Animation<double> animation) {
                            return FadeTransition(
                              opacity: Tween<double>(begin: 0, end: 1)
                                  .animate(animation),
                              child: SlideTransition(
                                  position: Tween<Offset>(
                                          begin: Offset(0, 0.1),
                                          end: Offset(0, 0))
                                      .animate(animation),
                                  child: infoWidget(labels[index], data[index],
                                      icons[index], index)),
                            );
                          },
                          itemCount: 2,
                          controller: _scrollController),
                ),
                Container(
                  child: loading
                      ? SliverToBoxAdapter()
                      : SliverToBoxAdapter(
                          child: Animated(
                          value: loading ? 0 : 1,
                          builder: (context, child, animation) =>
                              Transform.translate(
                            offset: Offset(0, (1 - animation.value) * 40),
                            child: Opacity(
                              opacity: animation.value,
                              child: child,
                            ),
                          ),
                          child: Container(
                            height: height * 0.2,
                            width: width * 0.4,
                            margin: EdgeInsets.only(
                              bottom: width * 0.03,
                              top: width * 0.03,
                            ),
                            child: Stack(children: [
                              Align(
                                alignment: Alignment.center,
                                child: Container(
                                    child: Text(
                                  subs.length.toString(),
                                  style: TextStyle(
                                    fontSize: 36,
                                    color: HomeStyles().primaryColor,
                                    fontFamily: 'Raleway',
                                    fontWeight: FontWeight.w600,
                                  ),
                                )),
                              ),
                              Align(
                                alignment: Alignment.bottomCenter,
                                child: Text(
                                  'subscriptions',
                                  style: HomeStyles().buttonText,
                                ),
                              ),
                            ]),
                          ),
                        )),
                ),
              ],
            ),
          )),
    );
  }

  Widget infoWidget(String label, var data, IconData icon, int index) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    List subs = Provider.of<UserReqProvider>(context).getSubs;
    List friends = Provider.of<UserReqProvider>(context).getFriends;
    bool loading = Provider.of<UserReqProvider>(context).getLoading;
    bool premium = Provider.of<UserReqProvider>(context).getPremium;

    return Container(
      margin: EdgeInsets.only(
          right: (index % 2 == 0) ? width * 0.03 : 0,
          bottom: width * 0.03,
          top: width * 0.03,
          left: (index % 2 != 0) ? 0 : width * 0.03),
      child: Stack(children: [
        Align(
          alignment: Alignment.center,
          child: Container(
            child: (index != 1)
                ? Text(
                    data,
                    style: TextStyle(
                      fontSize: 36,
                      color: HomeStyles().primaryColor,
                      fontFamily: 'Raleway',
                      fontWeight: FontWeight.w600,
                    ),
                  )
                : FaIcon(
                    premium ? FontAwesomeIcons.check : FontAwesomeIcons.times,
                    color:
                        premium ? HomeStyles().accentColor : Color(0xffFF5558),
                    size: height * 0.035,
                  ),
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Text(
            label,
            style: HomeStyles().buttonText,
          ),
        ),
      ]),
    );
  }
}
