import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hydrogen/home/home_styles.dart';

class FriendWidget extends StatelessWidget {
  final String _name;
  final String _email;
  final String _pfp;

  FriendWidget(this._name, this._email, this._pfp);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return Container(
      width: width * 0.8,
      height: height * 0.133,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: height * 0.085,
                width: width * 0.3,
                decoration: BoxDecoration(
                    color: Color(0xffEEEEEE),
                    border: Border.all(width: 2, color: Color(0xffEEEEEE)),
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(21),
                        bottomRight: Radius.circular(21))),
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(19),
                      bottomRight: Radius.circular(19)),
                  child: CachedNetworkImage(
                    imageUrl: _pfp,
                    fit: BoxFit.cover,
                    placeholder: (context, url) {
                      return Container(
                        height: height * 0.085,
                        width: width * 0.3,
                        padding: EdgeInsets.only(right: width * 0.05),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Transform.scale(
                              scale: 0.5,
                              child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation(
                                    HomeStyles().accentColor),
                                backgroundColor: HomeStyles().secondaryAccent,
                              )),
                        ),
                      );
                    },
                  ),
                ),
              ),
              SizedBox(width: width * 0.03),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '$_name',
                    style: TextStyle(
                      fontFamily: 'Raleway',
                      fontSize: 18,
                      color: HomeStyles().primaryColor,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: height * 0.01),
                  Container(
                    width: width * 0.4,
                    child: Text(
                      '$_email',
                      style: TextStyle(
                        fontFamily: 'Raleway',
                        fontSize: 12,
                        color: Color(0xffAAAAAA),
                        fontWeight: FontWeight.w400,
                      ),
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
