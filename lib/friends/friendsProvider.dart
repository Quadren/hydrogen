import 'package:flutter/cupertino.dart';

class FriendsProvider extends ChangeNotifier {
  List _friends = [];
  bool _empty = false;
  bool _loading = false;
  bool _adding = false;
  List _requests = [];
  List _requestsRender = [];
  bool _requestsLoading = false;
  bool _addingFriend = false;

  List get getFriends => _friends;
  bool get getEmpty => _empty;
  bool get getLoading => _loading;
  bool get getAdding => _adding;
  List get getRequests => _requests;
  List get getRenderRequests => _requestsRender;
  bool get getRequestsLoading => _requestsLoading;
  bool get getAddingFriend => _addingFriend;

  void setLoading(bool value) {
    _loading = value;
    notifyListeners();
  }

  void setEmpty(bool value) {
    _empty = value;
    notifyListeners();
  }

  void setAdding(bool value) {
    _adding = value;
    notifyListeners();
  }

  void resetFriends() {
    _loading = true;
    _friends = [];
    notifyListeners();
  }

  void setRequests(List data) {
    _requests = data;
    notifyListeners();
  }

  void addFriend(String name, String email, String pfp) {
    Map tempMap = {'name': name, 'email': email, 'pfp': pfp};
    _friends.add(tempMap);
    notifyListeners();
  }

  void removeFriend(String name, String email, String pfp) {
    Map tempMap = {'name': name, 'email': email, 'pfp': pfp};
    _friends.removeWhere((element) => element == tempMap);
    notifyListeners();
  }

  void addRequest(String name, String email, String pfp, String timeStamp) {
    Map tempMap = {
      'name': name,
      'email': email,
      'pfp': pfp,
      'timeStamp': timeStamp
    };
    _requestsRender.add(tempMap);
    notifyListeners();
  }

  void removeRequest(String email) {
    var element1;
    var element2;

    _requestsRender.forEach((element) {
      Map data = element;
      if (data.containsValue(email)) {
        element1 = element;
      }
    });

    _requests.forEach((element) {
      Map data = element;
      if (data.containsValue(email)) {
        element2 = element;
      }
    });

    _requests.remove(element2);
    _requestsRender.remove(element1);

    notifyListeners();
  }

  void resetRequests() {
    _requestsRender = [];
    notifyListeners();
  }

  void setRequestsLoading(bool value) {
    _requestsLoading = value;
    notifyListeners();
  }

  void setAddingFriend(bool value) {
    _addingFriend = value;
    notifyListeners();
  }
}
