import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hydrogen/friends/friendsProvider.dart';
import 'package:provider/provider.dart';

Future<void> getAllFriends(String email, context) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('users');
    Provider.of<FriendsProvider>(context, listen: false).setLoading(true);
    Provider.of<FriendsProvider>(context, listen: false).resetFriends();

    final result = await ref.doc(email).get().then((value) {
      if (value.exists) {
        List friends = value.data()['friends'];
        List requests = value.data()['requests'];

        if (friends.isEmpty) {
          Provider.of<FriendsProvider>(context, listen: false)
              .setLoading(false);

          if (requests.isNotEmpty) {
            print("User has friend requests");
            Provider.of<FriendsProvider>(context, listen: false)
                .setRequests(requests);
            Provider.of<FriendsProvider>(context, listen: false)
                .setEmpty(false);
          } else {
            print("User has no friend requests");
            Provider.of<FriendsProvider>(context, listen: false).setEmpty(true);
          }
        } else {
          Provider.of<FriendsProvider>(context, listen: false).setEmpty(false);
          if (requests.isNotEmpty) {
            print("User has friend requests");
            Provider.of<FriendsProvider>(context, listen: false)
                .setRequests(requests);
          }
          friends.forEach((element) async {
            final result = await ref.doc(element).get().then((val2) {
              String name = val2.data()['name'];
              String pfp = val2.data()['pfp'];
              Provider.of<FriendsProvider>(context, listen: false)
                  .addFriend(name, element, pfp);
            });
          });
        }
      }
    });

    Provider.of<FriendsProvider>(context, listen: false).setLoading(false);
  });
}
