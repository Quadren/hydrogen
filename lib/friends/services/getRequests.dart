import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:hydrogen/friends/friendsProvider.dart';
import 'package:provider/provider.dart';

Future<void> getRequests(BuildContext context) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('users');

    Provider.of<FriendsProvider>(context, listen: false)
        .setRequestsLoading(true);

    List requests =
        Provider.of<FriendsProvider>(context, listen: false).getRequests;

    requests.forEach((element) async {
      Map data = element;

      final result = await ref.doc(data['sender']).get().then((value) {
        if (value.exists) {
          if (Provider.of<FriendsProvider>(context, listen: false)
                  .getRenderRequests
                  .length ==
              requests.length) {
            Provider.of<FriendsProvider>(context, listen: false)
                .setRequestsLoading(false);
          } else {
            String name = value.data()['name'];
            String pfp = value.data()['pfp'];
            String email = value.data()['email'];
            String timeStamp = element['timeStamp'];
            Provider.of<FriendsProvider>(context, listen: false)
                .addRequest(name, email, pfp, timeStamp);
          }
        }
      });
    });
  });
}
