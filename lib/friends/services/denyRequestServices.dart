import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/friends/friendsProvider.dart';
import 'package:hydrogen/friends/requests/denyRequest.dart';
import 'package:hydrogen/friends/requests/requests.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

Future<void> denyRequest(String email, BuildContext context) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('users');

    String userEmail = Hive.box('userData').get('email');

    final result = await ref.doc(email).get().then((value) async {
      if (value.exists) {
        List reqSent = value.data()['reqSent'];

        var elementToDelete;

        final result2 = await ref.doc(userEmail).get().then((val2) async {
          List requests = val2.data()['requests'];
          requests.forEach((element) async {
            Map data = element;
            if (data.containsValue(email)) {
              print("Request found");
              elementToDelete = element;
            }
          });

          reqSent.remove(userEmail);
          requests.remove(elementToDelete);

          final result3 = await ref.doc(email).update({'reqSent': reqSent});
          final result4 =
              await ref.doc(userEmail).update({'requests': requests});
          denyRequestController.nextPage(
              duration: Duration(milliseconds: 500),
              curve: Curves.easeInOutCubic);
          Fluttertoast.showToast(
              msg: 'Friend request denied.',
              backgroundColor: Colors.red[300],
              textColor: Colors.white);

          Provider.of<FriendsProvider>(context, listen: false)
              .removeRequest(email);

          Future.delayed(Duration(seconds: 2), () {
            Navigator.pop(context);
            Navigator.pushReplacement(
                context,
                PageTransition(
                    child: RequestsPage(),
                    type: PageTransitionType.leftToRight,
                    curve: Curves.easeInOutCubic,
                    duration: Duration(milliseconds: 500)));
          });
        });
      }
    });
  });
}
