import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/friends/friendsProvider.dart';
import 'package:hydrogen/friends/requests/confirmRequest.dart';
import 'package:hydrogen/friends/requests/requests.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

Future<void> confirmRequestAndAdd(String email, BuildContext context) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('users');
    Provider.of<FriendsProvider>(context, listen: false).setAddingFriend(true);

    String userEmail = Hive.box('userData').get('email');

    var elementToDelete;

    try {
      final result = ref.doc(userEmail).get().then((userSnapshot) async {
        List friends = userSnapshot.data()['friends'];
        List requests = userSnapshot.data()['requests'];

        final result2 = await ref.doc(email).get().then((friendSnapshot) async {
          List friends2 = friendSnapshot.data()['friends'];
          List reqSent = friendSnapshot.data()['reqSent'];
          String name = friendSnapshot.data()['name'];
          String pfp = friendSnapshot.data()['pfp'];

          if (friends.isEmpty) {
            friends = [email];
            requests.forEach((element) {
              Map data = element;
              if (data.containsValue(email)) {
                print("Found request");
                elementToDelete = element;
              }
            });
          } else {
            friends.add(email);
            requests.forEach((element) {
              Map data = element;
              if (data.containsValue(email)) {
                print("Found request");
                elementToDelete = element;
              }
            });
          }

          requests.remove(elementToDelete);

          if (friends2.isEmpty) {
            friends2 = [userEmail];
            reqSent.remove(userEmail);
          } else {
            friends2.add(userEmail);
            reqSent.remove(userEmail);
          }

          final result3 = await ref
              .doc(email)
              .update({'reqSent': reqSent, 'friends': friends2});
          final result4 = await ref
              .doc(userEmail)
              .update({'requests': requests, 'friends': friends});

          confirmRequestController.nextPage(
              duration: Duration(milliseconds: 500),
              curve: Curves.easeInOutCubic);

          Fluttertoast.showToast(
              msg: 'You are now friends with $email!',
              backgroundColor: HomeStyles().accentColor,
              textColor: Colors.white);

          Provider.of<FriendsProvider>(context, listen: false)
              .addFriend(name, email, pfp);
          Provider.of<FriendsProvider>(context, listen: false)
              .removeRequest(email);

          Future.delayed(Duration(seconds: 2), () {
            Navigator.pop(context);
            Navigator.pushReplacement(
                context,
                PageTransition(
                    child: RequestsPage(),
                    type: PageTransitionType.leftToRight,
                    curve: Curves.easeInOutCubic,
                    duration: Duration(milliseconds: 500)));
          });
        });
      });
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.toString(),
          backgroundColor: Colors.red[300],
          textColor: Colors.white);
    }
  });
}
