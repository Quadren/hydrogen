import 'package:flutter/material.dart';

class UserReqProvider extends ChangeNotifier {
  bool _loading = true;
  List _friends = [];
  List _subscriptions = [];
  bool _premium = false;

  bool get getLoading => _loading;
  List get getFriends => _friends;
  List get getSubs => _subscriptions;
  bool get getPremium => _premium;

  void setLoading(bool value) {
    _loading = value;
    notifyListeners();
  }

  void setFriends(List data) {
    _friends = data;
    notifyListeners();
  }

  void setSubs(List data) {
    _subscriptions = data;
    notifyListeners();
  }

  void setPremium(bool value) {
    _premium = value;
    notifyListeners();
  }
}
