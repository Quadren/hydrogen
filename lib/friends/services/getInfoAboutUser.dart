import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hydrogen/friends/services/userReqProvider.dart';
import 'package:provider/provider.dart';

Future<void> getInfoAboutReq(String email, BuildContext context) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('users');

    Provider.of<UserReqProvider>(context, listen: false).setLoading(true);

    try {
      final result = await ref.doc(email).get().then((value) {
        List friends = value.data()['friends'];
        List subscriptions = value.data()['subscriptions'];
        bool premium = value.data()['premium'];

        if (friends.isEmpty) {
          Provider.of<UserReqProvider>(context, listen: false).setFriends([]);
        } else {
          Provider.of<UserReqProvider>(context, listen: false)
              .setFriends(friends);
        }

        Provider.of<UserReqProvider>(context, listen: false)
            .setPremium(premium);
        if (subscriptions.isEmpty) {
          Provider.of<UserReqProvider>(context, listen: false).setSubs([]);
        } else {
          Provider.of<UserReqProvider>(context, listen: false)
              .setSubs(subscriptions);
        }
      });

      Provider.of<UserReqProvider>(context, listen: false).setLoading(false);
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.toString(),
          backgroundColor: Colors.red[300],
          textColor: Colors.white);
    }
  });
}
