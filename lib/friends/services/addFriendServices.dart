import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/friends/addFriend/addFriend.dart' as shared;
import 'package:hydrogen/friends/friendsProvider.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class AddFriendServices {
  Future<void> sendFriendRequest(String email, BuildContext context) async {
    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref = FirebaseFirestore.instance.collection('users');

      if (email == Hive.box('userData').get('email')) {
        shared.addFriendShakeController.shake();
        Provider.of<FriendsProvider>(context, listen: false).setAdding(false);
      } else {
        final result = await ref.doc(email).get().then((value) async {
          if (value.exists) {
            List friends = value.data()['friends'];

            ref.doc(Hive.box('userData').get('email')).get().then((val2) async {
              if (val2.exists) {
                List reqSent = val2.data()['reqSent'];
                if (friends.contains(email) || reqSent.contains(email)) {
                  shared.addFriendShakeController.shake();
                  Provider.of<FriendsProvider>(context, listen: false)
                      .setAdding(false);
                  if (reqSent.contains(email)) {
                    Fluttertoast.showToast(
                        msg: "You've already sent a request!",
                        backgroundColor: Colors.red[300],
                        textColor: Colors.white);
                  } else if (friends.contains(email)) {
                    Fluttertoast.showToast(
                        msg: 'This user is already friends with you!',
                        backgroundColor: Colors.red[300],
                        textColor: Colors.white);
                  }
                } else {
                  DateTime now = DateTime.now();
                  String date = DateFormat('yyyy-MM-dd - kk:mm').format(now);
                  List requests = value.data()['requests'];
                  Map tempMap = {
                    'sender': Hive.box('userData').get('email'),
                    'timeStamp': date
                  };
                  requests.add(tempMap);
                  reqSent.add(email);
                  final result =
                      await ref.doc(email).update({'requests': requests});
                  final result2 = await ref
                      .doc(Hive.box('userData').get('email'))
                      .update({'reqSent': reqSent});
                  Provider.of<FriendsProvider>(context, listen: false)
                      .setAdding(false);
                  shared.addFriendController.nextPage(
                      duration: Duration(milliseconds: 500),
                      curve: Curves.easeInOutCubic);
                  Fluttertoast.showToast(
                      msg: 'Request sent!',
                      backgroundColor: HomeStyles().accentColor,
                      textColor: Colors.white);
                  Future.delayed(Duration(seconds: 2), () {
                    Navigator.pop(context);
                  });
                }
              }
            });
          } else {
            shared.addFriendShakeController.shake();
            Provider.of<FriendsProvider>(context, listen: false)
                .setAdding(false);
          }
        });
      }
    });
  }
}
