import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/core/misc/shakeAnim.dart';
import 'package:hydrogen/friends/friendsProvider.dart';
import 'package:hydrogen/friends/services/addFriendServices.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';

void showAddFriend(BuildContext context) {
  showDialog(context: context, builder: (ctx) => AddFriendPage());
}

class AddFriendPage extends StatefulWidget {
  @override
  _AddFriendPageState createState() => _AddFriendPageState();
}

PageController addFriendController = PageController(initialPage: 0);
ShakeController addFriendShakeController;

class _AddFriendPageState extends State<AddFriendPage>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  @override
  void initState() {
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Sprung.criticallyDamped);

    addFriendShakeController = ShakeController(vsync: this);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
    super.initState();
  }

  TextEditingController _controller1 = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
            height: height * 0.4,
            width: width * 0.75,
            decoration: ShapeDecoration(
                color: HomeStyles().backgroundColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(21))),
            child: PageView(
              controller: addFriendController,
              physics: NeverScrollableScrollPhysics(),
              children: [
                pageOne(),
                Center(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.height * 0.2,
                    child: Lottie.asset('assets/lottie/check.json',
                        repeat: false, fit: BoxFit.contain),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget pageOne() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    OutlineInputBorder _border = OutlineInputBorder(
      borderRadius: BorderRadius.circular(21),
      borderSide: BorderSide(width: 1, color: HomeStyles().primaryColor),
    );

    OutlineInputBorder _errorBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(21),
      borderSide: BorderSide(width: 1, color: Colors.red[300]),
    );

    InputDecoration _formDecor = InputDecoration(
      border: _border,
      errorBorder: _errorBorder,
      disabledBorder: _border,
      enabledBorder: _border,
      focusedBorder: _border,
      contentPadding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.05,
          vertical: MediaQuery.of(context).size.height * 0.04),
      errorStyle: TextStyle(
        fontFamily: 'Raleway',
        fontSize: 12,
        color: Colors.red[300],
        fontWeight: FontWeight.w400,
      ),
    );

    bool absorb = Provider.of<FriendsProvider>(context).getAdding;

    return Padding(
        padding: EdgeInsets.all(width * 0.05),
        child: AnimatedOpacity(
          opacity: absorb ? 0.5 : 1,
          duration: Duration(milliseconds: 300),
          child: AbsorbPointer(
            absorbing: absorb,
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: height * 0.02),
                    Text("enter an email address",
                        style: HomeStyles().subtitleText),
                    SizedBox(height: height * 0.05),
                    ShakeView(
                        child: TextFormField(
                          controller: _controller1,
                          decoration: _formDecor,
                          cursorColor: HomeStyles().primaryColor,
                          style: HomeStyles().headerText,
                        ),
                        controller: addFriendShakeController)
                  ],
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: AbsorbPointer(
                    absorbing: (_controller1.text.length > 6) ? false : true,
                    child: InkWell(
                      onTap: () {
                        String text = _controller1.text;
                        if (text.length > 6) {
                          Provider.of<FriendsProvider>(context, listen: false)
                              .setAdding(true);
                          AddFriendServices().sendFriendRequest(text, context);
                        } else {
                          Fluttertoast.showToast(
                              msg: 'Minimum length is 6 characters.',
                              backgroundColor: Color(0xffff5558),
                              textColor: Colors.white);
                        }
                      },
                      child: AnimatedContainer(
                        duration: Duration(milliseconds: 250),
                        height: height * 0.075,
                        width: width * 0.3,
                        decoration: BoxDecoration(
                          color: (_controller1.text.length > 6)
                              ? HomeStyles().accentColor
                              : HomeStyles().accentColor.withOpacity(0.3),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text('Add', style: HomeStyles().buttonText3),
                              FaIcon(FontAwesomeIcons.arrowRight,
                                  color: HomeStyles().backgroundColor,
                                  size: height * 0.015)
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
