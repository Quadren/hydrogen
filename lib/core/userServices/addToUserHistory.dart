import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/quiz/quiz_provider.dart';
import 'package:provider/provider.dart';

Future<void> addToUserHistory(
    String action, String timeStamp, String id, context) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('users');
    CollectionReference ref2 =
        FirebaseFirestore.instance.collection('subscriptions');
    String email = Hive.box('userData').get('email');

    Provider.of<QuizProvider>(context, listen: false).setLoading(true);

    final result = await ref.doc(email).get().then((value) async {
      if (value.exists) {
        List log = value.data()['log'];
        Map tempMap = {'action': action, 'timeStamp': timeStamp};

        log.add(tempMap);

        final result = await ref2.doc(id).get().then((val2) async {
          if (val2.exists) {
            List studyLog = val2.data()['studyLog'];
            Map tempMap = {
              'action': 'User $email studied this subscription.',
              'timeStamp': timeStamp
            };
            studyLog.add(tempMap);

            final result = await ref.doc(email).update({'log': log});
            final result2 = await ref2.doc(id).update({'studyLog': studyLog});
            Provider.of<QuizProvider>(context, listen: false).setLoading(false);
            Navigator.pop(context);
            Fluttertoast.showToast(
                msg: 'Finished!',
                backgroundColor: HomeStyles().accentColor,
                textColor: Colors.white);
          }
        });
      }
    });
  });
}
