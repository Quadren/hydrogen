import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

Future<void> createNewUser(
    String email, String name, String uid, String pfp) async {
  Firebase.initializeApp().whenComplete(() {
    CollectionReference ref = FirebaseFirestore.instance.collection('users');
    var box = Hive.box('userData');

    String _name = name;

    // write values to Hive, then write to Firebase

    ref.doc(email).get().then((value) {
      try {
        box.put('name', _name);
        box.put('premium', false);
        box.put('email', email);
        box.put('pfp', pfp);

        ref.doc(email).set({
          'name': _name,
          'email': email,
          'premium': false,
          'subscriptions': [],
          'sprints': [],
          'friends': [],
          'pfp': pfp,
          'requests': [],
          'log': [],
          'reqSent': [],
          'sprintReq': [],
        });
      } catch (e) {
        // on PlatformException
        Fluttertoast.showToast(
            msg: 'Failed to create new user',
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  });
}
