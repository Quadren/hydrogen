import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

// this funciton gets data from Firebase if the user exists, and then writes the data to Hive

Future<void> getInfoFromFirebase(String email) async {
  Firebase.initializeApp().whenComplete(() {
    CollectionReference ref = FirebaseFirestore.instance.collection('users');
    var box = Hive.box('userData');

    ref.doc(email).get().then((value) {
      if (value.exists) {
        String name = value.data()['name'];
        String uid = value.data()['uid'];
        bool premium = value.data()['premium'];
        
        box.put('name', name);
        box.put('uid', uid);
        box.put('premium', premium);
        box.put('email', email);
      } else {
        Fluttertoast.showToast(msg: 'Failed to get user info', backgroundColor: Colors.red[300], textColor: Colors.white);
      }
    });
  });
}