import 'package:flutter/cupertino.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/auth/services/googleSignIn.dart';

void signOutService(BuildContext context) {
  signOutGoogle().whenComplete(() {
    Hive.box('userData').clear();
    Hive.box('categories').clear();
    Phoenix.rebirth(context);
  });
}
