import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:hive/hive.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hydrogen/core/userServices/userProvider.dart';
import 'package:provider/provider.dart';

Future<void> getUserData(BuildContext context) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('users');
    String email = Hive.box('userData').get('email');
    Provider.of<UserProvider>(context, listen: false).setLoading(false);

    final result = await ref.doc(email).get().then((value) {
      if (value.exists) {
        Provider.of<UserProvider>(context, listen: false)
            .setPfp(value.data()['pfp']);
        Provider.of<UserProvider>(context, listen: false).setLoading(false);
      }
    });
  });
}
