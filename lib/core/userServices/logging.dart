import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/core/providers/loadingProvider.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

Future<void> logEntry(String action, int type, BuildContext context) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('users');
    String email = Hive.box('userData').get('email');

    try {
      final result = await ref.doc(email).get().then((value) {
        List log = value.data()['log'];
        DateTime now = DateTime.now();
        String formatted = DateFormat('yyyy-MM-dd - kk:mm').format(now);

        Map data = {'action': action, 'timeStamp': formatted};

        log.add(data);

        ref.doc(email).update({'log': log});
      });

      switch (type) {
        case 1:
          print("Logging sprint");
          Provider.of<LoadingProvider>(context, listen: false)
              .setSprintLogLoading(false);
          Navigator.pop(context);
          break;
        case 2:
          print("Logging _____");
          break;
      }
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.toString(),
          textColor: Colors.white,
          backgroundColor: Colors.red[300]);
    }
  });
}
