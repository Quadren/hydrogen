import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:provider/provider.dart';

void getUserCategories(context) {
  var box = Hive.box('categories');

  List categories = box.get('userCategories');

  if (categories != null) {
    categories.forEach((element) {
      int color = element['color'];
      Provider.of<CategoriesProvider>(context, listen: false)
          .createCategory(element['label'], element['emoji'], Color(color));
    });
  }
}
