import 'package:flutter/cupertino.dart';

class UserProvider extends ChangeNotifier {
  String _pfp;
  String _name;
  bool _loading = true;

  String get getPfp => _pfp;
  bool get getLoading => _loading;
  String get getName => _name;

  void setPfp(String url) {
    _pfp = url;
    notifyListeners();
  }

  void setName(String name) {
    _name = name;
    notifyListeners();
  }

  void setLoading(bool value) {
    _loading = value;
    notifyListeners();
  }

  // setup stuff
  bool _new = false;

  bool get getNew => _new;

  void setNew(bool value) {
    _new = value;
    notifyListeners();
  }
}
