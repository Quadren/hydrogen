import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/core/providers/loadingProvider.dart';
import 'package:hydrogen/core/providers/popupProvider.dart';
import 'package:hydrogen/tritium/purchaseComplete.dart';
import 'package:hydrogen/tritium/purchasePage.dart';
import 'package:hydrogen/tritium/tritiumProvider.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:purchases_flutter/purchases_flutter.dart';

class RevenueCatServices {
  Future<void> revenueCatFetch(BuildContext context) async {
    Provider.of<LoadingProvider>(context, listen: false).setPricesLoading(true);
    final result = await initPlatformState();
    final result2 = await fetchOfferings(context);
    Provider.of<LoadingProvider>(context, listen: false)
        .setPricesLoading(false);
    Navigator.pop(context);
    Navigator.push(
        context,
        PageTransition(
            child: PurchasePage(),
            type: PageTransitionType.rightToLeft,
            curve: Curves.easeInOutCubic,
            duration: Duration(milliseconds: 500)));
  }

  Future<void> popupCatFetch(BuildContext context) async {
    Provider.of<LoadingProvider>(context, listen: false).setPricesLoading(true);
    final result = await initPlatformState();
    final result2 = await fetchOfferings(context);
    Provider.of<LoadingProvider>(context, listen: false)
        .setPricesLoading(false);
    Provider.of<PopupProvider>(context, listen: false).setShow(false);
    Navigator.push(
        context,
        PageTransition(
            child: PurchasePage(),
            type: PageTransitionType.rightToLeft,
            curve: Curves.easeInOutCubic,
            duration: Duration(milliseconds: 500)));
  }

  Future<void> initPlatformState() async {
    await Purchases.setDebugLogsEnabled(false);
    await Purchases.setup("QlgqfgaWleBEqJNsXuYcyxtqjnVdWvFy");
  }

  Future<void> fetchOfferings(BuildContext context) async {
    try {
      Offerings offerings = await Purchases.getOfferings();
      if (offerings.current != null &&
          offerings.current.availablePackages.isNotEmpty) {
        print("Fetched offerings");
        Product monthly = offerings.current.monthly.product;
        Product yearly = offerings.current.annual.product;
        print('${monthly.price}, ${yearly.price}');
        Provider.of<TritiumProvider>(context, listen: false)
            .setOfferings(monthly, yearly, null);
      }
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.toString(),
          backgroundColor: Colors.red[300],
          textColor: Colors.white);
    }
  }

  Future<void> makePurchase(String product, BuildContext context) async {
    try {
      PurchaserInfo purchaserInfo = await Purchases.purchaseProduct(product);
      if (purchaserInfo.entitlements.all['Tritium'].isActive) {
        makePremium(context);
      }
    } catch (e) {
      Fluttertoast.showToast(
          msg: 'Failed to make purchase.',
          backgroundColor: Colors.red[300],
          textColor: Colors.white);
    }
  }

  Future<void> makePremium(BuildContext context) async {
    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref = FirebaseFirestore.instance.collection('users');
      String email = Hive.box('userData').get('email');

      try {
        final result = await ref.doc(email).update({'premium': true});
        Navigator.pushAndRemoveUntil(
            context,
            PageTransition(
                child: PurchaseComplete(),
                type: PageTransitionType.rightToLeft,
                curve: Curves.easeInOutCubic,
                duration: Duration(milliseconds: 500)),
            (route) => false);
      } catch (e) {
        Fluttertoast.showToast(
            msg: e.toString(),
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  }
}
