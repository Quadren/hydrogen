import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:hydrogen/core/premium/revenueCatServices.dart';
import 'package:hydrogen/home/home_styles.dart';

class FreeLimit extends StatefulWidget {
  @override
  _FreeLimitState createState() => _FreeLimitState();
}

class _FreeLimitState extends State<FreeLimit> {
  bool _loading = false;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return Stack(
      children: [
        Align(
          alignment: Alignment.topCenter,
          child: Padding(
              padding: EdgeInsets.only(top: height * 0.03),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Free limit reached',
                    style: HomeStyles().headerText,
                  ),
                  SizedBox(height: height * 0.02),
                  Text('upgrade to Tritium to continue',
                      style: HomeStyles().subtitleText),
                ],
              )),
        ),
        Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
                padding: EdgeInsets.only(bottom: height * 0.03),
                child: BouncingWidget(
                  child: Container(
                    height: height * 0.1,
                    width: width * 0.9,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(18),
                        gradient: LinearGradient(
                            colors: [
                              HomeStyles().tertiaryAccent,
                              HomeStyles().paletteColors[2]
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight)),
                    child: Center(
                      child: _loading
                          ? CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation(
                                  HomeStyles().backgroundColor),
                              backgroundColor: Colors.transparent,
                            )
                          : Text(
                              'Get Tritium',
                              style: HomeStyles().buttonText3,
                            ),
                    ),
                  ),
                  onPressed: () {
                    if (!_loading) {
                      setState(() {
                        _loading = true;
                      });
                      RevenueCatServices().revenueCatFetch(context);
                    }
                  },
                  duration: Duration(milliseconds: 150),
                ))),
      ],
    );
  }
}
