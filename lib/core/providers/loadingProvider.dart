import 'package:flutter/material.dart';

class LoadingProvider extends ChangeNotifier {
  bool _sprintLogLoading = false;
  bool _pricesLoading = false;

  bool get getSprintLogLoading => _sprintLogLoading;
  bool get getPricesLoading => _pricesLoading;

  void setSprintLogLoading(bool value) {
    _sprintLogLoading = value;
    notifyListeners();
  }

  void setPricesLoading(bool value) {
    _pricesLoading = value;
    notifyListeners();
  }
}
