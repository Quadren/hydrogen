import 'package:flutter/material.dart';

class PopupProvider extends ChangeNotifier {
  bool _show = false;
  int _type = 0;

  /*
  Types are:
  0 -> Instagram
  1 -> Rate
  2 -> Tritium
  */

  bool get show => _show;
  int get getType => _type;

  void setShow(bool value) {
    _show = value;
    notifyListeners();
  }

  void setType(int type) {
    _type = type;
    notifyListeners();
  }
}
