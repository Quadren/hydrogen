import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get_version/get_version.dart';
import 'package:hydrogen/auth/services/checkIfSignedIn.dart';
import 'package:hydrogen/core/misc/updatePage.dart';
import 'package:in_app_update/in_app_update.dart';
import 'package:page_transition/page_transition.dart';

class UpdateServices {
  void checkPlatform(BuildContext context) {
    checkForUpdateUniversal(context);
  }

  void bypassDebug(BuildContext context) {
    checkIfSignedIn(context);
  }

  Future<void> checkForUpdateAndroid(BuildContext context) async {
    AppUpdateInfo updateInfo;
    InAppUpdate.checkForUpdate().then((info) {
      updateInfo = info;

      if (updateInfo.updateAvailable) {
        InAppUpdate.performImmediateUpdate().catchError((e) =>
            Fluttertoast.showToast(
                msg: e.toString(),
                backgroundColor: Colors.red[300],
                textColor: Colors.white));
      } else {
        print("No updates available");
        checkIfSignedIn(context);
      }
    }).catchError((e) => Fluttertoast.showToast(
        msg: e.toString(),
        backgroundColor: Colors.red[300],
        textColor: Colors.white));
  }

  Future<void> checkForUpdateUniversal(BuildContext context) async {
    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref =
          FirebaseFirestore.instance.collection('appMetadata');

      String version;

      try {
        version = await GetVersion.projectVersion;

        final result = await ref.doc('metaData').get().then((value) async {
          String currentVersion = value.data()['versionName'];

          if (version != currentVersion) {
            print("Out of date");
            Navigator.pushAndRemoveUntil(
                context,
                PageTransition(
                    child: UpdatePage(version, currentVersion),
                    type: PageTransitionType.rightToLeft,
                    curve: Curves.easeInOutCubic,
                    duration: Duration(milliseconds: 500)),
                (route) => false);
          } else {
            checkIfSignedIn(context);
          }
        });
      } catch (e) {
        Fluttertoast.showToast(
            msg: 'Failed to fetch platform info. Try restarting Hydrogen.',
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  }
}
