import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'dart:io' show Platform;
import 'package:url_launcher/url_launcher.dart';

class UpdatePage extends StatefulWidget {
  final String _newUpdate;
  final String _currentUpdate;

  UpdatePage(this._currentUpdate, this._newUpdate);
  @override
  _UpdatePageState createState() =>
      _UpdatePageState(_currentUpdate, _newUpdate);
}

class _UpdatePageState extends State<UpdatePage> {
  final String _newUpdate;
  final String _currentUpdate;

  _UpdatePageState(this._currentUpdate, this._newUpdate);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              '😶',
              style: TextStyle(fontSize: 72),
            ),
            SizedBox(height: height * 0.05),
            Text(
              'Hydrogen needs\nan update',
              style: HomeStyles().bigText.copyWith(fontSize: 24),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: height * 0.02),
            Text(
              'installed: $_currentUpdate // latest: $_newUpdate',
              style: HomeStyles().subtitleText,
              textAlign: TextAlign.center,
            ),
            SizedBox(height: height * 0.2),
            BouncingWidget(
                child: Container(
                  height: height * 0.1,
                  width: width * 0.6,
                  decoration: BoxDecoration(
                    color: HomeStyles().accentColor,
                    borderRadius: BorderRadius.circular(21),
                  ),
                  child: Center(
                    child: Text('Update now', style: HomeStyles().buttonText3),
                  ),
                ),
                onPressed: () async {
                  if (Platform.isAndroid) {
                    if (await canLaunch(
                        'https://play.google.com/store/apps/details?id=com.quadren.hydrogen')) {
                      Fluttertoast.showToast(
                          msg: "Tap 'update' to start updating Hydrogen.",
                          backgroundColor: HomeStyles().accentColor,
                          textColor: Colors.white);
                      await launch(
                          'https://play.google.com/store/apps/details?id=com.quadren.hydrogen');
                    } else {
                      Fluttertoast.showToast(
                          msg: 'Failed to launch Play Store.',
                          backgroundColor: Colors.red[300],
                          textColor: Colors.white);
                    }
                  } else {
                    Fluttertoast.showToast(
                        msg: 'Unsupported platform detected. Failed to update.',
                        backgroundColor: Colors.red[300],
                        textColor: Colors.white);
                  }
                },
                duration: Duration(microseconds: 150)),
          ],
        ),
      ),
    );
  }
}
