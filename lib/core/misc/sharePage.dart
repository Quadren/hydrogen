import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:hydrogen/widgets/backBar.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';

class ShareSubPage extends StatefulWidget {
  final int _index;

  ShareSubPage(this._index);

  @override
  _ShareSubPageState createState() => _ShareSubPageState(_index);
}

class _ShareSubPageState extends State<ShareSubPage> {
  final int _index;

  _ShareSubPageState(this._index);

  bool _subID = false;
  bool _hydLink = true;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    Map subData =
        Provider.of<SubscriptionProvider>(context).getSubscriptions[_index];

    return Scaffold(
      backgroundColor: HomeStyles().backgroundColor,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: Backbar(),
      ),
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: width * 0.1),
            child: CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: height * 0.22),
                      Text(
                        'Share\nDeck',
                        style: HomeStyles().bigText,
                      ),
                      SizedBox(height: height * 0.05),
                      Divider(color: Color(0xffAAAAAA)),
                      SizedBox(height: height * 0.05),
                      Text(
                        'message composer',
                        style: HomeStyles().subtitleText,
                      ),
                      SizedBox(height: height * 0.05),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text('Link to Hydrogen',
                              style: HomeStyles().buttonText),
                          Switch(
                              activeColor: HomeStyles().accentColor,
                              value: _hydLink,
                              onChanged: (bool value) {
                                setState(() {
                                  _hydLink = value;
                                });
                              })
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: BouncingWidget(
              child: Container(
                margin: EdgeInsets.only(bottom: height * 0.02),
                height: height * 0.1,
                width: width * 0.8,
                decoration: BoxDecoration(
                  color: HomeStyles().accentColor,
                  borderRadius: BorderRadius.circular(21),
                ),
                child: Center(
                  child: Text(
                    'Share deck',
                    style: HomeStyles().buttonText3,
                  ),
                ),
              ),
              onPressed: () {
                if (!_hydLink && !_subID) {
                  String msg =
                      "Hey! You're invited to join deck '${subData['title']}' on Hydrogen. Use ID ${subData['id']} to join!";
                  print(msg);
                  Share.share(msg, subject: 'Share deck');
                } else if (_hydLink && !_subID) {
                  String msg =
                      "Hey! You're invited to join deck '${subData['title']}' on Hydrogen. Use ID ${subData['id']} to join.\n\nYou can download Hydrogen for free from here: https://play.google.com/store/apps/details?id=com.quadren.hydrogen";
                  print(msg);
                  Share.share(msg, subject: 'Share deck');
                }
              },
              duration: Duration(milliseconds: 150),
            ),
          ),
        ],
      ),
    );
  }
}
