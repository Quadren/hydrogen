import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:url_launcher/url_launcher.dart';

class Marketplace extends StatefulWidget {
  @override
  _MarketplaceState createState() => _MarketplaceState();
}

class _MarketplaceState extends State<Marketplace> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Container(
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: height * 0.1),
                Text(
                  'coming soon',
                  style: HomeStyles().subtitleText,
                )
              ],
            ),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: height * 0.1,
                  width: height * 0.1,
                  decoration: BoxDecoration(
                    color: HomeStyles().primaryColor,
                    borderRadius: BorderRadius.circular(21),
                  ),
                  child: Center(
                      child: FaIcon(
                    FontAwesomeIcons.shoppingBag,
                    color: HomeStyles().backgroundColor,
                    size: height * 0.035,
                  )),
                ),
                SizedBox(height: height * 0.03),
                Text('Marketplace',
                    style: HomeStyles().bigText.copyWith(fontSize: 28)),
                SizedBox(height: height * 0.02),
                Text(
                  'get curated flash cards\nfrom trusted vendors',
                  style: HomeStyles().subtitleText,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: height * 0.1),
                BouncingWidget(
                  child: Container(
                    height: height * 0.1,
                    width: width * 0.6,
                    decoration: BoxDecoration(
                        color: HomeStyles().primaryColor,
                        borderRadius: BorderRadius.circular(21)),
                    child: Center(
                      child: Text(
                        'follow us for updates',
                        style: HomeStyles().buttonText3,
                      ),
                    ),
                  ),
                  onPressed: () async {
                    if (await canLaunch('https://instagram.com/quadrendev')) {
                      await launch('https://instagram.com/quadrendev');
                    } else {
                      Fluttertoast.showToast(
                          msg: 'Failed to launch URL',
                          textColor: Colors.white,
                          backgroundColor: Colors.red[300]);
                    }
                  },
                  duration: Duration(milliseconds: 150),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
