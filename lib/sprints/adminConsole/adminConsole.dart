import 'dart:math';

import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/adminConsole/adminAddPortion.dart';
import 'package:hydrogen/sprints/adminConsole/pages/dismissSprint.dart';
import 'package:hydrogen/sprints/adminConsole/pages/inviteToSprint.dart';
import 'package:hydrogen/sprints/adminConsole/pages/modifyDates.dart';
import 'package:hydrogen/sprints/adminConsole/pages/modifyMetadata.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:provider/provider.dart';

class AdminConsoleSprints extends StatefulWidget {
  @override
  _AdminConsoleSprintsState createState() => _AdminConsoleSprintsState();
}

class _AdminConsoleSprintsState extends State<AdminConsoleSprints> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    List portions =
        Provider.of<SprintProvider>(context).getSprintData['portion'];

    return ScrollConfiguration(
        behavior: AntiScrollGlow(),
        child: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: height * 0.22),
                  Text('Admin\nConsole', style: HomeStyles().bigText),
                  SizedBox(height: height * 0.05),
                  Divider(color: HomeStyles().primaryColor),
                  SizedBox(height: height * 0.05),
                ],
              ),
            ),
            SliverToBoxAdapter(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'portions',
                    style: HomeStyles()
                        .subtitleText
                        .copyWith(color: HomeStyles().primaryColor),
                  ),
                  SizedBox(height: height * 0.02),
                ],
              ),
            ),
            SliverList(
                delegate: SliverChildBuilderDelegate((context, int index) {
              return Container(
                margin: EdgeInsets.symmetric(vertical: height * 0.01),
                child: Text(
                  '${index + 1}. ${portions[index]}',
                  style: HomeStyles().subtitleText.copyWith(
                      color: HomeStyles().primaryColor,
                      fontSize: 24,
                      fontWeight: FontWeight.w600),
                ),
              );
            }, childCount: portions.length)),
            SliverToBoxAdapter(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: height * 0.05),
                  BouncingWidget(
                    child: Container(
                      height: height * 0.05,
                      width: width * 0.25,
                      decoration: BoxDecoration(
                        color: HomeStyles().primaryColor,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Center(
                        child: FaIcon(
                          FontAwesomeIcons.plus,
                          color: HomeStyles().backgroundColor,
                          size: height * 0.015,
                        ),
                      ),
                    ),
                    onPressed: () {
                      showAdminAddPortion(context);
                    },
                    duration: Duration(milliseconds: 150),
                  ),
                  SizedBox(height: height * 0.05),
                  Divider(color: HomeStyles().primaryColor),
                  SizedBox(height: height * 0.05),
                  Text(
                    'actions',
                    style: HomeStyles()
                        .subtitleText
                        .copyWith(color: HomeStyles().primaryColor),
                  ),
                  SizedBox(height: height * 0.02),
                ],
              ),
            ),
            actions(),
            SliverToBoxAdapter(
              child: SizedBox(height: height * 0.05),
            ),
          ],
        ));
  }

  Widget actions() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    List<String> labels = ['Dates', 'Invite', 'Modify', 'Dismiss'];

    List<IconData> icons = [
      FontAwesomeIcons.calendar,
      FontAwesomeIcons.userPlus,
      FontAwesomeIcons.edit,
      FontAwesomeIcons.times
    ];

    List callbacks = [
      () {
        showModifyDates(context);
      },
      () {
        showInviteToSprint(context);
      },
      () {
        showModifyMetadata(context);
      },
      () {
        dismissSprintPopup(context);
      },
    ];

    return SliverGrid(
        delegate: SliverChildBuilderDelegate((context, int index) {
          return actionButton(
              labels[index], icons[index], callbacks[index], index);
        }, childCount: 4),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: width * 0.05,
            crossAxisSpacing: width * 0.05));
  }

  Widget actionButton(
      String title, IconData icon, VoidCallback callback, int index) {
    var size = MediaQuery.of(context).size;
    double height = size.height;

    return ClipOval(
        child: GestureDetector(
      onTap: callback,
      child: Container(
        height: height * 0.133,
        width: height * 0.133,
        child: Container(
          height: height * 0.133,
          width: height * 0.133,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
            border:
                Border.all(color: HomeStyles().paletteColors[index], width: 3),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              FaIcon(
                icon,
                size: height * 0.025,
                color: HomeStyles().primaryColor,
              ),
              SizedBox(height: height * 0.02),
              Text(
                title,
                style: HomeStyles().buttonText,
              )
            ],
          ),
        ),
      ),
    ));
  }
}
