import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hydrogen/sprints/adminConsole/pages/modifyMetadata.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:provider/provider.dart';

Future<void> updateMetadata(
    String title, String id, BuildContext context) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('sprints');

    try {
      final result = await ref.doc(id).get().then((value) {
        String oldTitle = value.data()['title'];
        print("Changing title from '$oldTitle' to '$title'");

        Map data =
            Provider.of<SprintProvider>(context, listen: false).getSprintData;

        List sprints =
            Provider.of<SprintProvider>(context, listen: false).getSprints;

        Map toBeUpdated;

        sprints.forEach((element) {
          Map sprintData = element;

          if (sprintData.containsValue(id)) {
            toBeUpdated = element;
          }
        });

        toBeUpdated.update('title', (value) => title);
        Provider.of<SprintProvider>(context, listen: false)
            .updateSprint(toBeUpdated);

        data.update('title', (value) => title);

        ref.doc(id).update({'title': title});
      });

      modifyMetadataController.nextPage(
          duration: Duration(milliseconds: 500), curve: Curves.easeInOutCubic);
      Future.delayed(Duration(seconds: 2), () {
        Navigator.pop(context);
      });
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.toString(),
          backgroundColor: Colors.red[300],
          textColor: Colors.white);
    }
  });
}
