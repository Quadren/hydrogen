import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hydrogen/sprints/adminConsole/pages/modifyDates.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:provider/provider.dart';

class ModifyDateServices {
  void showDateSelector(BuildContext context, double height) async {
    Map data =
        Provider.of<SprintProvider>(context, listen: false).getSprintData;

    Map dates = data['dates'];
    Timestamp startTime = dates['start'];
    Timestamp endTime = dates['end'];

    DateTime start =
        DateTime.fromMillisecondsSinceEpoch(startTime.millisecondsSinceEpoch);
    DateTime end =
        DateTime.fromMillisecondsSinceEpoch(endTime.millisecondsSinceEpoch);

    final DateTime picked = await showRoundedDatePicker(
        context: context,
        initialDate: end,
        firstDate: DateTime(2020),
        lastDate: DateTime(2025),
        borderRadius: 21,
        fontFamily: 'Raleway',
        theme: ThemeData(primarySwatch: Colors.purple),
        height: height * 0.55);

    if (picked != null) {
      if (picked.isAfter(start)) {
        Provider.of<SprintProvider>(context, listen: false).setNewEnd(picked);
      } else {
        Fluttertoast.showToast(
            msg: 'End cannot be before start!',
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    }
  }

  Future<void> modifyDate(
      BuildContext context, String id, DateTime newEnd) async {
    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref =
          FirebaseFirestore.instance.collection('sprints');

      try {
        final result = await ref.doc(id).get().then((value) {
          Map dates = value.data()['dates'];
          Timestamp start = dates['start'];

          Map newDates = {'start': start, 'end': newEnd};

          ref.doc(id).update({'dates': newDates});
        });

        modifyDatesController.nextPage(
            duration: Duration(milliseconds: 500),
            curve: Curves.easeInOutCubic);
        Future.delayed(Duration(seconds: 2), () {
          Phoenix.rebirth(context);
        });
      } catch (e) {
        Fluttertoast.showToast(
            msg: e.toString(),
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  }
}
