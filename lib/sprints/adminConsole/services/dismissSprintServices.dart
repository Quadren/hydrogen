import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:fluttertoast/fluttertoast.dart';

Future<void> dismissSprint(BuildContext context, String id) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('sprints');
    CollectionReference userRef =
        FirebaseFirestore.instance.collection('users');

    try {
      final result = await ref.doc(id).get().then((sSnap) {
        List members = sSnap.data()['members'];

        members.forEach((element) async {
          final result2 = await userRef.doc(element).get().then((value) {
            List sprints = value.data()['sprints'];
            sprints.remove(id);

            userRef.doc(element).update({'sprints': sprints});
          });
        });
      });
      final delResult = await ref.doc(id).delete();
      Fluttertoast.showToast(
          msg: 'Deleted sprint successfully.',
          backgroundColor: Colors.red[300],
          textColor: Colors.white);
      Phoenix.rebirth(context);
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.toString(),
          backgroundColor: Colors.red[300],
          textColor: Colors.white);
    }
  });
}
