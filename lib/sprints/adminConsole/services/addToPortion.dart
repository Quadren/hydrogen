import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hydrogen/sprints/adminConsole/adminAddPortion.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:provider/provider.dart';

Future<void> addToPortionAdminConsole(
    String id, String text, BuildContext context) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('sprints');

    try {
      print(id);
      final result = await ref.doc(id).get().then((value) {
        List portion = value.data()['portion'];
        portion.add(text);

        Map data =
            Provider.of<SprintProvider>(context, listen: false).getSprintData;
        List requiredSubs =
            Provider.of<SprintProvider>(context, listen: false).getRequired;

        List sprintPortionData = data['portion'];

        sprintPortionData.add(text);
        requiredSubs.add(text);

        data.update('portion', (value) => sprintPortionData);
        Provider.of<SprintProvider>(context, listen: false)
            .updateSprintData(data);
        Provider.of<SprintProvider>(context, listen: false)
            .setRequired(requiredSubs);

        ref.doc(id).update({'portion': portion});
      });

      adminPortionAddingController.nextPage(
          duration: Duration(milliseconds: 500), curve: Curves.easeInOutCubic);
      Future.delayed(Duration(seconds: 2), () {
        Navigator.pop(context);
      });
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.toString(),
          textColor: Colors.white,
          backgroundColor: Colors.red);
    }
  });
}
