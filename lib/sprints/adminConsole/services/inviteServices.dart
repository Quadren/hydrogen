import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/friends/friendsProvider.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/adminConsole/pages/inviteToSprint.dart';
import 'package:provider/provider.dart';

class InviteSprintServices {
  Future<void> loadFriends(BuildContext context) async {
    Provider.of<FriendsProvider>(context, listen: false).setLoading(true);
    Provider.of<FriendsProvider>(context, listen: false).resetFriends();

    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref = FirebaseFirestore.instance.collection('users');
      String email = Hive.box('userData').get('email');

      try {
        final result = await ref.doc(email).get().then((value) {
          List friends = value.data()['friends'];

          friends.forEach((element) {
            ref.doc(element.toString()).get().then((uVal) {
              String name = uVal.data()['name'];
              String pfp = uVal.data()['pfp'];

              Provider.of<FriendsProvider>(context, listen: false)
                  .addFriend(name, element.toString(), pfp);
            });
          });
        });

        Provider.of<FriendsProvider>(context, listen: false).setLoading(false);
      } catch (e) {
        Fluttertoast.showToast(
            msg: e.toString(),
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  }

  Future<void> inviteToSprint(
      List invitees, String id, BuildContext context) async {
    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref = FirebaseFirestore.instance.collection('users');
      CollectionReference ref2 =
          FirebaseFirestore.instance.collection('sprints');

      try {
        final result = await ref2.doc(id).get().then((sSnap) {
          List invited = sSnap.data()['invited'];

          invitees.forEach((element) async {
            if (invited.contains(element.toString())) {
              print("Existing invite, skipping!");
            } else {
              invited.add(element.toString());
              final result2 =
                  await ref.doc(element.toString()).get().then((uSnap) {
                List sprintReq = uSnap.data()['sprintReq'];
                sprintReq.add(id);
                ref.doc(element).update({'sprintReq': sprintReq});
              });
            }
          });

          ref2.doc(id).update({'invited': invited});
        });

        inviteSprintController.nextPage(
            duration: Duration(milliseconds: 500),
            curve: Curves.easeInOutCubic);
        Fluttertoast.showToast(
          msg: 'Sent requests!',
          backgroundColor: HomeStyles().accentColor,
          textColor: Colors.white,
        );
        Future.delayed(Duration(seconds: 2), () {
          Navigator.pop(context);
        });
      } catch (e) {
        Fluttertoast.showToast(
            msg: e.toString(),
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  }
}
