import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/adminConsole/services/updateMetadata.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';

void showModifyMetadata(BuildContext context) {
  showDialog(context: context, builder: (ctx) => ModifyMetadata());
}

class ModifyMetadata extends StatefulWidget {
  @override
  _ModifyMetadataState createState() => _ModifyMetadataState();
}

PageController modifyMetadataController = PageController(initialPage: 0);

class _ModifyMetadataState extends State<ModifyMetadata>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  TextEditingController _controller1 = TextEditingController();

  @override
  void initState() {
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Sprung.criticallyDamped);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return Center(
        child: Material(
      color: Colors.transparent,
      child: ScaleTransition(
        scale: scaleAnimation,
        child: Container(
          height: height * 0.4,
          width: width * 0.75,
          decoration: ShapeDecoration(
              color: HomeStyles().backgroundColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(21))),
          child: PageView(
            controller: modifyMetadataController,
            physics: NeverScrollableScrollPhysics(),
            children: [
              Container(
                padding: EdgeInsets.symmetric(
                    horizontal: height * 0.03, vertical: height * 0.03),
                child: pageOne(),
              ),
              Center(
                child: Transform.scale(
                  scale: 1.7,
                  child: CircularProgressIndicator(
                    strokeWidth: 7,
                    valueColor:
                        AlwaysStoppedAnimation(HomeStyles().accentColor),
                    backgroundColor: HomeStyles().secondaryAccent,
                  ),
                ),
              ),
              Center(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: MediaQuery.of(context).size.height * 0.2,
                  child: Lottie.asset('assets/lottie/check.json',
                      repeat: false, fit: BoxFit.contain),
                ),
              ),
            ],
          ),
        ),
      ),
    ));
  }

  Widget pageOne() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    String currentTitle =
        Provider.of<SprintProvider>(context).getSprintData['title'];
    String id = Provider.of<SprintProvider>(context).getSprintData['id'];

    OutlineInputBorder _border = OutlineInputBorder(
      borderRadius: BorderRadius.circular(21),
      borderSide: BorderSide(width: 1, color: HomeStyles().primaryColor),
    );

    OutlineInputBorder _errorBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(21),
      borderSide: BorderSide(width: 1, color: Colors.red[300]),
    );

    InputDecoration _formDecor = InputDecoration(
      border: _border,
      errorBorder: _errorBorder,
      disabledBorder: _border,
      enabledBorder: _border,
      focusedBorder: _border,
      contentPadding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.05,
          vertical: MediaQuery.of(context).size.height * 0.04),
      errorStyle: TextStyle(
        fontFamily: 'Raleway',
        fontSize: 12,
        color: Colors.red[300],
        fontWeight: FontWeight.w400,
      ),
    );

    return Stack(
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('modify title', style: HomeStyles().subtitleText),
            SizedBox(height: height * 0.03),
            TextFormField(
              maxLength: 32,
              controller: _controller1,
              decoration: _formDecor,
              cursorColor: HomeStyles().primaryColor,
              style: HomeStyles().headerText,
            ),
          ],
        ),
        Align(
            alignment: Alignment.bottomCenter,
            child: BouncingWidget(
              child: AnimatedContainer(
                duration: Duration(milliseconds: 300),
                curve: Curves.easeInOutCubic,
                height: height * 0.085,
                width: width * 0.75,
                decoration: BoxDecoration(
                  color: (_controller1.text.length > 3)
                      ? HomeStyles().accentColor
                      : Color(0xffAAAAAA),
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Center(
                  child: Text(
                    'Update',
                    style: HomeStyles().buttonText3,
                  ),
                ),
              ),
              onPressed: () {
                if (_controller1.text.length > 3 &&
                    _controller1.text != currentTitle) {
                  modifyMetadataController.nextPage(
                      duration: Duration(milliseconds: 500),
                      curve: Curves.easeInOutCubic);
                  updateMetadata(_controller1.text, id, context);
                }
              },
              duration: Duration(
                milliseconds: 150,
              ),
            )),
      ],
    );
  }
}
