import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/adminConsole/services/dismissSprintServices.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';

void dismissSprintPopup(BuildContext context) {
  showDialog(context: context, builder: (ctx) => DismissSprint());
}

class DismissSprint extends StatefulWidget {
  @override
  _DismissSprintState createState() => _DismissSprintState();
}

PageController dismissSprintController = PageController(initialPage: 0);

class _DismissSprintState extends State<DismissSprint>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  @override
  void initState() {
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Sprung.criticallyDamped);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
    super.initState();
  }

  String _text = '';

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    String id = Provider.of<SprintProvider>(context).getSprintData['id'];

    return Center(
        child: Material(
            color: Colors.transparent,
            child: ScaleTransition(
              scale: scaleAnimation,
              child: Container(
                height: height * 0.6,
                width: width * 0.75,
                decoration: ShapeDecoration(
                    color: HomeStyles().backgroundColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(21))),
                child: PageView(
                  controller: dismissSprintController,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    Stack(
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(height: height * 0.05),
                            Text(
                              'dismiss sprint',
                              style: HomeStyles().headerText,
                            ),
                            SizedBox(height: height * 0.02),
                            Text(
                              "type 'yes' to confirm",
                              style: HomeStyles().subtitleText,
                            ),
                            SizedBox(height: height * 0.05),
                            Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: width * 0.2),
                                child: PinCodeTextField(
                                  appContext: context,
                                  length: 3,
                                  onChanged: null,
                                  cursorColor: Colors.transparent,
                                  animationType: AnimationType.slide,
                                  enablePinAutofill: false,
                                  keyboardType: TextInputType.text,
                                  enableActiveFill: false,
                                  textStyle: TextStyle(
                                    fontFamily: 'Raleway',
                                    fontSize: 24,
                                    color: HomeStyles().primaryColor,
                                    fontWeight: FontWeight.w600,
                                  ),
                                  onCompleted: (value) {
                                    setState(() {
                                      _text = value;
                                    });
                                  },
                                  pinTheme: PinTheme(
                                      shape: PinCodeFieldShape.underline,
                                      borderRadius: null,
                                      activeColor: Color(0xff6953F5),
                                      disabledColor: HomeStyles().primaryColor,
                                      selectedColor: HomeStyles().primaryColor,
                                      inactiveColor: HomeStyles()
                                          .primaryColor
                                          .withOpacity(0.3)),
                                )),
                          ],
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Padding(
                              padding: EdgeInsets.only(
                                  bottom: height * 0.02,
                                  left: height * 0.02,
                                  right: height * 0.02),
                              child: BouncingWidget(
                                child: AnimatedContainer(
                                  duration: Duration(milliseconds: 300),
                                  height: height * 0.1,
                                  width: width * 0.75,
                                  decoration: BoxDecoration(
                                    color: (_text.toLowerCase() == 'yes')
                                        ? HomeStyles().accentColor
                                        : Color(0xffAAAAAA),
                                    borderRadius: BorderRadius.circular(21),
                                  ),
                                  child: Center(
                                    child: Text(
                                      'Erase sprint',
                                      style: HomeStyles().buttonText3,
                                    ),
                                  ),
                                ),
                                onPressed: () {
                                  if (_text.toLowerCase() != 'yes') {
                                    Fluttertoast.showToast(
                                        msg: "Type in 'yes' first!",
                                        backgroundColor: Colors.red[300],
                                        textColor: Colors.white);
                                  } else {
                                    dismissSprintController.nextPage(
                                        duration: Duration(milliseconds: 500),
                                        curve: Curves.easeInOutCubic);
                                    dismissSprint(context, id);
                                  }
                                },
                                duration: Duration(milliseconds: 150),
                              )),
                        ),
                      ],
                    ),
                    Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Transform.scale(
                            scale: 1.7,
                            child: CircularProgressIndicator(
                              strokeWidth: 7,
                              valueColor: AlwaysStoppedAnimation(
                                  HomeStyles().accentColor),
                              backgroundColor: HomeStyles().secondaryAccent,
                            ),
                          ),
                          SizedBox(height: height * 0.1),
                          Container(
                            width: width * 0.5,
                            child: Text(
                              'obliterating with a giant laser...',
                              style: HomeStyles().subtitleText,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )));
  }
}
