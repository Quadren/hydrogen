import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/adminConsole/services/modifyDateServices.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';

void showModifyDates(BuildContext context) {
  showDialog(context: context, builder: (ctx) => ModifyDates());
}

class ModifyDates extends StatefulWidget {
  @override
  _ModifyDatesState createState() => _ModifyDatesState();
}

PageController modifyDatesController = PageController(initialPage: 0);

class _ModifyDatesState extends State<ModifyDates>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      Provider.of<SprintProvider>(context, listen: false)
          .setNewEnd(DateTime.now());
    });
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Sprung.criticallyDamped);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return Center(
        child: Material(
      color: Colors.transparent,
      child: ScaleTransition(
        scale: scaleAnimation,
        child: Container(
          height: height * 0.4,
          width: width * 0.75,
          decoration: ShapeDecoration(
              color: HomeStyles().backgroundColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(21))),
          child: PageView(
            controller: modifyDatesController,
            physics: NeverScrollableScrollPhysics(),
            children: [
              Container(
                padding: EdgeInsets.symmetric(
                    horizontal: height * 0.03, vertical: height * 0.03),
                child: actions(height, width),
              ),
              Center(
                child: Transform.scale(
                  scale: 1.7,
                  child: CircularProgressIndicator(
                    strokeWidth: 7,
                    valueColor:
                        AlwaysStoppedAnimation(HomeStyles().accentColor),
                    backgroundColor: HomeStyles().secondaryAccent,
                  ),
                ),
              ),
              Center(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: MediaQuery.of(context).size.height * 0.2,
                  child: Lottie.asset('assets/lottie/check.json',
                      repeat: false, fit: BoxFit.contain),
                ),
              ),
            ],
          ),
        ),
      ),
    ));
  }

  Widget actions(double height, double width) {
    Map dates = Provider.of<SprintProvider>(context, listen: false)
        .getSprintData['dates'];

    Timestamp start = dates['start'];
    Timestamp end = dates['end'];
    DateTime newEnd = Provider.of<SprintProvider>(context).getNewEnd;

    var startDateTime =
        DateTime.fromMillisecondsSinceEpoch(start.millisecondsSinceEpoch);
    var endDateTime =
        DateTime.fromMillisecondsSinceEpoch(end.millisecondsSinceEpoch);

    String startFormatted = DateFormat('dd-MM-yyyy').format(startDateTime);
    String endFormatted = DateFormat('dd-MM-yyyy').format(endDateTime);
    String newEndFormatted = DateFormat('dd-MM-yyyy').format(newEnd);

    String id = Provider.of<SprintProvider>(context).getSprintData['id'];

    return Stack(
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: height * 0.01),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text('start', style: HomeStyles().subtitleText),
                  Text(
                    startFormatted,
                    style: HomeStyles()
                        .subtitleText
                        .copyWith(color: HomeStyles().primaryColor),
                  )
                ],
              ),
              SizedBox(height: height * 0.02),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text('end', style: HomeStyles().subtitleText),
                  Text(
                    endFormatted,
                    style: HomeStyles()
                        .subtitleText
                        .copyWith(color: HomeStyles().primaryColor),
                  )
                ],
              ),
            ],
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: height * 0.01),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text('new end', style: HomeStyles().subtitleText),
                    Text(
                      newEndFormatted,
                      style: HomeStyles()
                          .subtitleText
                          .copyWith(color: HomeStyles().primaryColor),
                    )
                  ],
                ),
              ),
              SizedBox(height: height * 0.02),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  BouncingWidget(
                    onPressed: () {
                      ModifyDateServices().showDateSelector(context, height);
                    },
                    duration: Duration(milliseconds: 150),
                    child: Container(
                      height: height * 0.075,
                      width: width * 0.3,
                      decoration: BoxDecoration(
                        color: HomeStyles().accentColor,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Center(
                        child: FaIcon(
                          FontAwesomeIcons.calendar,
                          color: HomeStyles().backgroundColor,
                          size: height * 0.025,
                        ),
                      ),
                    ),
                  ),
                  BouncingWidget(
                    onPressed: () {
                      modifyDatesController.nextPage(
                          duration: Duration(milliseconds: 500),
                          curve: Curves.easeInOutCubic);
                      ModifyDateServices().modifyDate(context, id, newEnd);
                    },
                    duration: Duration(milliseconds: 150),
                    child: AnimatedContainer(
                      duration: Duration(milliseconds: 300),
                      curve: Curves.easeInOutCubic,
                      height: height * 0.075,
                      width: width * 0.3,
                      decoration: BoxDecoration(
                        color: HomeStyles().primaryColor,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Center(
                        child: FaIcon(
                          FontAwesomeIcons.check,
                          color: HomeStyles().backgroundColor,
                          size: height * 0.025,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
