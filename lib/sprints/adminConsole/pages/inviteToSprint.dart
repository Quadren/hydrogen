import 'dart:math';

import 'package:animated/animated.dart';
import 'package:auto_animated/auto_animated.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/friends/friendsProvider.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/adminConsole/services/inviteServices.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';

void showInviteToSprint(BuildContext context) {
  showDialog(context: context, builder: (ctx) => InviteToSprintPage());
}

class InviteToSprintPage extends StatefulWidget {
  @override
  _InviteToSprintPageState createState() => _InviteToSprintPageState();
}

PageController inviteSprintController = PageController(initialPage: 0);

class _InviteToSprintPageState extends State<InviteToSprintPage>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      InviteSprintServices().loadFriends(context);
    });
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Sprung.criticallyDamped);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
    super.initState();
  }

  ScrollController _scrollController = ScrollController();

  List selected = [];

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    List friends = Provider.of<FriendsProvider>(context).getFriends;
    bool loading = Provider.of<FriendsProvider>(context).getLoading;
    String id = Provider.of<SprintProvider>(context).getSprintData['id'];

    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
            height: height * 0.6,
            width: width * 0.75,
            decoration: ShapeDecoration(
                color: HomeStyles().backgroundColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(21))),
            child: AbsorbPointer(
              absorbing: loading,
              child: PageView(
                controller: inviteSprintController,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  Stack(
                    children: [
                      Center(
                        child: loading
                            ? Transform.scale(
                                scale: 1.7,
                                child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation(
                                      HomeStyles().accentColor),
                                  backgroundColor: HomeStyles().secondaryAccent,
                                  strokeWidth: 7,
                                ),
                              )
                            : null,
                      ),
                      CustomScrollView(
                        controller: _scrollController,
                        slivers: [
                          SliverToBoxAdapter(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(height: height * 0.05),
                                Text('tap to select',
                                    style: HomeStyles().subtitleText),
                                SizedBox(height: height * 0.03),
                              ],
                            ),
                          ),
                          LiveSliverList(
                            itemBuilder: (context, index, animation) {
                              return FadeTransition(
                                opacity: Tween<double>(begin: 0, end: 1)
                                    .animate(animation),
                                child: SlideTransition(
                                  position: Tween<Offset>(
                                          begin: Offset(0, 0.1),
                                          end: Offset(0, 0))
                                      .animate(animation),
                                  child: GestureDetector(
                                    onTap: () {
                                      if (selected
                                          .contains(friends[index]['email'])) {
                                        setState(() {
                                          selected
                                              .remove(friends[index]['email']);
                                        });
                                      } else {
                                        setState(() {
                                          selected.add(friends[index]['email']);
                                        });
                                      }
                                    },
                                    child: AnimatedContainer(
                                      duration: Duration(milliseconds: 300),
                                      curve: Curves.easeInOutCubic,
                                      margin: EdgeInsets.symmetric(
                                          horizontal: width * 0.05,
                                          vertical: height * 0.01),
                                      decoration: BoxDecoration(
                                        color: (selected.contains(
                                                friends[index]['email']))
                                            ? HomeStyles().accentColor
                                            : Color(0xffEEEEEE),
                                        borderRadius: BorderRadius.circular(21),
                                      ),
                                      padding: EdgeInsets.only(
                                          left: width * 0.05,
                                          top: height * 0.01,
                                          bottom: height * 0.01,
                                          right: height * 0.01),
                                      height: height * 0.085,
                                      width: width * 0.4,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            width: width * 0.25,
                                            child: Text(
                                              friends[index]['name'],
                                              style: (selected.contains(
                                                      friends[index]['email']))
                                                  ? HomeStyles().buttonText3
                                                  : HomeStyles().buttonText,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ),
                                          Container(
                                            height: height * 0.085,
                                            width: height * 0.085,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(15),
                                                color: HomeStyles()
                                                    .backgroundColor),
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(15),
                                              child: CachedNetworkImage(
                                                  imageUrl: friends[index]
                                                      ['pfp'],
                                                  fit: BoxFit.cover),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                            itemCount: friends.length,
                            controller: _scrollController,
                          )
                        ],
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: BouncingWidget(
                            child: AnimatedOpacity(
                              opacity: loading ? 0 : 1,
                              duration: Duration(milliseconds: 500),
                              child: AnimatedContainer(
                                duration: Duration(milliseconds: 300),
                                curve: Curves.easeInOutCubic,
                                margin: EdgeInsets.symmetric(
                                    vertical: width * 0.05,
                                    horizontal: width * 0.05),
                                width: width * 0.75,
                                height: height * 0.085,
                                decoration: BoxDecoration(
                                  boxShadow: (selected.isEmpty)
                                      ? null
                                      : [
                                          BoxShadow(
                                              offset: Offset(-4, 18),
                                              color: Colors.black
                                                  .withOpacity(0.05),
                                              blurRadius: 19,
                                              spreadRadius: -1),
                                        ],
                                  border: (selected.isEmpty)
                                      ? null
                                      : Border.all(
                                          width: 3,
                                          color: HomeStyles().tertiaryAccent),
                                  color: (selected.isEmpty)
                                      ? Color(0xffEEEEEE)
                                      : HomeStyles().backgroundColor,
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                padding: EdgeInsets.symmetric(
                                    horizontal: width * 0.085),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Send invites',
                                      style: HomeStyles().buttonText,
                                    ),
                                    FaIcon(
                                      FontAwesomeIcons.arrowRight,
                                      color: HomeStyles().primaryColor,
                                      size: height * 0.02,
                                    )
                                  ],
                                ),
                              ),
                            ),
                            onPressed: () {
                              if (selected.isNotEmpty) {
                                inviteSprintController.nextPage(
                                    duration: Duration(milliseconds: 500),
                                    curve: Curves.easeInOutCubic);
                                InviteSprintServices()
                                    .inviteToSprint(selected, id, context);
                              }
                            },
                            duration: Duration(milliseconds: 150)),
                      ),
                    ],
                  ),
                  Center(
                    child: Transform.scale(
                        scale: 1.7,
                        child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation(HomeStyles().accentColor),
                          backgroundColor: HomeStyles().secondaryAccent,
                          strokeWidth: 7,
                        )),
                  ),
                  Center(
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.2,
                      width: MediaQuery.of(context).size.height * 0.2,
                      child: Lottie.asset('assets/lottie/check.json',
                          repeat: false, fit: BoxFit.contain),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
