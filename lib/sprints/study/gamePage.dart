import 'dart:math';

import 'package:animated/animated.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/quiz/pages/expandedView.dart';
import 'package:hydrogen/quiz/quiz_provider.dart';
import 'package:hydrogen/subscriptions/addCards/catexPreview.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';

class GamePageSprint extends StatefulWidget {
  final Map _data;

  GamePageSprint(this._data);

  @override
  _GamePageSprintState createState() => _GamePageSprintState(_data);
}

class _GamePageSprintState extends State<GamePageSprint> {
  final Map _data;

  _GamePageSprintState(this._data);

  void randomCard() {
    List cards = _data['cards'];
    int number = Random().nextInt(cards.length);

    Provider.of<QuizProvider>(context, listen: false).setCard(
        cards[number]['front'], cards[number]['back'], cards[number]['eqn']);
  }

  GlobalKey<FlipCardState> _cardKey = GlobalKey<FlipCardState>();

  bool _animate = false;

  bool _animate2 = false;
  bool _animate3 = false;
  bool _animate4 = false;

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      Provider.of<QuizProvider>(context, listen: false).resetWrong();
      randomCard();
    });
    Future.delayed(Duration(milliseconds: 1200), () {
      setState(() {
        _animate = true;
      });
    });

    super.initState();
  }

  void nextCard() {
    _cardKey.currentState.toggleCard();
    setState(() {
      _animate = false;
    });
    Future.delayed(Duration(milliseconds: 150), () {
      randomCard();
      setState(() {
        _animate = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    String front = Provider.of<QuizProvider>(context).getFront;
    String back = Provider.of<QuizProvider>(context).getBack;
    String eqn = Provider.of<QuizProvider>(context).getEqn;

    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: height * 0.15),
              Text('In session', style: HomeStyles().headerText),
              SizedBox(height: height * 0.06),
            ],
          ),
        ),
        SliverToBoxAdapter(
            child: Animated(
          duration: Duration(milliseconds: 1500),
          curve: Sprung.criticallyDamped,
          value: _animate ? 1 : 0,
          builder: (context, child, animation) => Transform.translate(
            offset: Offset(0, (1 - animation.value) * 700),
            child: child,
          ),
          child: FlipCard(
            onFlip: () {
              if (_cardKey.currentState.isFront) {
                print("Showing back");
                setState(() {
                  _animate2 = true;
                });
                Future.delayed(Duration(milliseconds: 50), () {
                  setState(() {
                    _animate3 = true;
                  });
                });
                Future.delayed(Duration(milliseconds: 200), () {
                  setState(() {
                    _animate4 = true;
                  });
                });
              } else {
                print("Showing front");
                setState(() {
                  _animate2 = false;
                });
                Future.delayed(Duration(milliseconds: 50), () {
                  setState(() {
                    _animate3 = false;
                    _animate4 = false;
                  });
                });
              }
            },
            key: _cardKey,
            front: cardSide('front', front, eqn),
            back: cardSide('back', back, eqn),
          ),
        )),
        SliverToBoxAdapter(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: height * 0.03),
              Animated(
                value: _animate2 ? 1 : 0,
                builder: (context, child, animation) => Transform.translate(
                  offset: Offset(0, (1 - animation.value) * 20),
                  child: Opacity(
                    opacity: animation.value,
                    child: child,
                  ),
                ),
                child: Text(
                  'did you get it right?',
                  style: HomeStyles().buttonText,
                ),
              ),
              SizedBox(height: height * 0.05),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  animatedButton(_animate3, FontAwesomeIcons.check,
                      HomeStyles().accentColor, () {
                    nextCard();
                  }),
                  SizedBox(width: width * 0.05),
                  animatedButton(
                      _animate4, FontAwesomeIcons.times, Color(0xFFFF5558), () {
                    nextCard();
                  }),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget animatedButton(
      bool trigger, IconData icon, Color color, VoidCallback callback) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    return Animated(
      curve: Sprung.criticallyDamped,
      duration: Duration(milliseconds: 1500),
      value: trigger ? 1 : 0,
      builder: (context, child, animation) => Transform.translate(
        offset: Offset(0, (1 - animation.value) * 200),
        child: child,
      ),
      child: BouncingWidget(
          child: Container(
            height: height * 0.1,
            width: height * 0.1,
            decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.circular(21),
            ),
            child: Center(
              child: FaIcon(icon,
                  color: HomeStyles().backgroundColor, size: height * 0.02),
            ),
          ),
          duration: Duration(milliseconds: 150),
          onPressed: callback),
    );
  }

  Widget cardSide(String facing, String text, String eqn) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Container(
      margin: EdgeInsets.symmetric(horizontal: width * 0.175),
      padding: EdgeInsets.all(width * 0.05),
      height: height * 0.475,
      decoration: BoxDecoration(
        color: Color(0xFFEEEEEE),
        borderRadius: BorderRadius.circular(21),
      ),
      child: Stack(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: width * 0.05),
            child: Align(
              alignment: Alignment.center,
              child: Container(
                height: height * 0.3,
                child: Text(
                  text,
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontFamily: 'Raleway',
                    fontSize: (facing == 'front') ? 18 : 14,
                    fontWeight:
                        (facing == 'front') ? FontWeight.w600 : FontWeight.w400,
                    color: HomeStyles().primaryColor,
                  ),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 8,
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Text(
              facing,
              style: TextStyle(
                fontFamily: 'Raleway',
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Color(0xFFAAAAAAA),
              ),
              overflow: TextOverflow.ellipsis,
            ),
          ),
          Align(
              alignment: Alignment.topCenter,
              child: (facing == 'front')
                  ? null
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          child: (eqn == null || eqn == 'none')
                              ? null
                              : GestureDetector(
                                  onTap: () {
                                    showCatex(eqn, context);
                                  },
                                  child: Container(
                                    height: height * 0.05,
                                    width: height * 0.05,
                                    decoration: BoxDecoration(
                                      color: HomeStyles().backgroundColor,
                                      borderRadius: BorderRadius.circular(12),
                                    ),
                                    child: Center(
                                      child: FaIcon(
                                        FontAwesomeIcons.equals,
                                        color: HomeStyles().accentColor,
                                        size: height * 0.015,
                                      ),
                                    ),
                                  ),
                                ),
                        ),
                        SizedBox(width: width * 0.03),
                        Container(
                          child: (facing == 'front')
                              ? null
                              : GestureDetector(
                                  onTap: () {
                                    showExpanded(text, context);
                                  },
                                  child: Container(
                                    height: height * 0.05,
                                    width: height * 0.05,
                                    decoration: BoxDecoration(
                                      color: HomeStyles().backgroundColor,
                                      borderRadius: BorderRadius.circular(12),
                                    ),
                                    child: Center(
                                      child: FaIcon(
                                        FontAwesomeIcons.expand,
                                        color: HomeStyles().accentColor,
                                        size: height * 0.015,
                                      ),
                                    ),
                                  ),
                                ),
                        ),
                      ],
                    )),
        ],
      ),
    );
  }
}
