import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/core/providers/loadingProvider.dart';
import 'package:hydrogen/core/userServices/logging.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:hydrogen/sprints/study/gamePage.dart';
import 'package:provider/provider.dart';

class StudySub extends StatefulWidget {
  final Map _data;

  StudySub(this._data);

  @override
  _StudySubState createState() => _StudySubState(_data);
}

class _StudySubState extends State<StudySub> {
  final Map _data;

  _StudySubState(this._data);

  @override
  Widget build(BuildContext context) {
    Map _sprintData = Provider.of<SprintProvider>(context).getSprintData;
    bool _writing = Provider.of<LoadingProvider>(context).getSprintLogLoading;

    return Scaffold(
      backgroundColor: HomeStyles().backgroundColor,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: HomeStyles().primaryColor),
        leading: InkWell(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          onTap: () {
            if (!_writing) {
              Provider.of<LoadingProvider>(context, listen: false)
                  .setSprintLogLoading(true);
              logEntry(
                  "You studied subscription '${_data['title']}' from sprint '${_sprintData['title']}'.",
                  1,
                  context);
            }
          },
          child: Container(
            margin: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.05,
                top: MediaQuery.of(context).size.height * 0.03),
            decoration: BoxDecoration(
              color: HomeStyles().backgroundColor,
              borderRadius: BorderRadius.circular(100),
            ),
            child: Center(
                child: _writing
                    ? Transform.scale(
                        scale: 0.7,
                        child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation(HomeStyles().primaryColor),
                          backgroundColor: Colors.transparent,
                        ),
                      )
                    : FaIcon(
                        FontAwesomeIcons.stopCircle,
                        color: HomeStyles().primaryColor,
                        size: MediaQuery.of(context).size.height * 0.025,
                      )),
          ),
        ),
      ),
      body: GamePageSprint(_data),
    );
  }
}
