import 'package:flutter/material.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:lottie/lottie.dart';
import 'package:sprung/sprung.dart';

void showLoadingInvite(BuildContext context) {
  showDialog(
    context: context,
    builder: (ctx) => LoadingSprintInvite(),
  );
}

class LoadingSprintInvite extends StatefulWidget {
  @override
  _LoadingSprintInviteState createState() => _LoadingSprintInviteState();
}

PageController loadingSprintInviteController = PageController(initialPage: 0);

class _LoadingSprintInviteState extends State<LoadingSprintInvite>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  @override
  void initState() {
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Sprung.criticallyDamped);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
    super.initState();
  }

  @override
  void dispose() {
    this.controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
            height: height * 0.41,
            width: width * 0.75,
            decoration: ShapeDecoration(
                color: HomeStyles().backgroundColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(21))),
            child: PageView(
              controller: loadingSprintInviteController,
              physics: NeverScrollableScrollPhysics(),
              children: [
                Center(
                  child: Transform.scale(
                    scale: 1.7,
                    child: CircularProgressIndicator(
                      strokeWidth: 7,
                      valueColor:
                          AlwaysStoppedAnimation(HomeStyles().accentColor),
                      backgroundColor: HomeStyles().secondaryAccent,
                    ),
                  ),
                ),
                Center(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.height * 0.2,
                    child: Lottie.asset('assets/lottie/check.json',
                        repeat: false, fit: BoxFit.contain),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
