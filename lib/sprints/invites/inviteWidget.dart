import 'dart:math';

import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/invites/loadingSprintInvite.dart';
import 'package:hydrogen/sprints/services/sprintInviteServices.dart';

class SprintInviteWidget extends StatelessWidget {
  final String _id;
  final String _title;
  final int _members;
  final String _owner;

  SprintInviteWidget(this._title, this._id, this._members, this._owner);

  final int _colorIndex = Random().nextInt(HomeStyles().paletteColors.length);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Dismissible(
        onDismissed: (direction) {
          SprintInviteServices().declineSprintInvite(_id, context);
          showLoadingInvite(context);
        },
        background: Container(
          color: Colors.red[300],
          padding: EdgeInsets.symmetric(horizontal: width * 0.1),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: FaIcon(
                  FontAwesomeIcons.times,
                  color: HomeStyles().backgroundColor,
                  size: height * 0.025,
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: FaIcon(
                  FontAwesomeIcons.times,
                  color: HomeStyles().backgroundColor,
                  size: height * 0.025,
                ),
              ),
            ],
          ),
        ),
        key: UniqueKey(),
        child: Container(
          height: height * 0.1,
          margin: EdgeInsets.symmetric(
              horizontal: width * 0.1, vertical: height * 0.01),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(21),
            border: Border.all(
                width: 3, color: HomeStyles().paletteColors[_colorIndex]),
          ),
          padding: EdgeInsets.symmetric(horizontal: width * 0.05),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: width * 0.5,
                    child: Text(
                      _title,
                      style: HomeStyles().buttonText,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),
                  ),
                  SizedBox(height: height * 0.005),
                  Container(
                    width: width * 0.6,
                    child: Text('owned by $_owner',
                        style: HomeStyles().subtitleText.copyWith(fontSize: 12),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1),
                  ),
                ],
              ),
              BouncingWidget(
                child: Container(
                  height: height * 0.03,
                  width: height * 0.03,
                  child: Center(
                    child: FaIcon(
                      FontAwesomeIcons.check,
                      color: HomeStyles().primaryColor,
                      size: height * 0.02,
                    ),
                  ),
                ),
                onPressed: () {
                  SprintInviteServices().acceptSprintInvite(_id, context);
                  showLoadingInvite(context);
                },
                duration: Duration(milliseconds: 150),
              )
            ],
          ),
        ));
  }
}
