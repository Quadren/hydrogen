import 'package:auto_animated/auto_animated.dart';
import 'package:flutter/material.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/invites/inviteWidget.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:hydrogen/widgets/backBar.dart';
import 'package:provider/provider.dart';

class SprintInvites extends StatefulWidget {
  @override
  _SprintInvitesState createState() => _SprintInvitesState();
}

class _SprintInvitesState extends State<SprintInvites> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    List invites = Provider.of<SprintProvider>(context).getInvites;
    ScrollController _scrollController = ScrollController();

    return Scaffold(
      backgroundColor: HomeStyles().backgroundColor,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: Backbar(),
      ),
      extendBodyBehindAppBar: true,
      body: ScrollConfiguration(
        behavior: AntiScrollGlow(),
        child: CustomScrollView(
          controller: _scrollController,
          slivers: [
            SliverToBoxAdapter(
                child: Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.1),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: height * 0.15),
                  Text('Sprint\nInvites', style: HomeStyles().bigText),
                  SizedBox(
                    height: height * 0.01,
                  ),
                  Text(
                    'swipe away to decline',
                    style: HomeStyles().subtitleText,
                  ),
                  SizedBox(height: height * 0.05),
                ],
              ),
            )),
            LiveSliverList(
                itemBuilder: (context, int index, Animation<double> animation) {
                  return FadeTransition(
                    opacity: Tween<double>(begin: 0, end: 1).animate(animation),
                    child: SlideTransition(
                        position: Tween<Offset>(
                                begin: Offset(0, 0.1), end: Offset(0, 0))
                            .animate(animation),
                        child: SprintInviteWidget(
                            invites[index]['title'],
                            invites[index]['id'],
                            invites[index]['members'],
                            invites[index]['owner'])),
                  );
                },
                itemCount: invites.length,
                controller: _scrollController)
          ],
        ),
      ),
    );
  }
}
