import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/core/misc/shakeAnim.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/importSprint/importSprintService.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:hydrogen/widgets/backBar.dart';
import 'package:lottie/lottie.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';

class IDInputPage extends StatefulWidget {
  @override
  _IDInputPageState createState() => _IDInputPageState();
}

ShakeController idInputShake;
PageController importSprintController = PageController(initialPage: 0);

class _IDInputPageState extends State<IDInputPage>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      Provider.of<SprintProvider>(context, listen: false).setDisable(false);
    });
    idInputShake = ShakeController(vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    bool disable = Provider.of<SprintProvider>(context).getDisable;

    return Scaffold(
      backgroundColor: HomeStyles().backgroundColor,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: Backbar(),
      ),
      extendBodyBehindAppBar: true,
      body: ScrollConfiguration(
          behavior: AntiScrollGlow(),
          child: PageView(
            controller: importSprintController,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                child: CustomScrollView(
                  slivers: [
                    SliverToBoxAdapter(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: height * 0.22),
                          Text('Enter\nSprint ID', style: HomeStyles().bigText),
                          SizedBox(height: height * 0.01),
                          Text(
                            "exclude suffix 'qsprint.'",
                            style: HomeStyles().subtitleText,
                          ),
                          SizedBox(height: height * 0.1),
                          AbsorbPointer(
                              absorbing: disable,
                              child: AnimatedOpacity(
                                opacity: disable ? 0.5 : 1,
                                duration: Duration(milliseconds: 150),
                                child: ShakeView(
                                    child: Form(
                                      child: PinCodeTextField(
                                        appContext: context,
                                        length: 5,
                                        onChanged: null,
                                        cursorColor: Colors.transparent,
                                        animationType: AnimationType.slide,
                                        enablePinAutofill: false,
                                        keyboardType: TextInputType.text,
                                        enableActiveFill: false,
                                        textStyle: TextStyle(
                                          fontFamily: 'Raleway',
                                          fontSize: 24,
                                          color: HomeStyles().primaryColor,
                                          fontWeight: FontWeight.w600,
                                        ),
                                        onCompleted: (value) {
                                          Provider.of<SprintProvider>(context,
                                                  listen: false)
                                              .setDisable(true);
                                          String id = 'qsprnt.' + value;
                                          ImportSprintService()
                                              .importSprint(context, id);
                                        },
                                        pinTheme: PinTheme(
                                          shape: PinCodeFieldShape.underline,
                                          borderRadius: null,
                                          activeColor:
                                              HomeStyles().tertiaryAccent,
                                          disabledColor:
                                              HomeStyles().primaryColor,
                                          selectedColor:
                                              HomeStyles().primaryColor,
                                          inactiveColor: HomeStyles()
                                              .primaryColor
                                              .withOpacity(0.3),
                                        ),
                                      ),
                                    ),
                                    controller: idInputShake),
                              )),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Center(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: MediaQuery.of(context).size.height * 0.2,
                  child: Lottie.asset('assets/lottie/check.json',
                      repeat: false, fit: BoxFit.contain),
                ),
              ),
            ],
          )),
    );
  }
}
