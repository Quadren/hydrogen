import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/sprints/importSprint/idInputPage.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class ImportSprintService {
  Future<void> importSprint(BuildContext context, String id) async {
    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref = FirebaseFirestore.instance.collection('users');
      CollectionReference ref2 =
          FirebaseFirestore.instance.collection('sprints');

      String email = Hive.box('userData').get('email');

      bool exists;

      try {
        final result = await ref2.doc(id).get().then((value) {
          if (value.exists) {
            exists = true;
            List members = value.data()['members'];
            String title = value.data()['title'];
            List portion = value.data()['portion'];
            List submitted = value.data()['submitted'];
            List subscriptions = value.data()['subscriptions'];

            if (members.contains(email)) {
              exists = false;
              idInputShake.shake();
              Provider.of<SprintProvider>(context, listen: false)
                  .setDisable(false);
              Fluttertoast.showToast(
                  msg: "You're already a part of this sprint!",
                  backgroundColor: Colors.red[300],
                  textColor: Colors.white);
            } else {
              double progress =
                  ((submitted.length / portion.length) * 100).clamp(0, 100);
              members.add(email);
              Provider.of<SprintProvider>(context, listen: false)
                  .addSprint(title, progress, subscriptions, id);

              ref2.doc(id).update({'members': members});
            }
          } else {
            exists = false;
            idInputShake.shake();
            Provider.of<SprintProvider>(context, listen: false)
                .setDisable(false);
          }
        });

        if (exists) {
          final result2 = await ref.doc(email).get().then((val2) {
            List sprints = val2.data()['sprints'];
            sprints.add(id);
            List log = val2.data()['log'];
            DateTime now = DateTime.now();

            String formatted = DateFormat('yyyy-MM-dd - kk:mm').format(now);

            Map logEntry = {
              'action': 'You joined sprint with id $id.',
              'timeStamp': formatted
            };

            log.add(logEntry);

            ref.doc(email).update({'log': log, 'sprints': sprints});

            Provider.of<SprintProvider>(context, listen: false)
                .setDisable(false);
            importSprintController.nextPage(
                duration: Duration(milliseconds: 500),
                curve: Curves.easeInOutCubic);
            Future.delayed(Duration(seconds: 2), () {
              Navigator.pop(context);
            });
          });
        }
      } catch (e) {
        Fluttertoast.showToast(
            msg: e.toString(),
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  }
}
