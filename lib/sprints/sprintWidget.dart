import 'package:flutter/material.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/sprintPage/sprintPage.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class SprintWidget extends StatefulWidget {
  final int _index;

  SprintWidget(this._index);

  @override
  _SprintWidgetState createState() => _SprintWidgetState(_index);
}

class _SprintWidgetState extends State<SprintWidget> {
  final int _index;

  _SprintWidgetState(this._index);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    Map data = Provider.of<SprintProvider>(context).getSprints[_index];

    String title = data['title'];

    return Container(
      margin: (_index % 2 == 0)
          ? EdgeInsets.only(
              left: width * 0.1,
              right: width * 0.03,
              top: width * 0.03,
              bottom: width * 0.03)
          : EdgeInsets.only(
              left: width * 0.03,
              right: width * 0.1,
              top: width * 0.03,
              bottom: width * 0.03),
      decoration: BoxDecoration(
        color: (_index == 0) ? HomeStyles().accentColor : Color(0xffEEEEEE),
        borderRadius: BorderRadius.circular(21),
      ),
      padding: EdgeInsets.all(width * 0.05),
      child: GestureDetector(
        child: Container(
          width: width * 0.4,
          height: height * 0.25,
          color: Colors.white.withOpacity(0),
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  width: width * 0.3,
                  child: Text(
                    title,
                    style: (_index == 0)
                        ? HomeStyles().buttonText3
                        : HomeStyles().buttonText,
                    overflow: TextOverflow.fade,
                    maxLines: 2,
                  ),
                ),
              ),
            ],
          ),
        ),
        onTap: () {
          Navigator.push(
              context,
              PageTransition(
                  child: SprintPage(data),
                  type: PageTransitionType.rightToLeft,
                  curve: Curves.easeInOutCubic,
                  duration: Duration(milliseconds: 500)));
        },
      ),
    );
  }
}
