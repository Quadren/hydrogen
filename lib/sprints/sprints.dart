import 'package:auto_animated/auto_animated.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/createSprints/createSprints.dart';
import 'package:hydrogen/sprints/importSprint/importSprintPopup.dart';
import 'package:hydrogen/sprints/importSprint/new/importSprintSheet.dart';
import 'package:hydrogen/sprints/intro/introToSprints.dart';
import 'package:hydrogen/sprints/invites/sprintInvites.dart';
import 'package:hydrogen/sprints/services/getAllSprints.dart';
import 'package:hydrogen/sprints/sprintWidget.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class SprintsPage extends StatefulWidget {
  @override
  _SprintsPageState createState() => _SprintsPageState();
}

class _SprintsPageState extends State<SprintsPage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  void checkIfFirstTime() {
    var box = Hive.box('userData');
    bool isFirst = box.get('isFirst');

    if (isFirst == null || isFirst == true) {
      print("First time");
      Provider.of<SprintProvider>(context, listen: false).setFirst(true);
    } else {
      print("Not first time");
      Provider.of<SprintProvider>(context, listen: false).setFirst(false);
      getAllSprints(context);
    }
  }

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      checkIfFirstTime();
    });
    super.initState();
  }

  ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    super.build(context);
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    bool isFirst = Provider.of<SprintProvider>(context).getFirst;
    bool isLoading = Provider.of<SprintProvider>(context).getLoading;

    List sprints = Provider.of<SprintProvider>(context).getSprints;
    List invites = Provider.of<SprintProvider>(context).getInvites;

    return Container(
      child: isFirst
          ? IntroToSprints()
          : ScrollConfiguration(
              behavior: AntiScrollGlow(),
              child: CustomScrollView(
                controller: _scrollController,
                slivers: [
                  SliverToBoxAdapter(
                      child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: height * 0.15),
                        BouncingWidget(
                          child: Text(
                            'Study\nSprints +',
                            style: HomeStyles().bigText,
                          ),
                          onPressed: () {
                            showImportSprintSheet(context);
                          },
                          duration: Duration(milliseconds: 150),
                        ),
                      ],
                    ),
                  )),
                  SliverToBoxAdapter(
                      child: BouncingWidget(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(height: height * 0.03),
                        Container(
                          height: height * 0.075,
                          width: width * 0.8,
                          decoration: BoxDecoration(
                            color: Color(0xffEEEEEE),
                            borderRadius: BorderRadius.circular(21),
                          ),
                          padding:
                              EdgeInsets.symmetric(horizontal: width * 0.075),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                'Requests',
                                style: TextStyle(
                                  fontFamily: 'Raleway',
                                  color: HomeStyles().primaryColor,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              Container(
                                  child: isLoading
                                      ? Transform.scale(
                                          scale: 0.7,
                                          child: CircularProgressIndicator(
                                            valueColor: AlwaysStoppedAnimation(
                                                HomeStyles().primaryColor),
                                            backgroundColor: Colors.transparent,
                                          ),
                                        )
                                      : Text(
                                          '${invites.length}',
                                          style: HomeStyles()
                                              .buttonText
                                              .copyWith(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w600),
                                        )),
                            ],
                          ),
                        ),
                        SizedBox(height: height * 0.05),
                      ],
                    ),
                    onPressed: () {
                      if (!isLoading) {
                        Navigator.push(
                            context,
                            PageTransition(
                                child: SprintInvites(),
                                type: PageTransitionType.rightToLeft,
                                curve: Curves.easeInOutCubic,
                                duration: Duration(milliseconds: 500)));
                      }
                    },
                    duration: Duration(milliseconds: 150),
                  )),
                  Container(
                    child: isLoading
                        ? SliverToBoxAdapter(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(height: height * 0.1),
                                Transform.scale(
                                  scale: 1.5,
                                  child: CircularProgressIndicator(
                                    strokeWidth: 7,
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Color(0xff6953F5)),
                                    backgroundColor: Color(0xFFFFDDE1),
                                  ),
                                ),
                              ],
                            ),
                          )
                        : LiveSliverGrid(
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2,
                                    childAspectRatio:
                                        ((width * 0.8) / (height * 0.5))),
                            itemBuilder: (context, index, animation) {
                              return FadeTransition(
                                opacity: Tween<double>(begin: 0, end: 1)
                                    .animate(animation),
                                child: SlideTransition(
                                  position: Tween<Offset>(
                                          begin: Offset(0, 0.1),
                                          end: Offset(0, 0))
                                      .animate(animation),
                                  child: SprintWidget(index),
                                ),
                              );
                            },
                            itemCount:
                                (sprints.length == 0) ? 0 : sprints.length,
                            controller: _scrollController),
                  ),
                ],
              )),
    );
  }
}
