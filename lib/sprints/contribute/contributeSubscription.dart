import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/services/contributionBackend.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';

void showContributeBrowser(
    BuildContext context, int index, String id, String submit) {
  showDialog(
      context: context, builder: (ctx) => ContributeBrowser(id, index, submit));
}

class ContributeBrowser extends StatefulWidget {
  final String _id;
  final int _index;
  final String _submit;

  ContributeBrowser(this._id, this._index, this._submit);

  @override
  _ContributeBrowserState createState() =>
      _ContributeBrowserState(_id, _index, _submit);
}

PageController contributeController = PageController(initialPage: 0);

class _ContributeBrowserState extends State<ContributeBrowser>
    with TickerProviderStateMixin {
  final String _id;
  final int _index;
  final String _submit;

  String _selected = 'None';
  String _selectedId = '';

  AnimationController controller;
  Animation<double> scaleAnimation;

  @override
  void initState() {
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Sprung.criticallyDamped);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
    super.initState();
  }

  @override
  void dispose() {
    this.controller.dispose();
    super.dispose();
  }

  _ContributeBrowserState(this._id, this._index, this._submit);

  bool loading = false;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: AbsorbPointer(
            absorbing: loading,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: height * 0.41,
                  width: width * 0.75,
                  decoration: ShapeDecoration(
                      color: HomeStyles().backgroundColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(21))),
                  child: PageView(
                    controller: contributeController,
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      pageOne(),
                      Center(
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.2,
                          width: MediaQuery.of(context).size.height * 0.2,
                          child: Lottie.asset('assets/lottie/check.json',
                              repeat: false, fit: BoxFit.contain),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: height * 0.02),
                AnimatedContainer(
                  duration: Duration(milliseconds: 150),
                  width: width * 0.75,
                  height: height * 0.1,
                  decoration: BoxDecoration(
                    border: (_selected == 'None')
                        ? null
                        : Border.all(width: 6, color: HomeStyles().accentColor),
                    color: HomeStyles().backgroundColor,
                    borderRadius: BorderRadius.circular(21),
                  ),
                  padding: EdgeInsets.symmetric(horizontal: width * 0.075),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        '$_selected',
                        style: HomeStyles().headerText.copyWith(
                            fontWeight: FontWeight.w600,
                            color: (_selected == 'None')
                                ? Color(0xffAAAAAA)
                                : HomeStyles().primaryColor),
                      ),
                      Container(
                        child: loading
                            ? Transform.scale(
                                scale: 0.75,
                                child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation(
                                      HomeStyles().primaryColor),
                                  backgroundColor: Colors.transparent,
                                ),
                              )
                            : BouncingWidget(
                                child: Container(
                                  height: height * 0.075,
                                  width: height * 0.05,
                                  child: Center(
                                    child: FaIcon(
                                      FontAwesomeIcons.arrowRight,
                                      color: (_selected == 'None')
                                          ? Color(0xffAAAAAAA)
                                          : HomeStyles().primaryColor,
                                      size: height * 0.02,
                                    ),
                                  ),
                                ),
                                onPressed: () {
                                  if (_selected != 'None') {
                                    setState(() {
                                      loading = true;
                                    });
                                    ContributionBackend().contribute(
                                        _id, _selectedId, _submit, context);
                                  }
                                },
                                duration: Duration(milliseconds: 150),
                              ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget pageOne() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    List subscriptions =
        Provider.of<SubscriptionProvider>(context).getSubscriptions;

    return Container(
      padding: EdgeInsets.all(width * 0.05),
      child: Stack(
        children: [
          ScrollConfiguration(
            behavior: AntiScrollGlow(),
            child: CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: height * 0.05),
                      Text(
                        'select a deck',
                        style: HomeStyles().subtitleText,
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: height * 0.05),
                    ],
                  ),
                ),
                SliverList(
                    delegate: SliverChildBuilderDelegate((context, int index) {
                  return subWidget(subscriptions[index]['title'],
                      subscriptions[index]['id']);
                }, childCount: subscriptions.length)),
              ],
            ),
          ),
          closeButton(height),
        ],
      ),
    );
  }

  Widget subWidget(String title, String id) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return Container(
      margin: EdgeInsets.symmetric(vertical: height * 0.02),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(title,
              style: HomeStyles().buttonText.copyWith(
                  color: (_selected == title)
                      ? HomeStyles().accentColor
                      : HomeStyles().primaryColor)),
          BouncingWidget(
            child: Container(
              height: height * 0.03,
              width: height * 0.03,
              child: Center(
                child: FaIcon(
                  (_selected == title)
                      ? FontAwesomeIcons.check
                      : FontAwesomeIcons.plus,
                  color: (_selected == title)
                      ? HomeStyles().accentColor
                      : HomeStyles().primaryColor,
                  size: height * 0.02,
                ),
              ),
            ),
            onPressed: () {
              if (_selected != title) {
                setState(() {
                  _selected = title;
                  _selectedId = id;
                });
              } else {
                setState(() {
                  _selected = 'None';
                });
              }
            },
            duration: Duration(milliseconds: 150),
          )
        ],
      ),
    );
  }

  Widget closeButton(double height) {
    return Align(
      alignment: Alignment.topRight,
      child: GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        child: Container(
          height: height * 0.05,
          width: height * 0.05,
          child: Center(
            child: FaIcon(
              FontAwesomeIcons.times,
              color: Color(0xffAAAAAA),
              size: height * 0.02,
            ),
          ),
        ),
      ),
    );
  }
}
