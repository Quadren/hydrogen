import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/contribute/contributeSubscription.dart';

class RequiredWidget extends StatelessWidget {
  final String _title;
  final String _id;
  final int _index;

  RequiredWidget(this._title, this._id, this._index);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return Container(
      height: height * 0.1,
      width: width * 0.8,
      margin: EdgeInsets.symmetric(vertical: height * 0.01),
      decoration: BoxDecoration(
        color: (_index == 0) ? HomeStyles().accentColor : Color(0xffEEEEEE),
        borderRadius: BorderRadius.circular(21),
      ),
      padding: EdgeInsets.all(width * 0.05),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: width * 0.5,
            child: Text('$_title',
                style: (_index == 0)
                    ? HomeStyles().buttonText3
                    : HomeStyles().buttonText,
                overflow: TextOverflow.ellipsis,
                maxLines: 2),
          ),
          BouncingWidget(
            child: Container(
              height: height * 0.08,
              width: height * 0.055,
              decoration: BoxDecoration(
                color: HomeStyles().backgroundColor,
                borderRadius: BorderRadius.circular(15),
              ),
              child: Center(
                child: FaIcon(FontAwesomeIcons.plus,
                    color: HomeStyles().primaryColor, size: height * 0.025),
              ),
            ),
            onPressed: () {
              showContributeBrowser(context, _index, _id, _title);
            },
            duration: Duration(milliseconds: 150),
          )
        ],
      ),
    );
  }
}
