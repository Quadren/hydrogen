import 'package:animated/animated.dart';
import 'package:auto_animated/auto_animated.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/adminConsole/adminConsole.dart';
import 'package:hydrogen/sprints/contribute/requiredWidget.dart';
import 'package:hydrogen/sprints/services/contributionBackend.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';

class SprintContributionPage extends StatefulWidget {
  final String _id;

  SprintContributionPage(this._id);

  @override
  _SprintContributionPageState createState() =>
      _SprintContributionPageState(_id);
}

class _SprintContributionPageState extends State<SprintContributionPage> {
  final String _id;

  _SprintContributionPageState(this._id);

  bool _isOwner = false;
  bool _expandConsole = false;
  bool _removePadding = false;
  bool _showButton = false;

  void _checkIfOwner() {
    String owner = Provider.of<SprintProvider>(context, listen: false)
        .getSprintData['owner'];

    String email = Hive.box('userData').get('email');

    if (owner == email) {
      setState(() {
        _isOwner = true;
      });
      Future.delayed(Duration(seconds: 1), () {
        setState(() {
          _showButton = true;
        });
      });
    }
  }

  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    _scrollController.addListener(() {
      if (_scrollController.offset.round() > 50 && _isOwner) {
        setState(() {
          _showButton = false;
        });
      } else if (_scrollController.offset.round() < 50 && _isOwner) {
        setState(() {
          _showButton = true;
        });
      }
    });
    Future.delayed(Duration.zero, () {
      _checkIfOwner();
    });
    ContributionBackend().getRequiredSubs(_id, context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    Map data = Provider.of<SprintProvider>(context).getSprintData;
    List requiredSubs = Provider.of<SprintProvider>(context).getRequired;

    return Scaffold(
      backgroundColor: HomeStyles().backgroundColor,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: InkWell(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          onTap: () {
            if (_expandConsole) {
              setState(() {
                _expandConsole = false;
                _removePadding = false;
              });
            } else {
              Navigator.pop(context);
            }
          },
          child: Container(
            margin: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.05,
                top: MediaQuery.of(context).size.height * 0.03),
            decoration: BoxDecoration(
              color: HomeStyles().backgroundColor,
              borderRadius: BorderRadius.circular(100),
            ),
            child: Center(
                child: FaIcon(
              _expandConsole
                  ? FontAwesomeIcons.times
                  : FontAwesomeIcons.arrowLeft,
              color: HomeStyles().primaryColor,
              size: MediaQuery.of(context).size.height * 0.025,
            )),
          ),
        ),
      ),
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          ScrollConfiguration(
            behavior: AntiScrollGlow(),
            child: CustomScrollView(
              controller: _scrollController,
              slivers: [
                SliverToBoxAdapter(
                    child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: height * 0.15),
                      Text('Contribute', style: HomeStyles().bigText),
                      SizedBox(height: height * 0.01),
                      Text('to "${data['title']}"',
                          style: HomeStyles().subtitleText),
                      SizedBox(height: height * 0.1),
                      Divider(
                        color: Color(0xffAAAAAA),
                      ),
                      SizedBox(height: height * 0.01),
                      Text('required', style: HomeStyles().subtitleText),
                      SizedBox(height: height * 0.03),
                    ],
                  ),
                )),
                Container(
                  child: (requiredSubs.length == 0)
                      ? SliverToBoxAdapter(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(height: height * 0.1),
                              Transform.scale(
                                scale: 1.5,
                                child: CircularProgressIndicator(
                                  strokeWidth: 7,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Color(0xff6953F5)),
                                  backgroundColor: Color(0xFFFFDDE1),
                                ),
                              ),
                            ],
                          ),
                        )
                      : LiveSliverList(
                          itemBuilder: (context, int index,
                              Animation<double> animation) {
                            return Padding(
                              padding:
                                  EdgeInsets.symmetric(horizontal: width * 0.1),
                              child: Center(
                                child: RequiredWidget(
                                    requiredSubs[index], _id, index),
                              ),
                            );
                          },
                          itemCount: requiredSubs.length,
                          controller: _scrollController),
                ),
                SliverToBoxAdapter(
                  child: SizedBox(height: height * 0.05),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Animated(
              value: (_showButton) ? 1 : 0,
              duration: Duration(milliseconds: 1500),
              curve: Sprung.criticallyDamped,
              builder: (context, child, animation) {
                return Transform.translate(
                  offset: Offset(0, (1 - animation.value) * 200),
                  child: child,
                );
              },
              child: GestureDetector(
                onTap: () {
                  if (!_removePadding) {
                    setState(() {
                      _expandConsole = true;
                      _removePadding = true;
                    });
                  }
                },
                child: AnimatedPadding(
                  padding: _removePadding
                      ? EdgeInsets.zero
                      : EdgeInsets.only(bottom: height * 0.05),
                  duration: Duration(milliseconds: 1500),
                  curve: Sprung.overDamped,
                  child: AnimatedContainer(
                    duration: Duration(milliseconds: 1500),
                    curve: Sprung.overDamped,
                    height: _expandConsole ? height : height * 0.1,
                    width: _expandConsole ? width : width * 0.8,
                    decoration: BoxDecoration(
                      color: _expandConsole
                          ? HomeStyles().backgroundColor
                          : HomeStyles().primaryColor,
                      borderRadius: BorderRadius.circular(21),
                    ),
                    padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                    child: Center(
                      child: Stack(
                        children: [
                          Center(
                            child: AnimatedOpacity(
                              opacity: _expandConsole ? 0 : 1,
                              duration: Duration(milliseconds: 300),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    'Admin Console',
                                    style: HomeStyles().buttonText3,
                                  ),
                                  FaIcon(
                                    FontAwesomeIcons.arrowUp,
                                    size: height * 0.025,
                                    color: HomeStyles().backgroundColor,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          AnimatedOpacity(
                            opacity: _expandConsole ? 1 : 0,
                            duration: Duration(milliseconds: 1000),
                            child: AdminConsoleSprints(),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
