import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:provider/provider.dart';

class GuideToSprints extends StatefulWidget {
  @override
  _GuideToSprintsState createState() => _GuideToSprintsState();
}

class _GuideToSprintsState extends State<GuideToSprints> {
  PageController _controller = PageController(initialPage: 0);
  int _pageIndex = 0;

  @override
  void initState() {
    _controller.addListener(() {
      if (_controller.page.round() != _pageIndex) {
        setState(() {
          _pageIndex = _controller.page.round();
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: HomeStyles().primaryColor),
        leading: InkWell(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          onTap: () => Navigator.pop(context),
          child: Container(
            margin: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.05,
                top: MediaQuery.of(context).size.height * 0.03),
            decoration: BoxDecoration(
              color: HomeStyles().backgroundColor,
              borderRadius: BorderRadius.circular(100),
            ),
            child: Center(
                child: FaIcon(
              FontAwesomeIcons.arrowLeft,
              color: HomeStyles().primaryColor,
              size: MediaQuery.of(context).size.height * 0.025,
            )),
          ),
        ),
      ),
      backgroundColor: HomeStyles().backgroundColor,
      body: body(),
    );
  }

  Widget body() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Stack(
      children: [
        PageView(
          controller: _controller,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.1),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'A guide to\nstudy sprints',
                    style: HomeStyles().bigText,
                  ),
                  SizedBox(height: height * 0.02),
                  Text(
                    'swipe to get started',
                    style: HomeStyles().subtitleText,
                  ),
                  SizedBox(height: height * 0.1),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.1),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'What are\n"sprints"?',
                    style: HomeStyles().bigText,
                  ),
                  SizedBox(height: height * 0.02),
                  Container(
                    width: width * 0.7,
                    child: Text(
                      'Study sprints allow you and your classmates to prepare for an upcoming exam as quickly as possible by pooling together resources.',
                      style: HomeStyles().subtitleText,
                    ),
                  ),
                  SizedBox(height: height * 0.1),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.1),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'How can these\nbenefit me?',
                    style: HomeStyles().bigText,
                  ),
                  SizedBox(height: height * 0.02),
                  Container(
                    width: width * 0.7,
                    child: Text(
                      'Instead of one person doing all the work for every subscription, you can pool together your subscriptions by assigning specific topics to each person.\n\nIn the end, everyone does a bit of work for a collective outcome!',
                      style: HomeStyles().subtitleText,
                    ),
                  ),
                  SizedBox(height: height * 0.1),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.1),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'How do I\nget started?',
                    style: HomeStyles().bigText,
                  ),
                  SizedBox(height: height * 0.02),
                  Container(
                    width: width * 0.7,
                    child: Text(
                      'Hit "finish", and then you can create a sprint and invite others! You have to be invited to a sprint to join.',
                      style: HomeStyles().subtitleText,
                    ),
                  ),
                  SizedBox(height: height * 0.05),
                  BouncingWidget(
                    child: Container(
                      height: height * 0.1,
                      width: width * 0.4,
                      decoration: BoxDecoration(
                        color: HomeStyles().accentColor,
                        borderRadius: BorderRadius.circular(21),
                      ),
                      child: Center(
                        child: Text(
                          'Finish',
                          style: HomeStyles()
                              .buttonText3
                              .apply(fontWeightDelta: 2),
                        ),
                      ),
                    ),
                    onPressed: () {
                      Hive.box('userData').put('isFirst', false);
                      Provider.of<SprintProvider>(context, listen: false)
                          .setFirst(false);
                      Fluttertoast.showToast(
                          msg: 'We hope sprints help you study better!',
                          backgroundColor: HomeStyles().accentColor,
                          textColor: Colors.white);
                      Fluttertoast.showToast(
                          msg: 'Loading your sprints now!',
                          backgroundColor: HomeStyles().accentColor,
                          textColor: Colors.white);
                      Phoenix.rebirth(context);
                    },
                    duration: Duration(milliseconds: 150),
                  )
                ],
              ),
            ),
          ],
        ),
        progressBar(),
      ],
    );
  }

  Widget progressBar() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Align(
      alignment: Alignment.bottomCenter,
      child: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: width * 0.1, vertical: height * 0.02),
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              AnimatedContainer(
                duration: Duration(milliseconds: 100),
                height: (_pageIndex == 0) ? height * 0.0075 : height * 0.005,
                width: width * 0.19,
                decoration: BoxDecoration(
                  color: (_pageIndex == 0)
                      ? HomeStyles().accentColor
                      : Color(0xffEEEEEE),
                  borderRadius: BorderRadius.circular(100),
                ),
              ),
              AnimatedContainer(
                duration: Duration(milliseconds: 100),
                height: (_pageIndex == 1) ? height * 0.0075 : height * 0.005,
                width: width * 0.19,
                decoration: BoxDecoration(
                  color: (_pageIndex == 1)
                      ? HomeStyles().accentColor
                      : Color(0xffEEEEEE),
                  borderRadius: BorderRadius.circular(100),
                ),
              ),
              AnimatedContainer(
                duration: Duration(milliseconds: 100),
                height: (_pageIndex == 2) ? height * 0.0075 : height * 0.005,
                width: width * 0.19,
                decoration: BoxDecoration(
                  color: (_pageIndex == 2)
                      ? HomeStyles().accentColor
                      : Color(0xffEEEEEE),
                  borderRadius: BorderRadius.circular(100),
                ),
              ),
              AnimatedContainer(
                duration: Duration(milliseconds: 100),
                height: (_pageIndex == 3) ? height * 0.0075 : height * 0.005,
                width: width * 0.19,
                decoration: BoxDecoration(
                  color: (_pageIndex == 3)
                      ? HomeStyles().accentColor
                      : Color(0xffEEEEEE),
                  borderRadius: BorderRadius.circular(100),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
