import 'package:animated/animated.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/intro/guideToSprints.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';

class IntroToSprints extends StatefulWidget {
  @override
  _IntroToSprintsState createState() => _IntroToSprintsState();
}

class _IntroToSprintsState extends State<IntroToSprints> {
  bool _animate1 = false;
  bool _animate2 = false;
  bool _animate3 = false;
  bool _animate4 = false;
  bool _animate5 = false;

  void animateFunc() {
    Future.delayed(Duration(milliseconds: 100), () {
      setState(() {
        _animate1 = true;
      });
    });
    Future.delayed(Duration(milliseconds: 1000), () {
      setState(() {
        _animate2 = true;
      });
    });
    Future.delayed(Duration(milliseconds: 1500), () {
      setState(() {
        _animate3 = true;
      });
    });
    Future.delayed(Duration(milliseconds: 3000), () {
      setState(() {
        _animate4 = true;
      });
    });
    Future.delayed(Duration(milliseconds: 4000), () {
      setState(() {
        _animate5 = true;
      });
    });
  }

  void animateOut() {
    Future.delayed(Duration(milliseconds: 100), () {
      setState(() {
        _animate5 = false;
      });
    });
    Future.delayed(Duration(milliseconds: 200), () {
      setState(() {
        _animate4 = false;
      });
    });
    Future.delayed(Duration(milliseconds: 300), () {
      setState(() {
        _animate3 = false;
      });
    });
    Future.delayed(Duration(milliseconds: 400), () {
      setState(() {
        _animate2 = false;
      });
    });
    Future.delayed(Duration(milliseconds: 500), () {
      setState(() {
        _animate1 = false;
      });
    });
  }

  @override
  void initState() {
    animateFunc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Stack(
      children: [
        Container(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                animate(
                  _animate1,
                  Text('introducing', style: HomeStyles().subtitleText),
                ),
                SizedBox(height: height * 0.01),
                animate(
                  _animate2,
                  Text(
                    'Study Sprints!',
                    style: HomeStyles().bigText,
                  ),
                ),
                SizedBox(height: height * 0.03),
                animate(
                  _animate3,
                  Container(
                    width: width * 0.4,
                    child: Text(
                      'the quickest way to study for an exam.',
                      style: HomeStyles().buttonText2,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                SizedBox(height: height * 0.1),
                animate(
                  _animate4,
                  BouncingWidget(
                    child: Container(
                      height: height * 0.1,
                      width: width * 0.45,
                      decoration: BoxDecoration(
                        color: HomeStyles().accentColor,
                        borderRadius: BorderRadius.circular(21),
                      ),
                      child: Center(
                        child: Text(
                          'Show me around',
                          style: HomeStyles()
                              .buttonText3
                              .apply(fontWeightDelta: 2),
                        ),
                      ),
                    ),
                    onPressed: () {
                      animateOut();
                      Future.delayed(Duration(seconds: 1), () {
                        Navigator.push(
                            context,
                            PageTransition(
                                child: GuideToSprints(),
                                type: PageTransitionType.rightToLeft,
                                curve: Curves.easeInOutCubic,
                                duration: Duration(milliseconds: 500)));
                      });
                    },
                    duration: Duration(milliseconds: 150),
                  ),
                ),
                SizedBox(height: height * 0.02),
                animate(
                  _animate5,
                  BouncingWidget(
                    child: Container(
                      child: Center(
                        child: Text(
                          'or skip tour',
                          style: HomeStyles().subtitleText,
                        ),
                      ),
                    ),
                    onPressed: () {
                      animateOut();
                      Provider.of<SprintProvider>(context, listen: false)
                          .setFirst(false);
                      Hive.box('userData').put('isFirst', false);
                      Fluttertoast.showToast(
                          msg: 'Loading your sprints now!',
                          backgroundColor: HomeStyles().accentColor,
                          textColor: Colors.white);
                      Phoenix.rebirth(context);
                    },
                    duration: Duration(milliseconds: 150),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget animate(bool trigger, Widget widget) {
    return Animated(
      duration: trigger == _animate1
          ? Duration(seconds: 2)
          : Duration(milliseconds: 1500),
      curve: Sprung.overDamped,
      value: trigger ? 1 : 0,
      builder: (context, child, animation) => Transform.translate(
        offset: Offset(0, (1 - animation.value) * 40),
        child: Opacity(
          opacity: animation.value,
          child: child,
        ),
      ),
      child: widget,
    );
  }
}
