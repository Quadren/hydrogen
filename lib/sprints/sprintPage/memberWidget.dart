import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hydrogen/home/home_styles.dart';

class MemberWidget extends StatelessWidget {
  final String _name;
  final String _pfp;
  final int _index;

  MemberWidget(this._name, this._pfp, this._index);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: height * 0.133,
            width: height * 0.133,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
              color: Color(0xffEEEEEE),
            ),
            child: ClipOval(
              child: CachedNetworkImage(
                fit: BoxFit.cover,
                imageUrl: _pfp,
                placeholder: (context, url) {
                  return Center(
                    child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation(HomeStyles().primaryColor),
                      backgroundColor: Colors.transparent,
                    ),
                  );
                },
              ),
            ),
          ),
          SizedBox(height: height * 0.02),
          Container(
            width: width * 0.3,
            child: Text(
              '$_name',
              textAlign: TextAlign.center,
              style: HomeStyles().buttonText2,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
    );
  }
}
