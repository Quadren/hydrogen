import 'package:auto_animated/auto_animated.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animation_progress_bar/flutter_animation_progress_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/adminConsole/pages/dismissSprint.dart';
import 'package:hydrogen/sprints/contribute/sprintContribute.dart';
import 'package:hydrogen/sprints/services/getSprintData.dart';
import 'package:hydrogen/sprints/services/leaveSprint.dart';
import 'package:hydrogen/sprints/sprintPage/deadlinesWidget.dart';
import 'package:hydrogen/sprints/sprintPage/memberWidget.dart';
import 'package:hydrogen/sprints/sprintPage/subscriptionWidgetSprint.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:hydrogen/widgets/backBar.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class SprintPage extends StatefulWidget {
  final Map _data;

  SprintPage(this._data);

  @override
  _SprintPageState createState() => _SprintPageState(_data);
}

class _SprintPageState extends State<SprintPage>
    with SingleTickerProviderStateMixin {
  final Map _data;

  _SprintPageState(this._data);

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      getSprintData(_data['id'], context).then((value) {
        setState(() {});
      });
    });
    super.initState();
  }

  ScrollController _controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    Map data = Provider.of<SprintProvider>(context).getSprintData;
    bool loading = Provider.of<SprintProvider>(context).getSprintLoading;
    List subscriptionData =
        Provider.of<SprintProvider>(context).getSubscriptions;
    List members = Provider.of<SprintProvider>(context).getMembers;
    String owner = data['owner'];

    return Scaffold(
      backgroundColor: HomeStyles().backgroundColor,
      appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: Backbar()),
      extendBodyBehindAppBar: true,
      body: ScrollConfiguration(
          behavior: AntiScrollGlow(),
          child: CustomScrollView(
            controller: _controller,
            slivers: [
              SliverToBoxAdapter(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: height * 0.15),
                      Container(
                        width: width * 0.7,
                        child: Text(
                          '${_data['title']}',
                          style: HomeStyles().bigText,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      SizedBox(height: height * 0.03),
                      Container(
                        width: width * 0.8,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child: loading
                              ? LinearProgressIndicator(
                                  backgroundColor: Color(0xffEEEEEE),
                                  valueColor: AlwaysStoppedAnimation(
                                      HomeStyles().accentColor),
                                )
                              : FAProgressBar(
                                  changeProgressColor:
                                      HomeStyles().tertiaryAccent,
                                  changeColorValue: 100,
                                  size: 5,
                                  backgroundColor: Color(0xffEEEEEE),
                                  progressColor: HomeStyles().accentColor,
                                  currentValue:
                                      loading ? 0 : data['progress'].toInt(),
                                  animatedDuration: Duration(milliseconds: 400),
                                  maxValue: 100,
                                ),
                        ),
                      ),
                      SizedBox(height: height * 0.01),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          data['progress'] == null
                              ? 'fetching data'
                              : '${(data['progress']).toInt()}% done',
                          style: HomeStyles().subtitleText,
                        ),
                      ),
                      SizedBox(height: height * 0.02),
                      Container(
                        child: loading
                            ? datePlaceholder(height, width)
                            : Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    DeadlinesWidget(data['dates']),
                                    Container(
                                      child: (owner ==
                                              Hive.box('userData').get('email'))
                                          ? Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                SizedBox(height: height * 0.03),
                                                BouncingWidget(
                                                  child: Container(
                                                    height: height * 0.05,
                                                    width: width * 0.4,
                                                    decoration: BoxDecoration(
                                                      color: Colors.red[300],
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              15),
                                                    ),
                                                    child: Center(
                                                      child: Text('end sprint',
                                                          style: HomeStyles()
                                                              .buttonText2
                                                              .copyWith(
                                                                  color: HomeStyles()
                                                                      .backgroundColor)),
                                                    ),
                                                  ),
                                                  onPressed: () {
                                                    dismissSprintPopup(context);
                                                  },
                                                  duration: Duration(
                                                      milliseconds: 150),
                                                )
                                              ],
                                            )
                                          : Container(),
                                    ),
                                  ],
                                ),
                              ),
                      ),
                      SizedBox(height: height * 0.1),
                      Center(
                        child: Text('decks', style: HomeStyles().subtitleText),
                      ),
                      SizedBox(height: height * 0.03),
                    ],
                  ),
                ),
              ),
              Container(
                child: loading
                    ? SliverToBoxAdapter(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(height: height * 0.1),
                            Transform.scale(
                              scale: 1.7,
                              child: CircularProgressIndicator(
                                strokeWidth: 7,
                                valueColor: AlwaysStoppedAnimation(
                                    HomeStyles().accentColor),
                                backgroundColor: HomeStyles().secondaryAccent,
                              ),
                            ),
                          ],
                        ),
                      )
                    : LiveSliverList(
                        itemBuilder:
                            (context, int index, Animation<double> animation) {
                          return (subscriptionData.length == 0)
                              ? null
                              : FadeTransition(
                                  opacity: Tween<double>(begin: 0, end: 1)
                                      .animate(animation),
                                  child: SlideTransition(
                                    position: Tween<Offset>(
                                            begin: Offset(0, 0.1),
                                            end: Offset(0, 0))
                                        .animate(animation),
                                    child: Center(
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: width * 0.1),
                                        child: SubscriptionSprintWidget(
                                            subscriptionData[index]['title'],
                                            subscriptionData[index]['cards'],
                                            subscriptionData[index]['pfp'],
                                            subscriptionData[index]['id'],
                                            index,
                                            subscriptionData[index]
                                                ['contributorName']),
                                      ),
                                    ),
                                  ),
                                );
                        },
                        itemCount: loading ? 0 : subscriptionData.length,
                        controller: _controller),
              ),
              SliverToBoxAdapter(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: height * 0.1),
                    Text('members', style: HomeStyles().subtitleText),
                    SizedBox(height: height * 0.03),
                  ],
                ),
              ),
              Container(
                child: loading
                    ? SliverToBoxAdapter(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(height: height * 0.1),
                            Transform.scale(
                              scale: 1.7,
                              child: CircularProgressIndicator(
                                strokeWidth: 7,
                                valueColor: AlwaysStoppedAnimation(
                                    HomeStyles().accentColor),
                                backgroundColor: HomeStyles().secondaryAccent,
                              ),
                            ),
                          ],
                        ),
                      )
                    : LiveSliverGrid(
                        itemBuilder:
                            (context, int index, Animation<double> animation) {
                          return FadeTransition(
                            opacity: Tween<double>(begin: 0, end: 1)
                                .animate(animation),
                            child: SlideTransition(
                              position: Tween<Offset>(
                                      begin: Offset(0, 0.1), end: Offset(0, 0))
                                  .animate(animation),
                              child: Center(
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: width * 0.1),
                                  child: MemberWidget(members[index]['name'],
                                      members[index]['pfp'], index),
                                ),
                              ),
                            ),
                          );
                        },
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            childAspectRatio: ((width * 0.3) / (height * 0.2))),
                        itemCount: members.length,
                        controller: _controller),
              ),
              SliverToBoxAdapter(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: height * 0.1),
                    Container(
                      child: (owner != Hive.box('userData').get('email'))
                          ? BouncingWidget(
                              child: Container(
                                height: height * 0.1,
                                width: width * 0.8,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(21),
                                    color: Colors.red[300]),
                                child: Center(
                                  child: Text(
                                    'Leave sprint',
                                    style: HomeStyles().buttonText3,
                                  ),
                                ),
                              ),
                              onPressed: () {
                                showLeaveConfirmation(context);
                              },
                              duration: Duration(milliseconds: 150),
                            )
                          : null,
                    ),
                    SizedBox(height: height * 0.05),
                  ],
                ),
              ),
            ],
          )),
      floatingActionButton: loading
          ? null
          : Container(
              child: (data['progress'] == 100)
                  ? null
                  : BouncingWidget(
                      child: Container(
                        height: height * 0.085,
                        width: height * 0.085,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(21),
                          color: HomeStyles().accentColor,
                        ),
                        child: Center(
                          child: FaIcon(
                            FontAwesomeIcons.plus,
                            color: HomeStyles().backgroundColor,
                            size: height * 0.025,
                          ),
                        ),
                      ),
                      onPressed: () {
                        if (!loading) {
                          Navigator.push(
                              context,
                              PageTransition(
                                  child: SprintContributionPage(_data['id']),
                                  type: PageTransitionType.rightToLeft,
                                  curve: Curves.easeInOutCubic,
                                  duration: Duration(milliseconds: 500)));
                        } else {
                          Fluttertoast.showToast(
                              msg: 'Wait for the loading to finish!',
                              backgroundColor: Colors.red[300],
                              textColor: Colors.white);
                        }
                      },
                      duration: Duration(milliseconds: 150),
                    ),
            ),
    );
  }

  Widget datePlaceholder(double height, double width) {
    return Container(
      height: height * 0.2,
      width: width * 0.8,
      padding: EdgeInsets.symmetric(
          horizontal: width * 0.1, vertical: height * 0.02),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('started', style: HomeStyles().subtitleText),
              Text(
                'loading',
                style: HomeStyles()
                    .subtitleText
                    .copyWith(fontWeight: FontWeight.w600),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('ends', style: HomeStyles().subtitleText),
              Text(
                'loading',
                style: HomeStyles().subtitleText.copyWith(
                      fontWeight: FontWeight.w600,
                    ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('status', style: HomeStyles().subtitleText),
              Text('loading',
                  style: HomeStyles().subtitleText.copyWith(
                        fontWeight: FontWeight.w600,
                      ))
            ],
          ),
        ],
      ),
    );
  }
}
