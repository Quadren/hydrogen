import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/subscriptionPreview/subscriptionPreview.dart';
import 'package:page_transition/page_transition.dart';

class SubscriptionSprintWidget extends StatefulWidget {
  final String _title;
  final String _pfp;
  final String _id;
  final int _index;
  final List _cards;
  final String _contributor;

  SubscriptionSprintWidget(this._title, this._cards, this._pfp, this._id,
      this._index, this._contributor);

  @override
  _SubscriptionSprintWidgetState createState() =>
      _SubscriptionSprintWidgetState(
          _title, _cards, _pfp, _id, _index, _contributor);
}

class _SubscriptionSprintWidgetState extends State<SubscriptionSprintWidget> {
  final String _title;
  final String _pfp;
  final String _id;
  final int _index;
  final List _cards;
  final String _contributor;

  _SubscriptionSprintWidgetState(this._title, this._cards, this._pfp, this._id,
      this._index, this._contributor);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    Map data = {
      'title': _title,
      'pfp': _pfp,
      'id': _id,
      'cards': _cards,
      'contributor': _contributor
    };

    return Container(
        height: height * 0.1,
        width: width * 0.8,
        margin: EdgeInsets.symmetric(vertical: height * 0.01),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(21),
          color: (_index == 0) ? HomeStyles().accentColor : Color(0xffEEEEEE),
        ),
        child: GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                PageTransition(
                    child: SubscriptionPreview(data),
                    type: PageTransitionType.rightToLeft,
                    curve: Curves.easeInOutCubic,
                    duration: Duration(milliseconds: 500)));
          },
          child: Container(
            padding: EdgeInsets.all(width * 0.05),
            height: height * 0.1,
            width: width * 0.8,
            color: Colors.white.withOpacity(0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: width * 0.5,
                  child: Text(
                    _title,
                    style: (_index == 0)
                        ? HomeStyles().buttonText3
                        : HomeStyles().buttonText,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                ),
                Container(
                  height: height * 0.1,
                  width: height * 0.05,
                  decoration: BoxDecoration(
                      color: HomeStyles().backgroundColor,
                      borderRadius: BorderRadius.circular(15)),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: CachedNetworkImage(
                      imageUrl: _pfp,
                      fit: BoxFit.cover,
                      placeholder: (context, url) {
                        return Center(
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.transparent,
                            valueColor: AlwaysStoppedAnimation(
                                HomeStyles().primaryColor),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
