import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class DeadlinesWidget extends StatelessWidget {
  final Map _dates;

  DeadlinesWidget(this._dates);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    Timestamp start = _dates['start'];
    Timestamp end = _dates['end'];

    var startDateTime =
        DateTime.fromMillisecondsSinceEpoch(start.millisecondsSinceEpoch);
    var endDateTime =
        DateTime.fromMillisecondsSinceEpoch(end.millisecondsSinceEpoch);

    String startFormatted = DateFormat('dd-MM-yyyy').format(startDateTime);
    String endFormatted = DateFormat('dd-MM-yyyy').format(endDateTime);

    DateTime now = DateTime.now();
    String today = DateFormat('dd-MM-yyyy').format(now);

    String status = (now.isAfter(endDateTime) || today == endFormatted)
        ? 'finished'
        : 'in progress';

    String owner = Provider.of<SprintProvider>(context).getSprintData['owner'];
    String user = Hive.box('userData').get('email');

    return Container(
      height: height * 0.2,
      width: width * 0.8,
      padding: EdgeInsets.symmetric(
          horizontal: width * 0.1, vertical: height * 0.02),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('started', style: HomeStyles().subtitleText),
              Text(
                startFormatted,
                style: HomeStyles().subtitleText.copyWith(
                    fontWeight: FontWeight.w600,
                    color: HomeStyles().primaryColor),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('ends', style: HomeStyles().subtitleText),
              Text(
                endFormatted,
                style: HomeStyles().subtitleText.copyWith(
                    fontWeight: FontWeight.w600,
                    color: HomeStyles().primaryColor),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('status', style: HomeStyles().subtitleText),
              Text(
                status,
                style: HomeStyles().subtitleText.copyWith(
                    fontWeight: FontWeight.w600,
                    color: (status == 'in progress')
                        ? Colors.green[300]
                        : Colors.red[300]),
              )
            ],
          ),
        ],
      ),
    );
  }
}
