import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/sprints/subscriptionPreview/subscribeSheet.dart';

Future<void> addToUserSubscriptionsSprint(
    BuildContext context, String id) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('users');
    String email = Hive.box('userData').get('email');
    try {
      final result = await ref.doc(email).get().then((value) {
        List subscriptions = value.data()['subscriptions'];
        if (subscriptions.contains(id)) {
          Fluttertoast.showToast(
              msg: "You're already subscribed to this!",
              backgroundColor: Colors.red[300],
              textColor: Colors.white);
          Navigator.pop(context);
        } else {
          print("Adding $id to subscriptions");
          subscriptions.add(id);
          ref.doc(email).update({'subscriptions': subscriptions});
          subscribeSheetController.nextPage(
              duration: Duration(milliseconds: 500),
              curve: Curves.easeInOutCubic);
          Future.delayed(Duration(seconds: 2), () {
            Phoenix.rebirth(context);
          });
        }
      });
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.toString(),
          backgroundColor: Colors.red[300],
          textColor: Colors.white);
    }
  });
}
