import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/subscriptionPreview/subActions.dart';
import 'package:hydrogen/subscriptions/addCards/catexPreview.dart';
import 'package:hydrogen/widgets/backBar.dart';
import 'package:page_transition/page_transition.dart';

class SubscriptionPreview extends StatefulWidget {
  final Map _data;

  SubscriptionPreview(this._data);

  @override
  _SubscriptionPreviewState createState() => _SubscriptionPreviewState(_data);
}

class _SubscriptionPreviewState extends State<SubscriptionPreview> {
  final Map _data;

  _SubscriptionPreviewState(this._data);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    List cards = _data['cards'];

    return Scaffold(
      backgroundColor: HomeStyles().backgroundColor,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: Backbar(),
      ),
      extendBodyBehindAppBar: true,
      body: ScrollConfiguration(
          behavior: AntiScrollGlow(),
          child: CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                  child: Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: height * 0.15),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: width * 0.6,
                              child: Text(
                                '${_data['title']}',
                                style: HomeStyles().bigText,
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            SizedBox(height: height * 0.01),
                            Container(
                              width: width * 0.7,
                              child: Text(
                                'contributed by ${_data['contributor']}',
                                style: HomeStyles().subtitleText,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ],
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                PageTransition(
                                    child: SubActions(_data),
                                    type: PageTransitionType.rightToLeft,
                                    curve: Curves.easeInOutCubic,
                                    duration: Duration(milliseconds: 500)));
                          },
                          child: Container(
                            child: FaIcon(
                              FontAwesomeIcons.arrowRight,
                              color: HomeStyles().primaryColor,
                              size: height * 0.025,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: height * 0.05),
                    Divider(
                      color: Color(0xffAAAAAA),
                    ),
                    SizedBox(height: height * 0.05),
                    Center(
                      child: Text(
                        'cards',
                        style: HomeStyles().subtitleText,
                      ),
                    ),
                    SizedBox(height: height * 0.05),
                  ],
                ),
              )),
              SliverGrid(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  childAspectRatio: (width * 0.4) / (height * 0.3),
                  crossAxisCount: 2,
                ),
                delegate: SliverChildBuilderDelegate((context, int index) {
                  String front = cards[index]['front'];
                  String back = cards[index]['back'];
                  String eqn = cards[index]['eqn'];
                  return GestureDetector(
                    child: FlipCard(
                        front: Container(
                          padding: EdgeInsets.all(width * 0.05),
                          margin: EdgeInsets.only(
                              top: width * 0.03,
                              right:
                                  (index % 2 == 0) ? width * 0.03 : width * 0.1,
                              left:
                                  (index % 2 == 0) ? width * 0.1 : width * 0.03,
                              bottom: width * 0.03),
                          decoration: BoxDecoration(
                            color: Color(0xFFEEEEEE),
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Stack(
                            children: [
                              Align(
                                alignment: Alignment.bottomLeft,
                                child: Container(
                                  child: Text(
                                    front,
                                    overflow: TextOverflow.fade,
                                    style: TextStyle(
                                      fontFamily: 'Raleway',
                                      fontSize: 12,
                                      color: HomeStyles().primaryColor,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Align(
                                alignment: Alignment.topRight,
                                child: Text(
                                  '${index + 1}',
                                  overflow: TextOverflow.fade,
                                  style: TextStyle(
                                    fontFamily: 'Raleway',
                                    fontSize: 12,
                                    color: Color(0xFFAAAAAA),
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        back: Container(
                          padding: EdgeInsets.all(width * 0.05),
                          margin: EdgeInsets.only(
                              top: width * 0.03,
                              right:
                                  (index % 2 == 0) ? width * 0.03 : width * 0.1,
                              left:
                                  (index % 2 == 0) ? width * 0.1 : width * 0.03,
                              bottom: width * 0.03),
                          decoration: BoxDecoration(
                            color: HomeStyles().accentColor,
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Stack(
                            children: [
                              Align(
                                alignment: Alignment.topLeft,
                                child: (eqn == null || eqn == 'none')
                                    ? null
                                    : GestureDetector(
                                        onTap: () {
                                          showCatex(eqn, context);
                                        },
                                        child: Container(
                                          height: height * 0.05,
                                          width: height * 0.05,
                                          decoration: BoxDecoration(
                                            color: HomeStyles().backgroundColor,
                                            borderRadius:
                                                BorderRadius.circular(12),
                                          ),
                                          child: Center(
                                            child: FaIcon(
                                              FontAwesomeIcons.equals,
                                              color: HomeStyles().accentColor,
                                              size: height * 0.015,
                                            ),
                                          ),
                                        ),
                                      ),
                              ),
                              Align(
                                alignment: Alignment.bottomLeft,
                                child: Container(
                                  child: Text(
                                    back,
                                    overflow: TextOverflow.fade,
                                    style: TextStyle(
                                      fontFamily: 'Raleway',
                                      fontSize: 12,
                                      color: HomeStyles().backgroundColor,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                              Align(
                                alignment: Alignment.topRight,
                                child: Text(
                                  '${index + 1}',
                                  overflow: TextOverflow.fade,
                                  style: TextStyle(
                                    fontFamily: 'Raleway',
                                    fontSize: 12,
                                    color: HomeStyles().backgroundColor,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )),
                  );
                }, childCount: cards.length),
              ),
            ],
          )),
    );
  }
}
