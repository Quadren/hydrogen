import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/study/studySub.dart';
import 'package:hydrogen/sprints/subscriptionPreview/subscribeSheet.dart';
import 'package:hydrogen/widgets/backBar.dart';
import 'package:page_transition/page_transition.dart';

class SubActions extends StatefulWidget {
  final Map _data;

  SubActions(this._data);

  @override
  _SubActionsState createState() => _SubActionsState(_data);
}

class _SubActionsState extends State<SubActions> {
  final Map _data;
  _SubActionsState(this._data);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    List cards = _data['cards'];

    return Scaffold(
      backgroundColor: HomeStyles().backgroundColor,
      appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: Backbar()),
      extendBodyBehindAppBar: true,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('choose an action', style: HomeStyles().buttonText),
            SizedBox(height: height * 0.03),
            button('Subscribe', HomeStyles().accentColor, () {
              showSubscribeSheet(context, _data);
            }),
            SizedBox(height: height * 0.01),
            button('Study', HomeStyles().tertiaryAccent, () {
              if (cards.length < 1) {
                Fluttertoast.showToast(
                    msg: 'This deck has no cards!',
                    backgroundColor: Colors.red[300],
                    textColor: Colors.white);
              } else {
                Navigator.push(
                    context,
                    PageTransition(
                        child: StudySub(_data),
                        type: PageTransitionType.rightToLeft,
                        curve: Curves.easeInOutCubic,
                        duration: Duration(milliseconds: 800)));
              }
            }),
          ],
        ),
      ),
    );
  }

  Widget button(String title, Color color, VoidCallback callback) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return BouncingWidget(
      child: Container(
        height: height * 0.1,
        width: width * 0.8,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(21),
        ),
        child: Center(
          child: Text(
            title,
            style: HomeStyles().buttonText3,
          ),
        ),
      ),
      onPressed: callback,
      duration: Duration(milliseconds: 150),
    );
  }
}
