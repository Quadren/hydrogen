import 'package:animated/animated.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/subscriptionPreview/addToSubscriptions.dart';
import 'package:lottie/lottie.dart';
import 'package:sprung/sprung.dart';

void showSubscribeSheet(BuildContext context, Map data) {
  showModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(21), topRight: Radius.circular(21)),
      ),
      backgroundColor: HomeStyles().backgroundColor,
      context: context,
      builder: (ctx) {
        return SubscribeSheet(data);
      });
}

class SubscribeSheet extends StatefulWidget {
  final Map _data;

  SubscribeSheet(this._data);

  @override
  _SubscribeSheetState createState() => _SubscribeSheetState(_data);
}

PageController subscribeSheetController = PageController(initialPage: 0);

class _SubscribeSheetState extends State<SubscribeSheet> {
  final Map _data;

  _SubscribeSheetState(this._data);

  bool _animate = false;
  bool _loading = false;

  bool _fade1 = false;
  bool _fade2 = false;

  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 200), () {
      setState(() {
        _fade1 = true;
      });
    });
    Future.delayed(Duration(milliseconds: 800), () {
      setState(() {
        _fade2 = true;
      });
    });
    Future.delayed(Duration(seconds: 1), () {
      setState(() {
        _animate = true;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return Container(
      height: height * 0.4,
      padding: EdgeInsets.all(width * 0.1),
      child: PageView(
        controller: subscribeSheetController,
        children: [
          Stack(
            children: [
              Align(
                  alignment: Alignment.topCenter,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      animate(
                        _fade1,
                        Text(
                          'you are subscribing to',
                          style: HomeStyles().subtitleText,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                        ),
                      ),
                      SizedBox(height: height * 0.02),
                      animate(
                          _fade2,
                          Text(
                            "'${_data['title']}'",
                            style: HomeStyles().bigText.copyWith(fontSize: 32),
                            textAlign: TextAlign.center,
                          )),
                    ],
                  )),
              Align(
                alignment: Alignment.bottomCenter,
                child: Animated(
                  curve: Sprung.criticallyDamped,
                  duration: Duration(milliseconds: 1500),
                  value: _animate ? 1 : 0,
                  builder: (context, child, animation) {
                    return Transform.translate(
                      offset: Offset(0, (1 - animation.value) * 200),
                      child: child,
                    );
                  },
                  child: BouncingWidget(
                    child: Container(
                      height: height * 0.1,
                      width: width * 0.8,
                      decoration: BoxDecoration(
                        color: HomeStyles().accentColor,
                        borderRadius: BorderRadius.circular(21),
                      ),
                      child: Center(
                        child: _loading
                            ? CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation(
                                    HomeStyles().backgroundColor),
                                backgroundColor: Colors.transparent,
                              )
                            : Text(
                                'Add to my decks',
                                style: HomeStyles().buttonText3,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                      ),
                    ),
                    onPressed: () {
                      if (!_loading) {
                        setState(() {
                          _loading = true;
                        });
                        addToUserSubscriptionsSprint(context, _data['id']);
                      } else {}
                    },
                    duration: Duration(milliseconds: 150),
                  ),
                ),
              ),
            ],
          ),
          Center(
            child: Container(
              height: MediaQuery.of(context).size.height * 0.2,
              width: MediaQuery.of(context).size.height * 0.2,
              child: Lottie.asset('assets/lottie/check.json',
                  repeat: false, fit: BoxFit.contain),
            ),
          ),
        ],
      ),
    );
  }

  Widget animate(bool trigger, Widget _child) {
    return Animated(
        value: trigger ? 1 : 0,
        duration: Duration(milliseconds: 1500),
        curve: Sprung.overDamped,
        builder: (context, child, animation) {
          return Transform.translate(
              offset: Offset(0, (1 - animation.value) * 40),
              child: Opacity(
                opacity: animation.value,
                child: child,
              ));
        },
        child: _child);
  }
}
