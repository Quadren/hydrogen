import 'package:flutter/cupertino.dart';

class SprintProvider extends ChangeNotifier {
  bool _firstTime = false;
  List _sprints = [];
  bool _isLoading = false;
  bool _sprintLoading = false;
  List _invites = [];

  Map _sprintData = {};
  List _required = [];
  List _subscriptions = [];
  List _members = [];

  bool get getFirst => _firstTime;
  List get getSprints => _sprints;
  bool get getLoading => _isLoading;
  Map get getSprintData => _sprintData;
  bool get getSprintLoading => _sprintLoading;
  List get getRequired => _required;
  List get getSubscriptions => _subscriptions;
  List get getMembers => _members;
  List get getInvites => _invites;

  void setFirst(bool value) {
    _firstTime = value;
    notifyListeners();
  }

  void addMember(Map data) {
    _members.add(data);
    notifyListeners();
  }

  void removeMember(String email) {
    int index;

    _members.forEach((element) {
      Map data = element;

      if (data.containsValue(email)) {
        index = _members.indexOf(element);
      }
    });

    _members.removeAt(index);

    notifyListeners();
  }

  void addToInvites(Map data) {
    _invites.add(data);
    notifyListeners();
  }

  void removeFromInvites(String id) {
    int elementToRemove;

    _invites.forEach((element) {
      Map data = element;

      if (data.containsValue(id)) {
        elementToRemove = _invites.indexOf(element);
      }
    });

    _invites.removeAt(elementToRemove);
    notifyListeners();
  }

  void addToSubscriptions(Map data) {
    _subscriptions.add(data);
    notifyListeners();
  }

  void resetSubs() {
    _subscriptions = [];
    _members = [];
    notifyListeners();
  }

  void resetRequired() {
    _required = [];
    notifyListeners();
  }

  void setRequired(List data) {
    _required = data;
    notifyListeners();
  }

  void removeRequired(String value) {
    _required.removeWhere((element) => element == value);
    notifyListeners();
  }

  void setSprintLoading(bool value) {
    _sprintLoading = value;
    notifyListeners();
  }

  void setSprints(List data) {
    _sprints = data;
    notifyListeners();
  }

  void resetSprints() {
    _sprints = [];
    _invites = [];
    notifyListeners();
  }

  void setSprintData(Map data) {
    _sprintData = data;
    notifyListeners();
  }

  void updateSprintData(Map data) {
    _sprintData = data;
    notifyListeners();
  }

  void resetSprintData() {
    _sprintData = {};
    notifyListeners();
  }

  void updateSprint(Map data) {
    int index = _sprints.indexWhere((element) => element == data);

    _sprints.removeAt(index);
    _sprints.insert(index, data);
  }

  void addSprint(String title, double progress, List subscriptions, String id) {
    Map data = {
      'title': title,
      'progress': progress,
      'subscriptions': subscriptions,
      'id': id,
    };
    _sprints.add(data);
    notifyListeners();
  }

  void setLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  // for creating sprints

  DateTime _start = DateTime.now();
  DateTime _end = DateTime.now();

  List _portion = [];
  String _id = '';
  bool _creating = false;

  DateTime get getStart => _start;
  DateTime get getEnd => _end;
  List get getPortion => _portion;
  String get getId => _id;
  bool get getCreating => _creating;

  void setId(String id) {
    _id = id;
    notifyListeners();
  }

  void setStart(DateTime start) {
    _start = start;
    notifyListeners();
  }

  void setEnd(DateTime end) {
    _end = end;
    notifyListeners();
  }

  void addToPortion(String text) {
    _portion.add(text);
    notifyListeners();
  }

  void removeFromPortion(String text) {
    _portion.removeWhere((element) => element == text);
    notifyListeners();
  }

  void setCreating(bool value) {
    _creating = value;
    notifyListeners();
  }

  void resetCreate() {
    _portion = [];
    _id = '';
    _creating = false;
    _start = DateTime.now();
    _end = DateTime.now();
    notifyListeners();
  }

  // modifying sprints
  DateTime _newEnd = DateTime.now();

  DateTime get getNewEnd => _newEnd;

  void setNewEnd(DateTime time) {
    _newEnd = time;
    notifyListeners();
  }

  // importing sprints
  bool _disable = false;

  bool get getDisable => _disable;

  void setDisable(bool value) {
    _disable = value;
    notifyListeners();
  }
}
