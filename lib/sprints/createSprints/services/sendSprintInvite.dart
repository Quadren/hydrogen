import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hydrogen/home/home_styles.dart';

Future<void> inviteToSprint(String email, String id) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('users');
    CollectionReference ref2 = FirebaseFirestore.instance.collection('sprints');

    final result = await ref.doc(email).get().then((value) async {
      List sprintReq = value.data()['sprintReq'];
      sprintReq.add(id);

      final result2 = await ref2.doc(id).get().then((val2) async {
        List invited = val2.data()['invited'];
        invited.add(email);

        ref.doc(email).update({'sprintReq': sprintReq});
        ref2.doc(id).update({'invited': invited});
      });

      Fluttertoast.showToast(
          msg: 'Sent request!',
          backgroundColor: HomeStyles().accentColor,
          textColor: Colors.white);
      ;
    });
  });
}
