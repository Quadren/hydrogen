import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/sprints/createSprints/createSprints.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:provider/provider.dart';

class CreateSprintServices {
  Future<void> createSprint(
    String title,
    List portion,
    String id,
    DateTime start,
    DateTime end,
    BuildContext context,
  ) async {
    Firebase.initializeApp().whenComplete(() async {
      Provider.of<SprintProvider>(context, listen: false).setCreating(true);

      CollectionReference ref =
          FirebaseFirestore.instance.collection('sprints');
      CollectionReference userRef =
          FirebaseFirestore.instance.collection('users');

      String email = Hive.box('userData').get('email');
      final result = await ref.doc(id).set({
        'title': title,
        'members': [email],
        'portion': portion,
        'owner': email,
        'log': [],
        'catenation': {'cesium': false, 'carbon': false},
        'dates': {'start': start, 'end': end},
        'invited': [],
        'subscriptions': [],
        'categories': [
          {'All': []}
        ],
        'submitted': [],
      });

      final result2 = await userRef.doc(email).get().then((value) {
        List sprints = value.data()['sprints'];
        sprints.add(id);
        userRef.doc(email).update({'sprints': sprints});
      });

      Provider.of<SprintProvider>(context, listen: false).setCreating(false);
      Provider.of<SprintProvider>(context, listen: false)
          .addSprint(title, 0, [], id);
      createSprintController.nextPage(
          duration: Duration(milliseconds: 500), curve: Curves.easeInOutCubic);
    });
  }
}
