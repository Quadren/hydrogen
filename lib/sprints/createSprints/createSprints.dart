import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/createSprints/addPortion/addPortion.dart';
import 'package:hydrogen/sprints/createSprints/addPortion/addPortionServices.dart';
import 'package:hydrogen/sprints/createSprints/dateSelector.dart';
import 'package:hydrogen/sprints/createSprints/invitePage.dart';
import 'package:hydrogen/sprints/createSprints/services/createSprintService.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:hydrogen/widgets/backBar.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:random_string/random_string.dart';

class CreateSprints extends StatefulWidget {
  @override
  _CreateSprintsState createState() => _CreateSprintsState();
}

PageController createSprintController = PageController(initialPage: 0);

class _CreateSprintsState extends State<CreateSprints> {
  TextEditingController _titleController = TextEditingController();

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      Provider.of<SprintProvider>(context, listen: false).resetCreate();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    InputDecoration _decoration = InputDecoration(
      hintText: 'Sprint Title',
      hintStyle: HomeStyles().bigText.copyWith(color: Color(0xffAAAAAA)),
      counterText: '',
      border: OutlineInputBorder(
        borderSide: BorderSide.none,
      ),
      errorBorder: OutlineInputBorder(
        borderSide: BorderSide.none,
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide.none,
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide.none,
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderSide: BorderSide.none,
      ),
      disabledBorder: OutlineInputBorder(
        borderSide: BorderSide.none,
      ),
    );

    DateTime start = Provider.of<SprintProvider>(context).getStart;
    DateTime end = Provider.of<SprintProvider>(context).getEnd;

    List portion = Provider.of<SprintProvider>(context).getPortion;

    bool loading = Provider.of<SprintProvider>(context).getCreating;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: HomeStyles().backgroundColor,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: Backbar(),
      ),
      body: ScrollConfiguration(
        behavior: AntiScrollGlow(),
        child: PageView(
          controller: createSprintController,
          physics: NeverScrollableScrollPhysics(),
          children: [
            CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: height * 0.15),
                        Container(
                          width: width * 0.6,
                          child: TextFormField(
                            maxLength: 32,
                            controller: _titleController,
                            style: HomeStyles().bigText,
                            cursorColor: HomeStyles().primaryColor,
                            decoration: _decoration,
                          ),
                        ),
                        Divider(
                          endIndent: width * 0.6,
                          indent: width * 0.03,
                          thickness: 2,
                          color: HomeStyles().primaryColor,
                        ),
                        SizedBox(height: height * 0.075),
                      ],
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      dates('start date', HomeStyles().accentColor, start, () {
                        FocusScope.of(context).requestFocus(new FocusNode());
                        showDateSelector(context, height, true);
                      }),
                      SizedBox(height: height * 0.02),
                      dates('end date', Color(0xffFF5558), end, () {
                        FocusScope.of(context).requestFocus(new FocusNode());
                        showDateSelector(context, height, false);
                      }),
                      SizedBox(height: height * 0.05),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Divider(
                              color: Color(0xffAAAAAA),
                            ),
                            SizedBox(height: height * 0.02),
                            Text(
                                'requirements\n1. Your sprint title must be at least 3 characters long.\n2. Your sprint must have at least two topics in it (part of portions).',
                                style: HomeStyles().subtitleText),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                SliverToBoxAdapter(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: height * 0.05),
                        Divider(
                          color: Color(0xffAAAAAA),
                        ),
                        SizedBox(height: height * 0.01),
                        Text('portions', style: HomeStyles().subtitleText),
                        SizedBox(height: height * 0.01),
                      ],
                    ),
                  ),
                ),
                SliverList(
                    delegate: SliverChildBuilderDelegate((context, int index) {
                  return Padding(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          padding:
                              EdgeInsets.symmetric(vertical: height * 0.01),
                          width: width * 0.7,
                          child: Text(
                            '${index + 1}. ${portion[index]}',
                            style: TextStyle(
                              fontFamily: 'Raleway',
                              fontSize: 16,
                              color: HomeStyles().primaryColor,
                              fontWeight: FontWeight.w400,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                          ),
                        ),
                        BouncingWidget(
                            child: Container(
                              height: height * 0.03,
                              width: height * 0.03,
                              child: Center(
                                child: FaIcon(
                                  FontAwesomeIcons.trash,
                                  color: HomeStyles().primaryColor,
                                  size: height * 0.015,
                                ),
                              ),
                            ),
                            onPressed: () {
                              removeFromPortion(portion[index], context);
                            },
                            duration: Duration(milliseconds: 150)),
                      ],
                    ),
                  );
                }, childCount: portion.length)),
                SliverToBoxAdapter(
                    child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: height * 0.05),
                      addButton(height, width),
                      SizedBox(height: height * 0.01),
                      Divider(
                        color: Color(0xffAAAAAA),
                      ),
                      SizedBox(height: height * 0.1),
                      Center(
                        child: AbsorbPointer(
                          absorbing: (_titleController.text.length > 3 &&
                                  portion.length > 1)
                              ? false
                              : true,
                          child: AnimatedOpacity(
                            opacity: (_titleController.text.length > 3 &&
                                    portion.length > 1)
                                ? 1
                                : 0.3,
                            duration: Duration(milliseconds: 300),
                            child: BouncingWidget(
                              child: Container(
                                height: height * 0.1,
                                width: width * 0.8,
                                decoration: BoxDecoration(
                                  color: HomeStyles().primaryColor,
                                  borderRadius: BorderRadius.circular(21),
                                ),
                                child: loading
                                    ? Center(
                                        child: CircularProgressIndicator(
                                          valueColor: AlwaysStoppedAnimation(
                                              HomeStyles().backgroundColor),
                                          backgroundColor: Colors.transparent,
                                        ),
                                      )
                                    : Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Text('Proceed to invites',
                                              style: TextStyle(
                                                  fontFamily: 'Raleway',
                                                  fontSize: 16,
                                                  color: HomeStyles()
                                                      .backgroundColor,
                                                  fontWeight: FontWeight.w600)),
                                          SizedBox(width: width * 0.1),
                                          FaIcon(
                                            FontAwesomeIcons.arrowRight,
                                            color: HomeStyles().backgroundColor,
                                            size: height * 0.025,
                                          )
                                        ],
                                      ),
                              ),
                              onPressed: () {
                                if (_titleController.text.length > 3 &&
                                    portion.length > 1 &&
                                    !loading) {
                                  if (start.day != end.day ||
                                      start.month != end.month ||
                                      start.year != end.year) {
                                    Provider.of<SprintProvider>(context,
                                            listen: false)
                                        .setId(
                                            'qsprnt.${randomAlphaNumeric(5)}');
                                    CreateSprintServices().createSprint(
                                        _titleController.text,
                                        portion,
                                        Provider.of<SprintProvider>(context,
                                                listen: false)
                                            .getId,
                                        start,
                                        end,
                                        context);
                                  } else {
                                    Fluttertoast.showToast(
                                        msg:
                                            'End date must be at least a day after the start!',
                                        backgroundColor: Colors.red[300],
                                        textColor: Colors.white);
                                  }
                                }
                              },
                              duration: Duration(milliseconds: 150),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: height * 0.02),
                    ],
                  ),
                )),
              ],
            ),
            InvitePage(),
          ],
        ),
      ),
    );
  }

  Widget dates(
      String label, Color color, DateTime dateTime, VoidCallback callback) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Align(
      alignment: Alignment.centerRight,
      child: Container(
        height: height * 0.133,
        width: width * 0.9,
        padding: EdgeInsets.symmetric(horizontal: width * 0.1),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(21), bottomLeft: Radius.circular(21)),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  label,
                  style: TextStyle(
                    fontFamily: 'Raleway',
                    fontSize: 14,
                    color: HomeStyles().backgroundColor,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(height: height * 0.01),
                Text(
                  '${DateFormat('d MMMM, y').format(dateTime)}',
                  style: TextStyle(
                    fontFamily: 'Raleway',
                    fontSize: 24,
                    color: HomeStyles().backgroundColor,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
            BouncingWidget(
              child: Container(
                height: height * 0.05,
                width: height * 0.05,
                child: Center(
                  child: FaIcon(
                    FontAwesomeIcons.calendarAlt,
                    color: HomeStyles().backgroundColor,
                    size: height * 0.03,
                  ),
                ),
              ),
              onPressed: callback,
              duration: Duration(milliseconds: 150),
            ),
          ],
        ),
      ),
    );
  }

  Widget addButton(double height, double width) {
    return BouncingWidget(
        duration: Duration(milliseconds: 150),
        child: Container(
          height: height * 0.05,
          width: width * 0.25,
          decoration: BoxDecoration(
            color: HomeStyles().primaryColor,
            borderRadius: BorderRadius.circular(15),
          ),
          child: Center(
            child: FaIcon(
              FontAwesomeIcons.plus,
              color: HomeStyles().backgroundColor,
              size: height * 0.015,
            ),
          ),
        ),
        onPressed: () {
          FocusScope.of(context).requestFocus(new FocusNode());
          showAddPortion(context);
        });
  }
}
