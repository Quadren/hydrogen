import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:provider/provider.dart';

void addToPortion(String text, BuildContext context) {
  Provider.of<SprintProvider>(context, listen: false).addToPortion(text);
  Fluttertoast.showToast(
      msg: "Added '$text' to sprint portions!",
      backgroundColor: HomeStyles().accentColor,
      textColor: Colors.white);
  Navigator.pop(context);
}

void removeFromPortion(String text, BuildContext context) {
  Provider.of<SprintProvider>(context, listen: false).removeFromPortion(text);
  Fluttertoast.showToast(
      msg: "Removed '$text' from sprint portions!",
      backgroundColor: Color(0xffff5558),
      textColor: Colors.white);
}
