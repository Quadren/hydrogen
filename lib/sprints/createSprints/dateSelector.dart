import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:provider/provider.dart';

void showDateSelector(BuildContext context, double height, bool start) async {
  final DateTime picked = await showRoundedDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2020),
      lastDate: DateTime(2025),
      borderRadius: 21,
      fontFamily: 'Raleway',
      theme: ThemeData(primarySwatch: Colors.purple),
      height: height * 0.55);

  if (picked != null) {
    if (start) {
      if (picked.isBefore(
          Provider.of<SprintProvider>(context, listen: false).getEnd)) {
        Provider.of<SprintProvider>(context, listen: false).setStart(picked);
      } else {
        Fluttertoast.showToast(
            msg: 'Start cannot be after end!',
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    } else {
      if (picked.isAfter(
          Provider.of<SprintProvider>(context, listen: false).getStart)) {
        Provider.of<SprintProvider>(context, listen: false).setEnd(picked);
      } else {
        Fluttertoast.showToast(
            msg: 'End cannot be before start!',
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    }
  }
}
