import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/friends/friendsProvider.dart';
import 'package:hydrogen/friends/services/getAllFriends.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/createSprints/services/sendSprintInvite.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:provider/provider.dart';
import 'package:random_string/random_string.dart';

class InvitePage extends StatefulWidget {
  @override
  _InvitePageState createState() => _InvitePageState();
}

class _InvitePageState extends State<InvitePage> {
  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      String email = Hive.box('userData').get('email');
      getAllFriends(email, context);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    bool loading = Provider.of<FriendsProvider>(context).getLoading;
    bool empty = Provider.of<FriendsProvider>(context).getEmpty;
    List friends = Provider.of<FriendsProvider>(context).getFriends;
    String _id = Provider.of<SprintProvider>(context).getId;

    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: width * 0.1),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: height * 0.15),
                Text(
                  'Invite\nFriends',
                  style: HomeStyles().bigText,
                ),
                SizedBox(height: height * 0.03),
                Divider(
                  color: Color(0xffAAAAAA),
                ),
                SizedBox(height: height * 0.03),
              ],
            ),
          ),
        ),
        Container(
          child: loading
              ? SliverToBoxAdapter(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: height * 0.3),
                      Transform.scale(
                        scale: 1.5,
                        child: CircularProgressIndicator(
                          strokeWidth: 7,
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Color(0xff6953F5)),
                          backgroundColor: Color(0xFFFFDDE1),
                        ),
                      ),
                    ],
                  ),
                )
              : Container(
                  child: empty
                      ? SliverToBoxAdapter(
                          child: Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: width * 0.1),
                          child: Text(
                            'no friends found :(',
                            style: HomeStyles().subtitleText,
                          ),
                        ))
                      : SliverList(
                          delegate:
                              SliverChildBuilderDelegate((context, int index) {
                          return friendWidget(friends[index]['name'],
                              friends[index]['email'], friends[index]['pfp']);
                        }, childCount: friends.length)),
                ),
        ),
        Container(
          child: loading
              ? SliverToBoxAdapter()
              : SliverToBoxAdapter(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(height: height * 0.1),
                        Container(
                          width: width,
                          height: height * 0.3,
                          decoration: BoxDecoration(
                            border: Border.all(
                                width: 3, color: HomeStyles().tertiaryAccent),
                            borderRadius: BorderRadius.circular(21),
                          ),
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text('not in your friends list?',
                                    style: HomeStyles().subtitleText),
                                SizedBox(height: height * 0.01),
                                Text(
                                  _id,
                                  style: TextStyle(
                                    fontFamily: 'Raleway',
                                    fontSize: 32,
                                    color: HomeStyles().tertiaryAccent,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                SizedBox(height: height * 0.05),
                                Text(
                                  'send this ID to others\nto allow them to join your sprint.',
                                  style: HomeStyles().buttonText2,
                                  textAlign: TextAlign.center,
                                ),
                                SizedBox(height: height * 0.02),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
        ),
      ],
    );
  }

  Widget friendWidget(String _name, String _email, String _pfp) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    bool loading = false;
    String _id = Provider.of<SprintProvider>(context).getId;

    return Container(
      width: width * 0.8,
      height: height * 0.133,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: height * 0.085,
                width: width * 0.3,
                decoration: BoxDecoration(
                    color: Color(0xffEEEEEE),
                    border: Border.all(width: 2, color: Color(0xffEEEEEE)),
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(21),
                        bottomRight: Radius.circular(21))),
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(19),
                      bottomRight: Radius.circular(19)),
                  child: CachedNetworkImage(
                    imageUrl: _pfp,
                    fit: BoxFit.cover,
                    placeholder: (context, url) {
                      return Container(
                        height: height * 0.085,
                        width: width * 0.3,
                        padding: EdgeInsets.only(right: width * 0.05),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Transform.scale(
                              scale: 0.5,
                              child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation(
                                    HomeStyles().accentColor),
                                backgroundColor: HomeStyles().secondaryAccent,
                              )),
                        ),
                      );
                    },
                  ),
                ),
              ),
              SizedBox(width: width * 0.03),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '$_name',
                    style: TextStyle(
                      fontFamily: 'Raleway',
                      fontSize: 18,
                      color: HomeStyles().primaryColor,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: height * 0.01),
                  Container(
                    width: width * 0.4,
                    child: Text(
                      '$_email',
                      style: TextStyle(
                        fontFamily: 'Raleway',
                        fontSize: 12,
                        color: Color(0xffAAAAAA),
                        fontWeight: FontWeight.w400,
                      ),
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                    ),
                  ),
                ],
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(right: width * 0.1),
            child: BouncingWidget(
              child: Align(
                alignment: Alignment.centerRight,
                child: Container(
                  height: height * 0.075,
                  width: width * 0.05,
                  child: Center(
                    child: (loading)
                        ? CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation(
                                HomeStyles().primaryColor),
                            backgroundColor: Colors.transparent,
                          )
                        : FaIcon(FontAwesomeIcons.userPlus,
                            color: HomeStyles().primaryColor,
                            size: height * 0.02),
                  ),
                ),
              ),
              onPressed: () {
                if (!loading) {
                  setState(() {
                    loading = true;
                  });
                  inviteToSprint(_email, _id).whenComplete(() {
                    setState(() {
                      loading = false;
                    });
                  });
                }
              },
              duration: Duration(milliseconds: 150),
            ),
          )
        ],
      ),
    );
  }
}
