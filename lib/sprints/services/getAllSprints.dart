import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:provider/provider.dart';

Future<void> getAllSprints(BuildContext context) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('users');
    String email = Hive.box('userData').get('email');
    CollectionReference ref2 = FirebaseFirestore.instance.collection('sprints');

    Provider.of<SprintProvider>(context, listen: false).setLoading(true);
    Provider.of<SprintProvider>(context, listen: false).resetSprints();

    try {
      final result = await ref.doc(email).get().then((value) {
        List sprints = value.data()['sprints'];
        List sprintReq = value.data()['sprintReq'];
        sprints.forEach((element) async {
          final result2 = await ref2.doc(element).get().then((val) {
            String title = val.data()['title'];
            List subscriptions = val.data()['subscriptions'];
            List portion = val.data()['portion'];
            List submitted = val.data()['submitted'];

            double progress =
                ((submitted.length / portion.length) * 100).clamp(0, 100);

            Provider.of<SprintProvider>(context, listen: false)
                .addSprint(title, progress, subscriptions, element.toString());
          });
        });

        if (sprintReq.isNotEmpty) {
          sprintReq.forEach((element) async {
            ref2.doc(element).get().then((val2) {
              String title = val2.data()['title'];
              List members = val2.data()['members'];
              String owner = val2.data()['owner'];

              Map data = {
                'title': title,
                'id': element.toString(),
                'members': members.length,
                'owner': owner
              };

              Provider.of<SprintProvider>(context, listen: false)
                  .addToInvites(data);
            });
          });
        }
      });

      Provider.of<SprintProvider>(context, listen: false).setLoading(false);
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.toString(),
          backgroundColor: Colors.red[300],
          textColor: Colors.white);
    }
  });
}
