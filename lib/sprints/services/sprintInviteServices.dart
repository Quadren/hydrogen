import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/sprints/invites/loadingSprintInvite.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class SprintInviteServices {
  Future<void> declineSprintInvite(String id, BuildContext context) async {
    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref =
          FirebaseFirestore.instance.collection('sprints');
      CollectionReference ref2 = FirebaseFirestore.instance.collection('users');
      String email = Hive.box('userData').get('email');

      try {
        final result = await ref2.doc(email).get().then((uSnap) {
          List sprintReq = uSnap.data()['sprintReq'];
          sprintReq.remove(id);
          ref2.doc(email).update({'sprintReq': sprintReq});

          Provider.of<SprintProvider>(context, listen: false)
              .removeFromInvites(id);
        });

        final result2 = await ref.doc(id).get().then((sSnap) {
          List invited = sSnap.data()['invited'];
          invited.remove(email);
          ref.doc(id).update({'invited': invited});
        });

        loadingSprintInviteController.nextPage(
            duration: Duration(milliseconds: 500),
            curve: Curves.easeInOutCubic);
        Future.delayed(Duration(seconds: 2), () {
          Navigator.pop(context);
        });
      } catch (e) {
        Fluttertoast.showToast(
            msg: e.toString(),
            textColor: Colors.white,
            backgroundColor: Colors.red[300]);
      }
    });
  }

  Future<void> acceptSprintInvite(String id, BuildContext context) async {
    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref =
          FirebaseFirestore.instance.collection('sprints');
      CollectionReference ref2 = FirebaseFirestore.instance.collection('users');
      String email = Hive.box('userData').get('email');

      try {
        final result = await ref.doc(id).get().then((sSnap) {
          List members = sSnap.data()['members'];
          List invited = sSnap.data()['invited'];

          String title = sSnap.data()['title'];
          List subscriptions = sSnap.data()['subscriptions'];
          List portion = sSnap.data()['portion'];
          List submitted = sSnap.data()['submitted'];

          double progress = ((submitted.length / portion.length) * 100)
              .toDouble()
              .clamp(0, 100);

          members.add(email);
          invited.remove(email);
          ref.doc(id).update({'members': members, 'invited': invited});

          Provider.of<SprintProvider>(context, listen: false)
              .addSprint(title, progress, subscriptions, id);
        });

        final result2 = await ref2.doc(email).get().then((uSnap) {
          List sprints = uSnap.data()['sprints'];
          List sprintReq = uSnap.data()['sprintReq'];
          List log = uSnap.data()['log'];

          DateTime now = DateTime.now();
          String date = DateFormat('yyyy-MM-dd - kk:mm').format(now);

          Map logEntry = {
            'action': 'You joined sprint with id $id.',
            'timeStamp': date
          };

          sprints.add(id);
          sprintReq.remove(id);

          Provider.of<SprintProvider>(context, listen: false)
              .removeFromInvites(id);

          ref2
              .doc(email)
              .update({'sprints': sprints, 'sprintReq': sprintReq, 'log': log});
        });

        loadingSprintInviteController.nextPage(
            duration: Duration(milliseconds: 500),
            curve: Curves.easeInOutCubic);
        Future.delayed(Duration(seconds: 2), () {
          Navigator.pop(context);
        });
      } catch (e) {
        Fluttertoast.showToast(
            msg: e.toString(),
            textColor: Colors.white,
            backgroundColor: Colors.red[300]);
      }
    });
  }
}
