import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/sprints/services/leaveSprintService.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';

void showLeaveConfirmation(BuildContext context) {
  showDialog(context: context, builder: (ctx) => LeaveSprintConfirmation());
}

class LeaveSprintConfirmation extends StatefulWidget {
  @override
  LeaveSprintConfirmationState createState() => LeaveSprintConfirmationState();
}

PageController leaveSprintController = PageController(initialPage: 0);

class LeaveSprintConfirmationState extends State<LeaveSprintConfirmation>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  @override
  void initState() {
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Sprung.criticallyDamped);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Map data = Provider.of<SprintProvider>(context).getSprintData;

    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
            height: height * 0.4,
            width: width * 0.75,
            decoration: ShapeDecoration(
                color: HomeStyles().backgroundColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(21))),
            child: PageView(
              controller: leaveSprintController,
              physics: NeverScrollableScrollPhysics(),
              children: [
                Stack(
                  children: [
                    Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            '😿',
                            style: TextStyle(
                              fontSize: 48,
                            ),
                          ),
                          SizedBox(height: height * 0.05),
                          Text(
                            'Leave sprint?',
                            style: TextStyle(
                              fontFamily: 'Raleway',
                              fontSize: 22,
                              color: HomeStyles().primaryColor,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(height: height * 0.01),
                          Text(
                            'this cannot be undone',
                            style: HomeStyles().subtitleText,
                          ),
                          SizedBox(height: height * 0.075),
                        ],
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        padding: EdgeInsets.all(width * 0.03),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            InkWell(
                              onTap: () {
                                String email =
                                    Hive.box('userData').get('email');

                                if (email == data['owner']) {
                                  Fluttertoast.showToast(
                                      toastLength: Toast.LENGTH_LONG,
                                      msg:
                                          "Owner cannot leave, you must disband the sprint through the admin console.",
                                      backgroundColor: Colors.red[300],
                                      textColor: Colors.white);
                                } else {
                                  leaveSprintController.nextPage(
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.easeInOutCubic);
                                  leaveSprintService(data['id'], context);
                                }
                              },
                              child: Container(
                                width: width * 0.33,
                                height: height * 0.065,
                                decoration: BoxDecoration(
                                  color: Colors.red[300],
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: Center(
                                  child: FaIcon(FontAwesomeIcons.check,
                                      color: HomeStyles().backgroundColor,
                                      size: height * 0.015),
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Container(
                                width: width * 0.33,
                                height: height * 0.065,
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                  border: Border.all(
                                      width: 1, color: Color(0xFFAAAAAA)),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: Center(
                                  child: FaIcon(FontAwesomeIcons.times,
                                      color: Color(0xFFAAAAAA),
                                      size: height * 0.015),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                Center(
                  child: Transform.scale(
                    scale: 1.7,
                    child: CircularProgressIndicator(
                      strokeWidth: 7,
                      valueColor:
                          AlwaysStoppedAnimation(HomeStyles().accentColor),
                      backgroundColor: HomeStyles().secondaryAccent,
                    ),
                  ),
                ),
                Center(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.height * 0.2,
                    child: Lottie.asset('assets/lottie/check.json',
                        repeat: false, fit: BoxFit.contain),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
