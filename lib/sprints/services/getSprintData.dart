import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:provider/provider.dart';

Future<void> getSprintData(String id, BuildContext context) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('sprints');
    CollectionReference ref2 =
        FirebaseFirestore.instance.collection('subscriptions');
    CollectionReference ref3 = FirebaseFirestore.instance.collection('users');

    Provider.of<SprintProvider>(context, listen: false).resetSprintData();
    Provider.of<SprintProvider>(context, listen: false).resetSubs();
    Provider.of<SprintProvider>(context, listen: false).setSprintLoading(true);

    try {
      final result = await ref.doc(id).get().then((value) {
        List subscriptions = value.data()['subscriptions'];
        List portion = value.data()['portion'];
        List members = value.data()['members'];
        String owner = value.data()['owner'];
        Map dates = value.data()['dates'];
        String title = value.data()['title'];
        List subscriptionData = [];
        List submitted = value.data()['submitted'];

        double progress = ((submitted.length / portion.length) * 100)
            .toDouble()
            .clamp(0, 100);

        print(progress);

        members.forEach((element) {
          ref3.doc(element).get().then((snapshot) {
            String pfp = snapshot.data()['pfp'];
            String name = snapshot.data()['name'];
            String email = element.toString();

            Map data = {'name': name, 'pfp': pfp, 'email': email};

            Provider.of<SprintProvider>(context, listen: false).addMember(data);
          });
        });

        if (subscriptions.isNotEmpty) {
          subscriptions.forEach((element) {
            Map data = element;
            ref2.doc(data['id']).get().then((snapshot) {
              String title = snapshot.data()['title'];
              String owner = snapshot.data()['owner'];
              List cards = snapshot.data()['cards'];

              print("Contributor: ${data['contributor']}");

              ref3.doc(data['contributor']).get().then((snap) {
                String pfp = snap.data()['pfp'];
                String name = snap.data()['name'];
                Map subData = {
                  'title': title,
                  'owner': owner,
                  'cards': cards,
                  'id': data['id'],
                  'pfp': pfp,
                  'dates': dates,
                  'contributorName': name,
                };
                subscriptionData.add(subData);
                print("This subscription has ${cards.length} cards");

                Provider.of<SprintProvider>(context, listen: false)
                    .addToSubscriptions(subData);
              });
            });
          });
        } else {
          subscriptionData = [];
          print("No subscriptions in sprint");
        }

        Map data = {
          'title': title,
          'subscriptions': subscriptionData,
          'portion': portion,
          'members': members,
          'owner': owner,
          'dates': dates,
          'progress': progress,
          'id': id.toString(),
        };

        Provider.of<SprintProvider>(context, listen: false).setSprintData(data);
      });

      Provider.of<SprintProvider>(context, listen: false)
          .setSprintLoading(false);
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.toString(),
          backgroundColor: Colors.red[300],
          textColor: Colors.white);
    }
  });
}
