import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/sprints/contribute/contributeSubscription.dart';
import 'package:hydrogen/sprints/sprint_provider.dart';
import 'package:provider/provider.dart';

class ContributionBackend {
  Future<void> getRequiredSubs(String id, BuildContext context) async {
    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref =
          FirebaseFirestore.instance.collection('sprints');

      try {
        final result = await ref.doc(id).get().then((value) {
          List submitted = value.data()['submitted'];
          List portions = value.data()['portion'];
          List requiredSubs = [];

          portions.forEach((element) {
            if (!submitted.contains(element)) {
              requiredSubs.add(element);
            }
          });

          Provider.of<SprintProvider>(context, listen: false)
              .setRequired(requiredSubs);
        });
      } catch (e) {
        Fluttertoast.showToast(
            msg: e.toString(),
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  }

  Future<void> contribute(
      String id, String subId, String submit, BuildContext context) async {
    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref =
          FirebaseFirestore.instance.collection('sprints');
      CollectionReference ref2 =
          FirebaseFirestore.instance.collection('subscriptions');

      String email = Hive.box('userData').get('email');

      try {
        print(id);
        final result = await ref.doc(id).get().then((value) {
          List submitted = value.data()['submitted'];
          List subscriptions = value.data()['subscriptions'];
          submitted.add(submit);
          Provider.of<SprintProvider>(context, listen: false)
              .removeRequired(submit);

          Map data = {'id': subId, 'contributor': email};
          subscriptions.add(data);

          List portion = value.data()['portion'];
          List members = value.data()['members'];
          String owner = value.data()['owner'];
          Map dates = value.data()['dates'];
          String title = value.data()['title'];
          List subscriptionData = [];

          double progress =
              (subscriptions.length ~/ portion.length).toDouble().clamp(0, 100);

          if (subscriptions.isNotEmpty) {
            subscriptions.forEach((element) {
              ref2.doc(element.toString()).get().then((snapshot) {
                String title = snapshot.data()['title'];
                String owner = snapshot.data()['owner'];
                List cards = snapshot.data()['cards'];

                subscriptionData.add({
                  'title': title,
                  'owner': owner,
                  'cards': cards,
                  'id': element
                });
              });
            });
          } else {
            subscriptionData = [];
            print("No subscriptions in sprint");
          }

          Map data2 = {
            'title': title,
            'subscriptions': subscriptionData,
            'portion': portion,
            'members': members,
            'owner': owner,
            'dates': dates,
            'progress': progress,
          };

          Provider.of<SprintProvider>(context, listen: false)
              .setSprintData(data2);

          ref
              .doc(id)
              .update({'submitted': submitted, 'subscriptions': subscriptions});
        });

        contributeController.nextPage(
            duration: Duration(milliseconds: 500),
            curve: Curves.easeInOutCubic);

        Future.delayed(Duration(seconds: 2), () {
          Phoenix.rebirth(context);
        });
      } catch (e) {
        Fluttertoast.showToast(
            msg: e.toString(),
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  }
}
