import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/sprints/services/leaveSprint.dart';

Future<void> leaveSprintService(String id, BuildContext context) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('sprints');
    CollectionReference ref2 = FirebaseFirestore.instance.collection('users');
    String email = Hive.box('userData').get('email');

    try {
      final result = await ref.doc(id).get().then((value) {
        List members = value.data()['members'];

        members.remove(email);
        ref.doc(id).update({'members': members});
      });

      final result2 = await ref2.doc(email).get().then((val2) {
        List sprints = val2.data()['sprints'];

        sprints.remove(id);

        ref2.doc(email).update({'sprints': sprints});
      });

      leaveSprintController.nextPage(
          duration: Duration(milliseconds: 500), curve: Curves.easeInOutCubic);
      Future.delayed(Duration(seconds: 2), () {
        Phoenix.rebirth(context);
      });
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.toString(),
          backgroundColor: Colors.red[300],
          textColor: Colors.white);
    }
  });
}
