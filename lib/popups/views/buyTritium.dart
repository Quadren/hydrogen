import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/core/premium/revenueCatServices.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/popups/views/content.dart';

class BuyTritiumPopup extends StatefulWidget {
  @override
  BuyTritiumPopupState createState() => BuyTritiumPopupState();
}

class BuyTritiumPopupState extends State<BuyTritiumPopup> {
  bool _loading = false;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Dismissible(
        key: UniqueKey(),
        child: Container(
          margin: EdgeInsets.only(bottom: height * 0.02),
          height: height * 0.085,
          width: width * 0.9,
          decoration: BoxDecoration(
            border: Border.all(width: 3, color: HomeStyles().accentColor),
            color: HomeStyles().backgroundColor,
            borderRadius: BorderRadius.circular(21),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.05),
                offset: Offset(0, -4),
                blurRadius: 13,
                spreadRadius: -1,
              ),
            ],
          ),
          child: Center(
            child: _loading
                ? Transform.scale(
                    scale: 0.7,
                    child: CircularProgressIndicator(
                      strokeWidth: 7,
                      valueColor:
                          AlwaysStoppedAnimation(HomeStyles().accentColor),
                      backgroundColor: HomeStyles().secondaryAccent,
                    ))
                : PopupContent('Update to Tritium!', FontAwesomeIcons.arrowUp,
                    () {
                    setState(() {
                      _loading = true;
                    });
                    RevenueCatServices().popupCatFetch(context);
                  }),
          ),
        ));
  }
}
