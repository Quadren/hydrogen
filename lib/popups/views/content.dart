import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/home/home_styles.dart';

class PopupContent extends StatelessWidget {
  final String _text;
  final IconData _icon;
  final VoidCallback _callback;

  PopupContent(this._text, this._icon, this._callback);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Container(
      padding: EdgeInsets.only(left: width * 0.05, right: width * 0.03),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              FaIcon(_icon,
                  size: height * 0.02, color: HomeStyles().primaryColor),
              SizedBox(width: width * 0.03),
              Text('$_text',
                  style: HomeStyles()
                      .buttonText
                      .copyWith(fontWeight: FontWeight.w600)),
            ],
          ),
          BouncingWidget(
            child: Container(
              height: height * 0.05,
              width: width * 0.2,
              decoration: BoxDecoration(
                color: (_text == 'Rate Hydrogen!')
                    ? HomeStyles().tertiaryAccent
                    : HomeStyles().accentColor,
                borderRadius: BorderRadius.circular(12),
              ),
              child: Center(
                child: FaIcon(
                  FontAwesomeIcons.arrowRight,
                  color: Colors.white,
                  size: height * 0.02,
                ),
              ),
            ),
            onPressed: _callback,
            duration: Duration(milliseconds: 150),
          )
        ],
      ),
    );
  }
}
