import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/core/providers/popupProvider.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/popups/views/content.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class FollowInsta extends StatefulWidget {
  @override
  _FollowInstaState createState() => _FollowInstaState();
}

class _FollowInstaState extends State<FollowInsta> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Dismissible(
        key: UniqueKey(),
        child: Container(
          margin: EdgeInsets.only(bottom: height * 0.02),
          height: height * 0.085,
          width: width * 0.9,
          decoration: BoxDecoration(
            border: Border.all(width: 3, color: HomeStyles().paletteColors[2]),
            color: HomeStyles().backgroundColor,
            borderRadius: BorderRadius.circular(21),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.05),
                offset: Offset(0, -4),
                blurRadius: 13,
                spreadRadius: -1,
              ),
            ],
          ),
          child: Center(
            child: PopupContent('Follow us!', FontAwesomeIcons.instagram,
                () async {
              String url = 'https://instagram.com/quadrendev';

              if (await canLaunch(url)) {
                await launch(url);
                Provider.of<PopupProvider>(context, listen: false)
                    .setShow(false);
              } else {
                Fluttertoast.showToast(
                    msg: 'Failed to launch',
                    backgroundColor: Colors.red[300],
                    textColor: Colors.white);
              }
            }),
          ),
        ));
  }
}
