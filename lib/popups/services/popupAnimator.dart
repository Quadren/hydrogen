import 'package:animated/animated.dart';
import 'package:flutter/material.dart';
import 'package:hydrogen/core/providers/popupProvider.dart';
import 'package:hydrogen/popups/services/popupSelector.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';

class PopupAnimator extends StatefulWidget {
  @override
  _PopupAnimatorState createState() => _PopupAnimatorState();
}

class _PopupAnimatorState extends State<PopupAnimator> {
  @override
  Widget build(BuildContext context) {
    bool show = Provider.of<PopupProvider>(context).show;

    return Animated(
      value: show ? 1 : 0,
      duration: Duration(milliseconds: 1500),
      curve: Sprung.criticallyDamped,
      builder: (context, child, animation) {
        return Transform.translate(
          offset: Offset(0, (1 - animation.value) * 250),
          child: child,
        );
      },
      child: PopupSelector(),
    );
  }
}
