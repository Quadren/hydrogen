import 'package:flutter/material.dart';
import 'package:hydrogen/core/providers/popupProvider.dart';
import 'package:hydrogen/popups/views/buyTritium.dart';
import 'package:hydrogen/popups/views/followInsta.dart';
import 'package:hydrogen/popups/views/rateOnStore.dart';
import 'package:provider/provider.dart';

class PopupSelector extends StatefulWidget {
  @override
  _PopupSelectorState createState() => _PopupSelectorState();
}

class _PopupSelectorState extends State<PopupSelector> {
  List<Widget> _options = [
    FollowInsta(),
    RateOnStorePopup(),
    BuyTritiumPopup(),
  ];

  @override
  Widget build(BuildContext context) {
    int _type = Provider.of<PopupProvider>(context).getType;
    return Container(
      child: _options[_type],
    );
  }
}
