import 'dart:math';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/core/providers/popupProvider.dart';
import 'package:provider/provider.dart';

void popupDecider(BuildContext context) {
  bool premium = Hive.box('userData').get('premium');
  bool showPromo = Hive.box('userData').get('showPromo');

  if (showPromo) {
    Random random = Random();

    int diceRoll = random.nextInt(4);

    if (diceRoll <= 2) {
      print("Selected");
      Provider.of<PopupProvider>(context, listen: false).setShow(true);

      if (premium) {
        int dice2 = random.nextInt(2);

        switch (dice2) {
          case 0:
            Provider.of<PopupProvider>(context, listen: false).setType(0);
            break;
          case 1:
            Provider.of<PopupProvider>(context, listen: false).setType(1);
            break;
        }
      } else {
        int dice2 = random.nextInt(3);

        switch (dice2) {
          case 0:
            Provider.of<PopupProvider>(context, listen: false).setType(0);
            break;
          case 1:
            Provider.of<PopupProvider>(context, listen: false).setType(1);
            break;
          case 2:
            Provider.of<PopupProvider>(context, listen: false).setType(2);
            break;
        }
      }
    }
  } else {
    print("Promo disabled");
  }
}
