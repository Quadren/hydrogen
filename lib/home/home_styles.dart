import 'package:flutter/material.dart';

class HomeStyles {
  // use these styles for the home page
  TextStyle headerText = TextStyle(
      fontFamily: 'Raleway',
      fontSize: 20,
      color: Color(0xFF1C1C1C),
      fontWeight: FontWeight.w400);
  TextStyle highlightedHeaderText = TextStyle(
      fontFamily: 'Raleway',
      fontSize: 32,
      color: Color(0xFFFED8C0),
      fontWeight: FontWeight.w600);

  TextStyle subtitleText = TextStyle(
      fontFamily: 'Raleway',
      fontSize: 16,
      color: Colors.grey[500],
      fontWeight: FontWeight.w400);

  TextStyle buttonText = TextStyle(
      fontFamily: 'Raleway',
      fontSize: 16,
      color: Color(0xFF1C1C1C),
      fontWeight: FontWeight.w400);

  TextStyle highlightedButtonText = TextStyle(
      fontFamily: 'Raleway',
      fontSize: 12,
      color: Color(0xFFFEFEFE),
      fontWeight: FontWeight.w600);

  TextStyle buttonText2 = TextStyle(
      fontFamily: 'Raleway',
      fontSize: 12,
      color: Color(0xFF1C1C1C),
      fontWeight: FontWeight.w400);

  TextStyle buttonText3 = TextStyle(
      fontFamily: 'Raleway',
      fontSize: 16,
      color: Color(0xFFFEFEFE),
      fontWeight: FontWeight.w400);

  TextStyle bigText = TextStyle(
      fontFamily: 'Raleway',
      fontSize: 36,
      color: Color(0xFF1C1C1C),
      fontWeight: FontWeight.w600);

  TextStyle labelText = TextStyle(
      fontFamily: 'Raleway',
      fontSize: 18,
      color: Color(0xFF1C1C1C),
      fontWeight: FontWeight.w400);

  Color accentColor = Color(0xFF6953F5);
  Color secondaryAccent = Color(0xFFFFDDE1);
  Color tertiaryAccent = Color(0xFFF6C86D);
  Color primaryColor = Color(0xFF1C1C1C);
  Color backgroundColor = Color(0xFFFEFEFE);

  List<Color> paletteColors = [
    Color(0xffFFDDE1),
    Color(0xffFFF1C4),
    Color(0xffFFC4CB),
    Color(0xffD1D8FF),
  ];
}
