import 'package:flutter/material.dart';
import 'package:hydrogen/categories/categories.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/marketplace/marketplace.dart';
import 'package:hydrogen/popups/services/popupAnimator.dart';
import 'package:hydrogen/popups/services/popupDecider.dart';
import 'package:hydrogen/popups/views/buyTritium.dart';
import 'package:hydrogen/sprints/sprints.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool _toggle = false;

  PageController _controller = PageController(initialPage: 1);
  int currentIndex = 1;

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      popupDecider(context);
    });
    _controller.addListener(() {
      if (_controller.page.round() != currentIndex) {
        setState(() {
          currentIndex = _controller.page.round();
        });
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    bool loading = Provider.of<SubscriptionProvider>(context).isLoading;

    return AbsorbPointer(
      absorbing: (loading == null) ? true : loading,
      child: Container(
        color: HomeStyles().backgroundColor,
        child: SafeArea(
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            backgroundColor: HomeStyles().backgroundColor,
            body: Stack(
              children: [
                ScrollConfiguration(
                    behavior: AntiScrollGlow(),
                    child: NestedScrollView(
                        headerSliverBuilder:
                            (context, bool isInnerBodyScrolled) {
                          return [];
                        },
                        body: PageView(
                          controller: _controller,
                          children: [
                            SprintsPage(),
                            CategoriesPage(),
                            Marketplace(),
                          ],
                        ))),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: EdgeInsets.only(bottom: height * 0.02),
                    child: Container(
                      height: height * 0.025,
                      width: width * 0.2,
                      decoration: BoxDecoration(
                        color: HomeStyles().backgroundColor,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.1),
                            blurRadius: 15,
                            spreadRadius: -1,
                          )
                        ],
                        borderRadius: BorderRadius.circular(100),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            height: height * 0.01,
                            width: height * 0.01,
                            decoration: BoxDecoration(
                                color: (currentIndex == 0)
                                    ? HomeStyles().accentColor
                                    : Color(0xffEEEEEE),
                                borderRadius: BorderRadius.circular(21)),
                          ),
                          Container(
                            height: height * 0.01,
                            width: height * 0.01,
                            decoration: BoxDecoration(
                                color: (currentIndex == 1)
                                    ? HomeStyles().accentColor
                                    : Color(0xffEEEEEE),
                                borderRadius: BorderRadius.circular(21)),
                          ),
                          Container(
                            height: height * 0.01,
                            width: height * 0.01,
                            decoration: BoxDecoration(
                                color: (currentIndex == 2)
                                    ? HomeStyles().accentColor
                                    : Color(0xffEEEEEE),
                                borderRadius: BorderRadius.circular(21)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: PopupAnimator(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
