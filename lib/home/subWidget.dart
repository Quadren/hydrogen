import 'package:animated/animated.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/individualSubscriptions/subscriptionPage.dart';
import 'package:hydrogen/subscriptions/moderation/moderators.dart';
import 'package:hydrogen/subscriptions/services/checkElevation.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class SubWidget extends StatefulWidget {
  final Map _data;
  final int _index;

  SubWidget(this._data, this._index);

  @override
  _SubWidgetState createState() => _SubWidgetState(_data, _index);
}

class _SubWidgetState extends State<SubWidget> {
  final Map _data;
  final int _index;

  _SubWidgetState(this._data, this._index);

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      Provider.of<ModeratorProvider>(context, listen: false).setLoading(false);
    });
    super.initState();
  }

  void _checkIfElevated() {
    var box = Hive.box('userData');
    String _owner = Provider.of<SubscriptionProvider>(context, listen: false)
        .getSubscriptions[_index]['owner'];

    if (_owner == box.get('email')) {
      Provider.of<ModeratorProvider>(context, listen: false).elevate(true);
      print("USER IS OWNER OF THIS SUBSCRIPTION");
      Navigator.push(
          context,
          PageTransition(
              child: SubscriptionPage(_index),
              type: PageTransitionType.rightToLeft,
              duration: Duration(milliseconds: 500),
              curve: Curves.easeInOutCubic));
    } else {
      String id = Provider.of<SubscriptionProvider>(context, listen: false)
          .getSubscriptions[_index]['id'];
      checkIfElevated(id, context);
      Navigator.push(
          context,
          PageTransition(
              child: SubscriptionPage(_index),
              type: PageTransitionType.rightToLeft,
              duration: Duration(milliseconds: 500),
              curve: Curves.easeInOutCubic));
    }
  }

  bool _animate = false;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    List cards = _data['cards'];
    int length = cards.length;

    bool loading = Provider.of<ModeratorProvider>(context).getLoading;

    return Animated(
      duration: Duration(milliseconds: 150),
      value: _animate ? 0.95 : 1,
      builder: (context, child, animation) => Transform.scale(
        scale: animation.value,
        child: child,
      ),
      child: Container(
        margin: EdgeInsets.only(bottom: height * 0.02),
        padding: EdgeInsets.all(width * 0.05),
        width: width * 0.8,
        height: height * 0.133,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(21),
          color: (_index == 0) ? HomeStyles().accentColor : Color(0xFFEEEEEE),
        ),
        child: GestureDetector(
          onTap: () {
            _checkIfElevated();
          },
          onTapDown: (details) {
            setState(() {
              _animate = true;
            });
          },
          onTapUp: (details) {
            setState(() {
              _animate = false;
            });
          },
          child: Container(
            height: height * 0.133,
            width: width * 0.8,
            color: Colors.white.withOpacity(0),
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.bottomLeft,
                  child: Container(
                    width: width * 0.6,
                    child: Text(
                      '${_data['title']}',
                      style: TextStyle(
                        fontFamily: 'Raleway',
                        fontSize: 18,
                        color: (_index == 0)
                            ? Color(0xFFFBF6F1)
                            : HomeStyles().primaryColor,
                        fontWeight: FontWeight.w600,
                      ),
                      overflow: TextOverflow.fade,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: loading
                      ? Transform.scale(
                          scale: 0.3,
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color>(
                              (_index == 0)
                                  ? Color(0xFFFBF6F1)
                                  : HomeStyles().primaryColor,
                            ),
                            backgroundColor: Colors.transparent,
                          ),
                        )
                      : Text(
                          '$length',
                          style: TextStyle(
                            fontFamily: 'Raleway',
                            fontSize: 18,
                            color: (_index == 0)
                                ? Color(0xFFFBF6F1)
                                : HomeStyles().primaryColor,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
