import 'package:auto_animated/auto_animated.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/core/userServices/userProvider.dart';
import 'package:hydrogen/dashboard/dashboard.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/home/subWidget.dart';
import 'package:hydrogen/subscriptions/addSubscriptions/addSubscriptionSheet.dart';
import 'package:hydrogen/subscriptions/services/getAllSubscriptions.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class SubList extends StatefulWidget {
  @override
  _SubListState createState() => _SubListState();
}

class _SubListState extends State<SubList>
    with AutomaticKeepAliveClientMixin<SubList> {
  final _controller = ScrollController();

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      Provider.of<SubscriptionProvider>(context, listen: false)
          .setLoading(true);
      getSubscriptions(context);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    List<Map> subscriptions =
        Provider.of<SubscriptionProvider>(context).getSubscriptions;
    bool _isEmpty = Provider.of<SubscriptionProvider>(context).isEmpty;
    bool _isLoading = Provider.of<SubscriptionProvider>(context).isLoading;

    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    String pfp = Provider.of<UserProvider>(context).getPfp;
    bool pfpLoading = Provider.of<UserProvider>(context).getLoading;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: width * 0.1),
      child: ScrollConfiguration(
        behavior: AntiScrollGlow(),
        child: CustomScrollView(
          primary: false,
          slivers: [
            SliverToBoxAdapter(
              child: Stack(
                children: [
                  Align(
                      alignment: Alignment.topRight,
                      child: Padding(
                        padding: EdgeInsets.only(top: height * 0.05),
                        child: BouncingWidget(
                          child: FaIcon(
                            FontAwesomeIcons.plus,
                            color: HomeStyles().primaryColor,
                            size: height * 0.025,
                          ),
                          onPressed: () {
                            showAddSubscriptionSheet(context);
                          },
                        ),
                      )),
                  Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                        padding: EdgeInsets.only(top: height * 0.05),
                        child: BouncingWidget(
                          child: Container(
                            height: height * 0.1,
                            width: width * 0.15,
                            decoration: BoxDecoration(
                              color: Color(0xFFEEEEEE),
                              borderRadius: BorderRadius.circular(21),
                              border: Border.all(
                                  width: 2, color: HomeStyles().accentColor),
                            ),
                            child: pfpLoading
                                ? null
                                : ClipRRect(
                                    borderRadius: BorderRadius.circular(21),
                                    child: CachedNetworkImage(
                                      imageUrl: pfp,
                                      fit: BoxFit.cover,
                                      placeholder: (context, url) {
                                        return Container(
                                          height: height * 0.1,
                                          width: width * 0.15,
                                          color: Color(0xFFEEEEEE),
                                        );
                                      },
                                    )),
                          ),
                          onPressed: () {
                            Navigator.push(
                                context,
                                PageTransition(
                                    child: Dashboard(),
                                    type: PageTransitionType.rightToLeft,
                                    curve: Curves.easeInOutCubic,
                                    duration: Duration(milliseconds: 500)));
                          },
                        ),
                      )),
                ],
              ),
            ),
            SliverToBoxAdapter(
              child: Stack(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: height * 0.125),
                      Text(
                        'Your\nSubscriptions (${subscriptions.length})',
                        style: TextStyle(
                          fontFamily: 'Raleway',
                          fontSize: 36,
                          color: HomeStyles().primaryColor,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(height: height * 0.05),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              child: _isLoading
                  ? SliverToBoxAdapter(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(height: height * 0.1),
                          Transform.scale(
                            scale: 1.5,
                            child: CircularProgressIndicator(
                              strokeWidth: 7,
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  Color(0xff6953F5)),
                              backgroundColor: Color(0xFFFFDDE1),
                            ),
                          ),
                        ],
                      ),
                    )
                  : Container(
                      child: _isEmpty
                          ? SliverToBoxAdapter(
                              child: Center(
                                child: Text(
                                  'no subscriptions!',
                                  style: HomeStyles().subtitleText,
                                ),
                              ),
                            )
                          : LiveSliverList(
                              itemBuilder: (context, index, animation) {
                                return FadeTransition(
                                  opacity: Tween<double>(begin: 0, end: 1)
                                      .animate(animation),
                                  child: SlideTransition(
                                    position: Tween<Offset>(
                                            begin: Offset(0, 0.1),
                                            end: Offset(0, 0))
                                        .animate(animation),
                                    child:
                                        SubWidget(subscriptions[index], index),
                                  ),
                                );
                              },
                              itemCount: subscriptions.length,
                              controller: _controller,
                            ),
                    ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
