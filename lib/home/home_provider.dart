import 'package:flutter/material.dart';

class HomeProvider extends ChangeNotifier {
  bool _quickSettings = false;

  bool get getQuickSettings => _quickSettings;

  void setQuick(bool value) {
    _quickSettings = value;
    notifyListeners();
  }
}
