import 'package:flutter/material.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/newLogin/pages/finalPage.dart';
import 'package:hydrogen/newLogin/pages/introPage.dart';
import 'package:hydrogen/newLogin/pages/secondPage.dart';
import 'package:hydrogen/newLogin/pages/thirdPage.dart';

class NewLogin extends StatefulWidget {
  @override
  _NewLoginState createState() => _NewLoginState();
}

PageController newLoginController = PageController(initialPage: 0);

class _NewLoginState extends State<NewLogin> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: HomeStyles().backgroundColor,
        body: ScrollConfiguration(
          behavior: AntiScrollGlow(),
          child: PageView(
            controller: newLoginController,
            physics: NeverScrollableScrollPhysics(),
            children: [
              NewLoginPageOne(),
              NewLoginPageTwo(),
              NewLoginPageThree(),
              FinalPageLoginNew(),
            ],
          ),
        ));
  }
}
