import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/core/userServices/userProvider.dart';
import 'package:hydrogen/newLogin/newLogin.dart';
import 'package:provider/provider.dart';

Future<void> checkIfUserExistsNew(
    String email, String pfp, BuildContext context) async {
  // whenComplete to make sure that for slow internets, Firebase actually initialises before executing the async functions
  Firebase.initializeApp().whenComplete(() {
    // get a reference to the users document on Firebase
    CollectionReference ref = FirebaseFirestore.instance.collection('users');
    // get reference to the userData box in the Hive database
    var box = Hive.box('userData');

    ref.doc(email).get().then((value) {
      // try to find the document of the user. If it does not exist, then create a new user
      if (value.exists) {
        print("User exists");
        box.put('email', email);
        box.put('pfp', pfp);
        Provider.of<UserProvider>(context, listen: false).setNew(false);
        newLoginController.nextPage(
            duration: Duration(milliseconds: 500),
            curve: Curves.easeInOutCubic);
      } else {
        box.put('email', email);
        box.put('pfp', pfp);
        print("New user");
        Provider.of<UserProvider>(context, listen: false).setNew(true);
        newLoginController.nextPage(
            duration: Duration(milliseconds: 500),
            curve: Curves.easeInOutCubic);
      }
    });
  });
}
