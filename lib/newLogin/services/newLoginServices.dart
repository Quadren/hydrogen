import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/core/userServices/userProvider.dart';
import 'package:provider/provider.dart';

import '../newLogin.dart';

class NewLoginServices {
  Future<void> checkIfUserExists(
      String email, String pfp, BuildContext context) async {
    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref = FirebaseFirestore.instance.collection('users');
      var box = Hive.box('userData');

      try {
        final result = await ref.doc(email).get().then((value) {
          if (value.exists) {
            print("User exists");
            String name = value.data()['name'];
            box.put('name', name);

            Provider.of<UserProvider>(context, listen: false).setNew(false);
            Provider.of<UserProvider>(context, listen: false).setName(name);
            Provider.of<UserProvider>(context, listen: false).setPfp(pfp);
          } else {
            print("New user");
            Provider.of<UserProvider>(context, listen: false).setNew(true);
          }
        });

        newLoginController.nextPage(
            duration: Duration(milliseconds: 500),
            curve: Curves.easeInOutCubic);
      } catch (e) {
        Fluttertoast.showToast(
            msg:
                'Failed to sign in. Try restarting the app, and make sure you are connected to the internet.',
            toastLength: Toast.LENGTH_LONG,
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  }

  Future<void> createNewUser(
      BuildContext context, String name, String email, String pfp) async {
    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref = FirebaseFirestore.instance.collection('users');
      var box = Hive.box('userData');

      try {
        box.put('name', name);
        box.put('email', email);
        box.put('pfp', pfp);
        box.put('premium', false);

        final result = await ref.doc(email).set({
          'name': name,
          'email': email,
          'premium': false,
          'subscriptions': [],
          'sprints': [],
          'friends': [],
          'pfp': pfp,
          'requests': [],
          'log': [],
          'reqSent': [],
          'sprintReq': [],
        });

        box.put('isSignedIn', false);

        newLoginController.nextPage(
            duration: Duration(milliseconds: 500),
            curve: Curves.easeInOutCubic);
      } catch (e) {
        Fluttertoast.showToast(
            msg:
                'Failed to sign in. Try restarting the app, and make sure you are connected to the internet.',
            toastLength: Toast.LENGTH_LONG,
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  }
}
