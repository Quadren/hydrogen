import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/auth/services/googleSignIn.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/newLogin/services/checkIfUserExists.dart';
import 'package:hydrogen/newLogin/services/newLoginServices.dart';

class NewLoginPageOne extends StatefulWidget {
  @override
  NewLoginPageOneState createState() => NewLoginPageOneState();
}

class NewLoginPageOneState extends State<NewLoginPageOne> {
  bool _loading = false;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return Stack(
      children: [
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: height * 0.2,
                width: height * 0.2,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(40),
                    image: DecorationImage(
                        image: AssetImage('assets/iconData/hydrogen.png'),
                        fit: BoxFit.cover)),
              ),
              SizedBox(height: height * 0.1),
              Container(
                width: width * 0.8,
                child: Text(
                  'Make studying collaborative.',
                  style: HomeStyles().bigText.copyWith(fontSize: 28),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: height * 0.03),
              Text(
                'Hydrogen by Quadren',
                style: HomeStyles().subtitleText,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: height * 0.05),
            ],
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: EdgeInsets.only(bottom: height * 0.03),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'this may take a bit',
                  style: HomeStyles().subtitleText,
                ),
                SizedBox(height: height * 0.02),
                BouncingWidget(
                  onPressed: () {
                    if (!_loading) {
                      setState(() {
                        _loading = true;
                      });
                      signInWithGoogle().then((value) {
                        if (value != null) {
                          print("Signed in successfuly.");
                          var box = Hive.box('userData');

                          box.put('email', email);
                          box.put('pfp', photoURL);
                          NewLoginServices()
                              .checkIfUserExists(email, photoURL, context);
                        } else {
                          Fluttertoast.showToast(
                              msg: 'Failed to sign in.',
                              backgroundColor: Colors.red[300],
                              textColor: Colors.white);
                        }
                      });
                    }
                  },
                  child: Container(
                    height: height * 0.1,
                    width: width * 0.8,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(21),
                        color: HomeStyles().accentColor),
                    child: Center(
                      child: _loading
                          ? CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation(
                                  HomeStyles().backgroundColor),
                              backgroundColor: Colors.transparent,
                            )
                          : Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                FaIcon(FontAwesomeIcons.google,
                                    color: HomeStyles().backgroundColor,
                                    size: height * 0.015),
                                SizedBox(
                                  width: width * 0.05,
                                ),
                                Text('Sign in with Google',
                                    style: HomeStyles().buttonText3),
                              ],
                            ),
                    ),
                  ),
                  duration: Duration(milliseconds: 150),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
