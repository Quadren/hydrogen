import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/core/userServices/userProvider.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/newLogin/newLogin.dart';
import 'package:hydrogen/newLogin/services/newLoginServices.dart';
import 'package:provider/provider.dart';

class NewLoginPageTwo extends StatefulWidget {
  @override
  _NewLoginPageTwoState createState() => _NewLoginPageTwoState();
}

class _NewLoginPageTwoState extends State<NewLoginPageTwo> {
  final validCharacters = RegExp(r'^[a-zA-Z0-9]+$');

  TextEditingController _controller = TextEditingController();

  void initFunc() {
    bool isNew = Provider.of<UserProvider>(context, listen: false).getNew;

    if (!isNew) {
      Future.delayed(Duration(seconds: 2), () {
        newLoginController.nextPage(
            duration: Duration(milliseconds: 500),
            curve: Curves.easeInOutCubic);
      });
      Hive.box('userData').put('isSignedIn', true);
    }
  }

  bool _loading = false;

  @override
  void initState() {
    initFunc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool newUser = Provider.of<UserProvider>(context).getNew;
    return Container(
      child: newUser
          ? nameInput()
          : Container(
              padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.1),
              child: welcomeBack(),
            ),
    );
  }

  Widget welcomeBack() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return Align(
      alignment: Alignment.centerLeft,
      child: Text(
        'Welcome\nback.',
        style: HomeStyles().bigText,
      ),
    );
  }

  Widget nameInput() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    InputDecoration decoration = InputDecoration(
      counterStyle: HomeStyles().buttonText2,
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: HomeStyles().primaryColor, width: 2),
      ),
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: HomeStyles().primaryColor, width: 2),
      ),
      border: UnderlineInputBorder(
        borderSide: BorderSide(color: HomeStyles().primaryColor, width: 2),
      ),
    );

    return Stack(
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: width * 0.1),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: height * 0.1),
              Text('Your name is...', style: HomeStyles().headerText),
              SizedBox(height: height * 0.02),
              Container(
                width: width * 0.6,
                child: TextFormField(
                  controller: _controller,
                  maxLength: 24,
                  decoration: decoration,
                  style: HomeStyles().bigText,
                  cursorColor: HomeStyles().accentColor,
                ),
              ),
              SizedBox(height: height * 0.03),
              Container(
                width: width * 0.7,
                child: Text(
                  'minimum 3 characters long, only alphanumerics (A-Z, a-z, 0-9). No spaces',
                  style: HomeStyles().subtitleText.copyWith(fontSize: 12),
                ),
              ),
              SizedBox(height: height * 0.2),
            ],
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: EdgeInsets.only(bottom: height * 0.03),
            child: BouncingWidget(
              child: Container(
                height: height * 0.1,
                width: height * 0.1,
                decoration: BoxDecoration(
                  color: HomeStyles().accentColor,
                  borderRadius: BorderRadius.circular(21),
                ),
                child: Center(
                  child: _loading
                      ? CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation(
                              HomeStyles().backgroundColor),
                          backgroundColor: Colors.transparent,
                        )
                      : FaIcon(
                          FontAwesomeIcons.arrowRight,
                          color: HomeStyles().backgroundColor,
                          size: height * 0.025,
                        ),
                ),
              ),
              onPressed: () {
                if (!_loading) {
                  if (validCharacters.hasMatch(_controller.text) &&
                      _controller.text.length >= 3) {
                    print("Valid");
                    setState(() {
                      _loading = true;
                    });
                    var box = Hive.box('userData');

                    String email = box.get('email');
                    String pfp = box.get('pfp');

                    NewLoginServices()
                        .createNewUser(context, _controller.text, email, pfp);
                  } else if (_controller.text.length < 3) {
                    Fluttertoast.showToast(
                        msg: 'Minimum length of name is 3 characters!',
                        backgroundColor: Colors.red[300],
                        textColor: Colors.white);
                  } else {
                    Fluttertoast.showToast(
                        msg: 'Invalid characters detected!',
                        backgroundColor: Colors.red[300],
                        textColor: Colors.white);
                  }
                }
              },
              duration: Duration(milliseconds: 150),
            ),
          ),
        ),
      ],
    );
  }
}
