import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/newLogin/newLogin.dart';
import 'package:url_launcher/url_launcher.dart';

class NewLoginPageThree extends StatefulWidget {
  @override
  _NewLoginPageThreeState createState() => _NewLoginPageThreeState();
}

class _NewLoginPageThreeState extends State<NewLoginPageThree> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            'A Guide to\nHydrogen',
            style: HomeStyles().bigText,
            textAlign: TextAlign.center,
          ),
          SizedBox(height: height * 0.03),
          Text(
            'you can also find this in the dashboard',
            style: HomeStyles().subtitleText,
          ),
          SizedBox(height: height * 0.2),
          BouncingWidget(
            child: Container(
              height: height * 0.1,
              width: width * 0.6,
              decoration: BoxDecoration(
                  color: HomeStyles().accentColor,
                  borderRadius: BorderRadius.circular(21)),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    FaIcon(
                      FontAwesomeIcons.globeAsia,
                      color: HomeStyles().backgroundColor,
                      size: height * 0.025,
                    ),
                    SizedBox(width: width * 0.05),
                    Text(
                      'Open tutorial',
                      style: HomeStyles().buttonText3,
                    )
                  ],
                ),
              ),
            ),
            onPressed: () async {
              String url = 'https://quadrendev.gitbook.io/hydrogen-1/';

              if (await canLaunch(url)) {
                await launch(url);
              } else {
                Fluttertoast.showToast(
                    msg: 'Failed to launch URL',
                    backgroundColor: Colors.red[300],
                    textColor: Colors.white);
              }
            },
            duration: Duration(milliseconds: 150),
          ),
          SizedBox(height: height * 0.04),
          BouncingWidget(
            child: Text(
              'skip tutorial',
              style: HomeStyles().subtitleText,
            ),
            onPressed: () {
              Hive.box('userData').put('isSignedIn', true);
              newLoginController.nextPage(
                  duration: Duration(milliseconds: 500),
                  curve: Curves.easeInOutCubic);
            },
            duration: Duration(milliseconds: 150),
          ),
        ],
      ),
    );
  }
}
