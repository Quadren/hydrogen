import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:lottie/lottie.dart';

class FinalPageLoginNew extends StatefulWidget {
  @override
  _FinalPageLoginNewState createState() => _FinalPageLoginNewState();
}

class _FinalPageLoginNewState extends State<FinalPageLoginNew> {
  @override
  void initState() {
    Future.delayed(Duration(seconds: 2), () {
      Phoenix.rebirth(context);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: MediaQuery.of(context).size.height * 0.2,
        width: MediaQuery.of(context).size.height * 0.2,
        child: Lottie.asset('assets/lottie/check.json',
            repeat: false, fit: BoxFit.contain),
      ),
    );
  }
}
