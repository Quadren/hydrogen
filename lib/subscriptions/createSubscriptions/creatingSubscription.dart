import 'package:flutter/material.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/services/createSubscriptionServices.dart';
import 'package:lottie/lottie.dart';
import 'package:sprung/sprung.dart';

void showCreatingSubscription(BuildContext context, String title, int pin, bool private) {
  showDialog(context: context, builder: (_) => CreatingSubscription(pin, private, title), barrierDismissible: false);
}

class CreatingSubscription extends StatefulWidget {

  final String _title;
  final int _pin;
  final bool _private;

  CreatingSubscription(this._pin, this._private, this._title);

  @override
  _CreatingSubscriptionState createState() => _CreatingSubscriptionState(_title, _private, _pin);
}

PageController createController = PageController(initialPage: 0);

class _CreatingSubscriptionState extends State<CreatingSubscription> with TickerProviderStateMixin{
  AnimationController controller;
  Animation<double> scaleAnimation;

  final String _title;
  final int _pin;
  final bool _private;

  _CreatingSubscriptionState(this._title, this._private, this._pin);

  @override
  void initState() {

    CreateSubscriptionServices().getInfoToCreateSubscription(_title, _pin, _private, context);

    super.initState();
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Sprung.criticallyDamped);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
            height: height * 0.5,
            width: width * 0.75,
            decoration: ShapeDecoration(
                color: HomeStyles().backgroundColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(21))),
            child: PageView(
              controller: createController,
              physics: NeverScrollableScrollPhysics(),
              children: [
                Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Transform.scale(
                    scale: 1.5,
                    child: CircularProgressIndicator(
                      strokeWidth: 7,
                    valueColor: AlwaysStoppedAnimation<Color>(HomeStyles().accentColor),
                    backgroundColor: HomeStyles().secondaryAccent,
                  ),
                  ),
                  SizedBox(height: height * 0.15),
                  Text('Creating subscription', style: HomeStyles().buttonText, textAlign: TextAlign.center),
                  SizedBox(height: height * 0.01),
                  Text('writing to the interwebs...', style: HomeStyles().subtitleText, textAlign: TextAlign.center,),
                ],
              ),
            ),
            Center(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.height * 0.2,
                    child: Lottie.asset('assets/lottie/check.json',
                        repeat: false, fit: BoxFit.contain),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}