import 'package:animated/animated.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/core/misc/shakeAnim.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/createSubscriptions/creatingSubscription.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class CreateSubscriptions extends StatefulWidget {
  @override
  _CreateSubscriptionsState createState() => _CreateSubscriptionsState();
}

class _CreateSubscriptionsState extends State<CreateSubscriptions>
    with SingleTickerProviderStateMixin {
  final _formKey1 = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();

  TextEditingController _controller1 = TextEditingController();
  TextEditingController _controller2 = TextEditingController();

  PageController _pageController = PageController(initialPage: 0);
  ShakeController _shakeController;

  String _title;
  int _pin;
  bool _private = false;

  bool _animate1 = false;
  bool _animate2 = false;

  @override
  void initState() {
    super.initState();
    _shakeController = ShakeController(vsync: this);
    Future.delayed(Duration(milliseconds: 100), () {
      setState(() {
        _animate1 = true;
      });
    });
    Future.delayed(Duration(milliseconds: 600), () {
      setState(() {
        _animate2 = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: HomeStyles().backgroundColor,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: HomeStyles().primaryColor),
        leading: InkWell(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          onTap: () => Navigator.pop(context),
          child: Container(
            margin: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.05,
                top: MediaQuery.of(context).size.height * 0.03),
            decoration: BoxDecoration(
              color: HomeStyles().backgroundColor,
              borderRadius: BorderRadius.circular(100),
            ),
            child: Center(
                child: FaIcon(
              FontAwesomeIcons.arrowLeft,
              color: HomeStyles().primaryColor,
              size: MediaQuery.of(context).size.height * 0.025,
            )),
          ),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: width * 0.1),
        child: Stack(
          children: [
            NestedScrollView(
                headerSliverBuilder: (context, bool isInnerBoxScrolled) {
                  return [
                    SliverToBoxAdapter(
                      child: Animated(
                        value: _animate1 ? 1 : 0,
                        duration: Duration(seconds: 1),
                        curve: Curves.easeInOutCubic,
                        builder: (context, child, animation) =>
                            Transform.translate(
                          offset: Offset(0, (1 - animation.value) * 40),
                          child:
                              Opacity(opacity: animation.value, child: child),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: height * 0.2),
                            Text('Create\nDeck', style: HomeStyles().bigText),
                            SizedBox(height: height * 0.05),
                            Divider(color: Color(0xFFAAAAAA)),
                            SizedBox(height: height * 0.05),
                          ],
                        ),
                      ),
                    ),
                  ];
                },
                body: Animated(
                  value: _animate2 ? 1 : 0,
                  builder: (context, child, animation) => Transform.translate(
                    offset: Offset(0, (1 - animation.value) * 40),
                    child: Opacity(opacity: animation.value, child: child),
                  ),
                  duration: Duration(seconds: 1),
                  curve: Curves.easeInOutCubic,
                  child: PageView(
                    physics: NeverScrollableScrollPhysics(),
                    controller: _pageController,
                    children: [
                      pageOne(),
                      pageTwo(),
                      pageThree(),
                    ],
                  ),
                )),
            Align(
              alignment: Alignment.bottomRight,
              child: Padding(
                  padding: EdgeInsets.only(bottom: width * 0.1),
                  child: InkWell(
                    onTap: () {
                      if (_pageController.page == 0) {
                        if (_formKey1.currentState.validate()) {
                          setState(() {
                            _title = _controller1.text;
                          });
                          _pageController.nextPage(
                              duration: Duration(milliseconds: 500),
                              curve: Curves.easeInOutCubic);
                        }
                      } else if (_pageController.page == 1) {
                        if (_controller2.text.length < 6) {
                          _shakeController.shake();
                        } else {
                          setState(() {
                            _pin = int.parse(_controller2.text);
                          });
                          _pageController.nextPage(
                              duration: Duration(milliseconds: 500),
                              curve: Curves.easeInOutCubic);
                        }
                      } else {
                        showCreatingSubscription(
                            context, _title, _pin, _private);
                      }
                    },
                    child: Container(
                      width: width * 0.9,
                      height: height * 0.1,
                      decoration: BoxDecoration(
                        color: (_controller1.text.length > 4)
                            ? HomeStyles().accentColor
                            : Color(0xFFAAAAAAA),
                        borderRadius: BorderRadius.circular(21),
                      ),
                      child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Continue',
                              style: TextStyle(
                                fontFamily: 'Raleway',
                                fontSize: 20,
                                color: HomeStyles().backgroundColor,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            SizedBox(width: width * 0.05),
                            FaIcon(FontAwesomeIcons.arrowRight,
                                color: HomeStyles().backgroundColor,
                                size: height * 0.02),
                          ],
                        ),
                      ),
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }

  Widget pageOne() {
    var size = MediaQuery.of(context).size;
    double height = size.height;

    OutlineInputBorder _border = OutlineInputBorder(
      borderRadius: BorderRadius.circular(21),
      borderSide: BorderSide(width: 1, color: HomeStyles().primaryColor),
    );

    OutlineInputBorder _errorBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(21),
      borderSide: BorderSide(width: 1, color: Colors.red[300]),
    );

    InputDecoration _formDecor = InputDecoration(
      border: _border,
      errorBorder: _errorBorder,
      disabledBorder: _border,
      enabledBorder: _border,
      focusedBorder: _border,
      contentPadding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.05,
          vertical: MediaQuery.of(context).size.height * 0.04),
      errorStyle: TextStyle(
        fontFamily: 'Raleway',
        fontSize: 12,
        color: Colors.red[300],
        fontWeight: FontWeight.w400,
      ),
    );

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("what's this deck called?", style: HomeStyles().subtitleText),
        SizedBox(height: height * 0.03),
        Form(
          key: _formKey1,
          child: TextFormField(
            controller: _controller1,
            validator: (value) {
              if (value.length > 4) {
                return null;
              } else {
                return "Title too short!";
              }
            },
            maxLength: 24,
            decoration: _formDecor,
            cursorColor: HomeStyles().primaryColor,
            style: HomeStyles().headerText,
          ),
        ),
      ],
    );
  }

  Widget pageTwo() {
    var size = MediaQuery.of(context).size;
    double height = size.height;

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("set a password", style: HomeStyles().subtitleText),
        SizedBox(height: height * 0.03),
        Form(
            key: _formKey2,
            child: ShakeView(
              controller: _shakeController,
              child: PinCodeTextField(
                controller: _controller2,
                appContext: context,
                length: 6,
                onChanged: null,
                cursorColor: Colors.transparent,
                animationType: AnimationType.slide,
                enablePinAutofill: false,
                keyboardType: TextInputType.number,
                enableActiveFill: false,
                textStyle: TextStyle(
                  fontFamily: 'Raleway',
                  fontSize: 24,
                  color: HomeStyles().primaryColor,
                  fontWeight: FontWeight.w600,
                ),
                onCompleted: (value) {},
                pinTheme: PinTheme(
                  shape: PinCodeFieldShape.underline,
                  borderRadius: null,
                  activeColor: HomeStyles().accentColor,
                  disabledColor: HomeStyles().primaryColor,
                  selectedColor: HomeStyles().primaryColor,
                  inactiveColor: HomeStyles().primaryColor.withOpacity(0.3),
                ),
              ),
            )),
      ],
    );
  }

  Widget pageThree() {
    var size = MediaQuery.of(context).size;
    double height = size.height;

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("deck settings", style: HomeStyles().subtitleText),
        SizedBox(height: height * 0.03),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('private', style: HomeStyles().buttonText),
            Switch(
              value: _private,
              onChanged: (value) {
                setState(() {
                  _private = value;
                });
              },
              activeColor: HomeStyles().accentColor,
            )
          ],
        ),
      ],
    );
  }
}
