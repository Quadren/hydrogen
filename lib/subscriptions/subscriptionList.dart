import 'package:flutter/material.dart';
import 'package:hydrogen/subscriptions/subscriptionWidget.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:provider/provider.dart';
import './subscriptions_shared.dart' as shared;

class SubscriptionList extends StatefulWidget {
  @override
  _SubscriptionListState createState() => _SubscriptionListState();
}

class _SubscriptionListState extends State<SubscriptionList> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    int _length = Provider.of<SubscriptionProvider>(context).getNumberOfSubscriptions;
    return PageView.builder(
      controller: shared.subscriptionController,
      scrollDirection: Axis.vertical,
      physics: AlwaysScrollableScrollPhysics(),
      itemBuilder: (context, int index) {
        return SubscriptionWidget(index);
      },
      itemCount: _length,
    );
  }
}