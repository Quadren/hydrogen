import 'package:animated/animated.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/core/misc/removeAnim.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/core/misc/sharePage.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/quiz/quiz.dart';
import 'package:hydrogen/subscriptions/addCards/addCards.dart';
import 'package:hydrogen/subscriptions/changeCategory.dart';
import 'package:hydrogen/subscriptions/individualSubscriptions/actionsSheet.dart';
import 'package:hydrogen/subscriptions/individualSubscriptions/cardWidget.dart';
import 'package:hydrogen/subscriptions/individualSubscriptions/subscriptionSettings.dart';
import 'package:hydrogen/subscriptions/moderation/moderators.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';

class SubscriptionPage extends StatefulWidget {
  final int _index;

  SubscriptionPage(this._index);

  @override
  _SubscriptionPageState createState() => _SubscriptionPageState(_index);
}

class _SubscriptionPageState extends State<SubscriptionPage> {
  final int _index;

  _SubscriptionPageState(this._index);

  ScrollController _scrollController = ScrollController();

  String _title;
  String _owner;
  String _id;

  bool _animate = false;

  void _getData() {
    String email = Hive.box('userData').get('email');
    Map tempMap = Provider.of<SubscriptionProvider>(context, listen: false)
        .getSubscriptions[_index];

    setState(() {
      _title = tempMap['title'];
      _owner = (tempMap['owner'] == email) ? 'you' : tempMap['owner'];
      _id = tempMap['id'];
    });

    print("User now viewing subscription with title '$_title'");
  }

  @override
  void initState() {
    _scrollController.addListener(() {
      if (_scrollController.position.userScrollDirection ==
          ScrollDirection.reverse) {
        if (_animate) {
          setState(() {
            _animate = false;
          });
        }
      } else if (_scrollController.position.userScrollDirection ==
          ScrollDirection.forward) {
        if (!_animate) {
          setState(() {
            _animate = true;
          });
        }
      }
    });
    Future.delayed(Duration.zero, () {
      Provider.of<ModeratorProvider>(context, listen: false).setLoading(false);
      _getData();
    });
    Future.delayed(Duration(milliseconds: 500), () {
      setState(() {
        _animate = true;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.removeListener(() {});
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    bool elevated = Provider.of<ModeratorProvider>(context).getElevation;

    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Scaffold(
      backgroundColor: HomeStyles().backgroundColor,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: HomeStyles().primaryColor),
        leading: InkWell(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          onTap: () => Navigator.pop(context),
          child: Container(
            margin: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.05,
                top: MediaQuery.of(context).size.height * 0.03),
            decoration: BoxDecoration(
              color: HomeStyles().backgroundColor,
              borderRadius: BorderRadius.circular(100),
            ),
            child: Center(
                child: FaIcon(
              FontAwesomeIcons.arrowLeft,
              color: HomeStyles().primaryColor,
              size: MediaQuery.of(context).size.height * 0.025,
            )),
          ),
        ),
      ),
      body: Stack(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: width * 0.075),
            child: ScrollConfiguration(
              behavior: AntiScrollGlow(),
              child: CustomScrollView(
                controller: _scrollController,
                slivers: [
                  SliverToBoxAdapter(
                    child: Stack(
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: height * 0.2),
                            Container(
                              width: width * 0.6,
                              child:
                                  Text('$_title', style: HomeStyles().bigText),
                            ),
                            SizedBox(height: height * 0.01),
                            Container(
                              width: width * 0.8,
                              child: Text('owned by $_owner',
                                  style: HomeStyles().subtitleText),
                            ),
                            SizedBox(height: height * 0.01),
                            Text(
                              'id: $_id',
                              style: HomeStyles().subtitleText,
                            ),
                            SizedBox(height: height * 0.05),
                            actions(),
                            SizedBox(height: height * 0.05),
                            Divider(
                              color: Color(0xFFAAAAAA),
                            ),
                            SizedBox(height: height * 0.05),
                          ],
                        ),
                      ],
                    ),
                  ),
                  CardWidget(_index, elevated),
                  SliverToBoxAdapter(
                    child: SizedBox(height: height * 0.05),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      floatingActionButtonAnimator: NoScalingAnimation(),
      floatingActionButton: actionButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Widget actionButton() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return Animated(
        value: _animate ? 1 : 0,
        duration: Duration(milliseconds: 1500),
        curve: Sprung.criticallyDamped,
        builder: (context, child, animation) {
          return Transform.translate(
            offset: Offset(0, (1 - animation.value) * 200),
            child: child,
          );
        },
        child: BouncingWidget(
          child: Container(
              height: height * 0.085,
              width: width * 0.4,
              decoration: BoxDecoration(
                  color: HomeStyles().accentColor,
                  borderRadius: BorderRadius.circular(100),
                  boxShadow: [
                    BoxShadow(
                      color: HomeStyles().accentColor.withOpacity(0.4),
                      offset: Offset(0, 14),
                      blurRadius: 15,
                      spreadRadius: -3,
                    )
                  ]),
              child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.05),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      FaIcon(FontAwesomeIcons.bars,
                          color: HomeStyles().backgroundColor,
                          size: height * 0.015),
                      SizedBox(width: width * 0.05),
                      Text('Actions', style: HomeStyles().buttonText3),
                    ],
                  ))),
          onPressed: () {
            showActionsSheet(context, _index);
          },
          duration: Duration(milliseconds: 150),
        ));
  }

  Widget actions() {
    List tempList = Provider.of<SubscriptionProvider>(context).getSubscriptions;
    Map tempMap = tempList[_index];
    String categoryLabel = tempMap['category'];

    Map categories = Provider.of<CategoriesProvider>(context).getCategories;
    Map category = categories[categoryLabel];
    Color categoryColor = category['color'];

    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(
          onTap: () {
            showChangeCategory(context, _index);
          },
          child: Container(
            height: height * 0.075,
            decoration: BoxDecoration(
              color: Color(0xFFEEEEEE),
              borderRadius: BorderRadius.circular(12),
            ),
            child: Center(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 0.05),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ClipOval(
                      child: Container(
                        height: height * 0.0125,
                        width: height * 0.0125,
                        color: categoryColor,
                      ),
                    ),
                    SizedBox(width: width * 0.03),
                    Text(
                      '$categoryLabel',
                      style: TextStyle(
                        fontFamily: 'Raleway',
                        color: HomeStyles().primaryColor,
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        SizedBox(width: width * 0.03),
        BouncingWidget(
          duration: Duration(milliseconds: 200),
          onPressed: () {
            Navigator.push(
                context,
                PageTransition(
                    child: SubscriptionSettings(_index),
                    type: PageTransitionType.rightToLeft,
                    duration: Duration(milliseconds: 500),
                    curve: Curves.easeInOutCubic));
          },
          child: Container(
            height: height * 0.075,
            width: height * 0.075,
            decoration: BoxDecoration(
              color: Color(0xFFEEEEEE),
              borderRadius: BorderRadius.circular(12),
            ),
            child: Center(
              child: FaIcon(FontAwesomeIcons.cog,
                  color: HomeStyles().primaryColor, size: height * 0.02),
            ),
          ),
        ),
        SizedBox(width: width * 0.03),
        GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                PageTransition(
                    child: ShareSubPage(_index),
                    type: PageTransitionType.rightToLeft,
                    duration: Duration(milliseconds: 500),
                    curve: Curves.easeInOutCubic));
          },
          child: Container(
            height: height * 0.075,
            width: height * 0.075,
            decoration: BoxDecoration(
              color: HomeStyles().accentColor,
              borderRadius: BorderRadius.circular(12),
            ),
            child: Center(
              child: FaIcon(FontAwesomeIcons.shareAlt,
                  color: HomeStyles().backgroundColor, size: height * 0.02),
            ),
          ),
        ),
      ],
    );
  }

  Widget floatingButton() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return BouncingWidget(
      child: Container(
        height: height * 0.1,
        width: width * 0.15,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(21),
          color: HomeStyles().accentColor,
        ),
        child: Center(
            child: FaIcon(FontAwesomeIcons.plus,
                color: HomeStyles().backgroundColor, size: height * 0.02)),
      ),
      onPressed: () {
        if (Provider.of<ModeratorProvider>(context, listen: false)
            .getElevation) {
          Navigator.push(
              context,
              PageTransition(
                  child: AddCards(_index),
                  type: PageTransitionType.rightToLeft,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.easeInOutCubic));
        } else {
          Fluttertoast.showToast(
              msg: 'This is a restricted area!',
              backgroundColor: Colors.red[300],
              textColor: Colors.white);
        }
      },
      duration: Duration(milliseconds: 150),
    );
  }
}
