import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/addCards/catexPreview.dart';
import 'package:hydrogen/subscriptions/individualSubscriptions/removeCard.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:provider/provider.dart';
import 'package:vibration/vibration.dart';

class CardWidget extends StatelessWidget {
  final int _index;
  final bool _elevated;

  CardWidget(this._index, this._elevated);

  @override
  Widget build(BuildContext context) {
    Map subscription =
        Provider.of<SubscriptionProvider>(context).getSubscriptions[_index];
    List cards = subscription['cards'];

    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return SliverGrid(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        childAspectRatio: (width * 0.4) / (height * 0.3),
        crossAxisCount: 2,
      ),
      delegate: SliverChildBuilderDelegate(
        (context, int index) {
          String front = cards[index]['front'];
          String back = cards[index]['back'];
          String eqn = cards[index]['eqn'];
          return GestureDetector(
            onLongPress: () {
              if (_elevated) {
                print("Prompt delete");
                Vibration.vibrate(amplitude: 1, duration: 50);
                showRemoveCard(
                    context, _index, index, {'front': front, 'back': back});
              }
            },
            child: FlipCard(
                front: Container(
                  padding: EdgeInsets.all(width * 0.05),
                  margin: EdgeInsets.all(width * 0.03),
                  decoration: BoxDecoration(
                    color: Color(0xFFEEEEEE),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Stack(
                    children: [
                      Align(
                        alignment: Alignment.bottomLeft,
                        child: Container(
                          child: Text(
                            front,
                            overflow: TextOverflow.fade,
                            style: TextStyle(
                              fontFamily: 'Raleway',
                              fontSize: 12,
                              color: HomeStyles().primaryColor,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.topRight,
                        child: Text(
                          '${index + 1}',
                          overflow: TextOverflow.fade,
                          style: TextStyle(
                            fontFamily: 'Raleway',
                            fontSize: 12,
                            color: Color(0xFFAAAAAA),
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                back: Container(
                  padding: EdgeInsets.all(width * 0.05),
                  margin: EdgeInsets.all(width * 0.03),
                  decoration: BoxDecoration(
                    color: HomeStyles().accentColor,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Stack(
                    children: [
                      Align(
                        alignment: Alignment.topLeft,
                        child: (eqn == null || eqn == 'none')
                            ? null
                            : GestureDetector(
                                onTap: () {
                                  showCatex(eqn, context);
                                },
                                child: Container(
                                  height: height * 0.05,
                                  width: height * 0.05,
                                  decoration: BoxDecoration(
                                    color: HomeStyles().backgroundColor,
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  child: Center(
                                    child: FaIcon(
                                      FontAwesomeIcons.equals,
                                      color: HomeStyles().accentColor,
                                      size: height * 0.015,
                                    ),
                                  ),
                                ),
                              ),
                      ),
                      Align(
                        alignment: Alignment.bottomLeft,
                        child: Container(
                          child: Text(
                            back,
                            overflow: TextOverflow.fade,
                            style: TextStyle(
                              fontFamily: 'Raleway',
                              fontSize: 12,
                              color: HomeStyles().backgroundColor,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.topRight,
                        child: Text(
                          '${index + 1}',
                          overflow: TextOverflow.fade,
                          style: TextStyle(
                            fontFamily: 'Raleway',
                            fontSize: 12,
                            color: HomeStyles().backgroundColor,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                    ],
                  ),
                )),
          );
        },
        childCount: cards.length,
      ),
    );
  }
}
