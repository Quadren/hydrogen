import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/services/removeCardServices.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';

void showRemoveCard(BuildContext context, int index, int cardIndex, Map card) {
  showDialog(
      context: context,
      builder: (ctx) => RemoveCardPopup(index, card, cardIndex));
}

class RemoveCardPopup extends StatefulWidget {
  final int _index;
  final Map _card;
  final int _cardIndex;

  RemoveCardPopup(this._index, this._card, this._cardIndex);

  @override
  _RemoveCardPopupState createState() =>
      _RemoveCardPopupState(_index, _card, _cardIndex);
}

PageController removeCardController = PageController(initialPage: 0);

class _RemoveCardPopupState extends State<RemoveCardPopup>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;
  final int _index;
  final Map _card;
  final int _cardIndex;

  _RemoveCardPopupState(this._index, this._card, this._cardIndex);

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      Provider.of<SubscriptionProvider>(context, listen: false)
          .setRemoving(false);
    });
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Sprung.criticallyDamped);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    bool removing = Provider.of<SubscriptionProvider>(context).getRemoving;
    String id = Provider.of<SubscriptionProvider>(context)
        .getSubscriptions[_index]['id'];

    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
            height: height * 0.4,
            width: width * 0.75,
            decoration: ShapeDecoration(
                color: HomeStyles().backgroundColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(21))),
            child: PageView(
              controller: removeCardController,
              physics: NeverScrollableScrollPhysics(),
              children: [
                Stack(
                  children: [
                    Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            '🗑',
                            style: TextStyle(
                              fontSize: 48,
                            ),
                          ),
                          SizedBox(height: height * 0.05),
                          Text(
                            'Remove card?',
                            style: TextStyle(
                              fontFamily: 'Raleway',
                              fontSize: 22,
                              color: HomeStyles().primaryColor,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(height: height * 0.01),
                          Text(
                            'this cannot be undone',
                            style: HomeStyles().subtitleText,
                          ),
                          SizedBox(height: height * 0.075),
                        ],
                      ),
                    ),
                    AbsorbPointer(
                      absorbing: removing ? true : false,
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          padding: EdgeInsets.all(width * 0.03),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              InkWell(
                                onTap: () {
                                  Provider.of<SubscriptionProvider>(context,
                                          listen: false)
                                      .setRemoving(true);
                                  RemoveCardServices().removeCard(
                                      _card, id, _index, _cardIndex, context);
                                },
                                child: Container(
                                  width: width * 0.33,
                                  height: height * 0.065,
                                  decoration: BoxDecoration(
                                    color: Colors.red[300],
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Center(
                                    child: removing
                                        ? Transform.scale(
                                            scale: 0.5,
                                            child: CircularProgressIndicator(
                                              valueColor:
                                                  AlwaysStoppedAnimation(
                                                      HomeStyles()
                                                          .backgroundColor),
                                              backgroundColor:
                                                  Colors.transparent,
                                            ),
                                          )
                                        : FaIcon(FontAwesomeIcons.check,
                                            color: HomeStyles().backgroundColor,
                                            size: height * 0.015),
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Container(
                                  width: width * 0.33,
                                  height: height * 0.065,
                                  decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    border: Border.all(
                                        width: 1, color: Color(0xFFAAAAAA)),
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Center(
                                    child: FaIcon(FontAwesomeIcons.times,
                                        color: Color(0xFFAAAAAA),
                                        size: height * 0.015),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Center(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.height * 0.2,
                    child: Lottie.asset('assets/lottie/check.json',
                        repeat: false, fit: BoxFit.contain),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
