import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/log/studyLog.dart';
import 'package:hydrogen/subscriptions/log/subscriptionLog.dart';
import 'package:hydrogen/subscriptions/moderation/moderators.dart';
import 'package:hydrogen/subscriptions/services/unsubscribe.dart';
import 'package:hydrogen/subscriptions/subscribers/subscribers.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class SubscriptionSettings extends StatefulWidget {
  final int _index;

  SubscriptionSettings(this._index);

  @override
  _SubscriptionSettingsState createState() =>
      _SubscriptionSettingsState(_index);
}

class _SubscriptionSettingsState extends State<SubscriptionSettings> {
  final int _index;

  _SubscriptionSettingsState(this._index);

  List<String> _labels = [
    'Moderators',
    'Subscribers',
    'Activity Log',
    'Study Log',
  ];
  List<IconData> _icons = [
    FontAwesomeIcons.userShield,
    FontAwesomeIcons.users,
    FontAwesomeIcons.clipboardList,
    FontAwesomeIcons.laughBeam,
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<VoidCallback> _callbacks = [
      () {
        Navigator.push(
            context,
            PageTransition(
                child: Moderators(_index),
                type: PageTransitionType.rightToLeft,
                curve: Curves.easeInOutCubic,
                duration: Duration(milliseconds: 500)));
      },
      () {
        Navigator.push(
            context,
            PageTransition(
                child: Subscribers(_index),
                type: PageTransitionType.rightToLeft,
                curve: Curves.easeInOutCubic,
                duration: Duration(milliseconds: 500)));
      },
      () {
        Navigator.push(
            context,
            PageTransition(
                child: SubscriptionLog(_index),
                type: PageTransitionType.rightToLeft,
                curve: Curves.easeInOutCubic,
                duration: Duration(milliseconds: 500)));
      },
      () {
        bool isElevated =
            Provider.of<ModeratorProvider>(context, listen: false).getElevation;

        if (isElevated) {
          Navigator.push(
              context,
              PageTransition(
                  child: StudyLog(_index),
                  type: PageTransitionType.rightToLeft,
                  curve: Curves.easeInOutCubic,
                  duration: Duration(milliseconds: 500)));
        } else {
          Fluttertoast.showToast(
              msg: 'This is a restricted area!',
              backgroundColor: Colors.red[300],
              textColor: Colors.white);
        }
      },
    ];

    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: HomeStyles().primaryColor),
        leading: InkWell(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          onTap: () => Navigator.pop(context),
          child: Container(
            margin: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.05,
                top: MediaQuery.of(context).size.height * 0.03),
            decoration: BoxDecoration(
              color: HomeStyles().backgroundColor,
              borderRadius: BorderRadius.circular(100),
            ),
            child: Center(
                child: FaIcon(
              FontAwesomeIcons.arrowLeft,
              color: HomeStyles().primaryColor,
              size: MediaQuery.of(context).size.height * 0.025,
            )),
          ),
        ),
      ),
      backgroundColor: HomeStyles().backgroundColor,
      body: Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.1),
          child: Stack(
            children: [
              CustomScrollView(
                primary: false,
                slivers: [
                  SliverToBoxAdapter(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: height * 0.2),
                        Text('Deck\nSettings', style: HomeStyles().bigText),
                        SizedBox(height: height * 0.03),
                        Divider(
                          color: Color(0xFFAAAAAA),
                        ),
                        SizedBox(height: height * 0.03),
                      ],
                    ),
                  ),
                  SliverGrid(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: ((width * 0.4) / (height * 0.2)),
                    ),
                    delegate: SliverChildBuilderDelegate((context, int index) {
                      return submenu(
                          _labels[index], _icons[index], _callbacks[index]);
                    }, childCount: 4),
                  ),
                ],
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: BouncingWidget(
                  duration: Duration(milliseconds: 200),
                  onPressed: () {
                    showUnsubscribeConfirmation(context, _index);
                  },
                  child: Container(
                    margin: EdgeInsets.only(bottom: height * 0.03),
                    width: width,
                    height: height * 0.1,
                    decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Colors.red[300]),
                      borderRadius: BorderRadius.circular(21),
                    ),
                    child: Center(
                      child: Text('Unsubscribe',
                          style: TextStyle(
                            fontFamily: 'Raleway',
                            fontSize: 18,
                            color: Colors.red[300],
                            fontWeight: FontWeight.w400,
                          )),
                    ),
                  ),
                ),
              ),
            ],
          )),
    );
  }

  Widget submenu(String label, IconData icon, VoidCallback callback) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return BouncingWidget(
        child: Container(
          padding: EdgeInsets.all(width * 0.05),
          margin: EdgeInsets.all(width * 0.02),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(21),
            color: HomeStyles().backgroundColor,
            border: Border.all(width: 1, color: Color(0xFFAAAAAA)),
          ),
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topRight,
                child: FaIcon(icon,
                    color: HomeStyles().primaryColor, size: height * 0.015),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Text(
                  label,
                  style: HomeStyles().buttonText,
                  overflow: TextOverflow.fade,
                ),
              ),
            ],
          ),
        ),
        onPressed: callback);
  }
}
