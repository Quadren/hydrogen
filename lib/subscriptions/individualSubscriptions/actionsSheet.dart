import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/quiz/quiz.dart';
import 'package:hydrogen/subscriptions/addCards/addCards.dart';
import 'package:hydrogen/subscriptions/moderation/moderators.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

void showActionsSheet(BuildContext context, int index) {
  showModalBottomSheet(
      context: context,
      builder: (ctx) => ActionsSheet(index),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(21), topRight: Radius.circular(21)),
      ));
}

class ActionsSheet extends StatefulWidget {
  final int _index;

  ActionsSheet(this._index);
  @override
  _ActionsSheetState createState() => _ActionsSheetState(_index);
}

class _ActionsSheetState extends State<ActionsSheet> {
  final int _index;

  _ActionsSheetState(this._index);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    Map data =
        Provider.of<SubscriptionProvider>(context).getSubscriptions[_index];

    List cards = data['cards'];

    bool elevated = Provider.of<ModeratorProvider>(context).getElevation;

    return Container(
      height: height * 0.15,
      decoration: BoxDecoration(
        color: HomeStyles().backgroundColor,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(21), topRight: Radius.circular(21)),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          BouncingWidget(
              child: Container(
                height: height * 0.075,
                width: width * 0.4,
                decoration: BoxDecoration(
                  color: elevated
                      ? HomeStyles().tertiaryAccent
                      : Color(0xffAAAAAA),
                  borderRadius: BorderRadius.circular(100),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      FaIcon(FontAwesomeIcons.plus,
                          color: HomeStyles().backgroundColor,
                          size: height * 0.015),
                      Text(
                        'New card',
                        style: HomeStyles().buttonText3,
                      )
                    ],
                  ),
                ),
              ),
              onPressed: () {
                if (elevated) {
                  Navigator.pop(context);
                  Navigator.push(
                      context,
                      PageTransition(
                          child: AddCards(_index),
                          type: PageTransitionType.rightToLeft,
                          curve: Curves.easeInOutCubic,
                          duration: Duration(milliseconds: 500)));
                } else {
                  Fluttertoast.showToast(
                      msg: 'This needs moderator permissions!',
                      backgroundColor: Colors.red[300],
                      textColor: Colors.white);
                }
              }),
          BouncingWidget(
              child: Container(
                height: height * 0.075,
                width: width * 0.4,
                decoration: BoxDecoration(
                  color: HomeStyles().accentColor,
                  borderRadius: BorderRadius.circular(100),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      FaIcon(FontAwesomeIcons.bookOpen,
                          color: HomeStyles().backgroundColor,
                          size: height * 0.015),
                      Text(
                        'Study',
                        style: HomeStyles().buttonText3,
                      )
                    ],
                  ),
                ),
              ),
              onPressed: () {
                if (cards.length > 0) {
                  Navigator.pop(context);
                  Navigator.push(
                      context,
                      PageTransition(
                          child: QuizPage(cards, _index),
                          type: PageTransitionType.rightToLeft,
                          duration: Duration(milliseconds: 500),
                          curve: Curves.easeInOutCubic));
                } else {
                  Fluttertoast.showToast(
                      msg: 'There are no cards in this subscription!',
                      backgroundColor: Colors.red[300],
                      textColor: Colors.white);
                }
              })
        ],
      ),
    );
  }
}
