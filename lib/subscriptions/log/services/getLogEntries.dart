import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hydrogen/subscriptions/log/log_provider.dart';
import 'package:provider/provider.dart';

class GetLogEntries {
  Future<void> getSubscriptionLog(String id, BuildContext context) async {
    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref =
          FirebaseFirestore.instance.collection('subscriptions');

      Provider.of<LogProvider>(context, listen: false).setLoading(true);
      Provider.of<LogProvider>(context, listen: false).clearActivity();

      final result = await ref.doc(id).get().then((value) {
        if (value.exists) {
          List activity = value.data()['log'];
          Provider.of<LogProvider>(context, listen: false)
              .setActivity(activity);
          Provider.of<LogProvider>(context, listen: false).setLoading(false);
        } else {
          Fluttertoast.showToast(
              msg: 'ERROR 001',
              backgroundColor: Colors.red[300],
              textColor: Colors.white);
        }
      });
    });
  }

  Future<void> getStudyLog(String id, BuildContext context) async {
    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref =
          FirebaseFirestore.instance.collection('subscriptions');

      Provider.of<LogProvider>(context, listen: false).setLoading(true);
      Provider.of<LogProvider>(context, listen: false).clearActivity();

      final result = await ref.doc(id).get().then((value) {
        if (value.exists) {
          List activity = value.data()['studyLog'];
          Provider.of<LogProvider>(context, listen: false)
              .setStudyLog(activity);
          Provider.of<LogProvider>(context, listen: false).setLoading(false);
        } else {
          Fluttertoast.showToast(
              msg: 'ERROR 001',
              backgroundColor: Colors.red[300],
              textColor: Colors.white);
        }
      });
    });
  }
}
