import 'package:flutter/cupertino.dart';

class LogProvider extends ChangeNotifier {
  bool _loading = true;
  List _activity = [];
  List _studyLog = [];

  bool get getLoading => _loading;
  List get getActivity => _activity;
  List get getStudyLog => _studyLog;

  void setLoading(bool value) {
    _loading = value;
    notifyListeners();
  }

  void setActivity(List data) {
    _activity = data;
    notifyListeners();
  }

  void setStudyLog(List data) {
    _studyLog = data;
    notifyListeners();
  }

  void clearActivity() {
    _activity = [];
    notifyListeners();
  }

  void cleraStudy() {
    _studyLog = [];
    notifyListeners();
  }
}
