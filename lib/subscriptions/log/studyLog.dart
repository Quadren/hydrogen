import 'package:auto_animated/auto_animated.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/log/log_provider.dart';
import 'package:hydrogen/subscriptions/log/services/getLogEntries.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:provider/provider.dart';

class StudyLog extends StatefulWidget {
  final int _index;

  StudyLog(this._index);

  @override
  _StudyLogState createState() => _StudyLogState(_index);
}

class _StudyLogState extends State<StudyLog> {
  final int _index;

  _StudyLogState(this._index);

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      Provider.of<LogProvider>(context, listen: false).setLoading(true);
      String id = Provider.of<SubscriptionProvider>(context, listen: false)
          .getSubscriptions[_index]['id'];
      GetLogEntries().getStudyLog(id, context);
    });
    super.initState();
  }

  ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    bool _loading = Provider.of<LogProvider>(context).getLoading;
    List _activity =
        Provider.of<LogProvider>(context).getStudyLog.reversed.toList();

    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          iconTheme: IconThemeData(color: HomeStyles().primaryColor),
          leading: InkWell(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onTap: () => Navigator.pop(context),
            child: Container(
              margin: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.05,
                  top: MediaQuery.of(context).size.height * 0.03),
              decoration: BoxDecoration(
                color: HomeStyles().backgroundColor,
                borderRadius: BorderRadius.circular(100),
              ),
              child: Center(
                  child: FaIcon(
                FontAwesomeIcons.arrowLeft,
                color: HomeStyles().primaryColor,
                size: MediaQuery.of(context).size.height * 0.025,
              )),
            ),
          ),
        ),
        backgroundColor: HomeStyles().backgroundColor,
        body: ScrollConfiguration(
            behavior: AntiScrollGlow(),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.1),
              child: CustomScrollView(
                controller: _scrollController,
                primary: false,
                slivers: [
                  SliverToBoxAdapter(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: height * 0.2),
                        Text('Study\nLog', style: HomeStyles().bigText),
                        SizedBox(height: height * 0.03),
                        Divider(
                          color: Color(0xFFAAAAAA),
                        ),
                        SizedBox(height: height * 0.03),
                      ],
                    ),
                  ),
                  Container(
                      child: _loading
                          ? SliverToBoxAdapter(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(height: height * 0.1),
                                  Transform.scale(
                                    scale: 1.5,
                                    child: CircularProgressIndicator(
                                      strokeWidth: 7,
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          Color(0xff6953F5)),
                                      backgroundColor: Color(0xFFFFDDE1),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : LiveSliverList(
                              itemBuilder: (context, int index,
                                  Animation<double> animation) {
                                return FadeTransition(
                                  opacity: Tween<double>(begin: 0, end: 1)
                                      .animate(animation),
                                  child: SlideTransition(
                                      position: Tween<Offset>(
                                              begin: Offset(0, 0.1),
                                              end: Offset(0, 0))
                                          .animate(animation),
                                      child: logWidget(
                                          _activity[index]['action'],
                                          _activity[index]['timeStamp'],
                                          height,
                                          width)),
                                );
                              },
                              itemCount: _activity.length,
                              controller: _scrollController)),
                  SliverToBoxAdapter(
                    child: _loading
                        ? null
                        : Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(height: height * 0.05),
                              Text(
                                "the beginning of the world...\nno wait, the deck",
                                textAlign: TextAlign.center,
                                style: HomeStyles().subtitleText,
                              ),
                              SizedBox(height: height * 0.05),
                            ],
                          ),
                  ),
                ],
              ),
            )));
  }

  Widget logWidget(
      String action, String timeStamp, double height, double width) {
    return FlipCard(
        direction: FlipDirection.VERTICAL,
        front: Container(
          height: height * 0.1,
          width: width * 0.8,
          margin: EdgeInsets.symmetric(vertical: height * 0.01),
          decoration: BoxDecoration(
            color: Color(0xffEEEEEE),
            borderRadius: BorderRadius.circular(21),
          ),
          padding: EdgeInsets.symmetric(horizontal: width * 0.05),
          child: Center(
            child: Container(
              height: height * 0.1,
              width: width * 0.8,
              color: Colors.white.withOpacity(0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: width * 0.5,
                    child: Text(
                      action,
                      style: TextStyle(
                        fontFamily: 'Raleway',
                        fontSize: 12,
                        color: HomeStyles().primaryColor,
                        fontWeight: FontWeight.w400,
                      ),
                      overflow: TextOverflow.fade,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        back: Container(
          height: height * 0.1,
          width: width * 0.8,
          margin: EdgeInsets.symmetric(vertical: height * 0.01),
          decoration: BoxDecoration(
            color: HomeStyles().accentColor,
            borderRadius: BorderRadius.circular(21),
          ),
          padding: EdgeInsets.symmetric(horizontal: width * 0.05),
          child: Center(
            child: Container(
              height: height * 0.1,
              width: width * 0.8,
              color: Colors.white.withOpacity(0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: width * 0.5,
                    child: Text(
                      'time stamp:\n\n$timeStamp',
                      style: TextStyle(
                        fontFamily: 'Raleway',
                        fontSize: 12,
                        color: HomeStyles().backgroundColor,
                        fontWeight: FontWeight.w600,
                      ),
                      overflow: TextOverflow.fade,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
