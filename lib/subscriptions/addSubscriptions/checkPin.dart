import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hydrogen/subscriptions/addSubscriptions/addToUserSubscriptions.dart';
import 'package:hydrogen/subscriptions/addSubscriptions/importFromWeb.dart';
import 'package:provider/provider.dart';
import 'add_shared.dart' as shared;

Future<void> checkPin(int pin, String id, BuildContext context) async {
  Firebase.initializeApp().whenComplete(() {
    CollectionReference ref = FirebaseFirestore.instance.collection('subscriptions');
    
    ref.doc(id).get().then((value) {
      if (value.exists) {
        if (value.data()['password'] == pin) {
          print ("CORRECT PIN");
          Provider.of<ImportProvider>(context, listen: false).setDisabled(false);
          addToUserSubscriptions(id, value.data()['title'], value.data()['cards'], value.data()['owner'], context);
          shared.controller.animateToPage(3, duration: Duration(milliseconds: 500), curve: Curves.easeInOutCubic);
        } else {
          print ("WRONG PIN");
          Provider.of<ImportProvider>(context, listen: false).setDisabled(false);
          shared.shakeController.shake();
        }
      }
     });
  });
}