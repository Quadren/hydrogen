import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

Future<void> addToUserSubscriptions(String id, String title, List cards,
    String owner, BuildContext context) async {
  Firebase.initializeApp().whenComplete(() {
    var box = Hive.box('userData');
    CollectionReference ref = FirebaseFirestore.instance.collection('users');
    CollectionReference ref2 =
        FirebaseFirestore.instance.collection('subscriptions');

    print("Adding to user ${box.get('email')}");

    ref.doc(box.get('email')).get().then((value) {
      if (value.exists) {
        List tempList = value.data()['subscriptions'];
        List log = value.data()['log'];
        DateTime now = DateTime.now();
        String date = DateFormat('yyyy-MM-dd - kk:mm').format(now);

        Map logEntry = {
          'action': 'You joined subscription $id!',
          'timeStamp': date
        };

        tempList.add(id);
        log.add(logEntry);
        ref
            .doc(box.get('email'))
            .update({'subscriptions': tempList, 'log': log});

        ref2.doc(id).get().then((val2) {
          if (val2.exists) {
            List log = val2.data()['log'];

            Map logEntry = {
              'action': 'User ${box.get('email')} joined this subscription.',
              'timeStamp': date,
            };

            List tempList = val2.data()['users'];
            log.add(logEntry);
            tempList.add(box.get('email'));
            ref2.doc(id).update({'users': tempList, 'log': log});
            print("Successfully added to user's subscriptions");
            Provider.of<SubscriptionProvider>(context, listen: false)
                .addSubscription(title, owner, cards, id, 'Uncategorized');
            Provider.of<SubscriptionProvider>(context, listen: false)
                .setEmpty(false);
          } else {
            Fluttertoast.showToast(
                msg: 'ERROR 001',
                backgroundColor: Colors.red[300],
                textColor: Colors.white);
          }
        });
      } else {
        Fluttertoast.showToast(
            msg: 'ERROR 002',
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  });
}

Future<void> getDataForPublicSubscriptio(
    String id, BuildContext context) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref =
        FirebaseFirestore.instance.collection('subscriptions');

    // ignore: unused_local_variable
    final result = await ref.doc(id).get().then((value) {
      if (value.exists) {
        addToUserSubscriptions(id, value.data()['title'], value.data()['cards'],
            value.data()['owner'], context);
      } else {
        Fluttertoast.showToast(
            msg: 'ERROR 001',
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  });
}
