import 'package:flutter/material.dart';
import 'package:hydrogen/core/misc/shakeAnim.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/addSubscriptions/checkPage.dart';
import 'package:hydrogen/subscriptions/addSubscriptions/checkPin.dart';
import 'package:hydrogen/subscriptions/addSubscriptions/importFromWeb.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';
import 'add_shared.dart' as shared;

class PinPage extends StatefulWidget {
  @override
  _PinPageState createState() => _PinPageState();
}

class _PinPageState extends State<PinPage> with SingleTickerProviderStateMixin {
  final _key = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    shared.shakeController = ShakeController(vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double width = size.width;
    double height = size.height;
    bool _disable = Provider.of<ImportProvider>(context).getDisabled;
    return ClipRRect(
      borderRadius: BorderRadius.circular(21),
      child: shared.protected
          ? Stack(
              children: [
                Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: height * 0.05),
                      Text('PIN Code', style: HomeStyles().subtitleText),
                      SizedBox(height: height * 0.05),
                      AnimatedOpacity(
                        opacity: _disable ? 0.5 : 1,
                        duration: Duration(milliseconds: 300),
                        child: AbsorbPointer(
                            absorbing: _disable,
                            child: ShakeView(
                                child: Form(
                                  key: UniqueKey(),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: width * 0.03),
                                    child: Form(
                                      key: _key,
                                      child: PinCodeTextField(
                                        appContext: context,
                                        length: 6,
                                        onChanged: null,
                                        cursorColor: Colors.transparent,
                                        animationType: AnimationType.slide,
                                        enablePinAutofill: false,
                                        keyboardType: TextInputType.number,
                                        enableActiveFill: false,
                                        textStyle: TextStyle(
                                          fontFamily: 'Raleway',
                                          fontSize: 24,
                                          color: HomeStyles().primaryColor,
                                          fontWeight: FontWeight.w600,
                                        ),
                                        onCompleted: (value) {
                                          print(value);
                                          Provider.of<ImportProvider>(context,
                                                  listen: false)
                                              .setDisabled(true);
                                          checkPin(
                                              int.parse(value),
                                              Provider.of<ImportProvider>(
                                                      context,
                                                      listen: false)
                                                  .getID,
                                              context);
                                        },
                                        pinTheme: PinTheme(
                                          shape: PinCodeFieldShape.underline,
                                          borderRadius: null,
                                          activeColor: Color(0xff6953F5),
                                          disabledColor:
                                              HomeStyles().primaryColor,
                                          selectedColor:
                                              HomeStyles().primaryColor,
                                          inactiveColor: HomeStyles()
                                              .primaryColor
                                              .withOpacity(0.3),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                controller: shared.shakeController)),
                      ),
                      SizedBox(height: height * 0.05),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: _disable
                      ? LinearProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Color(0xff6953F5)),
                          backgroundColor: Color(0xFFFFDDE1),
                        )
                      : null,
                ),
              ],
            )
          : CheckPage(),
    );
  }
}
