import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/subscriptions/addSubscriptions/addToUserSubscriptions.dart';
import 'package:hydrogen/subscriptions/addSubscriptions/importFromWeb.dart';
import 'package:provider/provider.dart';
import 'add_shared.dart' as shared;

Future<void> getSubscriptionUsingID(String id, BuildContext context) async {
  Firebase.initializeApp().whenComplete(() {
    CollectionReference ref =
        FirebaseFirestore.instance.collection('subscriptions');
    CollectionReference ref2 = FirebaseFirestore.instance.collection('users');
    var box = Hive.box('userData');

    ref.doc(id).get().then((value) {
      if (value.exists) {
        print("Found subscription with ID $id");
        ref2.doc(box.get('email')).get().then((val2) {
          if (val2.exists) {
            List templist = val2.data()['subscriptions'];
            if (templist.contains(id)) {
              print ("User already subscribed");
              shared.shakeController.shake();
              Fluttertoast.showToast(msg: "You're already subscribed to this!", backgroundColor: Colors.red[300], textColor: Colors.white);
              Provider.of<ImportProvider>(context, listen: false).setDisabled(false);
            } else {
              if (value.data()['public']) {
                Provider.of<ImportProvider>(context, listen: false)
                    .setProtected(false);
                getDataForPublicSubscriptio(id, context).whenComplete(() {
                  shared.protected = false;
                  shared.controller.animateToPage(2, duration: Duration(milliseconds: 500), curve: Curves.easeInOutCubic);
                });
              } else {
                Provider.of<ImportProvider>(context, listen: false)
                    .setProtected(true);
                print("PASSWORD PROTECTED");
                shared.controller.animateToPage(2, duration: Duration(milliseconds: 500), curve: Curves.easeInOutCubic);
                shared.protected = true;
              }
            }
          }
        });
        Provider.of<ImportProvider>(context, listen: false).setID(id);
        Provider.of<ImportProvider>(context, listen: false).setDisabled(false);
        // shared.controller.animateToPage(2,
        //     duration: Duration(milliseconds: 500), curve: Curves.easeInOutCubic);
      } else {
        print("Not found");
        Provider.of<ImportProvider>(context, listen: false).setDisabled(false);
        shared.shakeController.shake();
      }
    });
  });
}


