import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/core/premium/revenueCatServices.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/addSubscriptions/checkPage.dart';
import 'package:hydrogen/subscriptions/addSubscriptions/importFromWeb.dart';
import 'package:hydrogen/subscriptions/addSubscriptions/pinPage.dart';
import 'package:hydrogen/subscriptions/createSubscriptions/createSubscriptions.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';
import 'package:vibration/vibration.dart';
import './add_shared.dart' as shared;

void showAddSubscriptionSheet(BuildContext context) {
  showDialog(context: context, builder: (_) => AddSubscriptionSheet());
}

class AddSubscriptionSheet extends StatefulWidget {
  @override
  _AddSubscriptionSheetState createState() => _AddSubscriptionSheetState();
}

class _AddSubscriptionSheetState extends State<AddSubscriptionSheet>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  bool _pressed1 = false;
  bool _pressed2 = false;

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, () {
      Provider.of<ImportProvider>(context, listen: false).setDisabled(false);
    });

    setState(() {
      shared.scale = false;
      shared.scale2 = false;
      _loading = false;
    });

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Sprung.criticallyDamped);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  void dispose() {
    super.dispose();
  }

  bool _loading = false;

  @override
  Widget build(BuildContext context) {
    int subs =
        Provider.of<SubscriptionProvider>(context).getSubscriptions.length;

    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: AnimatedContainer(
            duration: Duration(milliseconds: 500),
            curve: Curves.easeInOutCubic,
            height: (Hive.box('userData').get('premium') == false && subs >= 20)
                ? height * 0.6
                : height * 0.35,
            width: shared.scale2 ? width * 0.9 : width * 0.75,
            decoration: ShapeDecoration(
                color: HomeStyles().backgroundColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(21))),
            child: Stack(
              children: [
                Center(
                  child: (subs < 20)
                      ? PageView(
                          physics: NeverScrollableScrollPhysics(),
                          controller: shared.controller,
                          children: [
                            pageOne(),
                            ImportFromWeb(),
                            PinPage(),
                            CheckPage(),
                          ],
                        )
                      : Container(
                          child: (Hive.box('userData').get('premium') == false)
                              ? freeLimit(height, width)
                              : PageView(
                                  physics: NeverScrollableScrollPhysics(),
                                  controller: shared.controller,
                                  children: [
                                    pageOne(),
                                    ImportFromWeb(),
                                    PinPage(),
                                    CheckPage(),
                                  ],
                                ),
                        ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget freeLimit(double height, double width) {
    return Container(
      child: Center(
        child: Stack(
          children: [
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text('😕', style: TextStyle(fontSize: 48)),
                  SizedBox(height: height * 0.03),
                  Text('Free limit reached', style: HomeStyles().headerText),
                  SizedBox(height: height * 0.02),
                  Container(
                    width: width * 0.6,
                    child: Text(
                      'you can only have 20 synced subscriptions for free',
                      style: HomeStyles().subtitleText,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: height * 0.1),
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: EdgeInsets.only(
                    bottom: height * 0.02,
                    left: height * 0.02,
                    right: height * 0.02),
                child: BouncingWidget(
                  child: Container(
                    height: height * 0.1,
                    width: width * 0.75,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(18),
                        gradient: LinearGradient(
                            colors: [
                              HomeStyles().tertiaryAccent,
                              HomeStyles().paletteColors[2]
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight)),
                    child: Center(
                      child: _loading
                          ? CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation(
                                  HomeStyles().backgroundColor),
                              backgroundColor: Colors.transparent,
                            )
                          : Text(
                              'Get Tritium',
                              style: HomeStyles().buttonText3,
                            ),
                    ),
                  ),
                  onPressed: () {
                    if (!_loading) {
                      setState(() {
                        _loading = true;
                      });
                      RevenueCatServices().revenueCatFetch(context);
                    }
                  },
                  duration: Duration(milliseconds: 150),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget pageOne() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(height: height * 0.05),
        Text('Tap and hold to select', style: HomeStyles().subtitleText),
        SizedBox(height: height * 0.05),
        buttons(),
      ],
    );
  }

  Widget buttons() {
    void moveToNext() {
      shared.controller.animateToPage(1,
          duration: Duration(milliseconds: 500), curve: Curves.easeInOutCubic);
    }

    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Container(
      width: width * 0.6,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          GestureDetector(
            onTapDown: (details) {
              setState(() {
                _pressed1 = true;
                _pressed2 = false;
              });
            },
            onTapUp: (details) {
              setState(() {
                _pressed1 = false;
                _pressed2 = false;
              });
            },
            onLongPress: () {
              setState(() {
                _pressed1 = true;
                _pressed2 = false;
              });
              Vibration.vibrate(amplitude: 1, duration: 50);
              Navigator.pop(context);
              Navigator.push(
                  context,
                  PageTransition(
                      child: CreateSubscriptions(),
                      type: PageTransitionType.rightToLeft,
                      curve: Curves.easeInOutCubic,
                      duration: Duration(milliseconds: 500)));
            },
            child: AnimatedContainer(
              duration: Duration(milliseconds: 150),
              height: height * 0.2,
              width: width * 0.27,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: _pressed1
                    ? Color(0xff6953F5)
                    : HomeStyles().backgroundColor,
                border: _pressed1
                    ? null
                    : Border.all(width: 1, color: Colors.grey[200]),
                boxShadow: _pressed1
                    ? [
                        BoxShadow(
                          color: Color(0xff6953F5).withOpacity(1),
                          offset: Offset(0, 12),
                          spreadRadius: -5,
                          blurRadius: 13,
                        )
                      ]
                    : null,
              ),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    FaIcon(
                      FontAwesomeIcons.plus,
                      color: _pressed1
                          ? HomeStyles().backgroundColor
                          : HomeStyles().primaryColor,
                      size: height * 0.015,
                    ),
                    SizedBox(height: height * 0.03),
                    Text(
                      'Create\nnew',
                      style: _pressed1
                          ? HomeStyles().highlightedButtonText
                          : HomeStyles().buttonText2,
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
              ),
            ),
          ),
          GestureDetector(
            onTapDown: (details) {
              setState(() {
                _pressed1 = false;
                _pressed2 = true;
              });
            },
            onTapUp: (details) {
              setState(() {
                _pressed1 = false;
                _pressed2 = false;
              });
            },
            onLongPress: () {
              setState(() {
                _pressed1 = false;
                _pressed2 = true;
              });
              Vibration.vibrate(amplitude: 1, duration: 50);
              moveToNext();
            },
            child: AnimatedContainer(
              duration: Duration(milliseconds: 150),
              height: height * 0.2,
              width: width * 0.27,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: _pressed2
                    ? Color(0xff6953F5)
                    : HomeStyles().backgroundColor,
                border: _pressed2
                    ? null
                    : Border.all(width: 1, color: Colors.grey[200]),
                boxShadow: _pressed2
                    ? [
                        BoxShadow(
                          color: Color(0xff6953F5).withOpacity(1),
                          offset: Offset(0, 12),
                          spreadRadius: -5,
                          blurRadius: 13,
                        )
                      ]
                    : null,
              ),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    FaIcon(
                      FontAwesomeIcons.wifi,
                      color: _pressed2
                          ? HomeStyles().backgroundColor
                          : HomeStyles().primaryColor,
                      size: height * 0.015,
                    ),
                    SizedBox(height: height * 0.03),
                    Text(
                      'Import from\nweb',
                      style: _pressed2
                          ? HomeStyles().highlightedButtonText
                          : HomeStyles().buttonText2,
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
