import 'package:flutter/material.dart';
import 'package:hydrogen/core/misc/shakeAnim.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/addSubscriptions/getSubscription.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';
import 'add_shared.dart' as shared;

class ImportProvider extends ChangeNotifier {
  bool _disable = false;
  bool _protected = false;
  String _id;
  int _pin;
  Map<String, dynamic> _data = {};

  bool get getDisabled => _disable;
  bool get getProtected => _protected;
  Map<String, dynamic> get getData => _data;
  String get getID => _id;
  int get getPin => _pin;

  void setDisabled(bool value) {
    _disable = value;
    notifyListeners();
  }

  void setPin(int data) {
    _pin = data;
    notifyListeners();
  }

  void setProtected(bool value) {
    _protected = value;
    notifyListeners();
  }

  void setData(String title, int length, String owner) {
    _data = {'title': title, 'length': length, 'owner': owner};
    notifyListeners();
  }

  void setID(String id) {
    _id = id;
    notifyListeners();
  }
}

class ImportFromWeb extends StatefulWidget {
  @override
  _ImportFromWebState createState() => _ImportFromWebState();
}

class _ImportFromWebState extends State<ImportFromWeb>
    with SingleTickerProviderStateMixin {
  final _key = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    shared.shakeController = ShakeController(vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double width = size.width;
    double height = size.height;
    bool _disable = Provider.of<ImportProvider>(context).getDisabled;
    return ClipRRect(
      borderRadius: BorderRadius.circular(21),
      child: Stack(
        children: [
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: height * 0.05),
                Text('Enter ID', style: HomeStyles().subtitleText),
                SizedBox(height: height * 0.05),
                AnimatedOpacity(
                  opacity: _disable ? 0.5 : 1,
                  duration: Duration(milliseconds: 300),
                  child: AbsorbPointer(
                      absorbing: _disable,
                      child: ShakeView(
                          child: Form(
                            key: _key,
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: width * 0.03),
                              child: PinCodeTextField(
                                appContext: context,
                                length: 6,
                                onChanged: null,
                                cursorColor: Colors.transparent,
                                animationType: AnimationType.slide,
                                enablePinAutofill: false,
                                keyboardType: TextInputType.name,
                                enableActiveFill: false,
                                textStyle: TextStyle(
                                  fontFamily: 'Raleway',
                                  fontSize: 24,
                                  color: HomeStyles().primaryColor,
                                  fontWeight: FontWeight.w600,
                                ),
                                onCompleted: (value) {
                                  print(value);
                                  Provider.of<ImportProvider>(context,
                                          listen: false)
                                      .setDisabled(true);
                                  getSubscriptionUsingID(value, context);
                                },
                                pinTheme: PinTheme(
                                  shape: PinCodeFieldShape.underline,
                                  borderRadius: null,
                                  activeColor: Color(0xff6953F5),
                                  disabledColor: HomeStyles().primaryColor,
                                  selectedColor: HomeStyles().primaryColor,
                                  inactiveColor: HomeStyles()
                                      .primaryColor
                                      .withOpacity(0.3),
                                ),
                              ),
                            ),
                          ),
                          controller: shared.shakeController)),
                ),
                SizedBox(height: height * 0.05),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: _disable
                ? LinearProgressIndicator(
                    valueColor:
                        AlwaysStoppedAnimation<Color>(Color(0xff6953F5)),
                    backgroundColor: Color(0xFFFFDDE1),
                  )
                : null,
          ),
        ],
      ),
    );
  }
}
