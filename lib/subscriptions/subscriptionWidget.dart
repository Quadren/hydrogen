import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/individualSubscriptions/subscriptionPage.dart';
import 'package:hydrogen/subscriptions/moderation/moderators.dart';
import 'package:hydrogen/subscriptions/services/checkElevation.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import './subscriptions_shared.dart' as shared;

class SubscriptionWidget extends StatefulWidget {
  final int _index;

  SubscriptionWidget(this._index);

  @override
  _SubscriptionWidgetState createState() => _SubscriptionWidgetState(_index);
}

class _SubscriptionWidgetState extends State<SubscriptionWidget> {
  final int _index;

  _SubscriptionWidgetState(this._index);

  String _title;
  String _owner;
  int _length;

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      Provider.of<ModeratorProvider>(context, listen: false).setLoading(false);
      _parseData();
    });
    super.initState();
  }

  void _checkIfElevated() {
    var box = Hive.box('userData');
    String _owner = Provider.of<SubscriptionProvider>(context, listen: false)
        .getSubscriptions[_index]['owner'];

    if (_owner == box.get('email')) {
      Provider.of<ModeratorProvider>(context, listen: false).elevate(true);
      print("USER IS OWNER OF THIS SUBSCRIPTION");
      Navigator.push(
          context,
          PageTransition(
              child: SubscriptionPage(_index),
              type: PageTransitionType.rightToLeft,
              duration: Duration(milliseconds: 500),
              curve: Curves.easeInOutCubic));
    } else {
      String id = Provider.of<SubscriptionProvider>(context, listen: false)
          .getSubscriptions[_index]['id'];
      checkIfElevated(id, context);
      Navigator.push(
          context,
          PageTransition(
              child: SubscriptionPage(_index),
              type: PageTransitionType.rightToLeft,
              duration: Duration(milliseconds: 500),
              curve: Curves.easeInOutCubic));
    }
  }

  void _parseData() {
    List tempList = Provider.of<SubscriptionProvider>(context, listen: false)
        .getSubscriptions;
    Map tempMap = tempList[_index];
    List tempcardslist = tempMap['cards'];

    var box = Hive.box('userData');

    if (box.get('email') == tempMap['owner']) {
      setState(() {
        _title = tempMap['title'];
        _owner = 'you';
        _length = tempcardslist.length;
      });
    } else {
      setState(() {
        _title = tempMap['title'];
        _owner = tempMap['owner'];
        _length = tempcardslist.length;
      });
    }

    print(_title);
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    bool loading = Provider.of<ModeratorProvider>(context).getLoading;

    String id = Provider.of<SubscriptionProvider>(context)
        .getSubscriptions[_index]['id'];

    return Container(
      height: height,
      padding: EdgeInsets.only(left: width * 0.1),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: height * 0.1),
          Container(
            width: width * 0.6,
            child: Text(
              '$_title',
              style: HomeStyles().bigText,
            ),
          ),
          SizedBox(height: height * 0.01),
          Container(
            width: width * 0.8,
            child: Text(
              '// owned by $_owner',
              style: HomeStyles().subtitleText,
            ),
          ),
          SizedBox(height: height * 0.03),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: height * 0.075,
                width: width * 0.35,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: HomeStyles().tertiaryAccent,
                ),
                child: Center(
                  child: Text(
                    '$_length cards',
                    style: HomeStyles().labelText,
                  ),
                ),
              ),
              SizedBox(width: width * 0.03),
              AbsorbPointer(
                absorbing: loading,
                child: InkWell(
                  onTap: () {
                    _checkIfElevated();
                  },
                  child: Container(
                    height: height * 0.075,
                    width: height * 0.075,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: HomeStyles().tertiaryAccent,
                    ),
                    child: Center(
                      child: loading
                          ? Transform.scale(
                              scale: 0.5,
                              child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation(
                                    HomeStyles().primaryColor),
                                backgroundColor: Colors.transparent,
                              ),
                            )
                          : FaIcon(FontAwesomeIcons.arrowRight,
                              color: HomeStyles().primaryColor,
                              size: height * 0.02),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: height * 0.04),
          Container(
            padding: EdgeInsets.only(left: width * 0.02),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    shared.subscriptionController.previousPage(
                        duration: Duration(milliseconds: 500),
                        curve: Curves.easeInOutCubic);
                  },
                  child: FaIcon(FontAwesomeIcons.chevronUp,
                      color: HomeStyles().primaryColor, size: height * 0.0175),
                ),
                SizedBox(width: width * 0.075),
                InkWell(
                  onTap: () {
                    shared.subscriptionController.nextPage(
                        duration: Duration(milliseconds: 500),
                        curve: Curves.easeInOutCubic);
                  },
                  child: FaIcon(FontAwesomeIcons.chevronDown,
                      color: HomeStyles().primaryColor, size: height * 0.0175),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
