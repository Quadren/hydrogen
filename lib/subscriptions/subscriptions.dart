import 'package:animated/animated.dart';
import 'package:flutter/material.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/services/getAllSubscriptions.dart';
import 'package:hydrogen/subscriptions/subscriptionList.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:hydrogen/widgets/pageHeaders.dart';
import 'package:provider/provider.dart';

class Subscriptions extends StatefulWidget {
  @override
  _SubscriptionsState createState() => _SubscriptionsState();
}

class _SubscriptionsState extends State<Subscriptions> {

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      Provider.of<SubscriptionProvider>(context,listen: false).setLoading(true);
      Provider.of<SubscriptionProvider>(context,listen: false).setAnimate(false);
      getSubscriptions(context);
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    bool _isEmpty = Provider.of<SubscriptionProvider>(context).isEmpty; 
    bool _isLoading = Provider.of<SubscriptionProvider>(context).isLoading;
    bool _animate = Provider.of<SubscriptionProvider>(context).animate;

    var size = MediaQuery.of(context).size;
    double height = size.height;
    return Container(
      child: _isLoading ? Center(
        child: CircularProgressIndicator(
          // TODO: replace with greyed out placeholder loading animation
          valueColor: AlwaysStoppedAnimation<Color>(HomeStyles().primaryColor),
        ),
      ) : _isEmpty ? Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('No subscriptions!', style: HomeStyles().headerText),
            SizedBox(height: height * 0.01),
            Text('tap the + icon to begin', style: HomeStyles().subtitleText,)
          ],
        ),
      ) : NestedScrollView(
        physics: NeverScrollableScrollPhysics(),
        headerSliverBuilder: (context, bool isInnerBoxScrolled) {
          return [
            SliverToBoxAdapter(
            child: PageHeaders('subscriptions'),
          ),
          ];
        },
        body: Animated(
          value: _animate ? 1 : 0,
          duration: Duration(milliseconds: 500),
          curve: Curves.easeInOutCubic,
          builder: (context, child, animation) => Transform.translate(
            offset: Offset((1-animation.value) * 200, 0),
            child: Opacity(
              opacity: animation.value,
              child: child,
            ),
          ),
          child: SubscriptionList(),
        ),
      ),
    );
  }
}