import 'package:animated/animated.dart';
import 'package:auto_animated/auto_animated.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/services/getSubscribers.dart';
import 'package:hydrogen/subscriptions/subscribers/subscriberWidget.dart';
import 'package:hydrogen/subscriptions/subscribers/subscribersProvider.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:provider/provider.dart';

class Subscribers extends StatefulWidget {
  final int _index;

  Subscribers(this._index);

  @override
  _SubscribersState createState() => _SubscribersState(_index);
}

class _SubscribersState extends State<Subscribers> {
  final int _index;
  bool _animate1 = false;

  _SubscribersState(this._index);

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      String id = Provider.of<SubscriptionProvider>(context, listen: false)
          .getSubscriptions[_index]['id'];
      getAllSubscribers(id, context);
    });
    setState(() {
      _animate1 = false;
    });
    Future.delayed(Duration(milliseconds: 200), () {
      setState(() {
        _animate1 = true;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: HomeStyles().backgroundColor,
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          iconTheme: IconThemeData(color: HomeStyles().primaryColor),
          leading: InkWell(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onTap: () => Navigator.pop(context),
            child: Container(
              margin: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.05,
                  top: MediaQuery.of(context).size.height * 0.03),
              decoration: BoxDecoration(
                color: HomeStyles().backgroundColor,
                borderRadius: BorderRadius.circular(100),
              ),
              child: Center(
                  child: FaIcon(
                FontAwesomeIcons.arrowLeft,
                color: HomeStyles().primaryColor,
                size: MediaQuery.of(context).size.height * 0.025,
              )),
            ),
          ),
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1),
          child: body(),
        ));
  }

  Widget body() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    // subscription data
    Map subscription =
        Provider.of<SubscriptionProvider>(context).getSubscriptions[_index];
    String id = subscription['id'];

    List<Map> subscribers =
        Provider.of<SubscribersListProvider>(context).getSubscribers;

    final scrollController = ScrollController();

    bool loading = Provider.of<SubscribersListProvider>(context).getLoading;

    return CustomScrollView(
      controller: scrollController,
      slivers: [
        SliverToBoxAdapter(
          child: Animated(
            duration: Duration(seconds: 1),
            curve: Curves.easeInOutCubic,
            value: _animate1 ? 1 : 0,
            builder: (context, child, animation) => Transform.translate(
              offset: Offset(0, (1 - animation.value) * 40),
              child: Opacity(
                opacity: animation.value,
                child: child,
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: height * 0.2),
                Text('Subscribers', style: HomeStyles().bigText),
                SizedBox(height: height * 0.01),
                Text('for deck with ID $id', style: HomeStyles().subtitleText),
                SizedBox(height: height * 0.05),
                Divider(color: Color(0xFFAAAAAA)),
                SizedBox(height: height * 0.05),
              ],
            ),
          ),
        ),
        Container(
          child: loading
              ? SliverToBoxAdapter(
                  child: Column(
                    children: [
                      SizedBox(height: height * 0.15),
                      Transform.scale(
                        scale: 1.5,
                        child: CircularProgressIndicator(
                          strokeWidth: 7,
                          valueColor: AlwaysStoppedAnimation<Color>(
                              HomeStyles().accentColor),
                          backgroundColor: HomeStyles().secondaryAccent,
                        ),
                      ),
                    ],
                  ),
                )
              : LiveSliverGrid(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: (height * 0.2) / (height * 0.25),
                  ),
                  itemBuilder: (context, index, animation) {
                    return FadeTransition(
                      opacity:
                          Tween<double>(begin: 0, end: 1).animate(animation),
                      child: SlideTransition(
                        position: Tween<Offset>(
                                begin: Offset(0, 0.1), end: Offset(0, 0))
                            .animate(animation),
                        child: SubscriberWidget(subscribers[index]['name'],
                            subscribers[index]['pfp']),
                      ),
                    );
                  },
                  itemCount: subscribers.length,
                  controller: scrollController,
                  showItemDuration: Duration(milliseconds: 250),
                ),
        ),
      ],
    );
  }
}
