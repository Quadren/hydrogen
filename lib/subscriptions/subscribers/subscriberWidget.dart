import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hydrogen/home/home_styles.dart';

class SubscriberWidget extends StatelessWidget {

  final String _name;
  final String _url;

  SubscriberWidget(this._name, this._url);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: height * 0.15,
            width: height * 0.15,
          decoration: BoxDecoration(
            border: Border.all(width: 3, color: HomeStyles().secondaryAccent),
            borderRadius: BorderRadius.circular(100),
          ),
          child: ClipOval(
            child: CachedNetworkImage(imageUrl: _url, fit: BoxFit.cover, placeholder: (context, String url) {
            return Center(
              child: Container(
                height: height * 0.2,
                width: height * 0.2,
                color: Color(0xFFAAAAAA),
              ),
            );
          },),
          ),
        ),
        SizedBox(height: height * 0.02),
        Text('$_name', style: HomeStyles().buttonText,)
      
        ],
      );
  }
}