import 'package:flutter/material.dart';

class SubscribersListProvider extends ChangeNotifier {
  List<Map> _subscribers = [];
  bool _loading = false;

  List<Map> get getSubscribers => _subscribers;
  bool get getLoading => _loading;

  void addSubscriber(String name, String pfp) {
    Map tempMap = {'name' : name, 'pfp' : pfp};
    _subscribers.add(tempMap);
    notifyListeners();
  }

  void finishLoading() {
    _loading = false;
    notifyListeners();
  }

  void resetSubscribers() {
    _loading = true;
    _subscribers = [];
    notifyListeners();
  }
}