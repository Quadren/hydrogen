import 'package:flutter/material.dart';

class CommitProvider extends ChangeNotifier {
  List<Map> _commits = [];
  bool _isBlank = false;
  bool _uploading = false;

  List<Map> get getCommits => _commits;
  bool get isBlank => _isBlank;
  bool get isUploading => _uploading;

  void addToCommits(String front, String back, String eqn, String attachment) {
    Map tempMap = {
      'front': front,
      'back': back,
      'eqn': eqn,
      'attachment': attachment
    };
    _commits.add(tempMap);
    notifyListeners();
  }

  void removeFromCommits(String front, String back) {
    Map tempMap = {'front': front, 'back': back};
    _commits.remove(tempMap);
    notifyListeners();
  }

  void setEmpty(bool value) {
    _isBlank = value;
    notifyListeners();
  }

  void resetCommits() {
    _commits = [];
    notifyListeners();
  }

  void setUploading(bool value) {
    _uploading = value;
    notifyListeners();
  }
}
