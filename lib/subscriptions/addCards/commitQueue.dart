import 'package:animated/animated.dart';
import 'package:auto_animated/auto_animated.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/addCards/commitCardWidget.dart';
import 'package:hydrogen/subscriptions/addCards/commitProvider.dart';
import 'package:hydrogen/subscriptions/services/addCardsServices.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';

class CommitQueue extends StatefulWidget {
  final String id;

  CommitQueue(this.id);

  @override
  _CommitQueueState createState() => _CommitQueueState(id);
}

class _CommitQueueState extends State<CommitQueue> {
  final _scrollController = ScrollController();

  final String id;

  _CommitQueueState(this.id);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    List<Map> cards = Provider.of<CommitProvider>(context).getCommits;
    bool isEmpty = Provider.of<CommitProvider>(context).isBlank;
    bool isUploading = Provider.of<CommitProvider>(context).isUploading;

    return Stack(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.03),
          child: CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: height * 0.2),
                    Text('Queued cards', style: HomeStyles().headerText),
                    SizedBox(height: height * 0.01),
                    Text('these will be added when you finish',
                        style: HomeStyles().subtitleText),
                    SizedBox(height: height * 0.15),
                  ],
                ),
              ),
              Container(
                child: isEmpty
                    ? SliverToBoxAdapter(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(height: height * 0.1),
                            Text(
                              'no cards added',
                              style: HomeStyles().subtitleText,
                            )
                          ],
                        ),
                      )
                    : LiveSliverGrid(
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio: (width * 0.4) / (height * 0.3),
                        ),
                        itemBuilder: (context, index, animation) {
                          return FadeTransition(
                            opacity: Tween<double>(begin: 0, end: 1)
                                .animate(animation),
                            child: SlideTransition(
                              position: Tween<Offset>(
                                      begin: Offset(0, 0.1), end: Offset(0, 0))
                                  .animate(animation),
                              child: CommitCardWidget(
                                  cards[index]['front'],
                                  cards[index]['back'],
                                  cards[index]['eqn'],
                                  index),
                            ),
                          );
                        },
                        itemCount: cards.length,
                        controller: _scrollController,
                        showItemDuration: Duration(milliseconds: 250),
                      ),
              ),
            ],
          ),
        ),
        Align(
          alignment: Alignment.bottomRight,
          child: Padding(
            padding: EdgeInsets.all(width * 0.075),
            child: Animated(
              value: (cards.length > 0) ? 1 : 0,
              curve: Sprung.criticallyDamped,
              duration: Duration(seconds: 1),
              builder: (context, child, animation) => Transform.translate(
                offset: Offset(0, (1 - animation.value) * 200),
                child: AbsorbPointer(
                  absorbing: isUploading,
                  child: BouncingWidget(
                    child: Container(
                      height: height * 0.085,
                      width: height * 0.085,
                      decoration: BoxDecoration(
                        color: HomeStyles().accentColor,
                        borderRadius: BorderRadius.circular(21),
                      ),
                      child: Center(
                        child: isUploading
                            ? Transform.scale(
                                scale: 0.5,
                                child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation(
                                      HomeStyles().backgroundColor),
                                  backgroundColor: Colors.transparent,
                                ),
                              )
                            : FaIcon(
                                FontAwesomeIcons.upload,
                                color: HomeStyles().backgroundColor,
                                size: height * 0.02,
                              ),
                      ),
                    ),
                    onPressed: () {
                      AddCardsServices()
                          .writeCommitsToFirebase(cards, id, context);
                    },
                    duration: Duration(milliseconds: 150),
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
