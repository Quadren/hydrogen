import 'package:animated/animated.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:catex/catex.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/addCards/catexPreview.dart';
import 'package:hydrogen/subscriptions/addCards/commitProvider.dart';
import 'package:hydrogen/subscriptions/addCards/commitQueue.dart';
import 'package:hydrogen/subscriptions/services/addCardsServices.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:hydrogen/widgets/backBar.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';
import 'package:url_launcher/url_launcher.dart';

class AddCards extends StatefulWidget {
  final int _index;

  AddCards(this._index);

  @override
  _AddCardsState createState() => _AddCardsState(_index);
}

class _AddCardsState extends State<AddCards> {
  final int _index;

  _AddCardsState(this._index);

  final _key1 = GlobalKey<FormState>();
  TextEditingController _controller1 = TextEditingController();

  final _key2 = GlobalKey<FormState>();
  TextEditingController _controller2 = TextEditingController();

  final _key3 = GlobalKey<FormState>();
  TextEditingController _controller3 = TextEditingController();

  @override
  void initState() {
    _controller1.addListener(() {
      setState(() {});
    });
    _controller2.addListener(() {
      setState(() {});
    });
    _controller3.addListener(() {
      setState(() {});
    });
    Future.delayed(Duration.zero, () {
      Provider.of<CommitProvider>(context, listen: false).setEmpty(true);
      Provider.of<CommitProvider>(context, listen: false).resetCommits();
    });
    super.initState();
  }

  void _openGuide() async {
    String url = 'https://oeis.org/wiki/List_of_LaTeX_mathematical_symbols';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      Fluttertoast.showToast(
          msg: 'Failed to launch TeX symbols guide',
          backgroundColor: Colors.red[300],
          textColor: Colors.white);
    }
  }

  PageController _controller = PageController(initialPage: 0);

  @override
  Widget build(BuildContext context) {
    Map subdata =
        Provider.of<SubscriptionProvider>(context).getSubscriptions[_index];

    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: HomeStyles().backgroundColor,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: HomeStyles().primaryColor),
        leading: Backbar(
          confirmQuit: true,
          id: subdata['id'],
        ),
      ),
      body: PageView(
        controller: _controller,
        children: [
          pageOne(),
          CommitQueue(Provider.of<SubscriptionProvider>(context, listen: false)
              .getSubscriptions[_index]['id']),
        ],
      ),
    );
  }

  Widget pageOne() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    OutlineInputBorder _border = OutlineInputBorder(
      borderRadius: BorderRadius.circular(21),
      borderSide: BorderSide(width: 2, color: HomeStyles().primaryColor),
    );

    OutlineInputBorder _errorBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(21),
      borderSide: BorderSide(width: 2, color: Colors.red[300]),
    );

    InputDecoration _formDecor = InputDecoration(
      counterStyle: TextStyle(
        fontFamily: 'Raleway',
        fontSize: 12,
        color: HomeStyles().primaryColor,
        fontWeight: FontWeight.w600,
      ),
      border: _border,
      errorBorder: _errorBorder,
      disabledBorder: _border,
      enabledBorder: _border,
      focusedBorder: _border,
      contentPadding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.05,
          vertical: MediaQuery.of(context).size.height * 0.03),
      errorStyle: TextStyle(
        fontFamily: 'Raleway',
        fontSize: 12,
        color: Colors.red[300],
        fontWeight: FontWeight.w400,
      ),
    );

    List<Map> commits = Provider.of<CommitProvider>(context).getCommits;

    return Stack(
      children: [
        ScrollConfiguration(
          behavior: AntiScrollGlow(),
          child: CustomScrollView(
            primary: false,
            slivers: [
              SliverToBoxAdapter(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: height * 0.15),
                    Text('Create card', style: HomeStyles().headerText),
                    SizedBox(height: height * 0.02),
                    BouncingWidget(
                      child: Container(
                          height: height * 0.05,
                          width: width * 0.4,
                          decoration: BoxDecoration(
                            color: HomeStyles().accentColor,
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Center(
                            child: Text('Go to commits (${commits.length})',
                                style: HomeStyles()
                                    .buttonText3
                                    .copyWith(fontSize: 12)),
                          )),
                      onPressed: () {
                        _controller.nextPage(
                            duration: Duration(milliseconds: 500),
                            curve: Curves.easeInOutCubic);
                      },
                      duration: Duration(milliseconds: 150),
                    ),
                    SizedBox(height: height * 0.15),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        BouncingWidget(
                          child: Container(
                            height: height * 0.08,
                            width: width * 0.1,
                            decoration: BoxDecoration(
                              color: Color(0xffEEEEEE),
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: Center(
                              child: FaIcon(
                                FontAwesomeIcons.equals,
                                color: (_controller3.text.length > 4)
                                    ? HomeStyles().accentColor
                                    : Color(0xffAAAAAA),
                                size: height * 0.015,
                              ),
                            ),
                          ),
                          onPressed: () {
                            if (_controller3.text.length > 4 == false) {
                              Fluttertoast.showToast(
                                  msg: 'No valid CaTeX equation found!',
                                  backgroundColor: Colors.red[300],
                                  textColor: Colors.white);
                            } else {
                              showCatex(_controller3.text, context);
                            }
                          },
                          duration: Duration(milliseconds: 150),
                        ),
                        FlipCard(
                            onFlip: () {
                              FocusScope.of(context).unfocus();
                            },
                            front: card(_controller1.text, height, width, true),
                            back:
                                card(_controller2.text, height, width, false)),
                        BouncingWidget(
                          child: Container(
                            height: height * 0.08,
                            width: width * 0.1,
                            decoration: BoxDecoration(
                              color: Color(0xffEEEEEE),
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: Center(
                              child: FaIcon(
                                FontAwesomeIcons.paperclip,
                                color: Color(0xffAAAAAA),
                                size: height * 0.015,
                              ),
                            ),
                          ),
                          onPressed: () {
                            Fluttertoast.showToast(
                                toastLength: Toast.LENGTH_LONG,
                                msg:
                                    'Attachments coming soon. Public roadmap: https://bit.ly/3rEZqJp',
                                backgroundColor: HomeStyles().accentColor,
                                textColor: Colors.white);
                          },
                          duration: Duration(milliseconds: 150),
                        ),
                      ],
                    ),
                    SizedBox(height: height * 0.133),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(left: width * 0.02),
                            child:
                                Text('front', style: HomeStyles().buttonText),
                          ),
                          SizedBox(height: height * 0.01),
                          Form(
                              key: _key1,
                              child: TextFormField(
                                keyboardType: TextInputType.multiline,
                                cursorColor: HomeStyles().accentColor,
                                controller: _controller1,
                                decoration: _formDecor,
                                maxLength: 240,
                                style: HomeStyles()
                                    .buttonText
                                    .copyWith(fontSize: 12),
                                maxLines: 5,
                              ))
                        ],
                      ),
                    ),
                    SizedBox(height: height * 0.02),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(left: width * 0.02),
                            child: Text('back', style: HomeStyles().buttonText),
                          ),
                          SizedBox(height: height * 0.01),
                          Form(
                              key: _key2,
                              child: TextFormField(
                                keyboardType: TextInputType.multiline,
                                cursorColor: HomeStyles().accentColor,
                                controller: _controller2,
                                decoration: _formDecor,
                                maxLength: 800,
                                style: HomeStyles()
                                    .buttonText
                                    .copyWith(fontSize: 12),
                                maxLines: 8,
                              ))
                        ],
                      ),
                    ),
                    SizedBox(height: height * 0.05),
                    Text('additional', style: HomeStyles().subtitleText),
                    SizedBox(height: height * 0.05),
                    Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: width * 0.05, vertical: height * 0.02),
                      height: height * 0.2,
                      width: width * 0.8,
                      decoration: BoxDecoration(
                        color: Color(0xffEEEEEE),
                        borderRadius: BorderRadius.circular(21),
                      ),
                      child: Stack(
                        children: [
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              height: height * 0.15,
                              child: Center(
                                child: CaTeX(_controller3.text),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Text('CaTeX Render Preview',
                                style: HomeStyles().subtitleText),
                          ),
                          Align(
                            alignment: Alignment.topRight,
                            child: GestureDetector(
                              onTap: () {
                                _openGuide();
                              },
                              child: FaIcon(
                                FontAwesomeIcons.questionCircle,
                                color: HomeStyles().primaryColor,
                                size: height * 0.02,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: height * 0.03),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: width * 0.1),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(left: width * 0.02),
                            child: Text('CaTeX Equation',
                                style: HomeStyles().buttonText),
                          ),
                          SizedBox(height: height * 0.01),
                          Form(
                              key: _key3,
                              child: TextFormField(
                                keyboardType: TextInputType.multiline,
                                cursorColor: HomeStyles().accentColor,
                                controller: _controller3,
                                decoration: _formDecor,
                                maxLength: 80,
                                style: HomeStyles()
                                    .buttonText
                                    .copyWith(fontSize: 12),
                                maxLines: 8,
                              ))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Align(
          alignment: Alignment.bottomRight,
          child: Padding(
            padding: EdgeInsets.all(width * 0.075),
            child: Animated(
              curve: Sprung.criticallyDamped,
              duration: Duration(seconds: 1),
              value:
                  (_controller1.text.length > 0 && _controller2.text.length > 0)
                      ? 1
                      : 0,
              builder: (context, child, animation) => Transform.translate(
                offset: Offset(0, (1 - animation.value) * 200),
                child: BouncingWidget(
                  child: Container(
                    height: height * 0.085,
                    width: height * 0.085,
                    decoration: BoxDecoration(
                      color: HomeStyles().primaryColor,
                      borderRadius: BorderRadius.circular(21),
                    ),
                    child: Center(
                      child: FaIcon(
                        FontAwesomeIcons.check,
                        color: HomeStyles().backgroundColor,
                        size: height * 0.02,
                      ),
                    ),
                  ),
                  onPressed: () {
                    if (_controller3.text.length > 0) {
                      AddCardsServices().addCardToCommit(
                          _controller1.text,
                          _controller2.text,
                          _controller3.text,
                          'none',
                          context);
                    } else {
                      AddCardsServices().addCardToCommit(_controller1.text,
                          _controller2.text, 'none', 'none', context);
                    }
                    _controller1.clear();
                    _controller2.clear();
                    _controller3.clear();
                    Fluttertoast.showToast(
                        msg: 'Added card to commits!',
                        backgroundColor: HomeStyles().backgroundColor,
                        textColor: HomeStyles().primaryColor);
                  },
                  duration: Duration(milliseconds: 150),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget card(String text, double height, double width, bool front) {
    return Container(
      height: height * 0.35,
      width: width * 0.5,
      decoration: BoxDecoration(
        color: Color(0xffEEEEEE),
        borderRadius: BorderRadius.circular(21),
      ),
      padding: EdgeInsets.symmetric(
          vertical: height * 0.02, horizontal: width * 0.05),
      child: Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: Text(
              front ? 'front' : 'back',
              style: HomeStyles().subtitleText,
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Container(
              height: height * 0.2,
              child: Text(
                text,
                style: HomeStyles().buttonText.copyWith(fontSize: 12),
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                maxLines: 12,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
