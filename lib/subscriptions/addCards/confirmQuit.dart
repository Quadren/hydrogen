import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/services/addCardsServices.dart';
import 'package:provider/provider.dart';

import 'commitProvider.dart';

void showConfirmCloseAddCards(BuildContext context, String id) {
  showModalBottomSheet(
      context: context,
      builder: (ctx) => ConfirmCloseAddCards(id),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(21), topRight: Radius.circular(21)),
      ));
}

class ConfirmCloseAddCards extends StatefulWidget {
  final String _id;

  ConfirmCloseAddCards(this._id);

  @override
  _ConfirmCloseAddCardsState createState() => _ConfirmCloseAddCardsState(_id);
}

class _ConfirmCloseAddCardsState extends State<ConfirmCloseAddCards> {
  final String _id;

  _ConfirmCloseAddCardsState(this._id);

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      Provider.of<CommitProvider>(context, listen: false).setUploading(false);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    List<Map> cards = Provider.of<CommitProvider>(context).getCommits;
    bool isUploading = Provider.of<CommitProvider>(context).isUploading;

    return Container(
      height: height * 0.33,
      decoration: BoxDecoration(
        color: HomeStyles().backgroundColor,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(21), topRight: Radius.circular(21)),
      ),
      padding: EdgeInsets.all(width * 0.05),
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: height * 0.03),
                Text(
                  'You have\nunsaved changes',
                  style: HomeStyles().headerText,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: height * 0.02),
                Text('there are cards in your commits',
                    style: HomeStyles().subtitleText),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                BouncingWidget(
                    child: Container(
                      height: height * 0.075,
                      width: width * 0.4,
                      decoration: BoxDecoration(
                        color: Color(0xffFF5558),
                        borderRadius: BorderRadius.circular(100),
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                        child: Center(
                          child: Text(
                            'Discard',
                            style: HomeStyles().buttonText3,
                          ),
                        ),
                      ),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                      Navigator.pop(context);
                    }),
                BouncingWidget(
                    child: Container(
                      height: height * 0.075,
                      width: width * 0.4,
                      decoration: BoxDecoration(
                        color: HomeStyles().accentColor,
                        borderRadius: BorderRadius.circular(100),
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: width * 0.03),
                        child: Center(
                          child: isUploading
                              ? Transform.scale(
                                  scale: 0.7,
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation(
                                        HomeStyles().backgroundColor),
                                    backgroundColor: Colors.transparent,
                                  ),
                                )
                              : Text(
                                  'Commit',
                                  style: HomeStyles().buttonText3,
                                ),
                        ),
                      ),
                    ),
                    onPressed: () {
                      if (!isUploading) {
                        AddCardsServices().writeCommitsToFirebase(
                            cards, _id, context,
                            closeOnLoad: true);
                      }
                    })
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class FaIcon {}
