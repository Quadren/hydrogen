import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/addCards/catexPreview.dart';

class CommitCardWidget extends StatelessWidget {
  final String _front;
  final String _back;
  final String _eqn;
  final int _index;

  CommitCardWidget(this._front, this._back, this._eqn, this._index);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return FlipCard(
        front: Container(
          decoration: BoxDecoration(
            color: Color(0xFFEEEEEE),
            borderRadius: BorderRadius.circular(21),
          ),
          padding: EdgeInsets.all(width * 0.05),
          margin: EdgeInsets.all(width * 0.03),
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  child: Text(
                    _front,
                    overflow: TextOverflow.fade,
                    style: TextStyle(
                      fontFamily: 'Raleway',
                      fontSize: 12,
                      color: HomeStyles().primaryColor,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: Text(
                  '${_index + 1}',
                  overflow: TextOverflow.fade,
                  style: TextStyle(
                    fontFamily: 'Raleway',
                    fontSize: 12,
                    color: Color(0xFFAAAAAA),
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ],
          ),
        ),
        back: Container(
          decoration: BoxDecoration(
            color: HomeStyles().accentColor,
            borderRadius: BorderRadius.circular(21),
          ),
          padding: EdgeInsets.all(width * 0.05),
          margin: EdgeInsets.all(width * 0.03),
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  child: Text(
                    _back,
                    overflow: TextOverflow.fade,
                    style: TextStyle(
                      fontFamily: 'Raleway',
                      fontSize: 12,
                      color: HomeStyles().backgroundColor,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: Text(
                  '${_index + 1}',
                  overflow: TextOverflow.fade,
                  style: TextStyle(
                    fontFamily: 'Raleway',
                    fontSize: 12,
                    color: HomeStyles().backgroundColor,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: (_eqn != 'none')
                    ? GestureDetector(
                        child: Container(
                          height: height * 0.05,
                          width: height * 0.05,
                          decoration: BoxDecoration(
                            color: HomeStyles().backgroundColor,
                            borderRadius: BorderRadius.circular(12),
                          ),
                          child: Center(
                            child: FaIcon(FontAwesomeIcons.equals,
                                color: HomeStyles().accentColor,
                                size: height * 0.015),
                          ),
                        ),
                        onTap: () {
                          showCatex(_eqn, context);
                        },
                      )
                    : null,
              ),
            ],
          ),
        ));
  }
}
