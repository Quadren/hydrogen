import 'package:catex/catex.dart';
import 'package:flutter/material.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:sprung/sprung.dart';

void showCatex(String text, BuildContext context) {
  showDialog(context: context, builder: (ctx) => CatexPopUp(text));
}

class CatexPopUp extends StatefulWidget {
  final String _text;

  CatexPopUp(this._text);

  @override
  _CatexPopUpState createState() => _CatexPopUpState(_text);
}

class _CatexPopUpState extends State<CatexPopUp> with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  final String _text;

  _CatexPopUpState(this._text);

  @override
  void initState() {
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Sprung.criticallyDamped);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
            height: height * 0.4,
            width: width * 0.75,
            padding: EdgeInsets.all(width * 0.05),
            decoration: ShapeDecoration(
                color: HomeStyles().backgroundColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(21))),
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.topCenter,
                  child: Text('CaTeX equation preview',
                      style: HomeStyles().subtitleText),
                ),
                Center(
                  child: CaTeX(_text),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
