import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/addSubscriptions/importFromWeb.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';

void showChangeCategory(BuildContext context, int index) {
  showDialog(context: context, builder: (_) => ChangeCategory(index));
}

class ChangeCategory extends StatefulWidget {
  final int _index;

  ChangeCategory(this._index);

  @override
  _ChangeCategoryState createState() => _ChangeCategoryState(_index);
}

class _ChangeCategoryState extends State<ChangeCategory>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  final int _index;

  _ChangeCategoryState(this._index);

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, () {
      Provider.of<ImportProvider>(context, listen: false).setDisabled(false);
    });

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Sprung.criticallyDamped);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  PageController _controller = PageController(initialPage: 0);

  @override
  Widget build(BuildContext context) {
    List categories =
        Provider.of<CategoriesProvider>(context).getCategories.values.toList();

    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
            height: height * 0.6,
            width: width * 0.75,
            decoration: ShapeDecoration(
                color: HomeStyles().backgroundColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(21))),
            child: PageView(
              controller: _controller,
              physics: NeverScrollableScrollPhysics(),
              children: [
                CustomScrollView(
                  primary: false,
                  slivers: [
                    SliverToBoxAdapter(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(height: height * 0.05),
                          Text('choose category',
                              style: HomeStyles().subtitleText),
                          SizedBox(height: height * 0.05),
                        ],
                      ),
                    ),
                    SliverList(
                      delegate: SliverChildBuilderDelegate(
                        (context, int index) {
                          return GestureDetector(
                            onTap: () {
                              var box = Hive.box('categories');
                              Map subscription =
                                  Provider.of<SubscriptionProvider>(context,
                                          listen: false)
                                      .getSubscriptions[_index];
                              String id = subscription['id'];
                              box.put(id, categories[index]['label']);
                              print(box.get(id));
                              Provider.of<SubscriptionProvider>(context,
                                      listen: false)
                                  .updateCategory(_index, box.get(id));
                              Provider.of<CategoriesProvider>(context,
                                      listen: false)
                                  .clearRender();
                              Navigator.pop(context);
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: width * 0.05,
                                  vertical: height * 0.025),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    '${categories[index]['label']}',
                                    style: TextStyle(
                                      fontFamily: 'Raleway',
                                      fontSize: 14,
                                      color: HomeStyles().primaryColor,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                  ClipOval(
                                    child: Container(
                                      height: height * 0.0125,
                                      width: height * 0.0125,
                                      color: categories[index]['color'],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                        childCount: categories.length,
                      ),
                    ),
                  ],
                ),
                Center(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.height * 0.2,
                    child: Lottie.asset('assets/lottie/check.json',
                        repeat: false, fit: BoxFit.contain),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
