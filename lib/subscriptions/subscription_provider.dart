import 'package:flutter/material.dart';

class SubscriptionProvider extends ChangeNotifier {
  List<Map> _subscriptions = [];
  bool _isEmpty;
  bool _loading;
  bool _animate;
  bool _removingCard;

  List<Map> get getSubscriptions => _subscriptions;
  int get getNumberOfSubscriptions => _subscriptions.length;
  bool get isEmpty => _isEmpty;
  bool get isLoading => _loading;
  bool get animate => _animate;
  bool get getRemoving => _removingCard;

  void clearSubscriptions() {
    _subscriptions.clear();
    notifyListeners();
  }

  void addSubscription(
      String title, String owner, List cards, String id, String category) {
    Map tempMap = {
      'title': title,
      'owner': owner,
      'cards': cards,
      'id': id,
      'category': category
    };
    _subscriptions.add(tempMap);
    notifyListeners();
  }

  void removeCard(int cardIndex, int index) {
    String title = _subscriptions[index]['title'];
    String owner = _subscriptions[index]['owner'];
    List cards = _subscriptions[index]['cards'];
    String id = _subscriptions[index]['id'];
    String category = _subscriptions[index]['category'];

    cards.removeAt(cardIndex);

    Map tempMap = {
      'title': title,
      'owner': owner,
      'cards': cards,
      'id': id,
      'category': category
    };

    _subscriptions.removeAt(index);
    _subscriptions.insert(index, tempMap);
    notifyListeners();
  }

  void removeSubscription(int index) {
    _subscriptions.removeAt(index);
    notifyListeners();
  }

  void setEmpty(bool condition) {
    _isEmpty = condition;
    notifyListeners();
  }

  void setLoading(bool state) {
    _loading = state;
    notifyListeners();
  }

  void setAnimate(bool value) {
    _animate = value;
    notifyListeners();
  }

  void checkIfEmpty() {
    if (_subscriptions.isEmpty) {
      _isEmpty = true;
    } else {
      _isEmpty = false;
    }
    notifyListeners();
  }

  void updateCategory(int index, String category) {
    Map tempMap = _subscriptions[index];
    String title = tempMap['title'];
    String owner = tempMap['owner'];
    List cards = tempMap['cards'];
    String id = tempMap['id'];

    Map old = _subscriptions[index];

    Map writeMap = {
      'title': title,
      'owner': owner,
      'cards': cards,
      'id': id,
      'category': category
    };
    _subscriptions.removeWhere((element) => element == old);
    _subscriptions.insert(index, writeMap);

    notifyListeners();
  }

  void setRemoving(bool value) {
    _removingCard = value;
    notifyListeners();
  }

  void updateSub(String id, List cards) {
    _subscriptions.forEach((element) {
      Map data = element;

      if (data.containsValue(id)) {
        print("Found correct subscription");
        int index = _subscriptions.indexOf(element);
        String title = _subscriptions[index]['title'];
        String owner = _subscriptions[index]['owner'];
        String id = _subscriptions[index]['id'];
        String category = _subscriptions[index]['category'];

        Map newData = {
          'title': title,
          'owner': owner,
          'cards': cards,
          'id': id,
          'category': category
        };

        _subscriptions.removeAt(index);
        _subscriptions.insert(index, newData);
        notifyListeners();
      }
    });
  }
}

class CategoriesProvider extends ChangeNotifier {
  Map<String, Map> _categories = {
    'Uncategorized': {
      'label': 'Uncategorized',
      'color': Color(0xFFEEEEEE),
      'emoji': '🤔'
    },
    'Biology': {'label': 'Biology', 'color': Color(0xFFC2E3FC), 'emoji': '🧬'},
    'Chemistry': {
      'label': 'Chemistry',
      'color': Color(0xFFC6F6D6),
      'emoji': '🧪'
    },
    'Physics': {'label': 'Physics', 'color': Color(0xFFFEE6C3), 'emoji': '⚖'},
    'Maths': {'label': 'Maths', 'color': Color(0xFFFFEEDE), 'emoji': '📏'},
  };

  int _selected = 0;
  String _selectedString = 'Uncategorized';
  List _renderedSubscriptions = [];
  bool _allSelected = true;

  Map<String, Map> get getCategories => _categories;
  int get getSelected => _selected;
  String get getSelectedString => _selectedString;
  List get getRender => _renderedSubscriptions;
  bool get getAllSelected => _allSelected;

  void setSelected(int value, String string) {
    _selected = value;
    _selectedString = string;
    notifyListeners();
  }

  void setAllSelected(bool value) {
    _allSelected = value;
    notifyListeners();
  }

  void setToRender(Map data) {
    _renderedSubscriptions.add(data);
    notifyListeners();
  }

  void clearRender() {
    _renderedSubscriptions = [];
    notifyListeners();
  }

// for creating categories
  Color _color = Color(0xFFEEEEEE);
  String _emoji = '👀';

  Color get getColor => _color;
  String get getEmoji => _emoji;

  void setColor(Color color) {
    _color = color;
    notifyListeners();
  }

  void setEmoji(String emoji) {
    _emoji = emoji;
    notifyListeners();
  }

  void createCategory(String title, String emoji, Color color) {
    Map tempMap = {
      'label': title,
      'color': color,
      'emoji': emoji,
    };

    _categories[title] = tempMap;
    notifyListeners();
  }

  void removeCategory(String title) {
    _categories.remove(title);
    notifyListeners();
  }
}
