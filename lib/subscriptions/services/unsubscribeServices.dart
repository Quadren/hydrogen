import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import '../subscriptions_shared.dart' as shared;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:provider/provider.dart';

void getSubscriptionDataForDelete(int index, context) {
  var box = Hive.box('userData');

  String email = box.get('email');
  Map subscription = Provider.of<SubscriptionProvider>(context, listen: false)
      .getSubscriptions[index];
  String id = subscription['id'];

  unsubFromSubscription(email, id, context, index);
}

Future<void> unsubFromSubscription(
    String email, String id, context, int index) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('users');
    CollectionReference ref2 =
        FirebaseFirestore.instance.collection('subscriptions');

    // ignore: unused_local_variable
    final result = await ref.doc(email).get().then((value) async {
      if (value.exists) {
        // ignore: unused_local_variable
        final result = await ref2.doc(id).get().then((sub) {
          if (sub.exists) {
            List userList = sub.data()['users'];
            userList.remove(email);
            ref2.doc(id).update({'users': userList});
            List tempList = value.data()['subscriptions'];
            print("Removing $id from subscriptions");
            tempList.remove(id);
            ref.doc(email).update({'subscriptions': tempList});
            print("New subscriptions: $tempList");
            shared.deleteController.nextPage(duration: Duration(milliseconds: 500), curve: Curves.easeInOutCubic);
            Future.delayed(Duration(seconds: 2), () {
              Provider.of<SubscriptionProvider>(context, listen: false)
                .removeSubscription(index);
              Phoenix.rebirth(context);
            });
          } else {
            Fluttertoast.showToast(
            msg: 'ERROR 001',
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
          }
        });
      } else {
        Fluttertoast.showToast(
            msg: 'ERROR 002',
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  });
}
