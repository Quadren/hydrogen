import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/subscriptions/moderation/moderators.dart';
import 'package:provider/provider.dart';

Future<void> getModerators(String id, context) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref =
        FirebaseFirestore.instance.collection('subscriptions');
    CollectionReference ref2 = FirebaseFirestore.instance.collection('users');
    var box = Hive.box('userData');

    Provider.of<ModeratorProvider>(context,listen: false).setLoading(true);
    Provider.of<ModeratorProvider>(context,listen: false).resetMods();

    // ignore: unused_local_variable
    final result = await ref.doc(id).get().then((value) {
      if (value.exists) {
        List mods = value.data()['moderators'];
        if (mods.length > 0) {
          print ("Moderators: $mods");
          Provider.of<ModeratorProvider>(context,listen: false).setEmpty(false);
          mods.forEach((mod) async {
            // ignore: unused_local_variable
            final result = await ref2.doc(mod).get().then((val2) {
              if (val2.exists) {
                Provider.of<ModeratorProvider>(context, listen: false)
                    .addMod(val2['name'], val2['pfp'], mod);
              } else {
                Fluttertoast.showToast(
                    msg: 'Could not find $mod',
                    backgroundColor: Colors.red[300],
                    textColor: Colors.white);
              }
            });
          });
          if (box.get('email') == value.data()['owner']) {
                Provider.of<ModeratorProvider>(context, listen: false).addMod('Add', 'https://firebasestorage.googleapis.com/v0/b/hydrogen-7a6c8.appspot.com/o/images%2Finvite.jpg?alt=media&token=0ca79797-bc34-49f4-a53e-7c521e749eb5', '');
              }
        } else {
          print ("No moderators");
          Provider.of<ModeratorProvider>(context,listen: false).setEmpty(true);
        }
      } else {
        Fluttertoast.showToast(
            msg: 'ERROR 001',
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
    Provider.of<ModeratorProvider>(context, listen: false).setLoading(false);
  });
}
