import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hydrogen/subscriptions/moderation/moderators.dart';
import 'package:hydrogen/subscriptions/services/getModerators.dart';
import 'package:provider/provider.dart';
import '../moderation/addModerator.dart' as shared;

Future<void> validateUser(String email, String id, context) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('users');

    // ignore: unused_local_variable
    final result = await ref.doc(email).get().then((value) {
      if (value.exists) {
        addModeratorFirebase(email, id, context);
      } else {
        shared.addModShakeController.shake();
        Provider.of<ModeratorProvider>(context, listen: false).setAbsorb(false);
      }
    });
  });
}

Future<void> addModeratorFirebase(String email, String id, context) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('subscriptions');

    // ignore: unused_local_variable
    final result = await ref.doc(id).get().then((value) {
      if (value.exists) {
        List modList = value.data()['moderators'];
        if (modList.contains(email) || email == value.data()['owner']) {
          Fluttertoast.showToast(msg: 'User is already elevated!', backgroundColor: Colors.red[300], textColor: Colors.white);
          shared.addModShakeController.shake();
          Provider.of<ModeratorProvider>(context, listen: false).setAbsorb(false);
        } else {
          modList.add(email);
          ref.doc(id).update({'moderators' : modList});
          Provider.of<ModeratorProvider>(context, listen: false).setAbsorb(false);
          Future.delayed(Duration(seconds: 2), () {
            getModerators(id, context);
          });
          shared.addModPageController.nextPage(duration: Duration(milliseconds: 500), curve: Curves.easeInOutCubic);
        }
      }
    });
  });
}