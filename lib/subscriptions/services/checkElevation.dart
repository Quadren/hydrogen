import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/subscriptions/moderation/moderators.dart';
import 'package:provider/provider.dart';

Future<void> checkIfElevated(String id, context) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('subscriptions');

    Provider.of<ModeratorProvider>(context, listen: false).setLoading(true);

    final result =  await ref.doc(id).get().then((value) {
      if (value.exists) {
        List moderators = value.data()['moderators'];
        String email = Hive.box('userData').get('email');

        if (moderators.contains(email)) {
          print ("USER IS A MODERATOR OF $id");
          Provider.of<ModeratorProvider>(context, listen: false).elevate(true);
          Provider.of<ModeratorProvider>(context, listen: false).setLoading(false);
        } else {
          print ("USER IS NOT A MODERATOR OF $id");
          Provider.of<ModeratorProvider>(context, listen: false).elevate(false);
          Provider.of<ModeratorProvider>(context, listen: false).setLoading(false);
        }
      }
    });

    
  });
}