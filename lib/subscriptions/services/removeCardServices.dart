import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import '../individualSubscriptions/removeCard.dart' as shared;

class RemoveCardServices {
  Future<void> removeCard(
      Map card, String id, int index, int cardIndex, context) async {
    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref =
          FirebaseFirestore.instance.collection('subscriptions');
      String email = Hive.box('userData').get('email');

      final result = await ref.doc(id).get().then((value) async {
        if (value.exists) {
          List cards = value.data()['cards'];
          List log = value.data()['log'];
          cards.removeAt(cardIndex);
          DateTime now = DateTime.now();
          String date = DateFormat('yyyy:MM:dd - kk:mm').format(now);
          Map logEntry = {
            'action': "$email deleted card with front '${card['front']}'.",
            'timeStamp': date
          };
          log.add(logEntry);

          final result = await ref.doc(id).update({'log': log, 'cards': cards});
          Provider.of<SubscriptionProvider>(context, listen: false)
              .setRemoving(false);
          Provider.of<SubscriptionProvider>(context, listen: false)
              .removeCard(cardIndex, index);
          shared.removeCardController.nextPage(
              duration: Duration(milliseconds: 500),
              curve: Curves.easeInOutCubic);
        } else {
          Fluttertoast.showToast(
              msg: 'ERROR 001',
              backgroundColor: Colors.red[300],
              textColor: Colors.white);
          Provider.of<SubscriptionProvider>(context, listen: false)
              .setRemoving(false);
        }
      });
    });
  }
}
