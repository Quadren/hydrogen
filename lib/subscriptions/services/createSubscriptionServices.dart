import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:hive/hive.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:intl/intl.dart';
import '../createSubscriptions/creatingSubscription.dart' as shared;

class CreateSubscriptionServices {
  final _chars =
      'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  Random _rnd = Random();

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

  Future<void> getInfoToCreateSubscription(
      String _title, int _pin, bool _private, context) async {
    var box = Hive.box('userData');

    String email = box.get('email');
    String id = getRandomString(6);

    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref =
          FirebaseFirestore.instance.collection('subscriptions');

      // ignore: unused_local_variable
      final result = await ref.doc().get().then((value) {
        if (value.exists) {
          id = getRandomString(6);
          createSubscriptionFirebase(
              _title, _pin, _private, email, id, context);
        } else {
          createSubscriptionFirebase(
              _title, _pin, _private, email, id, context);
        }
      });
    });
  }

  Future<void> createSubscriptionFirebase(String _title, int _pin,
      bool _private, String _owner, String id, context) async {
    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref =
          FirebaseFirestore.instance.collection('subscriptions');
      CollectionReference ref2 = FirebaseFirestore.instance.collection('users');

      DateTime now = DateTime.now();
      String date = DateFormat('yyyy-MM-dd - kk:mm').format(now);

      ref.doc(id).set({
        'title': _title,
        'owner': _owner,
        'cards': [],
        'password': _pin,
        'public': !_private,
        'users': [_owner],
        'moderators': [],
        'log': [
          {
            'action': "A new being is born! $_owner created '$_title'.",
            'timeStamp': date
          }
        ],
        'studyLog': [],
        'attachments': []
      }).whenComplete(() async {
        // ignore: unused_local_variable
        final result = await ref2.doc(_owner).get().then((value) {
          if (value.exists) {
            List subscriptions = value.data()['subscriptions'];
            subscriptions.add(id);
            ref2.doc(_owner).update({'subscriptions': subscriptions});
          }
        });
        shared.createController.nextPage(
            duration: Duration(milliseconds: 500),
            curve: Curves.easeInOutCubic);
        Future.delayed(Duration(seconds: 2), () {
          Phoenix.rebirth(context);
        });
      });
    });
  }
}
