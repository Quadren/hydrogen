import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hydrogen/subscriptions/subscribers/subscribersProvider.dart';
import 'package:provider/provider.dart';

Future<void> getAllSubscribers(String id, context) async {
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('subscriptions');
    CollectionReference ref2 = FirebaseFirestore.instance.collection('users');

    Provider.of<SubscribersListProvider>(context, listen: false).resetSubscribers();

    final result = await ref.doc(id).get().then((value) {
      if (value.exists) {
        List subscribers = value.data()['users'];

        subscribers.forEach((sub) async { 
          final result = await ref2.doc(sub).get().then((val2) {
            if (val2.exists) {
              String pfp = val2.data()['pfp'];
              String name = val2.data()['name'];
              Provider.of<SubscribersListProvider>(context, listen: false).addSubscriber(name, pfp);
            } else {
              Fluttertoast.showToast(msg: 'ERROR 002', backgroundColor: Colors.red[300], textColor: Colors.white);
            }
          });
        });
        Provider.of<SubscribersListProvider>(context, listen: false).finishLoading();
        print ("Subscribers: $subscribers");
      } else {
        Fluttertoast.showToast(msg: 'ERROR 001', backgroundColor: Colors.red[300], textColor: Colors.white);
      }
    });
  });
}