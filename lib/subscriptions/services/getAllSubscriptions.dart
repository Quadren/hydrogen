import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/core/userServices/getUserCategories.dart';
import 'package:hydrogen/core/userServices/userProvider.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:hydrogen/tritium/tritiumProvider.dart';
import 'package:hydrogen/tritium/unPremium/unpremium.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

Future<void> getSubscriptions(BuildContext context) async {
  Firebase.initializeApp().whenComplete(() {
    var box = Hive.box('userData');
    var box2 = Hive.box('categories');
    CollectionReference ref1 = FirebaseFirestore.instance.collection('users');
    CollectionReference ref2 =
        FirebaseFirestore.instance.collection('subscriptions');
    String email = box.get('email');

    Provider.of<SubscriptionProvider>(context, listen: false)
        .clearSubscriptions();
    Provider.of<UserProvider>(context, listen: false).setLoading(true);

    getUserCategories(context);

    ref1.doc(email).get().then((value) {
      if (value.exists) {
        List tempList = value.data()['subscriptions'];
        List sprints = value.data()['sprints'];
        String pfp = value.data()['pfp'];
        String name = value.data()['name'];
        bool premium = value.data()['premium'];

        bool cont;

        box.put('name', name);
        Provider.of<UserProvider>(context, listen: false).setName(name);

        if (premium) {
          print("User is premium");
          box.put('premium', true);
          cont = true;
        } else {
          print("User not premium");
          box.put('premium', false);
          if (tempList.length > 20 || sprints.length > 2) {
            cont = false;
            if (tempList.length > 20 && sprints.length <= 2) {
              Provider.of<TritiumProvider>(context, listen: false)
                  .setOverflowType(1);
            } else if (tempList.length <= 20 && sprints.length > 2) {
              Provider.of<TritiumProvider>(context, listen: false)
                  .setOverflowType(2);
            } else if (tempList.length > 20 && sprints.length > 2) {
              Provider.of<TritiumProvider>(context, listen: false)
                  .setOverflowType(3);
            }

            if (box2.length > 3) {
              Provider.of<TritiumProvider>(context, listen: false)
                  .setCatOverflow(true);
            } else {
              Provider.of<TritiumProvider>(context, listen: false)
                  .setCatOverflow(false);
            }

            Navigator.pushAndRemoveUntil(
                context,
                PageTransition(
                    child: UnPremium(),
                    type: PageTransitionType.rightToLeft,
                    curve: Curves.easeInOutCubic,
                    duration: Duration(milliseconds: 500)),
                (route) => false);
          } else {
            cont = true;
          }
        }
        if (tempList.isNotEmpty && cont == true) {
          tempList.forEach((id) async {
            // ignore: unused_local_variable
            final result = await ref2.doc(id).get().then((value2) {
              String title = value2.data()['title'];
              String owner = value2.data()['owner'];
              List cards = value2.data()['cards'];
              if (box2.get(id) != null) {
                print("$title has category");
                Provider.of<SubscriptionProvider>(context, listen: false)
                    .addSubscription(title, owner, cards, id, box2.get(id));
              } else {
                print("$title has no category");
                Provider.of<SubscriptionProvider>(context, listen: false)
                    .addSubscription(title, owner, cards, id, 'Uncategorized');
              }

              print("HIT: $title with ${cards.length} cards. Owned by $owner");
            });
          });
          Provider.of<UserProvider>(context, listen: false).setPfp(pfp);
          Provider.of<UserProvider>(context, listen: false).setName(name);
          Provider.of<UserProvider>(context, listen: false).setLoading(false);
          Provider.of<SubscriptionProvider>(context, listen: false)
              .setEmpty(false);
          Provider.of<SubscriptionProvider>(context, listen: false)
              .setLoading(false);
          Future.delayed(Duration(milliseconds: 1000), () {
            Provider.of<SubscriptionProvider>(context, listen: false)
                .setAnimate(true);
          });
        } else {
          Provider.of<SubscriptionProvider>(context, listen: false)
              .setEmpty(true);
          Provider.of<SubscriptionProvider>(context, listen: false)
              .setLoading(false);
          Provider.of<UserProvider>(context, listen: false).setPfp(pfp);
          Provider.of<UserProvider>(context, listen: false).setLoading(false);
          print("USER HAS NO SUBSCRIPTIONS");
        }
      }
    });
  });
}
