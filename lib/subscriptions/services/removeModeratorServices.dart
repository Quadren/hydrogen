import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hydrogen/subscriptions/moderation/moderators.dart';
import 'package:hydrogen/subscriptions/services/getModerators.dart';
import 'package:provider/provider.dart';
import '../moderation/removeModerator.dart' as shared;


Future<void> dismissModerator(String email, String photoURL, String name, String id, context) async{
  Firebase.initializeApp().whenComplete(() async {
    CollectionReference ref = FirebaseFirestore.instance.collection('subscriptions');

    // ignore: unused_local_variable
    final result = await ref.doc(id).get().then((value) async {
      if (value.exists) {
        List moderators = value.data()['moderators'];
        moderators.remove(email);
        Provider.of<ModeratorProvider>(context, listen: false).removeMod(email, name, photoURL);
        // ignore: unused_local_variable
        final result = await ref.doc(id).update({'moderators' : moderators});
        Future.delayed(Duration(seconds: 2), () {
          getModerators(id, context);
        });
        shared.removeModController.nextPage(duration: Duration(milliseconds: 500), curve: Curves.easeInOutCubic);
      }
      else {
        Fluttertoast.showToast(msg: 'ERROR 002', backgroundColor: Colors.red[300], textColor: Colors.white);
      }
    });
  });
}