import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/addCards/commitProvider.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class AddCardsServices {
  void addCardToCommit(String front, String back, String eqn, String attachment,
      BuildContext context) {
    Provider.of<CommitProvider>(context, listen: false)
        .addToCommits(front, back, eqn, attachment);
    Provider.of<CommitProvider>(context, listen: false).setEmpty(false);
    print("Added card!");
  }

  Future<void> writeCommitsToFirebase(
      List cards, String id, BuildContext context,
      {bool closeOnLoad}) async {
    Provider.of<CommitProvider>(context, listen: false).setUploading(true);

    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref =
          FirebaseFirestore.instance.collection('subscriptions');

      final result = await ref.doc(id).get().then((value) async {
        if (value.exists) {
          List oldCards = value.data()['cards'];
          List log = value.data()['log'];
          String email = Hive.box('userData').get('email');

          cards.forEach((card) {
            oldCards.add(card);
          });
          print("Added ${cards.length} cards");

          Provider.of<SubscriptionProvider>(context, listen: false)
              .updateSub(id, oldCards);

          DateTime now = DateTime.now();
          String date = DateFormat('yyyy:MM:dd - kk:mm').format(now);

          log.add({
            'action': '$email created ${cards.length} cards.',
            'timeStamp': date
          });

          final result =
              await ref.doc(id).update({'cards': oldCards, 'log': log});
          Provider.of<CommitProvider>(context, listen: false)
              .setUploading(false);
          Provider.of<CommitProvider>(context, listen: false).resetCommits();
          Provider.of<CommitProvider>(context, listen: false).setEmpty(true);
          Fluttertoast.showToast(
              msg: 'Sucessfully commited cards to your subscription.',
              backgroundColor: HomeStyles().accentColor,
              textColor: Colors.white);
          if (closeOnLoad != null && closeOnLoad == true) {
            Navigator.pop(context);
            Navigator.pop(context);
          }
        } else {
          Provider.of<CommitProvider>(context, listen: false)
              .setUploading(false);
          Fluttertoast.showToast(
              msg: 'ERROR 001',
              backgroundColor: Colors.red[300],
              textColor: Colors.white);
        }
      });
    });
  }
}
