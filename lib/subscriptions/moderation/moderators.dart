import 'package:animated/animated.dart';
import 'package:auto_animated/auto_animated.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/moderation/modWidget.dart';
import 'package:hydrogen/subscriptions/services/getModerators.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:provider/provider.dart';

class ModeratorProvider extends ChangeNotifier {
  List<Map> _mods = [];
  bool _loading = true;
  bool _isEmpty = true;
  bool _absorb = false;
  bool _animate = false;
  bool _isElevated = false;

  List<Map> get getMods => _mods;
  bool get getLoading => _loading;
  bool get getEmpty => _isEmpty;
  bool get getAbsorb => _absorb;
  bool get getAnimate => _animate;
  bool get getElevation => _isElevated;

  void resetMods() {
    _mods = [];
    notifyListeners();
  }

  void setAbsorb(bool value) {
    _absorb = value;
    notifyListeners();
  }

  void setLoading(bool value) {
    _loading = value;
    _animate = !value;
    notifyListeners();
  }

  void setEmpty(bool value) {
    _isEmpty = value;
    notifyListeners();
  }

  void addMod(String name, String photoURL, String email) {
    Map tempMap = {'name': name, 'photoURL': photoURL, 'email': email};
    _mods.add(tempMap);
    notifyListeners();
  }

  void removeMod(String email, String name, String photoURL) {
    Map tempMap = {'name': name, 'photoURL': photoURL, 'email': email};
    _mods.remove(tempMap);
    notifyListeners();
  }

  void elevate(bool value) {
    _isElevated = value;
    notifyListeners();
  }
}

class Moderators extends StatefulWidget {
  final int _index;

  Moderators(this._index);

  @override
  _ModeratorsState createState() => _ModeratorsState(_index);
}

class _ModeratorsState extends State<Moderators> {
  final int _index;

  _ModeratorsState(this._index);

  bool _isOwner;

  bool _animate1 = false;

  void _startup() {
    Provider.of<ModeratorProvider>(context, listen: false).resetMods();
    Provider.of<ModeratorProvider>(context, listen: false).setLoading(true);

    getModerators(
        Provider.of<SubscriptionProvider>(context, listen: false)
            .getSubscriptions[_index]['id'],
        context);
  }

  void checkIfOwner() {
    var box = Hive.box('userData');
    String owner = Provider.of<SubscriptionProvider>(context, listen: false)
        .getSubscriptions[_index]['owner'];

    if (box.get('email') == owner) {
      setState(() {
        _isOwner = true;
      });
    } else {
      setState(() {
        _isOwner = false;
      });
    }
  }

  @override
  void initState() {
    setState(() {
      _animate1 = false;
    });
    Future.delayed(Duration(milliseconds: 200), () {
      setState(() {
        _animate1 = true;
      });
    });
    checkIfOwner();
    Future.delayed(Duration.zero, () {
      _startup();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    final scrollController = ScrollController();

    Map subscription =
        Provider.of<SubscriptionProvider>(context).getSubscriptions[_index];
    String id = subscription['id'];

    List<Map> mods = Provider.of<ModeratorProvider>(context).getMods;

    var loading = Provider.of<ModeratorProvider>(context).getLoading;
    var empty = Provider.of<ModeratorProvider>(context).getEmpty;
    var animate2 = Provider.of<ModeratorProvider>(context).getAnimate;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: HomeStyles().backgroundColor,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: HomeStyles().primaryColor),
        leading: InkWell(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          onTap: () => Navigator.pop(context),
          child: Container(
            margin: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.05,
                top: MediaQuery.of(context).size.height * 0.03),
            decoration: BoxDecoration(
              color: HomeStyles().backgroundColor,
              borderRadius: BorderRadius.circular(100),
            ),
            child: Center(
                child: FaIcon(
              FontAwesomeIcons.arrowLeft,
              color: HomeStyles().primaryColor,
              size: MediaQuery.of(context).size.height * 0.025,
            )),
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: width * 0.1),
        child: Stack(
          children: [
            ScrollConfiguration(
              behavior: AntiScrollGlow(),
              child: NestedScrollView(
                controller: scrollController,
                headerSliverBuilder: (context, bool isInnerBoxScrolled) {
                  return [
                    SliverToBoxAdapter(
                      child: Animated(
                        duration: Duration(seconds: 1),
                        curve: Curves.easeInOutCubic,
                        value: _animate1 ? 1 : 0,
                        builder: (context, child, animation) =>
                            Transform.translate(
                          offset: Offset(0, (1 - animation.value) * 40),
                          child: Opacity(
                            opacity: animation.value,
                            child: child,
                          ),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: height * 0.2),
                            Text('Moderators', style: HomeStyles().bigText),
                            SizedBox(height: height * 0.01),
                            Text('for deck with ID $id',
                                style: HomeStyles().subtitleText),
                            SizedBox(height: height * 0.05),
                            Divider(color: Color(0xFFAAAAAA)),
                            SizedBox(height: height * 0.05),
                          ],
                        ),
                      ),
                    ),
                  ];
                },
                body: PageView(
                  children: [
                    Container(
                        child: loading
                            ? Column(
                                children: [
                                  SizedBox(height: height * 0.15),
                                  Transform.scale(
                                    scale: 1.5,
                                    child: CircularProgressIndicator(
                                      strokeWidth: 7,
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          HomeStyles().accentColor),
                                      backgroundColor:
                                          HomeStyles().secondaryAccent,
                                    ),
                                  ),
                                ],
                              )
                            : Animated(
                                duration: Duration(milliseconds: 500),
                                curve: Curves.easeInOutCubic,
                                value: animate2 ? 1 : 0,
                                builder: (context, child, animation) =>
                                    Transform.translate(
                                      offset:
                                          Offset(0, (1 - animation.value) * 40),
                                      child: Opacity(
                                        opacity: animation.value,
                                        child: child,
                                      ),
                                    ),
                                child: ScrollConfiguration(
                                  behavior: AntiScrollGlow(),
                                  child: CustomScrollView(
                                    primary: false,
                                    slivers: [
                                      SliverToBoxAdapter(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                                'moderators (long press to edit)',
                                                style:
                                                    HomeStyles().subtitleText),
                                            SizedBox(height: height * 0.03),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        child: empty
                                            ? LiveSliverGrid(
                                                gridDelegate:
                                                    SliverGridDelegateWithFixedCrossAxisCount(
                                                  crossAxisCount: 2,
                                                  childAspectRatio:
                                                      (height * 0.2) /
                                                          (height * 0.25),
                                                ),
                                                itemBuilder: (context, index,
                                                    animation) {
                                                  return FadeTransition(
                                                    opacity: Tween<double>(
                                                            begin: 0, end: 1)
                                                        .animate(animation),
                                                    child: ModWidget(
                                                        'https://firebasestorage.googleapis.com/v0/b/hydrogen-7a6c8.appspot.com/o/images%2Finvite.jpg?alt=media&token=0ca79797-bc34-49f4-a53e-7c521e749eb5',
                                                        'Add',
                                                        id,
                                                        _isOwner,
                                                        ''),
                                                  );
                                                },
                                                itemCount: 1,
                                                controller: scrollController,
                                                showItemDuration:
                                                    Duration(milliseconds: 250),
                                              )
                                            : LiveSliverGrid(
                                                gridDelegate:
                                                    SliverGridDelegateWithFixedCrossAxisCount(
                                                  crossAxisCount: 2,
                                                  childAspectRatio:
                                                      (height * 0.2) /
                                                          (height * 0.25),
                                                ),
                                                itemBuilder: (context, index,
                                                    animation) {
                                                  return FadeTransition(
                                                    opacity: Tween<double>(
                                                            begin: 0, end: 1)
                                                        .animate(animation),
                                                    child: SlideTransition(
                                                      position: Tween<Offset>(
                                                              begin: Offset(
                                                                  0, 0.1),
                                                              end: Offset(0, 0))
                                                          .animate(animation),
                                                      child: ModWidget(
                                                          mods[index]
                                                              ['photoURL'],
                                                          mods[index]['name'],
                                                          id,
                                                          _isOwner,
                                                          mods[index]['email']),
                                                    ),
                                                  );
                                                },
                                                itemCount: mods.length,
                                                controller: scrollController,
                                                showItemDuration:
                                                    Duration(milliseconds: 250),
                                              ),
                                      ),
                                    ],
                                  ),
                                ))),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
