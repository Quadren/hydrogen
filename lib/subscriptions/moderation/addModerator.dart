import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/core/misc/shakeAnim.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/moderation/moderators.dart';
import 'package:hydrogen/subscriptions/services/addModeratorServices.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:sprung/sprung.dart';

void showAddModerator(BuildContext context, String id) {
  showDialog(context: context, builder: (addCont) => AddModerator(id));
}

class AddModerator extends StatefulWidget {
  @override
  _AddModeratorState createState() => _AddModeratorState(_id);

  final String _id;

  AddModerator(this._id);
}

ShakeController addModShakeController;
PageController addModPageController = PageController(initialPage: 0);

class _AddModeratorState extends State<AddModerator>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  TextEditingController _controller1 = TextEditingController();

  final String _id;

  _AddModeratorState(this._id);

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      Provider.of<ModeratorProvider>(context, listen: false).setAbsorb(false);
    });
    addModShakeController = ShakeController(vsync: this);
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Sprung.criticallyDamped);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
            height: height * 0.4,
            width: width * 0.75,
            decoration: ShapeDecoration(
                color: HomeStyles().backgroundColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(21))),
            child: PageView(
              controller: addModPageController,
              physics: NeverScrollableScrollPhysics(),
              children: [
                pageOne(),
                pageTwo(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget pageOne() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    OutlineInputBorder _border = OutlineInputBorder(
      borderRadius: BorderRadius.circular(21),
      borderSide: BorderSide(width: 1, color: HomeStyles().primaryColor),
    );

    OutlineInputBorder _errorBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(21),
      borderSide: BorderSide(width: 1, color: Colors.red[300]),
    );

    InputDecoration _formDecor = InputDecoration(
      border: _border,
      errorBorder: _errorBorder,
      disabledBorder: _border,
      enabledBorder: _border,
      focusedBorder: _border,
      contentPadding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.05,
          vertical: MediaQuery.of(context).size.height * 0.04),
      errorStyle: TextStyle(
        fontFamily: 'Raleway',
        fontSize: 12,
        color: Colors.red[300],
        fontWeight: FontWeight.w400,
      ),
    );

    bool absorb = Provider.of<ModeratorProvider>(context).getAbsorb;

    return Padding(
        padding: EdgeInsets.all(width * 0.05),
        child: AnimatedOpacity(
          opacity: absorb ? 0.5 : 1,
          duration: Duration(milliseconds: 300),
          child: AbsorbPointer(
            absorbing: absorb,
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: height * 0.02),
                    Text("enter moderator email",
                        style: HomeStyles().subtitleText),
                    SizedBox(height: height * 0.05),
                    ShakeView(
                        child: TextFormField(
                          controller: _controller1,
                          decoration: _formDecor,
                          cursorColor: HomeStyles().primaryColor,
                          style: HomeStyles().headerText,
                        ),
                        controller: addModShakeController)
                  ],
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: InkWell(
                    onTap: () {
                      Provider.of<ModeratorProvider>(context, listen: false)
                          .setAbsorb(true);
                      validateUser(_controller1.text, _id, context);
                    },
                    child: AnimatedContainer(
                      duration: Duration(milliseconds: 250),
                      height: height * 0.075,
                      width: width * 0.3,
                      decoration: BoxDecoration(
                        color: (_controller1.text.length > 6)
                            ? HomeStyles().accentColor
                            : HomeStyles().accentColor.withOpacity(0.3),
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text('Add', style: HomeStyles().buttonText3),
                            FaIcon(FontAwesomeIcons.arrowRight,
                                color: HomeStyles().backgroundColor,
                                size: height * 0.015)
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  Widget pageTwo() {
    return Center(
      child: Container(
        height: MediaQuery.of(context).size.height * 0.2,
        width: MediaQuery.of(context).size.height * 0.2,
        child: Lottie.asset('assets/lottie/check.json',
            repeat: false, fit: BoxFit.contain),
      ),
    );
  }
}
