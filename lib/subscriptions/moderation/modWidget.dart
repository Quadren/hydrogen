import 'package:animated/animated.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/moderation/addModerator.dart';
import 'package:hydrogen/subscriptions/moderation/removeModerator.dart';
import 'package:vibration/vibration.dart';

class ModWidget extends StatefulWidget {
  final String _url;
  final String _name;
  final String _email;
  final String _id;
  final bool _isOwner;

  ModWidget(this._url, this._name, this._id, this._isOwner, this._email);

  @override
  _ModWidgetState createState() =>
      _ModWidgetState(_url, _name, _id, _isOwner, _email);
}

class _ModWidgetState extends State<ModWidget> {
  final String _url;
  final String _name;
  final String _email;
  final String id;
  final bool _isOwner;

  _ModWidgetState(this._url, this._name, this.id, this._isOwner, this._email);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    return Container(
      child: GestureDetector(
        onTap: () {
          if (_name == 'Add' && _isOwner) {
            print("Add button pressed");
            showAddModerator(context, id);
          } else if (!_isOwner) {
            Fluttertoast.showToast(
                msg: 'Owner locked function!',
                backgroundColor: Colors.red[300],
                textColor: Colors.white);
          }
        },
        onLongPress: () {
          if (_name != 'Add' && _isOwner) {
            Vibration.vibrate(amplitude: 1, duration: 50);
            print("Delete menu opened");
            removeModerator(id, _email, _url, _name, context);
          }
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: height * 0.15,
              width: height * 0.15,
              decoration: BoxDecoration(
                border:
                    Border.all(width: 3, color: HomeStyles().secondaryAccent),
                borderRadius: BorderRadius.circular(100),
              ),
              child: ClipOval(
                child: CachedNetworkImage(
                  imageUrl: _url,
                  fit: BoxFit.cover,
                  placeholder: (context, String url) {
                    return Center(
                      child: Container(
                        height: height * 0.2,
                        width: height * 0.2,
                        color: Color(0xFFAAAAAA),
                      ),
                    );
                  },
                ),
              ),
            ),
            SizedBox(height: height * 0.02),
            Text(
              '$_name',
              style: HomeStyles().buttonText,
            )
          ],
        ),
      ),
    );
  }
}
