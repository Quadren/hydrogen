import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/subscriptions/addSubscriptions/importFromWeb.dart';
import 'package:hydrogen/subscriptions/newAddSubscriptions/pages/importFromWeb.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class ImportServices {
  Future<void> checkIfValid(String id, BuildContext context) async {
    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref =
          FirebaseFirestore.instance.collection('subscriptions');
      String email = Hive.box('userData').get('email');

      bool cont;

      try {
        final result = await ref.doc(id).get().then((value) {
          if (value.exists) {
            bool public = value.data()['public'];
            int pin = value.data()['password'];
            List users = value.data()['users'];

            if (users.contains(email)) {
              cont = false;
              importFromWebShake.shake();
              Fluttertoast.showToast(
                  msg: "You're already subscribed to this!",
                  backgroundColor: Colors.red[300],
                  textColor: Colors.white);
            } else {
              if (public) {
                cont = false;
                Provider.of<ImportProvider>(context, listen: false)
                    .setProtected(false);
                Provider.of<ImportProvider>(context, listen: false).setPin(pin);
                addToSubscriptions(context, id);
              } else {
                cont = true;
                Provider.of<ImportProvider>(context, listen: false)
                    .setProtected(true);
                Provider.of<ImportProvider>(context, listen: false).setPin(pin);
              }
            }
          } else {
            cont = false;
            importFromWebShake.shake();
            Fluttertoast.showToast(
                msg: 'Invalid ID',
                backgroundColor: Colors.red[300],
                textColor: Colors.white);
          }
        });

        Provider.of<ImportProvider>(context, listen: false).setID(id);
        Provider.of<ImportProvider>(context, listen: false).setDisabled(false);
        if (cont) {
          impWebController.nextPage(
              duration: Duration(milliseconds: 500),
              curve: Curves.easeInOutCubic);
        }
      } catch (e) {
        Fluttertoast.showToast(
            msg: e.toString(),
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  }

  Future<void> addToSubscriptions(BuildContext context, String id) async {
    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref = FirebaseFirestore.instance.collection('users');
      CollectionReference ref2 =
          FirebaseFirestore.instance.collection('subscriptions');

      String email = Hive.box('userData').get('email');
      Provider.of<ImportProvider>(context, listen: false).setDisabled(true);

      try {
        final result = await ref.doc(email).get().then((value) {
          List subscriptions = value.data()['subscriptions'];
          List log = value.data()['log'];

          subscriptions.add(id);
          DateTime now = DateTime.now();
          String formatted = DateFormat('yyyy-MM-dd - kk:mm').format(now);
          Map entry = {
            'action': 'You joined subscription with ID $id.',
            'timeStamp': formatted
          };

          log.add(entry);

          ref.doc(email).update({'subscriptions': subscriptions, 'log': log});
        });

        final result2 = await ref2.doc(id).get().then((value) {
          List users = value.data()['users'];
          List log = value.data()['log'];
          String title = value.data()['title'];
          String owner = value.data()['owner'];
          List cards = value.data()['cards'];

          users.add(email);
          DateTime now = DateTime.now();
          String formatted = DateFormat('yyyy-MM-dd - kk:mm').format(now);
          Map entry = {
            'action': '$email joined this subscription.',
            'timeStamp': formatted
          };
          log.add(entry);

          ref2.doc(id).update({'users': users, 'log': log});

          Provider.of<SubscriptionProvider>(context, listen: false)
              .addSubscription(title, owner, cards, id, 'Uncategorized');
        });

        impWebController.nextPage(
            duration: Duration(milliseconds: 500),
            curve: Curves.easeInOutCubic);
        Provider.of<ImportProvider>(context, listen: false).setDisabled(false);
        Future.delayed(Duration(seconds: 2), () {
          Navigator.pop(context);
        });
      } catch (e) {
        Fluttertoast.showToast(
            msg: e.toString(),
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  }
}
