import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/createSubscriptions/createSubscriptions.dart';
import 'package:hydrogen/subscriptions/newAddSubscriptions/pages/importFromWeb.dart';
import 'package:page_transition/page_transition.dart';

class FirstPageAddSubSheet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return Stack(
      children: [
        Align(
          alignment: Alignment.topCenter,
          child: Padding(
            padding: EdgeInsets.only(top: height * 0.03),
            child: Text(
              'Select an option',
              style: HomeStyles().headerText,
            ),
          ),
        ),
        Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(bottom: height * 0.03),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  button('Create new', FontAwesomeIcons.plus, () {
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        PageTransition(
                            child: CreateSubscriptions(),
                            type: PageTransitionType.rightToLeft,
                            duration: Duration(milliseconds: 500),
                            curve: Curves.easeInOutCubic));
                  }, height, width),
                  button('Import from web', FontAwesomeIcons.wifi, () {
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        PageTransition(
                            child: ImportFromWebNew(),
                            type: PageTransitionType.rightToLeft,
                            duration: Duration(milliseconds: 500),
                            curve: Curves.easeInOutCubic));
                  }, height, width),
                ],
              ),
            )),
      ],
    );
  }

  Widget button(String title, IconData icon, VoidCallback func, double height,
      double width) {
    return BouncingWidget(
      child: Container(
        height: height * 0.15,
        width: width * 0.425,
        decoration: BoxDecoration(
          color: HomeStyles().backgroundColor,
          border: Border.all(width: 2, color: Color(0xffEEEEEE)),
          borderRadius: BorderRadius.circular(21),
        ),
        padding: EdgeInsets.all(width * 0.05),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            FaIcon(icon, size: height * 0.02, color: HomeStyles().primaryColor),
            SizedBox(height: height * 0.03),
            Text(
              title,
              textAlign: TextAlign.center,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: HomeStyles().buttonText2,
            ),
          ],
        ),
      ),
      onPressed: func,
      duration: Duration(milliseconds: 150),
    );
  }
}
