import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hydrogen/core/misc/shakeAnim.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/addSubscriptions/importFromWeb.dart';
import 'package:hydrogen/subscriptions/newAddSubscriptions/services/importServices.dart';
import 'package:hydrogen/widgets/backBar.dart';
import 'package:lottie/lottie.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';

class ImportFromWebNew extends StatefulWidget {
  @override
  _ImportFromWebNewState createState() => _ImportFromWebNewState();
}

ShakeController importFromWebShake;
PageController impWebController = PageController(initialPage: 0);

class _ImportFromWebNewState extends State<ImportFromWebNew>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    importFromWebShake = ShakeController(vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    importFromWebShake.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    bool secure = Provider.of<ImportProvider>(context).getProtected;
    bool _disable = Provider.of<ImportProvider>(context).getDisabled;

    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return Scaffold(
      backgroundColor: HomeStyles().backgroundColor,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: Backbar(),
      ),
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          PageView(
            physics: NeverScrollableScrollPhysics(),
            controller: impWebController,
            children: [
              pageOne(),
              Container(
                child: secure ? pageTwo() : pageTwoUnsecured(),
              ),
              pageTwoUnsecured(),
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
                width: width,
                child: AnimatedOpacity(
                  opacity: _disable ? 1 : 0,
                  duration: Duration(milliseconds: 150),
                  child: LinearProgressIndicator(
                    valueColor:
                        AlwaysStoppedAnimation(HomeStyles().accentColor),
                    backgroundColor: HomeStyles().secondaryAccent,
                    minHeight: 10,
                  ),
                )),
          ),
        ],
      ),
    );
  }

  final _key = GlobalKey<FormState>();
  final _key2 = GlobalKey<FormState>();

  Widget pageOne() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    bool _disable = Provider.of<ImportProvider>(context).getDisabled;

    return Stack(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: width * 0.1),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Enter\ndeck ID', style: HomeStyles().bigText),
                SizedBox(height: height * 0.05),
                AnimatedOpacity(
                  opacity: _disable ? 0.5 : 1,
                  duration: Duration(milliseconds: 300),
                  child: AbsorbPointer(
                      absorbing: _disable,
                      child: ShakeView(
                          child: Form(
                            key: _key,
                            child: Padding(
                              padding: EdgeInsets.zero,
                              child: PinCodeTextField(
                                appContext: context,
                                length: 6,
                                onChanged: null,
                                cursorColor: Colors.transparent,
                                animationType: AnimationType.slide,
                                enablePinAutofill: false,
                                keyboardType: TextInputType.name,
                                enableActiveFill: false,
                                textStyle: TextStyle(
                                  fontFamily: 'Raleway',
                                  fontSize: 24,
                                  color: HomeStyles().primaryColor,
                                  fontWeight: FontWeight.w600,
                                ),
                                onCompleted: (value) {
                                  FocusScope.of(context).unfocus();
                                  print(value);
                                  Provider.of<ImportProvider>(context,
                                          listen: false)
                                      .setDisabled(true);
                                  ImportServices().checkIfValid(value, context);
                                },
                                pinTheme: PinTheme(
                                  shape: PinCodeFieldShape.underline,
                                  borderRadius: null,
                                  activeColor: Color(0xff6953F5),
                                  disabledColor: HomeStyles().primaryColor,
                                  selectedColor: HomeStyles().primaryColor,
                                  inactiveColor: HomeStyles()
                                      .primaryColor
                                      .withOpacity(0.3),
                                ),
                              ),
                            ),
                          ),
                          controller: importFromWebShake)),
                ),
                SizedBox(height: height * 0.1),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget pageTwo() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    int pin = Provider.of<ImportProvider>(context).getPin;
    bool _disable = Provider.of<ImportProvider>(context).getDisabled;
    String id = Provider.of<ImportProvider>(context).getID;

    return Stack(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: width * 0.1),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Enter pin', style: HomeStyles().bigText),
                SizedBox(height: height * 0.05),
                AbsorbPointer(
                  absorbing: _disable,
                  child: AnimatedOpacity(
                    opacity: _disable ? 0.5 : 1,
                    duration: Duration(milliseconds: 150),
                    child: ShakeView(
                        child: Form(
                          key: _key2,
                          child: Padding(
                            padding: EdgeInsets.zero,
                            child: PinCodeTextField(
                              appContext: context,
                              length: 6,
                              onChanged: null,
                              cursorColor: Colors.transparent,
                              animationType: AnimationType.slide,
                              enablePinAutofill: false,
                              keyboardType: TextInputType.number,
                              enableActiveFill: false,
                              textStyle: TextStyle(
                                fontFamily: 'Raleway',
                                fontSize: 24,
                                color: HomeStyles().primaryColor,
                                fontWeight: FontWeight.w600,
                              ),
                              onCompleted: (value) {
                                FocusScope.of(context).unfocus();
                                if (int.parse(value) != pin) {
                                  importFromWebShake.shake();
                                  Fluttertoast.showToast(
                                      msg: 'Incorrect PIN',
                                      backgroundColor: Colors.red[300],
                                      textColor: Colors.white);
                                } else {
                                  ImportServices()
                                      .addToSubscriptions(context, id);
                                }
                              },
                              pinTheme: PinTheme(
                                shape: PinCodeFieldShape.underline,
                                borderRadius: null,
                                activeColor: Color(0xff6953F5),
                                disabledColor: HomeStyles().primaryColor,
                                selectedColor: HomeStyles().primaryColor,
                                inactiveColor:
                                    HomeStyles().primaryColor.withOpacity(0.3),
                              ),
                            ),
                          ),
                        ),
                        controller: importFromWebShake),
                  ),
                ),
                SizedBox(height: height * 0.1),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget pageTwoUnsecured() {
    return Center(
      child: Container(
        height: MediaQuery.of(context).size.height * 0.2,
        width: MediaQuery.of(context).size.height * 0.2,
        child: Lottie.asset('assets/lottie/check.json',
            repeat: false, fit: BoxFit.contain),
      ),
    );
  }
}
