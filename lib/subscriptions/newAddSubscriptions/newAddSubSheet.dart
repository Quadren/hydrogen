import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/core/premium/freeLimit.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/newAddSubscriptions/pages/firstPage.dart';
import 'package:hydrogen/subscriptions/subscription_provider.dart';
import 'package:provider/provider.dart';

void showAddNewSubSheet(BuildContext context) {
  showModalBottomSheet(
      context: context,
      builder: (ctx) => NewAddSubSheet(),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(21), topRight: Radius.circular(21)),
      ));
}

class NewAddSubSheet extends StatefulWidget {
  @override
  _NewAddSubSheetState createState() => _NewAddSubSheetState();
}

class _NewAddSubSheetState extends State<NewAddSubSheet> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    List subscriptions =
        Provider.of<SubscriptionProvider>(context).getSubscriptions;
    bool premium = Hive.box('userData').get('premium');

    return Container(
      height: height * 0.3,
      decoration: BoxDecoration(
        color: HomeStyles().backgroundColor,
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(21), topLeft: Radius.circular(21)),
      ),
      padding: EdgeInsets.symmetric(horizontal: width * 0.05),
      child: (subscriptions.length >= 20 && premium == false)
          ? FreeLimit()
          : FirstPageAddSubSheet(),
    );
  }
}
