import 'package:flutter/material.dart';

class LoginStyles {

  // use these styles for the login page
  TextStyle headerText = TextStyle(
    fontFamily: 'Raleway',
    fontSize: 32,
    color: Color(0xFF1C1C1C),
    fontWeight: FontWeight.w600
  );
  TextStyle highlightedHeaderText = TextStyle(
    fontFamily: 'Raleway',
    fontSize: 32,
    color: Color(0xFFFED8C0),
    fontWeight: FontWeight.w600
  );

  TextStyle subtitleText = TextStyle(
    fontFamily: 'Raleway',
    fontSize: 16,
    color: Colors.grey[500],
    fontWeight: FontWeight.w400
  );

  TextStyle buttonText = TextStyle(
    fontFamily: 'Raleway',
    fontSize: 16,
    color: Color(0xFF1C1C1C),
    fontWeight: FontWeight.w400
  );

  Color accentColor = Color(0xFFFED8C0);
  Color primaryColor = Color(0xFF1C1C1C);
  Color backgroundColor = Color(0xFFFEFEFE);
}