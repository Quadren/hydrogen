import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/auth/services/googleSignIn.dart';
import 'package:hydrogen/core/userServices/checkIfUserExists.dart';

import '../login_styles.dart';

class LoginPageTwo extends StatefulWidget {
  @override
  _LoginPageTwoState createState() => _LoginPageTwoState();
}

class _LoginPageTwoState extends State<LoginPageTwo> {
  bool _tapped = false;
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Container(
      padding: EdgeInsets.all(height * 0.05),
      child: Stack(
        children: [
          Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Sign in with Google', style: LoginStyles().headerText,),
          SizedBox(height: height * 0.02),
          Container(
            width: width * 0.6,
            child: Text("If you don't have an account, we'll make one for you", style: LoginStyles().subtitleText,),
          ),
        ],
      ),
      Align(
        alignment: Alignment.bottomRight,
        child: BouncingWidget(
          onPressed: () {
            setState(() {
                _tapped = true;
              });
            signInWithGoogle().then((value) {
              
              if (value != null) {
                print ("Signed in successfully");
                checkIfUserExists(email, photoURL).whenComplete(() {
                  setState(() {
                _tapped = false;
              });
                });
              } else {
                Fluttertoast.showToast(msg: 'Failed to sign in', backgroundColor: Colors.red[300], textColor: Colors.white);
                setState(() {
                _tapped = false;
              });
              }
            });
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              FaIcon(FontAwesomeIcons.google, color: LoginStyles().primaryColor, size: height * 0.03),
              SizedBox(width: width * 0.02),
              Container(
                child: _tapped ? Transform.scale(
                  scale: 0.7,
                  child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(LoginStyles().primaryColor),
                ),
                ) : Icon(Icons.arrow_forward, color: LoginStyles().primaryColor, size: height * 0.03),
              ),
            ],
          ),
        ),
      ),
        ],
      ),
    );
  }
}