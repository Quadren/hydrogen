import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hydrogen/home/home.dart';
import 'package:hydrogen/login/login_styles.dart';
import 'package:page_transition/page_transition.dart';

class LoginPageFour extends StatefulWidget {
  @override
  _LoginPageFourState createState() => _LoginPageFourState();
}

class _LoginPageFourState extends State<LoginPageFour> {

  @override
  void initState() {
    Timer(Duration(seconds: 3), () {
      Navigator.pushAndRemoveUntil(context, PageTransition(child: Home(), type: PageTransitionType.rightToLeft, duration: Duration(milliseconds: 500), curve: Curves.easeInOutCubic), (route) => false);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Container(
      padding: EdgeInsets.all(height * 0.05),
      child: Stack(
        children: [
          Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Wrapping up', style: LoginStyles().headerText,),
          SizedBox(height: height * 0.02),
          Container(
            width: width * 0.6,
            child: Text("we're just finishing up in the background", style: LoginStyles().subtitleText,),
          ),
        ],
      ),
      Align(
        alignment: Alignment.bottomRight,
        child: Transform.scale(
                  scale: 0.7,
                  child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(LoginStyles().primaryColor),
                ),
                ),
      ),
        ],
      ),
    );
  }
}