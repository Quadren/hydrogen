import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/core/userServices/createNewUser.dart';
import 'package:hydrogen/login/login_styles.dart';

import 'dart:math';
import '../login_shared.dart' as shared;

class LoginPageThree extends StatefulWidget {
  @override
  _LoginPageThreeState createState() => _LoginPageThreeState();
}

class _LoginPageThreeState extends State<LoginPageThree> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _controller = TextEditingController();

  bool _pressed = false;

  final _chars =
      'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  Random _rnd = Random();

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

  @override
  Widget build(BuildContext context) {
    OutlineInputBorder _border = OutlineInputBorder(
      borderRadius: BorderRadius.circular(21),
      borderSide: BorderSide(width: 1, color: LoginStyles().primaryColor),
    );

    OutlineInputBorder _errorBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(21),
      borderSide: BorderSide(width: 1, color: Colors.red[300]),
    );

    InputDecoration _formDecor = InputDecoration(
      border: _border,
      errorBorder: _errorBorder,
      disabledBorder: _border,
      enabledBorder: _border,
      focusedBorder: _border,
      contentPadding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.05,
          vertical: MediaQuery.of(context).size.height * 0.03),
      errorStyle: TextStyle(
        fontFamily: 'Raleway',
        fontSize: 12,
        color: Colors.red[300],
        fontWeight: FontWeight.w400,
      ),
    );

    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Container(
      padding: EdgeInsets.all(height * 0.05),
      child: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'What do we call you?',
                style: LoginStyles().headerText,
              ),
              SizedBox(height: height * 0.04),
              Container(
                width: width * 0.8,
                child: Form(
                  key: _formKey,
                  child: TextFormField(
                    maxLength: 16,
                    validator: (value) {
                      if (value.length < 3) {
                        return "Username is too short!";
                      } else {
                        return null;
                      }
                    },
                    cursorColor: LoginStyles().primaryColor,
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      color: LoginStyles().primaryColor,
                      fontFamily: 'Raleway',
                    ),
                    controller: _controller,
                    decoration: _formDecor,
                  ),
                ),
              ),
            ],
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: BouncingWidget(
              onPressed: () {
                if (!_pressed) {
                  setState(() {
                    _pressed = true;
                  });
                  if (_formKey.currentState.validate()) {
                    var box = Hive.box('userData');
                    createNewUser(box.get('email'), _controller.text,
                            getRandomString(16), box.get('pfp'))
                        .whenComplete(() {
                      shared.setupController.animateToPage(3,
                          duration: Duration(milliseconds: 500),
                          curve: Curves.easeInOutCubic);
                    }, );
                  }
                }
              },
              child: Container(
                child: _pressed
                    ? Transform.scale(
                        scale: 0.7,
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(
                              LoginStyles().primaryColor),
                        ),
                      )
                    : Icon(Icons.arrow_forward,
                        color: LoginStyles().primaryColor, size: height * 0.03),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
