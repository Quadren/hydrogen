import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:hydrogen/login/login_shared.dart' as shared;
import 'package:hydrogen/login/login_styles.dart';

class LoginPageOne extends StatefulWidget {
  @override
  _LoginPageOneState createState() => _LoginPageOneState();
}

class _LoginPageOneState extends State<LoginPageOne> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Container(
      padding: EdgeInsets.all(height * 0.05),
      child: Stack(
        children: [
          Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Make studying collaborative.', style: LoginStyles().headerText,),
          SizedBox(height: height * 0.02),
          Container(
            width: width * 0.6,
            child: Text('flash cards meant to be shared', style: LoginStyles().subtitleText,),
          ),
        ],
      ),
      Align(
        alignment: Alignment.bottomRight,
        child: BouncingWidget(
          onPressed: () {
            shared.setupController.animateToPage(1, duration: Duration(milliseconds: 500), curve: Curves.easeInOutCubic);
          },
          child: Icon(Icons.arrow_forward, color: LoginStyles().primaryColor, size: height * 0.03),
        ),
      ),
        ],
      ),
    );
  }
}