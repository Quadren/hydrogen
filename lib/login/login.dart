import 'package:flutter/material.dart';
import 'package:hydrogen/login/login_styles.dart';
import 'package:hydrogen/login/pages/loginPageFour.dart';
import 'package:hydrogen/login/pages/loginPageOne.dart';
import 'package:hydrogen/login/pages/loginPageThree.dart';
import 'package:hydrogen/login/pages/loginPageTwo.dart';
import './login_shared.dart' as shared;

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: _loginBody(),
    );
  }

  Widget _loginBody() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Stack(
      children: [
        //background image
        Container(
          height: height,
          width: width,
          decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage('assets/setup-bg.JPG'), fit: BoxFit.cover),
          ),
        ),
        Center(
          child: Container(
            height: height * 0.4,
            width: width * 0.9, 
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(21),
              color: LoginStyles().backgroundColor,
            ),
            child: Center(
              child: PageView(
                physics: NeverScrollableScrollPhysics(),
                controller: shared.setupController,
                children: [
                  LoginPageOne(),
                  LoginPageTwo(),
                  LoginPageThree(),
                  LoginPageFour(),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
