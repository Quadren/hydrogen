import 'package:flutter/material.dart';
import 'package:hydrogen/core/misc/checkUpdate.dart';
import 'package:hydrogen/home/home_styles.dart';

class Wrapper extends StatefulWidget {
  @override
  _WrapperState createState() => _WrapperState();
}

class _WrapperState extends State<Wrapper> {
  @override
  void initState() {
    // DO NOT USE BYPASSDEBUG FOR RELEASE BUILDS!
    Future.delayed(Duration.zero, () {
      UpdateServices().checkPlatform(context);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HomeStyles().backgroundColor,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('Hydrogen', style: HomeStyles().bigText),
            SizedBox(height: MediaQuery.of(context).size.height * 0.1),
            Transform.scale(
              scale: 1.7,
              child: CircularProgressIndicator(
                strokeWidth: 7,
                valueColor: AlwaysStoppedAnimation(HomeStyles().accentColor),
                backgroundColor: HomeStyles().secondaryAccent,
              ),
            )
          ],
        ),
      ),
    );
  }
}
