import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/newLogin/newLogin.dart';
import 'package:hydrogen/tritium/tritiumServices.dart';
import 'package:page_transition/page_transition.dart';

void checkIfSignedIn(BuildContext context) {
  var box = Hive.box('userData');

  if (box.get('isSignedIn') != null || box.isNotEmpty) {
    TritiumServices().initPlatformState(context);
  } else {
    push(NewLogin(), context);
  }
}

void push(Widget child, BuildContext context) {
  Navigator.pushAndRemoveUntil(
      context,
      PageTransition(
          child: child,
          type: PageTransitionType.fade,
          duration: Duration(milliseconds: 500),
          curve: Curves.easeInOutCubic),
      (route) => false);
}
