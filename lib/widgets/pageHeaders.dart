import 'package:flutter/material.dart';
import 'package:hydrogen/home/home_styles.dart';

class PageHeaders extends StatelessWidget {

  final String _label;

  PageHeaders(this._label);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(height: height * 0.125),
        Text(_label, style: HomeStyles().headerText),
        SizedBox(height: height * 0.075),
      ],
    );
  }
}