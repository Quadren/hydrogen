import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/home/home_styles.dart';

class CustomNavBar extends StatefulWidget {
  @override
  _CustomNavBarState createState() => _CustomNavBarState();
}

class _CustomNavBarState extends State<CustomNavBar> {

  bool _selected1 = false;
  bool _selected2 = true;
  bool _selected3 = false;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Container(
      height: height * 0.075,
      width: width * 0.8,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100),
        color: HomeStyles().backgroundColor,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          InkWell(
            onTap: () {
              if (!_selected1) {
                setState(() {
                  _selected1 = true;
                  _selected2 = false;
                  _selected3 = false;
                });
              }
            },
            child: FaIcon(FontAwesomeIcons.forward, color: _selected1 ? HomeStyles().primaryColor : Colors.grey[300], size: height * 0.025),
          ),
          InkWell(
            onTap: () {
              if (!_selected2) {
                setState(() {
                  _selected1 = false;
                  _selected2 = true;
                  _selected3 = false;
                });
              }
            },
            child: FaIcon(FontAwesomeIcons.home, color: _selected2 ? HomeStyles().primaryColor : Colors.grey[300], size: height * 0.025),
          ),
          InkWell(
            onTap: () {
              if (!_selected3) {
                setState(() {
                  _selected1 = false;
                  _selected2 = false;
                  _selected3 = true;
                });
              }
            },
            child: FaIcon(FontAwesomeIcons.shapes, color: _selected3 ? HomeStyles().primaryColor : Colors.grey[300], size: height * 0.025),
          ),
        ],
      ),
    );
  }
}