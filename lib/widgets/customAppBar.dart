import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/auth/services/googleSignIn.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/addSubscriptions/addSubscriptionSheet.dart';


class CustomAppbar extends StatefulWidget {
  @override
  _CustomAppbarState createState() => _CustomAppbarState();
}

class _CustomAppbarState extends State<CustomAppbar> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Container(
      padding: EdgeInsets.symmetric(
          horizontal: width * 0.06, vertical: height * 0.03),
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: InkWell(
                onTap: () {
                  signOutGoogle();
                  Hive.box('userData').clear();
                },
                child: FaIcon(FontAwesomeIcons.bars,
                    color: HomeStyles().primaryColor, size: height * 0.025)),
          ),
          addButton(),
        ],
      ),
    );
  }

  Widget addButton() {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    return Align(
      alignment: Alignment.topRight,
      child: InkWell(
        onTap: () {
          showAddSubscriptionSheet(context);
        },
        child: FaIcon(FontAwesomeIcons.plus,
            color: HomeStyles().primaryColor, size: height * 0.025),
      ),
    );
  }
}
