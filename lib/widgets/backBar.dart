import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart' as icons;
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/subscriptions/addCards/commitProvider.dart';
import 'package:hydrogen/subscriptions/addCards/confirmQuit.dart';
import 'package:provider/provider.dart';

class Backbar extends StatelessWidget {
  final bool confirmQuit;
  final String id;

  Backbar({this.confirmQuit, this.id});

  @override
  Widget build(BuildContext context) {
    List commits = Provider.of<CommitProvider>(context).getCommits;

    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () {
        if (confirmQuit == null) {
          Navigator.pop(context);
        } else {
          if (confirmQuit == true && commits.length > 0) {
            showConfirmCloseAddCards(context, id);
          } else {
            Navigator.pop(context);
          }
        }
      },
      child: Container(
        margin: EdgeInsets.only(
            left: MediaQuery.of(context).size.width * 0.05,
            top: MediaQuery.of(context).size.height * 0.03),
        decoration: BoxDecoration(
          color: HomeStyles().backgroundColor,
          borderRadius: BorderRadius.circular(100),
        ),
        child: Center(
            child: icons.FaIcon(
          icons.FontAwesomeIcons.arrowLeft,
          color: HomeStyles().primaryColor,
          size: MediaQuery.of(context).size.height * 0.025,
        )),
      ),
    );
  }
}
