import 'package:flutter/material.dart';
import 'package:purchases_flutter/object_wrappers.dart';

class TritiumProvider extends ChangeNotifier {
  Product _monthly;
  Product _annual;
  Product _lifetime;

  bool _loading = false;

  Product get getMonthly => _monthly;
  Product get getAnnual => _annual;
  Product get getLifetime => _lifetime;
  bool get getLoading => _loading;

  void setOfferings(Product monthly, Product annual, Product lifetime) {
    _monthly = monthly;
    _annual = annual;
    _lifetime = lifetime;

    notifyListeners();
  }

  void setLoading(bool value) {
    _loading = value;
    notifyListeners();
  }

  // overflow data
  int _overflowType;
  bool _categoryOverflow;

  List _subscriptions = [];
  List _sprints = [];

  bool _subsLoading = false;
  bool _sprintsLoading = false;

  // overflow types:
  // 1: subscription overflow
  // 2: sprint overflow
  // 3: both

  int get getOverflowType => _overflowType;
  bool get getCatOverflow => _categoryOverflow;
  List get getSubs => _subscriptions;
  List get getSprints => _sprints;
  bool get getSubsLoading => _subsLoading;
  bool get getSprintsLoading => _sprintsLoading;

  void setOverflowType(int value) {
    _overflowType = value;
    notifyListeners();
  }

  void setCatOverflow(bool value) {
    _categoryOverflow = value;
    notifyListeners();
  }

  void setSubs(List data) {
    _subscriptions = data;
    notifyListeners();
  }

  void setSprints(List data) {
    _sprints = data;
    notifyListeners();
  }

  void setSubsLoading(bool value) {
    _subsLoading = value;
    notifyListeners();
  }

  void addToSubs(Map data) {
    _subscriptions.add(data);
    notifyListeners();
  }

  void addToSprints(Map data) {
    _sprints.add(data);
    notifyListeners();
  }

  void setSprintsLoading(bool value) {
    _sprintsLoading = value;
    notifyListeners();
  }
}
