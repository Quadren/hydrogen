import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/tritium/tritiumProvider.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class UnPremiumServices {
  Future<void> fetchUserSubscriptions(BuildContext context) async {
    Provider.of<TritiumProvider>(context, listen: false).setSubsLoading(true);

    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref = FirebaseFirestore.instance.collection('users');
      CollectionReference ref2 =
          FirebaseFirestore.instance.collection('subscriptions');

      try {
        final result = await ref
            .doc(Hive.box('userData').get('email'))
            .get()
            .then((uSnap) {
          List subscriptions = uSnap.data()['subscriptions'];

          subscriptions.forEach((element) async {
            // ignore: unused_local_variable
            final result = await ref2.doc(element).get().then((value2) {
              String title = value2.data()['title'];
              Map tempMap = {'title': title, 'id': element.toString()};
              Provider.of<TritiumProvider>(context, listen: false)
                  .addToSubs(tempMap);
            });
          });
        });

        Provider.of<TritiumProvider>(context, listen: false)
            .setSubsLoading(false);
      } catch (e) {
        Fluttertoast.showToast(
            msg: e.toString(),
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  }

  Future<void> fetchUserSprints(BuildContext context) async {
    Provider.of<TritiumProvider>(context, listen: true)
        .setSprintsLoading(false);

    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref = FirebaseFirestore.instance.collection('users');
      CollectionReference ref2 =
          FirebaseFirestore.instance.collection('sprints');

      try {
        final result = await ref
            .doc(Hive.box('userData').get('email'))
            .get()
            .then((uSnap) {
          List sprints = uSnap.data()['sprints'];

          sprints.forEach((element) async {
            // ignore: unused_local_variable
            final result = await ref2.doc(element).get().then((value2) {
              String title = value2.data()['title'];
              Map tempMap = {'title': title, 'id': element.toString()};
              Provider.of<TritiumProvider>(context, listen: false)
                  .addToSprints(tempMap);
            });
          });
        });

        Provider.of<TritiumProvider>(context, listen: false)
            .setSprintsLoading(false);
      } catch (e) {
        Fluttertoast.showToast(
            msg: e.toString(),
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  }

  Future<void> fetchAllUserContent(BuildContext context) async {
    Provider.of<TritiumProvider>(context, listen: false)
        .setSprintsLoading(true);
    Provider.of<TritiumProvider>(context, listen: false).setSubsLoading(true);

    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref = FirebaseFirestore.instance.collection('users');
      CollectionReference ref2 =
          FirebaseFirestore.instance.collection('subscriptions');
      CollectionReference ref3 =
          FirebaseFirestore.instance.collection('sprints');

      try {
        final result = await ref
            .doc(Hive.box('userData').get('email'))
            .get()
            .then((uSnap) {
          List subscriptions = uSnap.data()['subscriptions'];
          List sprints = uSnap.data()['sprints'];

          subscriptions.forEach((element) async {
            // ignore: unused_local_variable
            final result = await ref2.doc(element).get().then((value2) {
              String title = value2.data()['title'];
              Map tempMap = {'title': title, 'id': element.toString()};
              Provider.of<TritiumProvider>(context, listen: false)
                  .addToSubs(tempMap);
            });
          });

          sprints.forEach((element) async {
            // ignore: unused_local_variable
            final result = await ref3.doc(element).get().then((value2) {
              String title = value2.data()['title'];
              Map tempMap = {'title': title, 'id': element.toString()};
              Provider.of<TritiumProvider>(context, listen: false)
                  .addToSprints(tempMap);
            });
          });
        });

        Provider.of<TritiumProvider>(context, listen: false)
            .setSubsLoading(false);
        Provider.of<TritiumProvider>(context, listen: false)
            .setSprintsLoading(false);
      } catch (e) {
        Fluttertoast.showToast(
            msg: e.toString(),
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  }

  Future<void> unsubscribe(
      BuildContext context, List unsub, List removeSprint) async {
    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref = FirebaseFirestore.instance.collection('users');
      CollectionReference ref2 =
          FirebaseFirestore.instance.collection('subscriptions');
      CollectionReference ref3 =
          FirebaseFirestore.instance.collection('sprints');

      String email = Hive.box('userData').get('email');

      try {
        final result = await ref.doc(email).get().then((value) {
          List subs = value.data()['subscriptions'];
          List sprints = value.data()['sprints'];
          List log = value.data()['log'];

          if (removeSprint.length > 0) {
            print("Sprints selected");
            removeSprint.forEach((element) {
              ref3.doc(element.toString()).get().then((val2) {
                List members = val2.data()['members'];
                members.remove(email);
                ref3.doc(element.toString()).update({'members': members});
              });
              sprints.removeWhere((item) => item == element.toString());
            });

            DateTime now = DateTime.now();
            String formatted = DateFormat('yyyy-MM-dd - kk:mm').format(now);

            Map logEntry = {
              'action': 'You left ${removeSprint.length} sprints.',
              'timeStamp': formatted,
            };
            log.add(logEntry);
          }

          if (unsub.length > 0) {
            print("Subs selected");
            unsub.forEach((element) async {
              ref2.doc(element.toString()).get().then((val2) {
                List members = val2.data()['users'];
                members.remove(email);
                ref2.doc(element.toString()).update({'users': members});
              });
              subs.removeWhere((item) => item == element.toString());
            });

            DateTime now = DateTime.now();
            String formatted = DateFormat('yyyy-MM-dd - kk:mm').format(now);

            Map logEntry = {
              'action': 'You unsubscribed from ${unsub.length} subscriptions.',
              'timeStamp': formatted,
            };
            log.add(logEntry);
          }

          ref
              .doc(email)
              .update({'sprints': sprints, 'subscriptions': subs, 'log': log});
        });

        Fluttertoast.showToast(
            msg: 'Finished',
            backgroundColor: HomeStyles().accentColor,
            textColor: Colors.white);
        Phoenix.rebirth(context);
      } catch (e) {
        Fluttertoast.showToast(
            msg: e.toString(),
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  }
}
