import 'package:animated/animated.dart';
import 'package:auto_animated/auto_animated.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/core/premium/revenueCatServices.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/tritium/tritiumProvider.dart';
import 'package:hydrogen/tritium/unPremium/unPremiumServices.dart';
import 'package:provider/provider.dart';

class UnPremium extends StatefulWidget {
  @override
  _UnPremiumState createState() => _UnPremiumState();
}

class _UnPremiumState extends State<UnPremium> {
  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      setState(() {
        _tritiumLoading = false;
      });
      _overflow();
    });
    super.initState();
  }

  void _overflow() {
    int type =
        Provider.of<TritiumProvider>(context, listen: false).getOverflowType;

    switch (type) {
      case 1:
        setState(() {
          _showSubs = true;
          _showSprints = false;
        });
        UnPremiumServices().fetchUserSubscriptions(context);
        break;
      case 2:
        setState(() {
          _showSubs = false;
          _showSprints = true;
        });
        UnPremiumServices().fetchUserSprints(context);
        break;
      case 3:
        setState(() {
          _showSubs = true;
          _showSprints = true;
        });
        UnPremiumServices().fetchAllUserContent(context);
        break;
    }
  }

  bool _showSprints;
  bool _showSubs;
  bool _tritiumLoading = false;
  bool _deleteLoading = false;

  List _selected = [];
  List _selectedSprints = [];

  ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    bool showCategories = Provider.of<TritiumProvider>(context).getCatOverflow;
    bool sprintLoading =
        Provider.of<TritiumProvider>(context).getSprintsLoading;
    bool subsLoading = Provider.of<TritiumProvider>(context).getSubsLoading;

    List subs = Provider.of<TritiumProvider>(context).getSubs;
    List sprints = Provider.of<TritiumProvider>(context).getSprints;

    return Scaffold(
      backgroundColor: HomeStyles().backgroundColor,
      body: ScrollConfiguration(
          behavior: AntiScrollGlow(),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: width * 0.1),
            child: CustomScrollView(
              controller: _scrollController,
              slivers: [
                SliverToBoxAdapter(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: height * 0.1),
                      BouncingWidget(
                        child: Container(
                          height: height * 0.22,
                          width: width * 0.8,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [
                                  HomeStyles().tertiaryAccent,
                                  HomeStyles().paletteColors[2]
                                ],
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight),
                            borderRadius: BorderRadius.circular(21),
                          ),
                          padding:
                              EdgeInsets.symmetric(horizontal: width * 0.05),
                          child: _tritiumLoading
                              ? Center(
                                  child: Transform.scale(
                                    scale: 0.8,
                                    child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation(
                                          HomeStyles().backgroundColor),
                                      backgroundColor: Colors.transparent,
                                    ),
                                  ),
                                )
                              : Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Get Tritium',
                                      style: HomeStyles().bigText.copyWith(
                                          fontSize: 24,
                                          color: HomeStyles().backgroundColor),
                                    ),
                                    SizedBox(width: width * 0.05),
                                    FaIcon(
                                      FontAwesomeIcons.chevronRight,
                                      color: HomeStyles().backgroundColor,
                                      size: height * 0.02,
                                    )
                                  ],
                                ),
                        ),
                        onPressed: () {
                          if (!_tritiumLoading) {
                            setState(() {
                              _tritiumLoading = true;
                            });
                            RevenueCatServices().revenueCatFetch(context);
                          }
                        },
                        duration: Duration(milliseconds: 150),
                      ),
                      SizedBox(height: height * 0.05),
                      Text('Free limit reached!',
                          style: HomeStyles().headerText),
                      SizedBox(height: height * 0.01),
                      Container(
                        width: width * 0.7,
                        child: Text(
                          'tap to select options to delete',
                          style: HomeStyles().subtitleText,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(height: height * 0.05),
                      Divider(color: Color(0xffAAAAAA)),
                      SizedBox(height: height * 0.05),
                    ],
                  ),
                ),
                SliverToBoxAdapter(
                  child: _showSubs
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'remove subscriptions',
                              style: HomeStyles().subtitleText,
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(height: height * 0.05),
                          ],
                        )
                      : Container(),
                ),
                Container(
                  child: _showSubs
                      ? Container(
                          child: subsLoading
                              ? SliverToBoxAdapter(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        height: height * 0.075,
                                        width: height * 0.075,
                                        child: CircularProgressIndicator(
                                          strokeWidth: 7,
                                          valueColor: AlwaysStoppedAnimation(
                                              HomeStyles().accentColor),
                                          backgroundColor:
                                              HomeStyles().secondaryAccent,
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              : Container(
                                  child: (subs == null || subs.length == 0)
                                      ? SliverToBoxAdapter()
                                      : LiveSliverList(
                                          itemBuilder:
                                              (context, index, animation) {
                                            return FadeTransition(
                                              opacity: Tween<double>(
                                                      begin: 0, end: 1)
                                                  .animate(animation),
                                              child: SlideTransition(
                                                  position: Tween<Offset>(
                                                          begin: Offset(0, 0.1),
                                                          end: Offset(0, 0))
                                                      .animate(animation),
                                                  child: subscriptionWidget(
                                                      subs[index]['id'],
                                                      subs[index]['title'])),
                                            );
                                          },
                                          itemCount: subs.length,
                                          controller: _scrollController,
                                        ),
                                ),
                        )
                      : SliverToBoxAdapter(),
                ),
                SliverToBoxAdapter(
                  child: _showSprints
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(height: height * 0.05),
                            Divider(
                              color: Color(0xffAAAAAA),
                            ),
                            SizedBox(height: height * 0.05),
                            Text(
                              'remove sprints',
                              style: HomeStyles().subtitleText,
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(height: height * 0.05),
                          ],
                        )
                      : Container(),
                ),
                Container(
                  child: _showSprints
                      ? Container(
                          child: sprintLoading
                              ? SliverToBoxAdapter(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        height: height * 0.075,
                                        width: height * 0.075,
                                        child: CircularProgressIndicator(
                                          strokeWidth: 7,
                                          valueColor: AlwaysStoppedAnimation(
                                              HomeStyles().accentColor),
                                          backgroundColor:
                                              HomeStyles().secondaryAccent,
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              : Container(
                                  child: (sprints == null ||
                                          sprints.length == 0)
                                      ? SliverToBoxAdapter()
                                      : LiveSliverList(
                                          itemBuilder:
                                              (context, index, animation) {
                                            return FadeTransition(
                                              opacity: Tween<double>(
                                                      begin: 0, end: 1)
                                                  .animate(animation),
                                              child: SlideTransition(
                                                  position: Tween<Offset>(
                                                          begin: Offset(0, 0.1),
                                                          end: Offset(0, 0))
                                                      .animate(animation),
                                                  child: sprintWidget(
                                                      sprints[index]['id'],
                                                      sprints[index]['title'])),
                                            );
                                          },
                                          itemCount: sprints.length,
                                          controller: _scrollController,
                                        ),
                                ),
                        )
                      : SliverToBoxAdapter(),
                ),
                SliverToBoxAdapter(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: height * 0.05),
                      Divider(
                        color: Color(0xffAAAAAA),
                      ),
                      SizedBox(height: height * 0.05),
                      finalButton(height, width, subs, sprints),
                      SizedBox(height: height * 0.02),
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }

  Widget finalButton(double height, double width, List subs, List sprints) {
    return BouncingWidget(
      child: AnimatedContainer(
        duration: Duration(milliseconds: 150),
        height: height * 0.1,
        width: width * 0.8,
        decoration: BoxDecoration(
          color: ((sprints.length - _selectedSprints.length < 3) &&
                  (subs.length - _selected.length <= 20))
              ? HomeStyles().accentColor
              : Color(0xffAAAAAA),
          borderRadius: BorderRadius.circular(21),
        ),
        child: Center(
          child: _deleteLoading
              ? Transform.scale(
                  scale: 0.7,
                  child: CircularProgressIndicator(
                    valueColor:
                        AlwaysStoppedAnimation(HomeStyles().backgroundColor),
                    backgroundColor: Colors.transparent,
                  ),
                )
              : Text(
                  'Continue',
                  style: HomeStyles().buttonText3.copyWith(
                      color: ((sprints.length - _selectedSprints.length < 3) &&
                              (subs.length - _selected.length <= 20))
                          ? HomeStyles().backgroundColor
                          : HomeStyles().primaryColor),
                ),
        ),
      ),
      onPressed: () {
        if ((sprints.length - _selectedSprints.length < 3) &&
            (subs.length - _selected.length <= 20) &&
            !_deleteLoading) {
          setState(() {
            _deleteLoading = true;
          });
          UnPremiumServices().unsubscribe(context, _selected, _selectedSprints);
        } else if (_deleteLoading) {
          Fluttertoast.showToast(
              msg: 'Please wait!',
              backgroundColor: Colors.red[300],
              textColor: Colors.white);
        } else {
          Fluttertoast.showToast(
              msg: 'Not enough selected',
              backgroundColor: Colors.red[300],
              textColor: Colors.white);
        }
      },
      duration: Duration(milliseconds: 150),
    );
  }

  Widget subscriptionWidget(String id, String title) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(vertical: height * 0.01),
      height: height * 0.075,
      width: width * 0.8,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(21),
        border: (_selected.contains(id)
            ? null
            : Border.all(width: 2, color: Color(0xffEEEEEE))),
        color: (_selected.contains(id))
            ? HomeStyles().accentColor
            : HomeStyles().backgroundColor,
      ),
      child: GestureDetector(
        onTap: () {
          if (_selected.contains(id)) {
            setState(() {
              _selected.remove(id);
            });
          } else {
            setState(() {
              _selected.add(id);
            });
          }
        },
        child: Container(
          height: height * 0.075,
          width: width * 0.8,
          padding: EdgeInsets.symmetric(horizontal: width * 0.05),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: width * 0.6,
                child: Text(
                  title,
                  style: HomeStyles().buttonText.copyWith(
                      color: (_selected.contains(id))
                          ? HomeStyles().backgroundColor
                          : HomeStyles().primaryColor),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget sprintWidget(String id, String title) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(vertical: height * 0.01),
      height: height * 0.075,
      width: width * 0.8,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(21),
        border: (_selectedSprints.contains(id)
            ? null
            : Border.all(width: 2, color: Color(0xffEEEEEE))),
        color: (_selectedSprints.contains(id))
            ? HomeStyles().accentColor
            : HomeStyles().backgroundColor,
      ),
      child: GestureDetector(
        onTap: () {
          if (_selectedSprints.contains(id)) {
            setState(() {
              _selectedSprints.remove(id);
            });
          } else {
            setState(() {
              _selectedSprints.add(id);
            });
          }
        },
        child: Container(
          height: height * 0.075,
          width: width * 0.8,
          padding: EdgeInsets.symmetric(horizontal: width * 0.05),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: width * 0.6,
                child: Text(
                  title,
                  style: HomeStyles().buttonText.copyWith(
                      color: (_selectedSprints.contains(id))
                          ? HomeStyles().backgroundColor
                          : HomeStyles().primaryColor),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
