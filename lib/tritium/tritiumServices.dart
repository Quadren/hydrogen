import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:hydrogen/home/home.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/tritium/purchaseComplete.dart';
import 'package:hydrogen/tritium/tritiumProvider.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:purchases_flutter/object_wrappers.dart';
import 'package:purchases_flutter/purchases_flutter.dart';

class TritiumServices {
  Future<void> setUserPremium(BuildContext context) async {
    Provider.of<TritiumProvider>(context, listen: false).setLoading(true);

    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref = FirebaseFirestore.instance.collection('users');
      String email = Hive.box('userData').get('email');

      try {
        final result = await ref.doc().update({'premium': true});
        Provider.of<TritiumProvider>(context, listen: false).setLoading(false);
        Navigator.pushAndRemoveUntil(
            context,
            PageTransition(
                child: PurchaseComplete(),
                type: PageTransitionType.fade,
                duration: Duration(milliseconds: 300)),
            (route) => false);
      } catch (e) {
        Fluttertoast.showToast(
            msg: e.toString(),
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  }

  Future<void> initPlatformState(BuildContext context) async {
    final result2 = await Purchases.setup("QlgqfgaWleBEqJNsXuYcyxtqjnVdWvFy");
    print("Initialised RevenueCat");

    try {
      PurchaserInfo purchaserInfo = await Purchases.getPurchaserInfo();

      if (purchaserInfo.activeSubscriptions.isNotEmpty) {
        print("Subscriptions active");
        Hive.box('userData').put('premium', true);
        Navigator.pushAndRemoveUntil(
            context,
            PageTransition(
                child: Home(),
                type: PageTransitionType.fade,
                duration: Duration(milliseconds: 300)),
            (route) => false);
      } else {
        print("Nothing active");
        if (Hive.box('userData').get('premium') == true) {
          deactivate(context);
        } else {
          print("User has no premium");
          Navigator.pushAndRemoveUntil(
              context,
              PageTransition(
                  child: Home(),
                  type: PageTransitionType.fade,
                  duration: Duration(milliseconds: 300)),
              (route) => false);
        }
      }
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.toString(),
          backgroundColor: Colors.red[300],
          textColor: Colors.white);
    }
  }

  Future<void> deactivate(BuildContext context) async {
    Firebase.initializeApp().whenComplete(() async {
      CollectionReference ref = FirebaseFirestore.instance.collection('users');
      String email = Hive.box('userData').get('email');

      try {
        final result = await ref.doc(email).update({'premium': false});
        Hive.box('userData').put('premium', false);
        Fluttertoast.showToast(
            msg: 'Your Tritium subscription has expired :(',
            backgroundColor: HomeStyles().accentColor,
            textColor: Colors.white);
        Future.delayed(Duration(seconds: 1), () {
          Navigator.pushAndRemoveUntil(
              context,
              PageTransition(
                  child: Home(),
                  type: PageTransitionType.fade,
                  duration: Duration(milliseconds: 500)),
              (route) => false);
        });
      } catch (e) {
        Fluttertoast.showToast(
            msg: e.toString(),
            backgroundColor: Colors.red[300],
            textColor: Colors.white);
      }
    });
  }
}
