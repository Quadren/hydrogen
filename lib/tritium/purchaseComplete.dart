import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:lottie/lottie.dart';

class PurchaseComplete extends StatefulWidget {
  @override
  _PurchaseCompleteState createState() => _PurchaseCompleteState();
}

class _PurchaseCompleteState extends State<PurchaseComplete> {
  @override
  void initState() {
    Future.delayed(Duration(seconds: 3), () {
      Phoenix.rebirth(context);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HomeStyles().backgroundColor,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.3,
              width: MediaQuery.of(context).size.height * 0.3,
              child: Lottie.asset('assets/lottie/check.json',
                  repeat: false, fit: BoxFit.contain),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.1),
            Text('Thank you!', style: HomeStyles().bigText),
            SizedBox(height: MediaQuery.of(context).size.height * 0.02),
            Container(
              width: MediaQuery.of(context).size.width * 0.8,
              child: Text(
                'your help allows us to keep making Hydrogen better :D',
                style: HomeStyles().buttonText,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
