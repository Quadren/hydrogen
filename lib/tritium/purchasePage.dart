import 'package:animated/animated.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrogen/core/misc/removeGlow.dart';
import 'package:hydrogen/core/premium/revenueCatServices.dart';
import 'package:hydrogen/home/home_styles.dart';
import 'package:hydrogen/tritium/tritiumProvider.dart';
import 'package:hydrogen/widgets/backBar.dart';
import 'package:provider/provider.dart';
import 'package:purchases_flutter/object_wrappers.dart';

class PurchasePage extends StatefulWidget {
  @override
  _PurchasePageState createState() => _PurchasePageState();
}

class _PurchasePageState extends State<PurchasePage> {
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      Provider.of<TritiumProvider>(context, listen: false).setLoading(false);
    });
    super.initState();
  }

  int _selected = 0;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    Product monthly = Provider.of<TritiumProvider>(context).getMonthly;
    Product annual = Provider.of<TritiumProvider>(context).getAnnual;

    String annualDiscount =
        'save ${(((((monthly.price * 12) - annual.price)) / annual.price) * 100).toInt()}% from monthly';

    List titles = [
      'Unlimited synced subscriptions',
      'Unlimited concurrent study sprints',
      'Upto 10GB of media uploads*',
      'Fund Hydrogen!'
    ];
    List emojis = ['🤩', '💠', '📸', '💝'];

    bool loading = Provider.of<TritiumProvider>(context).getLoading;

    return Scaffold(
        backgroundColor: HomeStyles().backgroundColor,
        appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            leading: Backbar()),
        extendBodyBehindAppBar: true,
        body: Stack(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.05),
              child: ScrollConfiguration(
                behavior: AntiScrollGlow(),
                child: CustomScrollView(
                  controller: _scrollController,
                  slivers: [
                    SliverToBoxAdapter(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(height: height * 0.22),
                          Text(
                            'Tritium',
                            style: HomeStyles().bigText,
                          ),
                          SizedBox(height: height * 0.01),
                          Text('Hydrogen unleashed.',
                              style: HomeStyles().subtitleText),
                          SizedBox(height: height * 0.1),
                        ],
                      ),
                    ),
                    SliverGrid(
                        delegate:
                            SliverChildBuilderDelegate((context, int index) {
                          return benefitWidget(
                              titles[index], emojis[index], index);
                        }, childCount: 4),
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            childAspectRatio: ((width * 0.4) / (height * 0.15)),
                            mainAxisSpacing: height * 0.02,
                            crossAxisSpacing: height * 0.02)),
                    SliverToBoxAdapter(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(height: height * 0.05),
                          Text(
                            '*Media upload support coming soon.',
                            style: HomeStyles().buttonText2,
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(height: height * 0.05),
                          Divider(
                            color: Color(0xffAAAAAA),
                            indent: width * 0.3,
                            endIndent: width * 0.3,
                          ),
                          SizedBox(height: height * 0.05),
                          Text('tap to select a plan',
                              style: HomeStyles().subtitleText),
                          SizedBox(height: height * 0.05),
                          Container(
                            height: height * 0.2,
                            child: ListView(
                              scrollDirection: Axis.horizontal,
                              children: [
                                priceWidget(
                                    'Monthly', monthly, 'base pricing', 1),
                                priceWidget(
                                    'Annual', annual, annualDiscount, 2),
                              ],
                            ),
                          ),
                          SizedBox(height: height * 0.05),
                          BouncingWidget(
                            child: AnimatedContainer(
                              duration: Duration(milliseconds: 150),
                              height: height * 0.1,
                              width: height * 0.1,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(21),
                                border: (_selected == 0)
                                    ? Border.all(
                                        width: 2, color: Color(0xffEEEEEE))
                                    : null,
                                color: (_selected == 0)
                                    ? HomeStyles().backgroundColor
                                    : HomeStyles().accentColor,
                              ),
                              child: Center(
                                child: loading
                                    ? CircularProgressIndicator(
                                        valueColor: AlwaysStoppedAnimation(
                                            HomeStyles().backgroundColor),
                                        backgroundColor: Colors.transparent,
                                      )
                                    : FaIcon(
                                        FontAwesomeIcons.arrowRight,
                                        color: (_selected == 0)
                                            ? Color(0xffEEEEEE)
                                            : HomeStyles().backgroundColor,
                                        size: height * 0.025,
                                      ),
                              ),
                            ),
                            onPressed: () {
                              if (_selected == 0) {
                                Fluttertoast.showToast(
                                    msg: 'Select a plan first!',
                                    backgroundColor: Colors.red[300],
                                    textColor: Colors.white);
                              } else {
                                Provider.of<TritiumProvider>(context,
                                        listen: false)
                                    .setLoading(true);
                                switch (_selected) {
                                  case 1:
                                    RevenueCatServices().makePurchase(
                                        monthly.identifier, context);
                                    break;
                                  case 2:
                                    RevenueCatServices().makePurchase(
                                        annual.identifier, context);
                                    break;
                                }
                              }
                            },
                            duration: Duration(milliseconds: 150),
                          ),
                          SizedBox(height: height * 0.05),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }

  Widget benefitWidget(String head, String emoji, int index) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return Container(
      decoration: BoxDecoration(
        border: Border.all(width: 3, color: HomeStyles().paletteColors[index]),
        borderRadius: BorderRadius.circular(21),
      ),
      padding: EdgeInsets.all(width * 0.05),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            emoji,
            style: TextStyle(fontSize: 24),
          ),
          SizedBox(height: height * 0.02),
          Text(
            head,
            style: HomeStyles().buttonText2,
            maxLines: 2,
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
          )
        ],
      ),
    );
  }

  Widget animate(Widget _child, bool trigger) {
    return Animated(
      duration: Duration(milliseconds: 1500),
      curve: Curves.easeInOutCubic,
      value: trigger ? 1 : 0,
      builder: (context, child, animation) {
        return Transform.translate(
            offset: Offset(0, (1 - animation.value) * 40),
            child: Opacity(
              opacity: animation.value,
              child: child,
            ));
      },
      child: _child,
    );
  }

  Widget priceWidget(
      String title, Product product, String discount, int index) {
    var size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;

    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.only(left: width * 0.03),
      height: height * 0.2,
      width: width * 0.55,
      decoration: BoxDecoration(
        color: (_selected == index)
            ? HomeStyles().accentColor
            : HomeStyles().backgroundColor,
        border: (_selected == index)
            ? null
            : Border.all(width: 2, color: Color(0xffEEEEEE)),
        borderRadius: BorderRadius.circular(21),
      ),
      child: GestureDetector(
        child: Container(
          height: height * 0.2,
          width: width * 0.45,
          padding: EdgeInsets.all(width * 0.05),
          color: Colors.white.withOpacity(0),
          child: Stack(
            children: [
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text('${product.priceString}',
                        style: HomeStyles().headerText.copyWith(
                            color: (_selected == index)
                                ? HomeStyles().backgroundColor
                                : HomeStyles().primaryColor)),
                    SizedBox(height: height * 0.01),
                    Text(
                      title,
                      style: HomeStyles().buttonText2.copyWith(
                          color: (_selected == index)
                              ? HomeStyles().backgroundColor
                              : HomeStyles().primaryColor),
                    ),
                    SizedBox(height: height * 0.03),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: (index == 1)
                    ? Text(discount,
                        textAlign: TextAlign.center,
                        style: HomeStyles().buttonText2.copyWith(
                            color: (_selected == index)
                                ? HomeStyles().backgroundColor
                                : HomeStyles().primaryColor))
                    : Text(discount,
                        textAlign: TextAlign.center,
                        style: HomeStyles().buttonText2.copyWith(
                            color: (_selected == index)
                                ? HomeStyles().backgroundColor
                                : Colors.green[300])),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: AnimatedOpacity(
                  opacity: (_selected == index) ? 1 : 0,
                  duration: Duration(milliseconds: 50),
                  child: FaIcon(
                    FontAwesomeIcons.checkCircle,
                    color: HomeStyles().backgroundColor,
                    size: height * 0.015,
                  ),
                ),
              ),
            ],
          ),
        ),
        onTap: () {
          if (_selected != index) {
            setState(() {
              _selected = index;
            });
          } else {
            setState(() {
              _selected = 0;
            });
          }
        },
      ),
    );
  }
}
