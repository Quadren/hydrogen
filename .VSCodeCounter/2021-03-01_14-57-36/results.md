# Summary

Date : 2021-03-01 14:57:36

Directory /Users/aneeshhegde/Desktop/Quadren/Apps/hydrogen/lib

Total : 210 files,  24808 codes, 73 comments, 1969 blanks, all 26850 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Dart | 210 | 24,808 | 73 | 1,969 | 26,850 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 210 | 24,808 | 73 | 1,969 | 26,850 |
| auth | 3 | 101 | 1 | 22 | 124 |
| auth/services | 2 | 61 | 0 | 18 | 79 |
| categories | 19 | 1,639 | 17 | 114 | 1,770 |
| categories/createCategory | 11 | 711 | 0 | 37 | 748 |
| categories/createCategory/emojis | 9 | 434 | 0 | 20 | 454 |
| categories/deleteCategory | 1 | 159 | 0 | 13 | 172 |
| categories/services | 4 | 106 | 0 | 28 | 134 |
| core | 23 | 2,325 | 16 | 130 | 2,471 |
| core/misc | 8 | 1,826 | 0 | 53 | 1,879 |
| core/popUps | 3 | 74 | 8 | 16 | 98 |
| core/premium | 2 | 167 | 0 | 12 | 179 |
| core/providers | 1 | 15 | 0 | 5 | 20 |
| core/userServices | 9 | 243 | 8 | 44 | 295 |
| dashboard | 7 | 720 | 0 | 49 | 769 |
| dashboard/pages | 3 | 418 | 0 | 20 | 438 |
| dashboard/services | 3 | 66 | 0 | 16 | 82 |
| friends | 16 | 1,730 | 0 | 160 | 1,890 |
| friends/addFriend | 1 | 180 | 0 | 17 | 197 |
| friends/requests | 4 | 697 | 0 | 49 | 746 |
| friends/services | 7 | 376 | 0 | 54 | 430 |
| home | 5 | 536 | 1 | 47 | 584 |
| login | 7 | 394 | 2 | 32 | 428 |
| login/pages | 4 | 304 | 0 | 22 | 326 |
| marketplace | 1 | 96 | 0 | 3 | 99 |
| newLogin | 7 | 561 | 4 | 49 | 614 |
| newLogin/pages | 4 | 417 | 0 | 31 | 448 |
| newLogin/services | 2 | 112 | 4 | 14 | 130 |
| quiz | 8 | 1,455 | 0 | 105 | 1,560 |
| quiz/pages | 6 | 1,316 | 0 | 83 | 1,399 |
| sprints | 49 | 7,085 | 3 | 565 | 7,653 |
| sprints/adminConsole | 11 | 1,506 | 0 | 136 | 1,642 |
| sprints/adminConsole/pages | 4 | 866 | 0 | 60 | 926 |
| sprints/adminConsole/services | 5 | 271 | 0 | 49 | 320 |
| sprints/contribute | 3 | 594 | 0 | 43 | 637 |
| sprints/createSprints | 7 | 958 | 0 | 52 | 1,010 |
| sprints/createSprints/addPortion | 2 | 162 | 0 | 15 | 177 |
| sprints/createSprints/services | 2 | 77 | 0 | 12 | 89 |
| sprints/importSprint | 4 | 687 | 0 | 46 | 733 |
| sprints/importSprint/new | 1 | 127 | 0 | 9 | 136 |
| sprints/intro | 2 | 462 | 0 | 15 | 477 |
| sprints/invites | 3 | 271 | 0 | 22 | 293 |
| sprints/services | 6 | 578 | 0 | 83 | 661 |
| sprints/sprintPage | 4 | 668 | 0 | 37 | 705 |
| sprints/study | 2 | 377 | 0 | 28 | 405 |
| sprints/subscriptionPreview | 4 | 538 | 0 | 35 | 573 |
| subscriptions | 54 | 6,638 | 17 | 573 | 7,228 |
| subscriptions/addCards | 6 | 966 | 0 | 67 | 1,033 |
| subscriptions/addSubscriptions | 9 | 815 | 3 | 64 | 882 |
| subscriptions/createSubscriptions | 2 | 413 | 0 | 34 | 447 |
| subscriptions/individualSubscriptions | 5 | 1,068 | 0 | 67 | 1,135 |
| subscriptions/log | 4 | 534 | 0 | 40 | 574 |
| subscriptions/log/services | 1 | 50 | 0 | 7 | 57 |
| subscriptions/moderation | 4 | 768 | 0 | 74 | 842 |
| subscriptions/newAddSubscriptions | 4 | 499 | 0 | 42 | 541 |
| subscriptions/newAddSubscriptions/pages | 2 | 333 | 0 | 19 | 352 |
| subscriptions/newAddSubscriptions/services | 1 | 123 | 0 | 17 | 140 |
| subscriptions/services | 11 | 723 | 11 | 75 | 809 |
| subscriptions/subscribers | 3 | 214 | 1 | 26 | 241 |
| tritium | 6 | 1,225 | 9 | 94 | 1,328 |
| tritium/unPremium | 2 | 679 | 4 | 45 | 728 |
| widgets | 4 | 184 | 0 | 18 | 202 |

[details](details.md)